
export type YoutubeDescriptionType = {
    title: string
    description: string
    tags: string[]
    categoryId: CategoryIdType
    privacyStatus: 'private' | 'public' | 'unlisted'
    publishAt: Date
}

export type YoutubeInsertParamType = {
    part:
        'contentDetails' |
        'id' |
        'liveStreamingDetails' |
        'localizations' |
        'paidProductPlacementDetails' |
        'player' |
        'processingDetails' |
        'recordingDetails' |
        'snippet' |
        'statistics' |
        'status' |
        'suggestions' |
        'topicDetails'
    notifySubscribers?: boolean
    onBehalfOfContentOwner?: string
    onBehalfOfContentOwnerChannel?: string
    requestBody: {
        snippet: YoutubeDescriptionType
        status: {
            embeddable?: boolean
            license?: string
            privacyStatus?: 'private' | 'public' | 'unlisted'
            publicStatsViewable?: boolean
            publishAt?: string
            selfDeclaredMadeForKids?: boolean
            containsSyntheticMedia?: boolean
        }
        recordingDetails?: {
            recordingDate?: string
        }
    }
}



export type YoutubeInsertResponseType = {
    kind: string,
    etag: string,
    id: string,
    snippet: {
        publishedAt: Date,
        channelId: string,
        title: string,
        description: string,
        thumbnails: {
            default: [Object],
            medium: [Object],
            high: [Object]
        },
        channelTitle: string,
        categoryId: number,
        liveBroadcastContent: 'none',
        localized: {
            title: string,
            description: string
        }
    },
    status: {
        uploadStatus: string,
        privacyStatus: 'private' | 'public' | 'unlisted',
        license: string,
        embeddable: boolean,
        publicStatsViewable: boolean
    }
}

/**
 * example
YoutubeInsertResponseType = {
    kind: 'youtube#video',
    etag: 'CbDq0Fg4LeZd7oLyDx8cOfM6Ca4',
    id: 'Ad5pzZ4j5E0',
    snippet: {
        publishedAt: '2025-01-12T05:58:37Z',
        channelId: 'UC1bl98a55AS2uAeG2lFqkbg',
        title: 'Node.js YouTube Upload Test',
        description: 'Testing YouTube upload via Google APIs Node.js Client',
        thumbnails: { default: [Object], medium: [Object], high: [Object] },
        channelTitle: 'Yasukosan',
        categoryId: '26',
        liveBroadcastContent: 'none',
        localized: {
        title: 'Node.js YouTube Upload Test',
        description: 'Testing YouTube upload via Google APIs Node.js Client'
        }
    },
    status: {
        uploadStatus: 'uploaded',
        privacyStatus: 'private',
        license: 'youtube',
        embeddable: true,
        publicStatsViewable: true
    }
}
 */

/**
 * 1: 映画とアニメ
 * 2: 自動車と乗り物
 * 10: 音楽
 * 15: ペットと動物
 * 17: スポーツ
 * 18: ショート ムービー
 * 19: 旅行とイベント
 * 20: ゲーム
 * 21: 動画ブログ
 * 22: ブログ
 * 23: コメディー
 * 24: エンターテイメント
 * 25: ニュースと政治
 * 26: ハウツーとスタイル
 * 27: 教育
 * 28: 科学と技術
 * 30: 映画
 * 31: アニメ
 * 32: アクション/アドベンチャー
 * 33: クラシック
 * 34: コメディー
 * 35: ドキュメンタリー
 * 36: ドラマ
 * 37: 家族向け
 * 38: 海外
 * 39: ホラー
 * 40: SF/ファンタジー
 * 41: サスペンス
 * 42: 短編
 * 43: 番組
 * 44: 予告編 
 */
export type CategoryIdType = '1' | '2' | '10' | '15' | '17' | '18' | '19' | '20' | '21' | '22' | '23' | '24' | '25' | '26' | '27' | '28' | '30' | '31' | '32' | '33' | '34' | '35' | '36' | '37' | '38' | '39' | '40' | '41' | '42' | '43' | '44'

export const categoryIds = [
    {
        "id": "1",
        "snippet": {
            "title": "映画とアニメ",
            "assignable": true,
        }
    },
    {
    "id": "2",
        "snippet": {
            "title": "自動車と乗り物",
            "assignable": true,
        }
    },
    {
        "id": "10",
        "snippet": {
            "title": "音楽",
            "assignable": true,
        }
    },
    {
        "id": "15",
        "snippet": {
            "title": "ペットと動物",
            "assignable": true,
        }
    },
    {
        "id": "17",
        "snippet": {
            "title": "スポーツ",
            "assignable": true,
    }
    },
    {
        "id": "18",
        "snippet": {
            "title": "ショート ムービー",
            "assignable": false,
        }
    },
    {
    "id": "19",
    "snippet": {
        "title": "旅行とイベント",
        "assignable": true,
    }
    },
    {
    "id": "20",
    "snippet": {
        "title": "ゲーム",
        "assignable": true,
    }
    },
    {
    "id": "21",
    "snippet": {
        "title": "動画ブログ",
        "assignable": false,
    }
    },
    {
    "id": "22",
    "snippet": {
        "title": "ブログ",
        "assignable": true,
    }
    },
    {
    "id": "23",
    "snippet": {
        "title": "コメディー",
        "assignable": true,
    }
    },
    {
    "id": "24",
    "snippet": {
        "title": "エンターテイメント",
        "assignable": true,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "25",
    "snippet": {
        "title": "ニュースと政治",
        "assignable": true,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "26",
    "snippet": {
        "title": "ハウツーとスタイル",
        "assignable": true,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "27",
    "snippet": {
        "title": "教育",
        "assignable": true,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "28",
    "snippet": {
        "title": "科学と技術",
        "assignable": true,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "30",
    "snippet": {
        "title": "映画",
        "assignable": false,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "31",
    "snippet": {
        "title": "アニメ",
        "assignable": false,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "32",
    "snippet": {
        "title": "アクション/アドベンチャー",
        "assignable": false,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "33",
    "snippet": {
        "title": "クラシック",
        "assignable": false,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "34",
    "snippet": {
        "title": "コメディー",
        "assignable": false,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "35",
    "snippet": {
        "title": "ドキュメンタリー",
        "assignable": false,
        "channelId": "UCBR8-60-B28hp2BmDPdntcQ"
    }
    },
    {
    "id": "36",
    "snippet": {
        "title": "ドラマ",
        "assignable": false,
    }
    },
    {
    "id": "37",
    "snippet": {
        "title": "家族向け",
        "assignable": false,
    }
    },
    {
    "id": "38",
    "snippet": {
        "title": "海外",
        "assignable": false,
    }
    },
    {
    "id": "39",
    "snippet": {
        "title": "ホラー",
        "assignable": false,
    }
    },
    {
    "id": "40",
    "snippet": {
        "title": "SF/ファンタジー",
        "assignable": false,
    }
    },
    {
    "id": "41",
    "snippet": {
        "title": "サスペンス",
        "assignable": false,
    }
    },
    {
    "id": "42",
    "snippet": {
        "title": "短編",
        "assignable": false,
    }
    },
    {
    "id": "43",
    "snippet": {
        "title": "番組",
        "assignable": false,
    }
    },
    {
    "id": "44",
    "snippet": {
        "title": "予告編",
        "assignable": false,
    }
    }
]

