import fs from 'fs'
import readline from 'readline'
import {google} from 'googleapis'
import { YoutubeDescriptionType } from './_description'

const TOKEN: any = process.env.GOOGLE_TOKEN || ''
const SECRET: any = process.env.GOOGLE_SECRET || ''

const MoveProperties: YoutubeDescriptionType = {
    'title': 'title',
    'description': '',
    'tags': [],
    'categoryId': '22',
    'privacyStatus': 'private',
    'publishAt': new Date()
}

export const setMoveProperties = (properties: Partial<YoutubeDescriptionType>) => {
    Object.assign(MoveProperties, properties)
}

export const uploadVideo = async (file: Uint8Array | string = '') => {

    if (file === '') return false

    const auth = googleAuth();
    const youtube = google.youtube({version: 'v3', auth});
    // console.log(youtube.channels)
    try {
        const _file = (typeof file === 'string') ? fs.createReadStream(file) : file
        const fileSize = (typeof file === 'string') ? fs.statSync(file).size : getFileSizeFromUint8Array(file)
        const res = await youtube.videos.insert({
            part: ['id','snippet','status'],
            notifySubscribers: false,
            requestBody: {
                snippet: MoveProperties,
                status: {
                    privacyStatus: 'private',
                },
            },
            media: {
                body: _file,
            },
        },
        {
            // Use the `onUploadProgress` event from Axios to track the
            // number of bytes uploaded to this point.
            onUploadProgress: evt => {
                const progress = (evt.bytesRead / fileSize) * 100;
                readline.clearLine(process.stdout, 0);
                readline.cursorTo(process.stdout, 0, undefined);
                process.stdout.write(`${Math.round(progress)}% complete`);
            },
            });
        console.log('\n\n');
        console.log(res.data);
        return res.data;

    } catch (error) {
        console.log('The API returned an error: ' + error);
    }
}

const getFileSizeFromUint8Array = (file: Uint8Array): number => {
    return file.byteLength;
}

const googleAuth = () => {
    const _TOKEN = JSON.parse(TOKEN)
    const _SECRET = JSON.parse(SECRET)
    const {client_secret, client_id, redirect_uris} = _SECRET.installed
    const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0])
    oAuth2Client.setCredentials(_TOKEN)
    return oAuth2Client
}