import jsQR from 'jsqr';
import { ImageSplitService } from '../image/image_split.service';

export class QRCodeReaderService {
    private OUT = '';
    private MultiOut: string[] = [];
    private VideoTarget: any = {};
    private Image: any = {};
    private iss: ImageSplitService;

    public constructor() {
        this.iss = new ImageSplitService();

        return this;
    }

    /**
     * videoタグのID名か、videoタグ自体を格納
     *
     * @param target string | HTMLVideoELement
     * @returns QRService
     */
    public setVideoTarget(target: any): QRCodeReaderService {
        if (typeof target === 'string') {
            this.VideoTarget = document.querySelector('#' + target);
        } else {
            this.VideoTarget = target;
        }
        return this;
    }

    /**
     * イメージデータか、imgタグを
     * imgタグ形式に揃えて保存
     *
     * @param image string | HTMLImageElement
     * @returns string
     */
    public async setImage(image: any): Promise<string> {
        if (typeof image === 'string') {
            const im: HTMLImageElement = document.createElement('img');
            im.src = image;
            return new Promise((resolve) => {
                im.onload = () => {
                    this.Image = im;
                    resolve('load');
                };
            });
        } else {
            this.Image = image;
        }

        return new Promise((resolve) => {
            resolve('load');
        });
    }

    /**
     * 読み込み結果を返す
     *
     * @returns string
     */
    public getResult(): string {
        return this.OUT;
    }

    public getMultiResult(): string[] {
        return this.MultiOut;
    }

    public async ReadStream(): Promise<any> {
        let st: any = {};
        const canvas: HTMLCanvasElement = document.createElement('canvas');
        const ctx: any = canvas.getContext('2d');

        return new Promise((result) => {
            const doRead = () => {
                ctx.drawImage(
                    this.VideoTarget,
                    0,
                    0,
                    canvas.width,
                    canvas.height
                );
                const imageData = ctx.getImageData(
                    0,
                    0,
                    canvas.width,
                    canvas.height
                );
                const code = jsQR(imageData.data, canvas.width, canvas.height);

                if (code) {
                    this.OUT = String(code.data);
                    clearInterval(st);
                    result(true);
                    return;
                }
                st = setTimeout(() => {
                    doRead();
                }, 200);
            };
            doRead();
        });
    }

    public async ReadStreamMulti(): Promise<any> {
        let st: any = {};
        const canvas: HTMLCanvasElement = document.createElement('canvas');
        const ctx: any = canvas.getContext('2d');

        return new Promise((result) => {
            const doRead = async () => {
                ctx.drawImage(
                    this.VideoTarget,
                    0,
                    0,
                    canvas.width,
                    canvas.height
                );
                await this.iss.setImage(canvas.toDataURL('image/jpg'));

                const img: any = await this.iss.splitQuatro();
                const size: number[] = this.iss.getSplitSize();

                const code1: any = jsQR(img[0], size[0], size[1]);
                const code2: any = jsQR(img[1], size[0], size[1]);
                const code3: any = jsQR(img[2], size[0], size[1]);
                const code4: any = jsQR(img[3], size[0], size[1]);

                if (code1 && code2 && code3 && code4) {
                    this.MultiOut = [
                        String(code1.data),
                        String(code2.data),
                        String(code3.data),
                        String(code4.data)
                    ];
                    clearInterval(st);
                    result(true);
                    return;
                }
                st = setTimeout(() => {
                    doRead();
                }, 200);
            };
            doRead();
        });
    }

    public async ReadImage(): Promise<any> {
        const canvas: HTMLCanvasElement = document.createElement('canvas');
        const ctx: any = canvas.getContext('2d');

        ctx.drawImage(this.Image, 0, 0, canvas.width, canvas.height);
        const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        const code = jsQR(imageData.data, canvas.width, canvas.height);

        if (code) {
            this.OUT = String(code.data);
            return true;
        } else {
            return false;
        }
    }

    public async Read2(): Promise<any> {
        let st: any = {};
        const canvas: HTMLCanvasElement = document.createElement('canvas');
        const ctx: any = canvas.getContext('2d');

        const hoge = () => {
            ctx.drawImage(this.VideoTarget, 0, 0, canvas.width, canvas.height);
            const imageData = ctx.getImageData(
                0,
                0,
                canvas.width,
                canvas.height
            );
            const code = jsQR(imageData.data, canvas.width, canvas.height);

            if (code) {
                this.OUT = String(code.data);
                clearInterval(st);
                console.log(code.data);
                return true;
            }
            st = setTimeout(() => {
                hoge();
            }, 200);
        };

        return await hoge();
    }
}
