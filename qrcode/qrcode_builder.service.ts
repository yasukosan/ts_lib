import QRCode from 'qrcode';

/**
 * Use node-qrcode
 * https://github.com/soldair/node-qrcode
 *
 */

export class QRCodeBuilderService {
    private static instance: QRCodeBuilderService
    private options = {
        text: '',
        mode: 'alphanumeric',
        width: 256,
        correctionLevel: 'H',
        type: 'image/jpqg',
        quority: 0.92,
        color: {
            dark: '#010599FF',
            light: '#ffffffff'
        }
    }

    private Result = ''


    public static call(): QRCodeBuilderService {
        if (!QRCodeBuilderService.instance) {
            QRCodeBuilderService.instance = new QRCodeBuilderService()
        }
        return QRCodeBuilderService.instance
    }

    /**
     * QRコードのサイズ指定
     * 正方形なので横幅だけ指定する
     *
     * @param width
     * @returns QRCodeBuilderService
     */
    public setSize(width: number): QRCodeBuilderService {
        this.options.width = width
        return this
    }

    /**
     * 変換文字列の登録、QRコードのモード選択（デフォルト[alphanumeric]）
     * numeric:　数値のみ
     * alphanumetic: 英数字
     * byte: バイナリ
     * kanji: 日本語
     *
     * @param txt string
     * @param mode string [numeric, alphanumeric, Byte, kanji]
     * @returns QRCodeBuilderService
     */
    public setText(txt: string, mode = 'alphanumeric'): QRCodeBuilderService {
        this.options.text = txt
        return this
    }

    /**
     * エラー訂正符号の割合 デフォルト「H」（30%）
     * L: ~7%, M: ~15%, Q: ~25%, H: ~30%
     * @param level string [L, M, Q, H]
     * @returns QRCodeBuilderService
     */
    public setCorrectionLevel(level: string): QRCodeBuilderService {
        const levels = ['L', 'M', 'Q', 'H']
        this.options.correctionLevel = level in levels ? level : 'H'
        return this
    }

    /**
     * QRコード画像の色指定
     *
     * @param dark string
     * @param light string
     * @returns QRCodeBuilderService
     */
    public setColor(dark: string, light: string): QRCodeBuilderService {
        this.options.color = {
            dark: dark,
            light: light
        };
        return this
    }

    /**
     * 出力画像フォーマットを指定
     * @param type string [png, jpeg, webp]
     * @returns QRCodeBUilderService
     */
    public setImageType(type: string): QRCodeBuilderService {
        const imtype = ['png', 'jpeg', 'webp']
        this.options.type = type in imtype ? 'image/' + type : 'image/jpeg'
        return this
    }

    /**
     * Jpeg webm出力時の精細度の指定
     * @param quolity number [0.0 ~ 1.0]
     * @returns
     */
    public setQuority(quolity: number): QRCodeBuilderService {
        this.options.quority = quolity
        return this
    }

    /**
     * 出力されたQRコードを受け取る
     *
     * @returns any dataURL形式
     */
    public getQR(): string {
        return this.Result
    }

    /**
     * QRコード作成
     * @returns Promise<QRCodeBuilderService>
     */
    public async build(): Promise<QRCodeBuilderService> {
        this.Result = ''
        this.Result = await QRCode.toDataURL(
            this.options.text,
            this.buildOption()
        );
        return this
    }

    private buildOption(): Object {
        return {
            errorCorrectionLevel: this.options.correctionLevel,
            mode: this.options.mode,
            type: this.options.type,
            quality: this.options.quority,
            margin: 1,
            color: this.options.color,
            width: this.options.width
        }
    }
}
