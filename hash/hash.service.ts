
export class HashService {
    static instance: HashService

    public static call(): HashService {
        if (!HashService.instance) {
            HashService.instance = new HashService()
        }
        return HashService.instance
    }

    /**
     * sha256ハッシュを生成
     * @param data string
     * @returns Promise<string>
     */
    public async sha256(data: string): Promise<string> {
        const msgUint8 = new TextEncoder().encode(data)
        const hash = await crypto.subtle.digest('SHA-256', msgUint8)
        return Array.from(new Uint8Array(hash)).map(b => b.toString(16).padStart(2, '0')).join('')
    }

    /**
     * sha512ハッシュを生成
     * @param data string
     * @returns Promise<string>
     */
    public async sha512(data: string): Promise<string> {
        const msgUint8 = new TextEncoder().encode(data)
        const hash = await crypto.subtle.digest('SHA-512', msgUint8)
        return Array.from(new Uint8Array(hash)).map(b => b.toString(16).padStart(2, '0')).join('');
    }

    /**
     * UUIDv4を生成
     * @returns string
     */
    public generateUUIDv4(): string {
        let uuid = ''
        const randomValues = new Uint8Array(16)
        crypto.getRandomValues(randomValues)
    
        randomValues.forEach((value, index) => {
            if (index === 6) {
                uuid += (4 | (value & 0x0F)).toString(16).padStart(2, '0')
            } else if (index === 8) {
                uuid += ((value & 0x3F) | 0x80).toString(16).padStart(2, '0')
            } else {
                uuid += value.toString(16).padStart(2, '0')
            }
    
            if (index === 3 || index === 5 || index === 7 || index === 9) {
                uuid += '-'
            }
        })
    
        return uuid
    }

    /**
     * ULIDを生成
     * @returns string
     */
    public generateULID(): string {
        const encodeTime = (time: number, length: number): string => {
            const chars = '0123456789ABCDEFGHJKMNPQRSTVWXYZ'
            let result = ''
            for (let i = length - 1; i >= 0; i--) {
                result = chars[time % 32] + result
                time = Math.floor(time / 32)
            }
            return result
        }
        
        const encodeRandom = (length: number): string => {
            const chars = '0123456789ABCDEFGHJKMNPQRSTVWXYZ'
            let result = ''
            const randomValues = new Uint8Array(length)
            crypto.getRandomValues(randomValues)
            for (let i = 0; i < length; i++) {
                result += chars[randomValues[i] % 32]
            }
            return result
        }
        const time = Date.now()
        const timePart = encodeTime(time, 10)
        const randomPart = encodeRandom(16)
    
        return timePart + randomPart
    }
}