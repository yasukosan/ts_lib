import * as tf from '@tensorflow/tfjs-core';
import '@tensorflow/tfjs-backend-webgl';
import * as tfjsWasm from '@tensorflow/tfjs-backend-wasm';
import * as posedetection from '@tensorflow-models/pose-detection';
import { PoseDetector } from '@tensorflow-models/pose-detection';

import { DrawPointHelper } from './_helper/draw_point.helper';
import { MuscleHelper } from './_helper/muscle.helper';

tfjsWasm.setWasmPaths(
    `https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm@${tfjsWasm.version_wasm}/dist/`
);

interface BodyDetect {
    keypoint: { y: number; x: number; score: number; name: string }[];
    score: number;
}

export interface DetectPose {
    keypoints: [
        {
            name: string;
            score: number;
            x: number;
            y: number;
            z: number;
        }
    ];
    keypoints3D: [
        {
            name: string;
            score: number;
            x: number;
            y: number;
            z: number;
        }
    ];
}

const initialDetect: BodyDetect = {
    keypoint: [{ y: 0, x: 0, score: 0, name: '' }],
    score: 0
};

type nextType = (result: any) => any;

export class BodyService {
    private static instance: BodyService;

    private camera: HTMLVideoElement | undefined;
    private detector: PoseDetector | undefined = undefined;
    private Next: nextType | boolean = false;
    private showDetect = false;

    private ScoreLimit = 0.6;
    private animationId = 0;

    private angleScore: number[] = [];
    private cordinators: number[][][] = [[[]]];

    public static call() {
        if (!BodyService.instance) {
            BodyService.instance = new BodyService();
        }
        return BodyService.instance;
    }

    /**
     * リターン関数の設定
     * @param next Function
     * @returns BodyService
     */
    public setNext(next: nextType): BodyService {
        this.Next = next;
        return this;
    }

    /**
     *
     * @param limit number 0.0 ~ 1.0
     * return BodyService
     */
    public setScireLimit(limit: number): BodyService {
        this.ScoreLimit = limit;
        return this;
    }

    /**
     * 角度情報配列を返す
     * @returns number[]
     */
    public getAngleScore(): number[] {
        return this.angleScore;
    }

    /**
     * * 検知対象の座標情報配列を返す
     * @returns number[][][]
     */
    public getCordinates(): number[][][] {
        return this.cordinators;
    }

    /**
     * 検知用モデル初期化
     */
    public async createDetector(): Promise<void> {
        // シンプル版の検出モデル
        //const model = posedetection.SupportedModels.MoveNet;
        // let modelType: any = {};
        // modelType = posedetection.movenet.modelType.SINGLEPOSE_LIGHTNING;
        // modelType = posedetection.movenet.modelType.SINGLEPOSE_THUNDER;
        // const modelConfig = {modelType};
        // this.detector = await posedetection.createDetector(model, mdoelConfig);

        // 作成済みのモデルを再利用すると、推論結果が前回の結果に引っ張られた値になり
        // 制度が著しく下がる、原因が不明のため下記コードコメント
        /*if (this.detector !== undefined) {
            return;
        }*/
        const model = posedetection.SupportedModels.BlazePose;
        const detectorConfig = {
            runtime: 'mediapipe',
            enableSmoothing: true,
            enableSegmentation: false,
            smoothSegmentation: true,
            modelType: 'lite',
            solutionPath: 'https://cdn.jsdelivr.net/npm/@mediapipe/pose'
            // or 'base/node_modules/@mediapipe/pose' in npm.
        } as unknown;
        // 推論用モデル作成
        this.detector = await posedetection.createDetector(
            model,
            detectorConfig as posedetection.BlazePoseTfjsModelConfig
        );
        return;
    }

    /**
     * カメラから映像を受け取る準備
     * @param camera HTMLVideoElement ※再生中のVideoタグ
     * @returns Promise<void>
     */
    public async setupCamera(
        camera: HTMLVideoElement,
        canvas: string,
        scatter: string
    ): Promise<HTMLVideoElement> {
        this.camera = camera;

        const videoWidth = this.camera.videoWidth;
        const videoHeight = this.camera.videoHeight;
        // ビデオサイズにvideoタグサイズを合わせる
        this.camera.width = videoWidth;
        this.camera.height = videoHeight;
        // 結果表示用のcanvasもサイズを合わせる
        DrawPointHelper.call()
            .init(canvas, scatter)
            .setScale(videoWidth, videoHeight);

        // TensorFlow実行バックグラウンドのセットアップを先に呼んでおく
        await tf.setBackend('wasm');

        // animationId初期化
        this.animationId = 0;
        return this.camera;
    }

    /**
     * ポージング判別用の角度情報を計算
     * @param detect
     * @returns number[]
     */
    public calcAngles(detect: any): BodyService {
        this.angleScore = MuscleHelper.call().convNormalization(detect);
        this.cordinators = MuscleHelper.call().getAngleCordinates();
        return this;
    }

    /**
     * 検知結果をCanvasに出力
     * @param view boolean true: 表示 false: 非表示
     * @returns BodyService
     */
    public viewDetect(view: boolean): BodyService {
        this.showDetect = view;
        return this;
    }

    /**
     * 映像から連続で推論を開始
     */
    public async renderPrediction() {
        await this.inference();
        if (this.animationId !== 99999999) {
            this.animationId = requestAnimationFrame(() => {
                this.renderPrediction();
            });
        }
    }

    /**
     * 検知ループを止める
     * @returns void
     */
    public stopAnimation(): void {
        if (this.animationId === 99999999) return;
        cancelAnimationFrame(this.animationId);
        DrawPointHelper.call().clearCtx();
        this.allReset();
        return;
    }

    /**
     * 映像から推論
     * 検知対象を一人にしているので
     * 複数人がカメラ内にいるとバグる
     * 複数検知の場合はパラメータを変更すべし
     */
    public async inference(): Promise<void> {
        // 推論実行
        const poses = (await this.doInference(
            this.camera as HTMLVideoElement
        )) as DetectPose[];
        this.after(poses);
    }

    /**
     * 画像から推論
     * @param image HTMLImageElement
     * @returns {keypoint: number[], keypoint3d: number[]}
     */
    public async inferenceSingle(image: HTMLImageElement): Promise<DetectPose> {
        // 推論実行
        const poses = (await this.doInference(image)) as DetectPose[];
        this.after(poses);
        return poses[0];
    }

    /**
     * ビデオ映像から連続推論
     * @param target HTMLVideoElement | HTMLImageElement
     * @returns Promise<DetecrPose[] | undefined> detectorが未設定の場合undefinedが返る
     */
    private async doInference(
        target: HTMLVideoElement | HTMLImageElement
    ): Promise<DetectPose[] | undefined> {
        if (this.detector === undefined) return undefined;
        try {
            return (await this.detector.estimatePoses(target, {
                maxPoses: 1,
                flipHorizontal: false
            })) as DetectPose[];
        } catch (error) {
            this.detector.dispose();
            this.detector = undefined;
            // alert(error);
        }
        return undefined;
    }

    /**
     * 後処理
     * コールバックが設定されている場合実行
     * 推論結果の描画が設定されている場合実行
     * @param poses
     */
    private after(poses: any[]) {
        // コールバック関数がセットされている場合は結果を渡す
        if (typeof this.Next === 'function') {
            if (poses && poses.length > 0) {
                // this.Next(MuscleHelper.call().convNormalization(poses[0].keypoints));
                this.Next(poses[0].keypoints);
            }
        }

        // 検出結果の描画が有効な場合結果を出力
        if (this.showDetect) {
            DrawPointHelper.call().drawCtx(this.camera as HTMLVideoElement);

            if (poses && poses.length > 0) {
                DrawPointHelper.call().drawResults(poses);
            }
        }
    }

    /**
     * 全項目を初期化
     * @returns BodyService
     */
    public allReset(): BodyService {
        this.camera = undefined;
        this.detector = undefined;
        this.Next = false;
        this.showDetect = false;
        this.ScoreLimit = 0.6;
        this.animationId = 0;

        this.clearResult();

        return this;
    }

    /**
     * 推論結果を消す
     * @returns BodyService
     */
    public clearResult(): BodyService {
        this.angleScore = [];
        this.cordinators = [[[]]];
        return this;
    }
}
