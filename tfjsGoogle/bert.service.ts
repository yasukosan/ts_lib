import * as tf from '@tensorflow/tfjs-core';
import '@tensorflow/tfjs-backend-webgl';
import * as tfjsWasm from '@tensorflow/tfjs-backend-wasm';
import * as qna from '@tensorflow-models/qna';

tfjsWasm.setWasmPaths(
    `https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm@${tfjsWasm.version_wasm}/dist/`
);

type nextType = (result: any) => any;

export class BertService {
    private static instance: BertService;

    private Model: any = null;
    private Next: nextType | boolean = false;

    public static call(): BertService {
        if (!BertService.instance) {
            BertService.instance = new BertService();
        }
        return BertService.instance;
    }

    public async load() {
        /*
        this.Model = await qna.load({
            modelUrl    : 'http://localhost:3000/pytorch_model.bin',
        });
        */
        this.Model = await qna.load();
    }

    public async answer(question: string, passage: string): Promise<string> {
        const _answer = await this.Model.findAnswers(question, passage);
        return _answer;
    }

    public setNext(next: nextType): BertService {
        this.Next = next;
        return this;
    }
}
