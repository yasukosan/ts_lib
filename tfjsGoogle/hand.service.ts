import * as tfjsWasm from '@tensorflow/tfjs-backend-wasm';
import * as handPoseDetection from '@tensorflow-models/hand-pose-detection';
import('@tensorflow/tfjs-backend-cpu');
import('@tensorflow/tfjs-backend-webgl');

tfjsWasm.setWasmPaths(
    `https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm@${tfjsWasm.version_wasm}/dist/`
);

export interface HandPredict {
    keypoints: { x: number; y: number; name: string }[];
    keypoints3D: { x: number; y: number; z: number; name: string }[];
    score: number;
    handedness: 'Right' | 'Left';
}

export class HandService {
    private static instance: HandService;

    private Detector: any = null;

    private Config: any = {
        runtime: 'mediapipe', // mediapipe or tfjs
        maxHands: 2, // 1以上
        solutionPath: 'https://cdn.jsdelivr.net/npm/@mediapipe/hands',
        modelType: 'full' // lite or full
    };

    public static call(): HandService {
        if (!HandService.instance) {
            HandService.instance = new HandService();
        }
        return HandService.instance;
    }

    /**
     * モデルの設定を変更
     * @param key 'runtime' | 'maxHands' | 'solutionPath' | 'modelType',
     * @param val number | string
     * @returns
     */
    public setConfig(
        key: 'runtime' | 'maxHands' | 'solutionPath' | 'modelType',
        val: number | string
    ): HandService {
        if (key in this.Config) {
            this.Config[key] = val;
        }

        return this;
    }

    /**
     * モデルデータ読み込み
     */
    public async load() {
        const model = await handPoseDetection.SupportedModels.MediaPipeHands;
        this.Detector = await handPoseDetection.createDetector(
            model,
            this.Config as handPoseDetection.MediaPipeHandsMediaPipeModelConfig
        );
    }

    /**
     * 推論開始
     * @param image HTMLImageElement
     * @returns Promise<string>
     */
    public async predict(
        image: HTMLImageElement | HTMLVideoElement | string
    ): Promise<string> {
        const _predict = await this.Detector.estimateHands(image);
        return _predict;
    }
}
