/*
import * as tf from '@tensorflow/tfjs';
import * as mobilenetModule from '@tensorflow-models/mobilenet';
import * as knnClassifier from '@tensorflow-models/knn-classifier';

export class KnnService {
    private static instance: KnnService;

    private classifier: knnClassifier.KNNClassifier | any;
    private mobilenet: mobilenetModule.MobileNet | any;
    public static call(): KnnService {
        if (!KnnService.instance) {
            KnnService.instance = new KnnService();
        }
        return KnnService.instance;
    }

    public async load() {
        this.classifier = knnClassifier.create();

        // Load mobilenet.
        this.mobilenet = await mobilenetModule.load();
    }

    public async setBasisImages(
        images: [
            {
                image: HTMLImageElement;
                index: number;
            }
        ]
    ) {
        // Add MobileNet activations to the model repeatedly for all classes.
        for (const key in images) {
            if (Object.prototype.hasOwnProperty.call(images, key)) {
                const img0: any = tf.browser.fromPixels(images[key].image);
                const logits0 = this.mobilenet.infer(img0, true);
                this.classifier.addExample(logits0, images[key].index);
            }
        }
    }

    public async predict(image: HTMLImageElement) {
        // Make a prediction.
        const x: any = tf.browser.fromPixels(image);
        const xlogits = this.mobilenet.infer(x, true);
        const predict = await this.classifier.predictClass(xlogits);
        console.log('Predictions:');
        console.log(predict);

        return predict;
    }
}
*/