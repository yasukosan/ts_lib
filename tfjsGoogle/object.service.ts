import * as tfjsWasm from '@tensorflow/tfjs-backend-wasm';
import('@tensorflow/tfjs-backend-cpu');
import('@tensorflow/tfjs-backend-webgl');

import * as cocoSsd from '@tensorflow-models/coco-ssd';

tfjsWasm.setWasmPaths(
    `https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm@${tfjsWasm.version_wasm}/dist/`
);

type nextType = (result: any) => any;

interface ObjectPredict {
    bbox: number[];
    class: string;
    score: number;
}
export class ObjectService {
    private static instance: ObjectService;

    private Model: any = null;
    private Next: nextType | boolean = false;

    public static call(): ObjectService {
        if (!ObjectService.instance) {
            ObjectService.instance = new ObjectService();
        }
        return ObjectService.instance;
    }

    /**
     * モデルデータ読み込み
     */
    public async load() {
        this.Model = await cocoSsd.load();
    }

    /**
     * 推論開始
     * @param image HTMLImageElement
     * @returns Promise<string>
     */
    public async predict(
        image: HTMLImageElement | HTMLVideoElement | string
    ): Promise<ObjectPredict[]> {
        const _predict = await this.Model.detect(image);
        return _predict;
    }

    public setNext(next: nextType): ObjectService {
        this.Next = next;
        return this;
    }
}
