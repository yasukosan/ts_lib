import * as posedetection from '@tensorflow-models/pose-detection';
import * as scatter from 'scatter-gl';

export class DrawPointHelper {
    private static instance: DrawPointHelper;

    private canvas: HTMLCanvasElement | undefined;
    private ctx: CanvasRenderingContext2D | undefined;
    private width = 0;
    private height = 0;

    private activeScatter = false;
    private scatterGLEL: HTMLDivElement | undefined;
    private scatterGL: scatter.ScatterGL | undefined;
    private scatterGLHasInitialized = false;

    private ANCHOR_POINTS = [
        [0, 0, 0],
        [0, 1, 0],
        [-1, 0, 0],
        [-1, -1, 0]
    ];

    public static call(): DrawPointHelper {
        if (!DrawPointHelper.instance) {
            DrawPointHelper.instance = new DrawPointHelper();
        }

        return DrawPointHelper.instance;
    }

    /**
     *
     * @param scatter boolean Scatter表示有無
     */
    public constructor(scatter = false) {
        this.activeScatter = scatter;
    }

    /**
     * 出力先Cavasターゲットの指定
     * Scatter有効時はScatter出力先の指定
     * @param canvas string CanvasタグのID
     * @param scatter string DIVタグのID
     * @returns DrawPointHelper
     */
    public init(canvas: string, scat = ''): DrawPointHelper {
        this.canvas = document.getElementById(canvas) as HTMLCanvasElement;
        this.ctx = this.canvas.getContext('2d') as CanvasRenderingContext2D;
        console.log(this.canvas);
        if (this.activeScatter) {
            this.scatterGLEL = document.getElementById(scat) as HTMLDivElement;
            this.scatterGL = new scatter.ScatterGL(this.scatterGLEL, {
                rotateOnStart: true,
                selectEnabled: false,
                styles: {
                    polyline: { defaultOpacity: 1, deselectedOpacity: 1 }
                }
            });
            this.scatterGLHasInitialized = false;
        }
        return this;
    }

    /**
     * 出力先のサイズ指定
     * @param width number ピクセル指定
     * @param height number ピクセル指定
     * @returns DrawPointHelper
     */
    public setScale(width: number, height: number): DrawPointHelper {
        console.log(this.ctx);
        this.width = width;
        this.height = height;
        if (this.canvas !== undefined && this.ctx !== undefined) {
            this.canvas.width = width;
            this.canvas.height = height;
            this.ctx.translate(width, 0);
            this.ctx.scale(-1, 1);
        }

        return this;
    }

    /**
     * 描画ターゲットにビデオストリームを出力
     * @param camera HTMLVideoElement
     * @return DrawPointHelper
     */
    public drawCtx(
        camera: HTMLVideoElement | HTMLImageElement
    ): DrawPointHelper {
        if (this.ctx === undefined) return this;
        this.ctx.drawImage(camera, 0, 0, this.width, this.height);
        return this;
    }

    /**
     * 描画ターゲットクリア
     * @return DrawPointHelper
     */
    public clearCtx(): DrawPointHelper {
        if (this.ctx === undefined) return this;
        this.ctx.clearRect(0, 0, this.width, this.height);
        return this;
    }
    /**
     *
     * @param poses A list of poses to render.
     */
    public drawResults(poses: any[]) {
        for (const pose of poses) {
            this.drawResult(pose);
        }
    }

    /**
     * Draw the keypoints and skeleton on the video.
     * @param pose A pose with keypoints to render.
     */
    private drawResult(pose: any) {
        if (pose.keypoints != null) {
            this.drawKeypoints(pose.keypoints);
            this.drawSkeleton(pose.keypoints);
        }
        if (pose.keypoints3D != null) {
            //this.drawKeypoints3D(pose.keypoints3D);
        }
    }

    /**
     * Draw the keypoints on the video.
     * @param keypoints A list of keypoints.
     */
    private drawKeypoints(keypoints: any): void {
        if (this.ctx === undefined) return;

        const keypointInd = posedetection.util.getKeypointIndexBySide(
            posedetection.SupportedModels.MoveNet
        );
        this.ctx.fillStyle = 'Red';
        this.ctx.strokeStyle = 'White';
        this.ctx.lineWidth = 2;

        for (const i of keypointInd.middle) {
            this.drawKeypoint(keypoints[i]);
        }

        this.ctx.fillStyle = 'Green';
        for (const i of keypointInd.left) {
            this.drawKeypoint(keypoints[i]);
        }

        this.ctx.fillStyle = 'Orange';
        for (const i of keypointInd.right) {
            this.drawKeypoint(keypoints[i]);
        }
    }

    /**
     * 検出ポイントを描画
     * @param keypoint
     */
    private drawKeypoint(keypoint: any): void {
        if (this.ctx === undefined) return;
        // If score is null, just show the keypoint.
        const score = keypoint.score != null ? keypoint.score : 1;
        const scoreThreshold = 0.4;

        if (score >= scoreThreshold) {
            const circle = new Path2D();
            circle.arc(keypoint.x, keypoint.y, 4, 0, 2 * Math.PI);

            this.ctx.fill(circle);
            this.ctx.stroke(circle);
        }
    }

    /**
     * Draw the skeleton of a body on the video.
     * @param keypoints A list of keypoints.
     */
    private drawSkeleton(keypoints: any) {
        if (this.ctx === undefined) return;

        // Each poseId is mapped to a color in the color palette.
        const color = 'White';
        this.ctx.fillStyle = color;
        this.ctx.strokeStyle = color;
        this.ctx.lineWidth = 2;

        posedetection.util
            .getAdjacentPairs(posedetection.SupportedModels.MoveNet)
            .forEach(([i, j]) => {
                const kp1 = keypoints[i];
                const kp2 = keypoints[j];

                // If score is null, just show the keypoint.
                const score1 = kp1.score != null ? kp1.score : 1;
                const score2 = kp2.score != null ? kp2.score : 1;
                const scoreThreshold = 0.4;

                if (score1 >= scoreThreshold && score2 >= scoreThreshold) {
                    if (this.ctx === undefined) return;

                    this.ctx.beginPath();
                    this.ctx.moveTo(kp1.x, kp1.y);
                    this.ctx.lineTo(kp2.x, kp2.y);
                    this.ctx.stroke();
                }
            });
    }
    /**
     * 検出ポイントを3次元で描画
     * @param keypoints
     */
    private drawKeypoints3D(keypoints: any): void {
        if (this.scatterGL === undefined) return;

        const scoreThreshold = 0.4;
        const pointsData = keypoints.map((keypoint: any) => [
            -keypoint.x,
            -keypoint.y,
            -keypoint.z
        ]);

        const dataset = new scatter.ScatterGL.Dataset([
            ...pointsData,
            ...this.ANCHOR_POINTS
        ]);

        const keypointInd = posedetection.util.getKeypointIndexBySide(
            posedetection.SupportedModels.MoveNet
        );
        this.scatterGL.setPointColorer((i: number): any => {
            if (keypoints[i] == null || keypoints[i].score < scoreThreshold) {
                // hide anchor points and low-confident points.
                return '#ffffff';
            }
            if (i === 0) {
                return '#ff0000' /* Red */;
            }
            if (keypointInd.left.indexOf(i) > -1) {
                return '#00ff00' /* Green */;
            }
            if (keypointInd.right.indexOf(i) > -1) {
                return '#ffa500' /* Orange */;
            }
        });

        if (!this.scatterGLHasInitialized) {
            this.scatterGL.render(dataset);
        } else {
            this.scatterGL.updateDataset(dataset);
        }
        const connections = posedetection.util.getAdjacentPairs(
            posedetection.SupportedModels.MoveNet
        );
        const sequences = connections.map((pair) => ({ indices: pair }));
        this.scatterGL.setSequences(sequences);
        this.scatterGLHasInitialized = true;
    }
}
