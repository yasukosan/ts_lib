import { MathService } from '../../Math/math.service';
import { TacksheetMakerService } from '../../pdf/maker/tacksheet-maker.service';

interface PoseInterface {
    leftShoulder1: { angle: number; bec: 'up' | 'down' };
    leftShoulder2: { angle: number; bec: 'up' | 'down' };
    leftShoulder3: { angle: number; bec: 'up' | 'down' };
    leftElbow1: { angle: number; bec: 'up' | 'down' };
    leftElbow2: { angle: number; bec: 'up' | 'down' };
    leftElbow3: { angle: number; bec: 'up' | 'down' };
    leftWrist1: { angle: number; bec: 'up' | 'down' };
    leftWrist2: { angle: number; bec: 'up' | 'down' };
    leftWrist3: { angle: number; bec: 'up' | 'down' };
    rightShoulder1: { angle: number; bec: 'up' | 'down' };
    rightShoulder2: { angle: number; bec: 'up' | 'down' };
    rightShoulder3: { angle: number; bec: 'up' | 'down' };
    rightElbow1: { angle: number; bec: 'up' | 'down' };
    rightElbow2: { angle: number; bec: 'up' | 'down' };
    rightElbow3: { angle: number; bec: 'up' | 'down' };
    rightWrist1: { angle: number; bec: 'up' | 'down' };
    rightWrist2: { angle: number; bec: 'up' | 'down' };
    rightWrist3: { angle: number; bec: 'up' | 'down' };
    leftHip1: { angle: number; bec: 'up' | 'down' };
    leftHip2: { angle: number; bec: 'up' | 'down' };
    leftHip3: { angle: number; bec: 'up' | 'down' };
    leftKnee1: { angle: number; bec: 'up' | 'down' };
    leftKnee2: { angle: number; bec: 'up' | 'down' };
    leftKnee3: { angle: number; bec: 'up' | 'down' };
    leftAnkle1: { angle: number; bec: 'up' | 'down' };
    leftAnkle2: { angle: number; bec: 'up' | 'down' };
    leftAnkle3: { angle: number; bec: 'up' | 'down' };
    rightHip1: { angle: number; bec: 'up' | 'down' };
    rightHip2: { angle: number; bec: 'up' | 'down' };
    rightHip3: { angle: number; bec: 'up' | 'down' };
    rightKnee1: { angle: number; bec: 'up' | 'down' };
    rightKnee2: { angle: number; bec: 'up' | 'down' };
    rightKnee3: { angle: number; bec: 'up' | 'down' };
    rightAnkle1: { angle: number; bec: 'up' | 'down' };
    rightAnkle2: { angle: number; bec: 'up' | 'down' };
    rightAnkle3: { angle: number; bec: 'up' | 'down' };
}

const initialMuscle: PoseInterface = {
    leftShoulder1: { angle: 0, bec: 'up' },
    leftShoulder2: { angle: 0, bec: 'up' },
    leftShoulder3: { angle: 0, bec: 'up' },
    leftElbow1: { angle: 0, bec: 'up' },
    leftElbow2: { angle: 0, bec: 'up' },
    leftElbow3: { angle: 0, bec: 'up' },
    leftWrist1: { angle: 0, bec: 'up' },
    leftWrist2: { angle: 0, bec: 'up' },
    leftWrist3: { angle: 0, bec: 'up' },
    rightShoulder1: { angle: 0, bec: 'up' },
    rightShoulder2: { angle: 0, bec: 'up' },
    rightShoulder3: { angle: 0, bec: 'up' },
    rightElbow1: { angle: 0, bec: 'up' },
    rightElbow2: { angle: 0, bec: 'up' },
    rightElbow3: { angle: 0, bec: 'up' },
    rightWrist1: { angle: 0, bec: 'up' },
    rightWrist2: { angle: 0, bec: 'up' },
    rightWrist3: { angle: 0, bec: 'up' },
    leftHip1: { angle: 0, bec: 'up' },
    leftHip2: { angle: 0, bec: 'up' },
    leftHip3: { angle: 0, bec: 'up' },
    leftKnee1: { angle: 0, bec: 'up' },
    leftKnee2: { angle: 0, bec: 'up' },
    leftKnee3: { angle: 0, bec: 'up' },
    leftAnkle1: { angle: 0, bec: 'up' },
    leftAnkle2: { angle: 0, bec: 'up' },
    leftAnkle3: { angle: 0, bec: 'up' },
    rightHip1: { angle: 0, bec: 'up' },
    rightHip2: { angle: 0, bec: 'up' },
    rightHip3: { angle: 0, bec: 'up' },
    rightKnee1: { angle: 0, bec: 'up' },
    rightKnee2: { angle: 0, bec: 'up' },
    rightKnee3: { angle: 0, bec: 'up' },
    rightAnkle1: { angle: 0, bec: 'up' },
    rightAnkle2: { angle: 0, bec: 'up' },
    rightAnkle3: { angle: 0, bec: 'up' }
};

export type MusclePose =
    | 'FrontRelax'
    | 'SideRelaxLeft'
    | 'SideRelaxRight'
    | 'RearRelax'
    | 'FrontDoubleBiceps'
    | 'FrontLatSpread'
    | 'SideChest'
    | 'BackDoubleBiceps'
    | 'BackLatSpread'
    | 'SideTriceps'
    | 'AbdominalsAndThighs'
    | 'MostMuscular'
    | 'None';

export class MuscleHelper {
    private static instance: MuscleHelper;

    private Pose: { [key in MusclePose]: PoseInterface } = {
        FrontRelax: initialMuscle,
        SideRelaxLeft: initialMuscle,
        SideRelaxRight: initialMuscle,
        RearRelax: initialMuscle,
        FrontDoubleBiceps: initialMuscle,
        FrontLatSpread: initialMuscle,
        SideChest: initialMuscle,
        BackDoubleBiceps: initialMuscle,
        BackLatSpread: initialMuscle,
        SideTriceps: initialMuscle,
        AbdominalsAndThighs: initialMuscle,
        MostMuscular: initialMuscle,
        None: initialMuscle
    };

    private checkAngles = [
        [8, 6, 5], // 右肩1
        [8, 6, 11], // 右肩2
        [8, 6, 12], // 右肩3
        [10, 8, 6], // 右肘1
        [12, 8, 6], // 右肘2
        [14, 8, 6], // 右肘3
        [6, 10, 8], // 右手首1
        [12, 10, 8], // 右手首2
        [14, 10, 8], // 右手首3
        [7, 5, 6], // 左肩1
        [7, 5, 11], // 左肩2
        [7, 5, 12], // 左肩3
        [9, 7, 5], // 左肘1
        [9, 7, 11], // 左肘2
        [9, 7, 13], // 左肘3
        [5, 9, 7], // 左手首
        [11, 9, 7], // 左手首
        [13, 9, 7], // 左手首
        [6, 12, 5], // 右腿1
        [6, 12, 11], // 右腿2
        [6, 12, 14], // 右腿3
        [12, 14, 11], // 右膝1
        [12, 14, 13], // 右膝2
        [12, 14, 16], // 右膝3
        [5, 11, 6], // 左腿1
        [5, 11, 12], // 左腿2
        [5, 11, 13], // 左腿3
        [11, 13, 12], // 左膝1
        [11, 13, 14], // 左膝2
        [11, 13, 15] // 左膝3
    ];

    // 角度計算に使用した各座標情報
    private AngleCordinates: number[][][] | undefined;

    public static call() {
        if (!MuscleHelper.instance) {
            MuscleHelper.instance = new MuscleHelper();
        }
        return MuscleHelper.instance;
    }

    /**
     * 角度計算に使った座標情報を返す
     * @returns number[][][]([部位:[x,y,z]])
     */
    public getAngleCordinates(): number[][][] {
        return this.AngleCordinates as number[][][];
    }

    /**
     * チェック部位の角度情報に変換
     * {[部位名]: [x:number,y:number,z:number]}の構造を
     * [3次元ベクトル角,....]の形式に変換
     * @param poses [ { [ key: string ]: number[] } ]
     * @returns number[]
     */
    public convNormalization(poses: any): number[] {
        const angles = new Array(10);
        for (let i = 0; i <= this.checkAngles.length - 1; i++) {
            const p: number[][] = [
                [
                    poses[this.checkAngles[i][0]].x,
                    poses[this.checkAngles[i][0]].y,
                    poses[this.checkAngles[i][0]].z
                ],
                [
                    poses[this.checkAngles[i][1]].x,
                    poses[this.checkAngles[i][1]].y,
                    poses[this.checkAngles[i][0]].z
                ],
                [
                    poses[this.checkAngles[i][2]].x,
                    poses[this.checkAngles[i][2]].y,
                    poses[this.checkAngles[i][0]].z
                ]
            ];

            // 角度情報に変換する前の座標情報を保持
            if (i === 0) {
                this.AngleCordinates = [p];
            } else {
                this.AngleCordinates?.push(p);
            }

            angles[i] = MathService.calc3DAnglar(
                [p[0][0], p[0][1], p[0][2]],
                [p[1][0], p[1][1], p[1][2]],
                [p[2][0], p[2][1], p[2][2]]
            );
        }
        return angles;
    }
}
