import * as tfjsWasm from '@tensorflow/tfjs-backend-wasm';
import('@tensorflow/tfjs-backend-cpu');
import('@tensorflow/tfjs-backend-webgl');
import * as tf from '@tensorflow/tfjs';

//const speechCommands = require('@tensorflow-models/speech-commands');
const speechCommands: any = {};
tfjsWasm.setWasmPaths(
    `https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm@${tfjsWasm.version_wasm}/dist/`
);

/**
 * 20220927
 *
 * 実利用にあたり、AudioContextをモデル側で制御されるのでゲインの調整等は出来ない
 * nodejsで動く前提に何故かなっており、nodejsのfsモジュールの参照が出来ないローカル
 * 実行環境では画面所のエラーを消す手段がない
 *
 * 推論可能な音声コマンドはデフォルトで用意されている英語の13語のみ
 * 実用はほぼ不可能
 */

export class SpeechService {
    private static instance: SpeechService;

    private Model: any = null;
    private Next: Function | boolean = false;

    public constructor() {}

    public static call(): SpeechService {
        if (!SpeechService.instance) {
            SpeechService.instance = new SpeechService();
        }
        return SpeechService.instance;
    }

    /**
     * モデルデータ読み込み
     */
    public async load() {
        this.Model = speechCommands.create('BROWSER_FFT');
        await this.Model.ensureModelLoaded();
        console.log(this.Model.wordLabels());
    }

    /**
     * 推論開始
     * @param image HTMLImageElement
     * @returns Promise<string>
     */
    public async predict(): Promise<void> {
        this.Model.listen(
            (result: any) => {
                // - result.scores contains the probability scores that correspond to
                //   recognizer.wordLabels().
                // - result.spectrogram contains the spectrogram of the recognized word.
                console.log(result);
            },
            {
                includeSpectrogram: true,
                probabilityThreshold: 0.75
            }
        );
        setTimeout(() => this.Model.stopListening(), 10e3);
    }

    public setNext(next: Function): SpeechService {
        this.Next = next;
        return this;
    }
}
