/**
 *
 * このファイルは必ずDocumentRootに置く
 *
 *
 */

/**
 * インストール時の処理
 */
self.addEventListener("install", (event) => {
    console.log("Service Worker install")
})

/**
 * Push通知を受け取った時の処理
 * 
 */
self.addEventListener('push', (event) => {
    if (event.data) {
        const data = event.data.json();

        const promiseChain = self.registration.showNotification(
            data.title,
            buildNoticeParam(data)
        );
        event.waitUntil(promiseChain);
    }
});

self.addEventListener('notificationclick', (event) => {
    console.log(event.notification.body);
    event.notification.close();

    if ('redirect' in Responce) {
        event.waitUntil(clients.openWindow(Responce[redirect]));
    }
});

self.addEventListener('message', (e) => {
    // メッセージを受け取る
    const message = e.data;
    // 何か処理する

    // 返事を作る
    const reply = 'this message is from sw.js';
    // 返事を返す
    e.ports[0].postMessage(reply);
});

let Responce = {};

const NotificationOptions = [
    'dir',
    'lang',
    'badge',
    'body',
    'tag',
    'icon',
    'image',
    'data',
    'vibrate',
    'renotify',
    'requireInteraction',
    'actions',
    'silent'
];

const buildNoticeParam = (data) => {
    Responce = data;
    const p = {};
    for (const key in data) {
        if (Object.hasOwnProperty.call(data, key)) {
            if (NotificationOptions.indexOf(key) >= 0) {
                p[key] = data[key];
            }
        }
    }
    return p;
};
