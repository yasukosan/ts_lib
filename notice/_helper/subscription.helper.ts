import { urlBase64ToUint8Array } from "../../_helper/convert.helper"
import { PushAPIService } from "../pushapi.service"

export type SubscriptionType = {
    name: string;
    mail: string;
    url: string;
    key: string;
    challenge: string;
}

export let ServiceWorker: PushAPIService | any = undefined
export let Subscription: PushSubscription | any = undefined
export const SubscriptionOptions: SubscriptionType = {
    name: 'guest',
    mail: '',
    url: 'http://localhost:8080',
    key: '',
    challenge: '',
}
export let WorkerFile: string = 'service.worker.js'


export const registerServiceWorker = async (
    output: boolean = false
): Promise<boolean> => {
    try {
        ServiceWorker = await navigator.serviceWorker.register(WorkerFile)
        if (output) {
            console.log(ServiceWorker)
        }
    } catch (err: any) {
        console.error(`[ServiceWorker Register ERROR] ${err.message}`)
        return false
    }
    return true
}


export const registerSubscription = async (
    output: boolean = false
): Promise<boolean> => {
    try {
        Subscription = await ServiceWorker.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array(SubscriptionOptions.key)
        })
        if (output) {
            console.log(ServiceWorker)
            console.log(Subscription)
        }
    } catch (err: any) {
        console.error(`[Subscription Register ERROR] ${err.message}`)
        return false
    }
    return true
}

export const unRegisterSubscription = async (): Promise<boolean> => {
    if (!checkRegisterSubscription()) return false

    try {
        await Subscription.unsubscribe()
        console.log(`[Subscription UnRegister SUCCESS]`)
    } catch (err: any) {
        console.error(`[Subscription UnRegister ERROR] ${err.message}`)
        return false
    }
    return true
}


export const supportServiceWorker = (): boolean => {
    if ('serviceWorker' in navigator) {
        return true
    }
    return false
}

export const checkRegisterServiceWorker = async (
    
): Promise<boolean> => {
    // ServiceWorkerの登録確認
    ServiceWorker = await navigator.serviceWorker.ready.then()

    if (!ServiceWorker) {
        console.log('ServiceWorker が無効です')
        return false
    }
    console.log('ServiceWorker が有効です')
    return true
}

export const checkRegisterSubscription = async (
    output: boolean = false
): Promise<boolean> => {
    // Subscriptionの登録確認
    Subscription = await ServiceWorker.pushManager.getSubscription()
    if (output) {
        console.log(Subscription)
    }

    if (!Subscription) {
        console.log('PushSubscription が無効です')
        return false
    }
    console.log('PushSubscription が有効です')
    return true
}

