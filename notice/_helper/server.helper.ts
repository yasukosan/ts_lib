
export type ServerOptionType = {
    url?: string
    path?: {
        subscribe: string
        unsubscribe: string
        allUnSubscribe: string
        getChallenge: string
        getSubscribe: string
        getPublicKey: string
        getGroup: string
        getTokens: string
        pushNotificationToUser: string
        pushNotificationToGroup: string
        pushNotificationToAll: string
    }
    name?: string
    mail?: string
    challenge?: string
}

export class ServerHelper {
    private static instance: ServerHelper;

    private ServerOptions: ServerOptionType = {
        url: 'https://localhost:8080',
        path: {
            subscribe: 'subscribe',
            unsubscribe: 'unsubscribe',
            allUnSubscribe: 'allUnSubscribe',
            getChallenge: 'getChallenge',
            getSubscribe: 'getSubscribe',
            getPublicKey: 'getPublicKey',
            getGroup: 'getGroup',
            getTokens: 'getTokens',
            pushNotificationToUser: 'pushNotificationToUser',
            pushNotificationToGroup: 'pushNotificationToGroup',
            pushNotificationToAll: 'pushNotificationToAll'
        },
        name: 'guest',
        mail: '',
        challenge: ''
    }

    private _SUBSCRIPTION: PushSubscription | any;

    public setSubscription(subscription: PushSubscription): ServerHelper {
        this._SUBSCRIPTION = subscription
        return this
    }

    public setOptions(options: ServerOptionType): ServerHelper {
        this.ServerOptions.url = options.url ?? this.ServerOptions.url
        this.ServerOptions.path = options.path ?? this.ServerOptions.path
        this.ServerOptions.name = options.name ?? this.ServerOptions.name
        this.ServerOptions.mail = options.mail ?? this.ServerOptions.mail
        this.ServerOptions.challenge = options.challenge ?? this.ServerOptions.challenge

        return this
    }

    public static call(): ServerHelper {
        if (!ServerHelper.instance) {
            ServerHelper.instance = new ServerHelper();
        }
        return ServerHelper.instance;
    }


    public async subscribe(): Promise<Response | false> {
        return await this.doFetch({
            subscribe: this._SUBSCRIPTION,
            user: {
                name:  this.ServerOptions.name,
                mail:  this.ServerOptions.mail
            }
        }, this.ServerOptions.path!.subscribe)
    }

    public async unSubscribe(): Promise<Response | false> {
        return await this.doFetch({
            unsubscribe: this._SUBSCRIPTION,
            user: {
                name:  this.ServerOptions.name,
                mail:  this.ServerOptions.mail
            }
        }, this.ServerOptions.path!.unsubscribe)
    }

    public async allUnSubscribe(): Promise<Response | false> {
        return await this.doFetch({
            unsubscribe: this._SUBSCRIPTION,
        }, this.ServerOptions.path!.allUnSubscribe)
    }

    public async getSubscribe(): Promise<Response | false> {
        return await this.doFetch({
            subscribe: this._SUBSCRIPTION,
            user: {
                name:  this.ServerOptions.name,
                mail:  this.ServerOptions.mail
            }
        }, this.ServerOptions.path!.getSubscribe)
    }

    public async getPublicKey(): Promise<Response | false> {
        return await this.doFetch({}, this.ServerOptions.path!.getPublicKey)
    }

    public async getGroup(): Promise<Response | false> {
        return await this.doFetch({
            subscribe: this._SUBSCRIPTION,
        }, this.ServerOptions.path!.getGroup)
    }

    public async getTokens(): Promise<Response | false> {
        return await this.doFetch({
            subscribe: this._SUBSCRIPTION,
        }, this.ServerOptions.path!.getTokens)
    }

    public async setChallenge(): Promise<Response | false> {
        return await this.doFetch({
            subscribe: this._SUBSCRIPTION,
        }, this.ServerOptions.path!.getTokens)
    }



    public async pushNotificationToUser(): Promise<Response | false> {
        return await this.doFetch({
            subscribe: this._SUBSCRIPTION,
            user: {
                name:  this.ServerOptions.name,
                mail:  this.ServerOptions.mail,
                challenge: this.ServerOptions.challenge
            }
        }, this.ServerOptions.path!.pushNotificationToUser)
    }

    public async pushNotificationToGroup(): Promise<Response | false> {
        return await this.doFetch({
            subscribe: this._SUBSCRIPTION,
            group: {
                group:  this.ServerOptions.name,
            }
        }, this.ServerOptions.path!.pushNotificationToGroup)
    }

    public async pushNotificationToAll(): Promise<Response | false> {
        return await this.doFetch({
            subscribe: this._SUBSCRIPTION,
        }, this.ServerOptions.path!.pushNotificationToAll)
    }
    
    /**
     * Subscriptionをサーバーに送信
     * @param body any 送信要素(だいたい文字列)
     * @returns
     */
    private async doFetch(body: any, path: string): Promise<Response | false> {
        try {
            return await fetch(this.ServerOptions.url + '/' +  path, {
                method: 'POST',
                body: JSON.stringify(body),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        } catch (error) {
            console.error(error)
            return false
        }
    }
}





