import { ServerHelper } from './_helper/server.helper';
import * as SH from './_helper/subscription.helper';

export class PushAPIService {
    private static instance: PushAPIService;

    private MessageChannel: MessageChannel | any

    public static call(): PushAPIService {
        if (!PushAPIService.instance) {
            PushAPIService.instance = new PushAPIService()
        }
        return PushAPIService.instance
    }

    /**
     * サーバー公開鍵を変数に格納
     * @param key
     * @returns
     */
    public setServerKey(key: string): PushAPIService {
        SH.SubscriptionOptions.key = key
        return this
    }

    public setOptions(
        options: {
            url?: string,
            name?: string,
            mail?: string,
            challenge?: string
        }
    ): PushAPIService {
        console.log(options)
        SH.SubscriptionOptions.url = options.url ?? SH.SubscriptionOptions.url
        SH.SubscriptionOptions.name = options.name ?? SH.SubscriptionOptions.name
        SH.SubscriptionOptions.mail = options.mail ?? SH.SubscriptionOptions.mail
        SH.SubscriptionOptions.challenge = options.challenge ?? SH.SubscriptionOptions.challenge
        return this
    }

    /**
     * Subscription情報を返す
     * 無い場合は新規作成
     * @param toString
     * @returns
     */
    public async getSubscription(toString = false): Promise<PushSubscription> {
        if (
            !await SH.checkRegisterServiceWorker()
            && !await SH.registerServiceWorker()
        ) {
            await SH.registerServiceWorker()
            await SH.registerSubscription()
        }

        return toString ? JSON.stringify(SH.Subscription) : SH.Subscription
    }

    public async getPublicKey(): Promise<Response | false> {
        if (
            !await SH.checkRegisterServiceWorker()
            && !await SH.registerServiceWorker()
        ) {
            return false
        }
        return await ServerHelper.call()
                        .setOptions({url: SH.SubscriptionOptions.url})
                        .getPublicKey()
    }

    /**
     * ユーザーにServiceWorkerの登録許可を申請
     * @param path
     * @returns
     */
    public async setServiceWorker(): Promise<boolean> {
        // ブラウザがサービスワーカーをサポートしているかチェック
        if (!SH.supportServiceWorker) return false

        // ブラウザにサービスを登録
        await SH.registerServiceWorker()

        if (Notification.permission === 'denied') {
            console.log(`[Notification permission] 購読がブロックされています。`)
            return false
        } else {
            console.log(`[Notification permission] 購読が許可されました。`)
            return true
        }
    }

    /**
     * PushSubscription を取得する
     * [通知]設定がデフォルトの場合は、ここで「許可 or ブロック」を聞いてくる
     * ブロックが選択された場合、この時点で例外が発生する
     * @returns
     */
    public async subscribe(): Promise<boolean> {
        if (!SH.ServiceWorker) return false
        if (SH.SubscriptionOptions.key === '') return false
        
        if (!await SH.registerSubscription()) return false

        try {
            // PushSubscription をサーバーに送る
            ServerHelper.call()
                .setOptions(SH.SubscriptionOptions)
                .setSubscription(SH.Subscription)
                .subscribe()

            console.log(`[Server register SUCCESS]`)
        } catch (err: any) {
            console.error(`[Server Register ERROR] ${err.message}`)
            return false
        }
        return true
    }

    /**
     * Subscribe登録解除
     * @returns Promise<boolena>
     */
    public async unSubscrive(job: 'name' | 'all' = 'name'): Promise<boolean> {

        try {
            ServerHelper.call()
                    .setOptions(SH.SubscriptionOptions)
                    .setSubscription(SH.Subscription)
            // PushSubscription登録解除をサーバーに送る
            if (job === 'name') await ServerHelper.call().unSubscribe()
            // 全てのSubscribe解除をサーバーに送る
            if (job === 'all') await ServerHelper.call().allUnSubscribe()
            
            console.log(`[Server Subscription UnRegister SUCCESS]`)
        } catch (err: any) {
            console.error(`[Server Subscription UnRegister ERROR] ${err.message}`)
            return false
        }

        // ブラウザのsubscriptionを解除
        return (await SH.unRegisterSubscription()) ? true : false
    }

    public async pushToUser(): Promise<boolean> {
        try {
            // PushSubscription をサーバーに送る
            await ServerHelper.call()
                .setOptions(SH.SubscriptionOptions)
                .setSubscription(SH.Subscription)
                .pushNotificationToUser()

            console.log(`[Server Push Notification SUCCESS]`)
        } catch (err: any) {
            console.error(`[Server Push Notification ERROR] ${err.message}`)
            return false
        }
        return true
    }

    public async pushToAll(): Promise<boolean> {
        try {
            // PushSubscription をサーバーに送る
            ServerHelper.call()
                .setOptions(SH.SubscriptionOptions)
                .setSubscription(SH.Subscription)
                .pushNotificationToUser()

            console.log(`[Server Push Notification SUCCESS]`)
        } catch (err: any) {
            console.error(`[Server Push Notification ERROR] ${err.message}`)
            return false
        }
        return true
    }

    public async checker(
        target: 'support' | 'serviceworker' | 'subscription'
    ): Promise<boolean> {
        if (target === 'support')
            return (! await SH.supportServiceWorker()) ? false : true
        if (target === 'serviceworker')
            return (! await SH.checkRegisterServiceWorker()) ? false : true
        if (target === 'subscription')
            return (! await SH.checkRegisterSubscription()) ? false : true
        return false
    }

    /**
     * serviceworkerと接続
     * @returns Promise<any>
     */
    public connectServiceWorker(callback: (message: any) => any): boolean {
        if (navigator.serviceWorker.controller === null) {
            console.error('navigator.serviceWorker.controller is null')
            return false
        }
        this.MessageChannel = new MessageChannel()
        this.MessageChannel.port1.onmessage = (event: any) => callback(event.data)
        return true
    }

    /**
     * servicewakerと通信
     * ブラウザ側から繋がないと相互通信は出来ない
     * @param message any 基本的に文字列
     * @returns Promise<any> service workerから戻りが合った場合だけ返る
     */
    public callServiceWorker(message: any): boolean {
        if (navigator.serviceWorker.controller === null) return false

        try {
            navigator.serviceWorker.controller.postMessage(message, [
                this.MessageChannel.port2
            ])
            return true
        }catch (err: any) {
            console.error(`[ServiceWorker PostMessage ERROR] ${err.message}`)
            return false
        }
    }
}
