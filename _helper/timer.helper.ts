

export class TimerHelper {
    private static instance: TimerHelper

    private Timer: number = 0
    private SubTimer: number = 0

    public static call(): TimerHelper {
        if (!TimerHelper.instance) {
            TimerHelper.instance = new TimerHelper()
        }
        return TimerHelper.instance
    }

    public start(): TimerHelper {
        this.reset()
        this.SubTimer = new Date().getTime()
        return this
    }

    public restart(): TimerHelper {
        this.SubTimer = this.SubTimer + (new Date().getTime() - this.SubTimer)
        return this
    }

    public pause(): TimerHelper {
        this.SubTimer = new Date().getTime() - this.SubTimer
        return this
    }

    public stop(): TimerHelper {
        this.Timer = new Date().getTime() - this.SubTimer
        return this
    }

    public get(): number {
        return this.Timer / 1000
    }

    public getLap(): number {
        return (new Date().getTime() - this.SubTimer) / 1000
    }

    private reset(): void {
        this.Timer = 0
        this.SubTimer = 0
    }
}

export const sleep = (sec: number): Promise<void> => {
    return new Promise(resolve => setTimeout(resolve, sec * 1000))
}