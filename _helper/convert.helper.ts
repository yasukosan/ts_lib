/**
 * Uint8Arrayをbase64URLに変換
 * @param uint8Array Uint8Array
 * @returns string
 */
export const uint8ArrayToUrlBase64 = (
    uint8Array: Uint8Array,
    extension: string = 'webm'
): string => {
    let binary = '';
    const chunkSize = 0x8000; // 32KB チャンクサイズ

    for (let i = 0; i < uint8Array.length; i += chunkSize) {
        binary += String.fromCharCode.apply(null, [...uint8Array.subarray(i, i + chunkSize)])
    }

    const base64String = btoa(binary);
    const dataURIHeader = extensionToDataURIHeader(extension)
    return dataURIHeader + base64String;
}


/**
 * base64URLをUint8Arrayに変換
 * @param base64String string base64文字列
 * @returns Uint8Array
 */
export const urlBase64ToUint8Array = (base64String: string): Uint8Array => {
    const padding = '='.repeat((4 - (base64String.length % 4)) % 4),
        base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/'),
        rawData = window.atob(base64),
        outputArray = new Uint8Array(rawData.length)
    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i)
    }
    return outputArray
}

/**
 * base64urlをArrayBufferに変換
 */
export const urlbase64ToArrayBuffer = (
    base64: string
): ArrayBuffer => {
    const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"

    // Use a lookup table to find the index.
    const lookup = new Uint8Array(256)

    for (let i = 0; i < chars.length; i++) {
        lookup[chars.charCodeAt(i)] = i
    }
    const bufferLength = base64.length * 0.75, len = base64.length
    let p = 0
    let encoded1: number, encoded2: number, encoded3: number, encoded4: number

    const arraybuffer = new ArrayBuffer(bufferLength),
    bytes = new Uint8Array(arraybuffer);

    for (let i = 0; i < len; i+=4) {
        encoded1 = lookup[base64.charCodeAt(i)]
        encoded2 = lookup[base64.charCodeAt(i+1)]
        encoded3 = lookup[base64.charCodeAt(i+2)]
        encoded4 = lookup[base64.charCodeAt(i+3)]

        bytes[p++] = (encoded1 << 2) | (encoded2 >> 4)
        bytes[p++] = ((encoded2 & 15) << 4) | (encoded3 >> 2)
        bytes[p++] = ((encoded3 & 3) << 6) | (encoded4 & 63)
    }

    return arraybuffer;
}

export const arrayBufferToBase64url = (
    arrayBuffer: ArrayBuffer
): string => {
    const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"
    const bytes = new Uint8Array(arrayBuffer)

    const len = bytes.length
    let base64 = ""

    for (let i = 0; i < len; i+=3) {
        base64 += chars[bytes[i] >> 2]
        base64 += chars[((bytes[i] & 3) << 4) | (bytes[i + 1] >> 4)]
        base64 += chars[((bytes[i + 1] & 15) << 2) | (bytes[i + 2] >> 6)]
        base64 += chars[bytes[i + 2] & 63]
    }

    if ((len % 3) === 2) {
        base64 = base64.substring(0, base64.length - 1)
    } else if (len % 3 === 1) {
        base64 = base64.substring(0, base64.length - 2)
    }

    return base64
}


/**
 * Fileをbase64に変換
 * @param file File
 * @returns 
 */
export const fileToBase64 = async(
    file: File
): Promise<string> => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => resolve(reader.result as string)
        reader.onerror = (error) => reject(error)
    })
}

/**
 * Blobをbase64に変換
 * @param blob Blob
 * @returns 
 */
export const blobToBase64 = async (
    blob: Blob
): Promise<string> => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsDataURL(blob)
        reader.onloadend = () => {
            resolve(reader.result as string)
        }
        reader.onerror = (e) => {
            reject(e)
        }
    })
}

/**
 * BlobをFileに変換
 * @param blob Blob
 * @param fileName string
 */
export const blobToFile = (
    blob: Blob,
    fileName: string = 'tmp'
): File => {
    return new File([blob], fileName)
}

/**
 * BlobをUint8Arrayに変換
 * @param blob Blob
 * @returns 
 */
export const blobToUint8Array = async (
    blob: Blob
): Promise<Uint8Array> => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.onload = () => {
            if (reader.result) {
                // console.log('Blob to Uint8Array', new Uint8Array(reader.result as ArrayBuffer)) 
                resolve(new Uint8Array(reader.result as ArrayBuffer))
            } else {
                reject(new Error("Failed to convert Blob to Uint8Array"))
            }
        }
        reader.onerror = reject
        reader.readAsArrayBuffer(blob)
    })
    //return new Uint8Array(await blob.arrayBuffer())
}


/**
 * Base64をUint8Arrayに変換
 * @param base64 string
 * @returns 
 */
export const base64ToUint8Array = (
    base64: string
): Uint8Array => {
    const binaryString = atob(base64)
    const len = binaryString.length
    const bytes = new Uint8Array(len)
    for (let i = 0; i < len; i++) {
        bytes[i] = binaryString.charCodeAt(i)
    }
    return bytes
}

/**
 * Base64をBlobに変換
 * @param base64 string
 * @param type string
 * @returns 
 */
export const base64ToBlob = (
    base64: string,
    type: string = 'video/webm'
): Blob => {
    const buffer = base64ToUint8Array(base64)
    return new Blob([buffer.buffer], { type: type })
}

/**
 * Base64をBufferに変換
 * @param base64 
 * @returns 
 */
export const base64ToBuffer = (base64: string): ArrayBuffer => {
    const binary_string = window.atob(base64)
    const len = binary_string.length
    const bytes = new Uint8Array(len)
    for (let i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i)
    }
    return bytes.buffer
}


export const bufferToBase64 = (buffer: ArrayBuffer): string => {
    let binary = ''
    const bytes = new Uint8Array(buffer)
    const len = bytes.byteLength
    for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i])
    }
    return window.btoa(binary)
}

export const bufferToUint8Array = (buffer: ArrayBuffer): Uint8Array => {
    return new Uint8Array(buffer)
}

export const extensionToDataURIHeader = (extension: string): string => {
    switch (extension) {
        case 'png': return 'data:image/png;base64,'
        case 'jpg': return 'data:image/jpeg;base64,'
        case 'jpeg': return 'data:image/jpeg;base64,'
        case 'gif': return 'data:image/gif;base64,'
        case 'bmp': return 'data:image/bmp;base64,'
        case 'webp': return 'data:image/webp;base64,'
        case 'svg': return 'data:image/svg+xml;base64,'
            
                // 映像データ
        case 'mp4': return 'data:video/mp4;base64,'
        case 'webm': return 'data:video/webm;base64,'
        case 'ogg': return 'data:video/ogg;base64,'
        case 'avi': return 'data:video/x-msvideo;base64,'
            
                // 音声データ
        case 'mp3': return 'data:audio/mpeg;base64,'
        case 'wav': return 'data:audio/wav;base64,'
        case 'm4a': return 'data:audio/mp4;base64,'
            
                // オフィスファイル
        case 'pdf': return 'data:application/pdf;base64,'
        case 'doc': return 'data:application/msword;base64,'
        case 'docx': return 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,'
        case 'xls': return 'data:application/vnd.ms-excel;base64,'
        case 'xlsx': return 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,'
        case 'ppt': return 'data:application/vnd.ms-powerpoint;base64,'
        case 'pptx': return 'data:application/vnd.openxmlformats-officedocument.presentationml.presentation;base64,'
            
                // テキストファイル
        case 'txt': return 'data:text/plain;base64,'
        case 'html': return 'data:text/html;base64,'
        case 'css': return 'data:text/css;base64,'
        case 'js': return 'data:text/javascript;base64,'
        case 'json': return 'data:application/json;base64,'
        case 'xml': return 'data:application/xml;base64,'
        default: return ''
    }
}