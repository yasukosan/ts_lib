
/**
 * テキストデータからJSONオブジェクトに変換する
 * 
 */
export const parseJson = (text: string): any => {
    try {
        return JSON.parse(text);
    } catch (e) {
        console.error(e);
        return null;
    }
}

/**
 * JSONオブジェクトを改行付き文字列に変換する
 */
export const stringifyJson = (json: any): string => {
    return JSON.stringify(json, null, 4)
}

/**
 * テキストデータがJSONかどうかを判定する
 */
export const isJson = (text: string): boolean => {
    try {
        JSON.parse(text)
        return true
    } catch (e) {
        return false
    }
}