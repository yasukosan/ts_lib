export class DataTypeHelper {
    private static instance: DataTypeHelper;

    private file: any;
    private result = '';

    public static call(): DataTypeHelper {
        if (!DataTypeHelper.instance) {
            DataTypeHelper.instance = new DataTypeHelper();
        }
        return DataTypeHelper.instance;
    }

    /**
     * 判定対象のデータをセット
     * @param file
     * @returns
     */
    public setFile(file: any): DataTypeHelper {
        this.file = file;
        return this;
    }

    /**
     * 判定結果を受け取る
     * @returns string
     */
    public getResult(): string {
        return this.result;
    }

    /**
     * 判定データの型を調べる
     * @returns
     */
    public check(): DataTypeHelper {
        const to = Object.prototype.toString;
        const r = to.call(this.file).replace('[object ', '').replace(']', '');
        console.log(r);
        this.result = r;
        return this;
    }

    /**
     * 判定データの型と一致しているか調べる
     * @param type
     * @returns
     */
    public checkType(type: string): boolean {
        this.check();
        return this.result === type ? true : false;
    }
}
