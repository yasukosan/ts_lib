export type Extentions =
    | 'json'
    | 'gif'
    | 'jpeg'
    | 'jpg'
    | 'png'
    | 'svg'
    | 'pdf'
    | 'txt'
    | 'csv'
    | 'doc'
    | 'docx'
    | 'xls'
    | 'xlsx'
    | 'xml'
    | 'html'
    | 'css'
    | 'scss'
    | 'js'
    | 'ppt'
    | 'pptx'
    | 'zip'
    | 'msg'
    | 'ipynb'
    | 'md'

export class ExportHelper {
    private static instance: ExportHelper

    private exportData: string | ArrayBuffer | Uint8Array = ''
    private extention = 'jpeg'
    // private encode = 'utf-8'

    #multibyte = false

    public static call(): ExportHelper {
        if (!ExportHelper.instance) {
            ExportHelper.instance = new ExportHelper()
        }
        return ExportHelper.instance
    }

    /**
     * 出力データを設定
     * @param data string | ArrayBuffer | Uint8Array
     * @returns ExportHelper
     */
    public setData(data: string | ArrayBuffer | Uint8Array): ExportHelper {
        if (typeof data === 'string') this.#multibyte = false
        else this.#multibyte = true

        this.unko(this.#multibyte)
        
        this.exportData = data

        return this
    }

    /**
     * 出力する拡張子を設定
     * 出力時のヘッダー上方となるため通常は設定したデータの拡張子を渡す
     *
     * ※設定可能な拡張子
     * 'json' 'gif ' 'jpeg' 'jpg ' 'png ' 'svg ' 'pdf '
     * 'txt ' 'csv ' 'doc ' 'docx' 'xls ' 'xlsx' 'xml '
     * @param extention Extentions 拡張子
     * @returns ExportHelper
     */
    public setExtension(extention: Extentions): ExportHelper {
        this.extention = extention in this.mimeList ? extention : 'txt'
        return this
    }

    /**
     * downloadイベントを発生させる
     * @param name string ファイル名（デフォルト：export）
     */
    public download(name = 'export'): void {
        const blob = new Blob([this.exportData], {
            type: this.mimeList[this.extention]
        })
        const url = window.URL.createObjectURL(blob)

        const _a = document.createElement('a')
        _a.setAttribute('href', url)
        _a.setAttribute('download', name + '.' + this.extention)
        _a.click()
        _a.remove()
    }

    /**
     * base64文字列を返す
     * @returns string base64
     */
    public base64(): string {
        const header = 'data:' + this.mimeList[this.extention] + ';base64,'
        if (typeof this.exportData === 'string') {
            return header + btoa(this.exportData)
        }

        const uint8Array = (this.exportData instanceof ArrayBuffer) 
                            ? new Uint8Array(this.exportData)
                            : this.exportData
        const binaryString = Array.from(uint8Array)
                                    .map(byte => String.fromCharCode(byte))
                                    .join('')
        return header + btoa(binaryString)

    }

    private mimeList: {[key: string]: string} = {
        "json": "application/json",
        "gif": "image/gif",
        "jpeg": "image/jpeg",
        "jpg": "image/jpeg",
        "png": "image/png",
        "svg": "image/svg+xml",
        "pdf": "application/pdf",
        "txt": "text/plain",
        "csv": "text/csv",
        "doc": "application/msword",
        "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "xls": "application/vnd.ms-excel",
        "xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "xml": "application/xml",
        "html": "text/html",
        "css": "text/css",
        "scss": "text/x-scss",
        "js": "application/javascript",
        "ppt": "application/vnd.ms-powerpoint",
        "pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "zip": "application/zip",
        "msg": "application/vnd.ms-outlook",
        "ipynb": "application/x-ipynb+json",
        "md": "text/markdown"
    }

    private unko(m: boolean): void {
        this.#multibyte = m
    }
}
