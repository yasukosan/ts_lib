export class RecruiteLayoutService {
    private FName = 'test';
    private Name = 'test';
    private image = '';

    constructor() {}
    setName(name: string): void {
        this.Name = name;
    }
    setFName(name: string): void {
        this.FName = name;
    }
    setImage(url: string): void {
        this.image = url;
    }
    makeRecruiteLayout(content: string): any {
        const docDefinition = {
            content: [
                {
                    image: content,
                    width: 595,
                    height: 842,
                    margin: [0, 0, 0, 0]
                },
                {
                    width: 100,
                    image: this.image,
                    margin: [430, 0, 0, 5]
                }
            ],
            defaultStyle: {
                font: 'ipag'
            }
        };
        return docDefinition;
    }
    makePdfLayout(): any {
        const docDefinition = {
            content: [
                {
                    columns: [
                        {
                            width: 380,
                            margin: [0, 45, 0, 0],
                            table: {
                                widths: [260, 90, 10],
                                body: [
                                    [
                                        {
                                            text: 'ふりがな',
                                            style: 'cell_small',
                                            colSpan: 2
                                        },
                                        ''
                                    ],
                                    [
                                        {
                                            text: '氏名\n　\n' + this.FName,
                                            style: 'cell_large',
                                            colSpan: 2
                                        },
                                        ''
                                    ],
                                    ['生年月日　\n', '性別\n']
                                ]
                            }
                        },
                        {
                            width: 100,
                            image: this.image,
                            margin: [50, 0, 0, 5]
                        }
                    ]
                },
                {
                    table: {
                        widths: [380, 100],
                        body: [
                            ['ふりがな', { text: '電話番号\n', rowSpan: 2 }],
                            [
                                {
                                    text: '現住所\n　\n　\n' + this.Name,
                                    rowSpan: 3
                                },
                                ''
                            ],
                            ['', { text: 'メールアドレス\n', rowSpan: 2 }],
                            ['', ''],
                            ['ふりがな', { text: '電話番号\n', rowSpan: 2 }],
                            [{ text: '連絡先\n　\n　\n', rowSpan: 3 }, ''],
                            ['', { text: 'メールアドレス\n', rowSpan: 2 }],
                            ['', '']
                        ]
                    },
                    margin: [0, 10, 0, 0]
                },
                {
                    table: {
                        widths: [40, 20, 420],
                        body: [
                            ['年', '月', '学歴・職歴'],
                            ['　\n\n', '', ''],
                            ['　\n\n', '', ''],
                            ['　\n\n', '', ''],
                            ['　\n\n', '', ''],
                            ['　\n\n', '', ''],
                            ['　\n\n', '', ''],
                            ['　\n\n', '', ''],
                            ['　\n\n', '', ''],
                            ['　\n\n', '', ''],
                            ['　\n\n', '', ''],
                            ['　\n\n', '', '']
                        ]
                    },
                    margin: [0, 10, 0, 0]
                }
            ],
            styles: {
                cell_small: {
                    fontSize: 9
                },
                cell_large: {
                    fontSize: 10
                },
                voice: {
                    fontSize: 24
                },
                right: {
                    alignment: 'right'
                }
            },
            defaultStyle: {
                font: 'ipag'
            }
        };
        return docDefinition;
    }
    convObjectToArray(content: any[]): any[] {
        const convData: any[] = [];
        const returnData: any[] = [];
        for (const key in content) {
            if (content.hasOwnProperty(key)) {
                const element = content[key];
                convData.push(
                    Object.keys(element).map(function (key) {
                        return element[key];
                    })
                );
                returnData.push(convData);
            }
        }
        return convData;
    }
}
