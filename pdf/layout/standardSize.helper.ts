export class StandardSize {
    private static instance: StandardSize;

    public static call(): StandardSize {
        if (!StandardSize.instance) {
            StandardSize.instance = new StandardSize();
        }
        return StandardSize.instance;
    }

    public check(name: string): boolean {
        return name in this.size ? true : false;
    }

    public getSize(name: Paper = 'A4'): any {
        if (name in this.size) {
            return this.size[name];
        }
        return this.size['A4'];
    }

    private size: PaperSize = {
        A0: [2383.94, 3370.39],
        A1: [1683.78, 2383.94],
        A2: [1190.55, 1683.78],
        A3: [841.89, 1190.55],
        A4: [595.28, 841.89],
        A5: [419.53, 595.28],
        A6: [297.64, 419.53],
        A7: [209.76, 297.64],
        A8: [147.4, 209.76],
        A9: [104.88, 147.4],
        A10: [73.7, 104.88],
        B0: [2834.65, 4008.19],
        B1: [2004.09, 2834.65],
        B2: [1417.32, 2004.09],
        B3: [1000.63, 1417.32],
        B4: [708.66, 1000.63],
        B5: [498.9, 708.66],
        B6: [354.33, 498.9],
        B7: [249.45, 354.33],
        B8: [175.75, 249.45],
        B9: [124.72, 175.75],
        B10: [87.87, 124.72],
        EXECUTIVE: [521.86, 756.0],
        FOLIO: [612.0, 936.0],
        LEGAL: [612.0, 1008.0],
        LETTER: [612.0, 792.0],
        TABLOID: [792.0, 1224.0]
    };
}

export type Paper =
    | 'A0'
    | 'A1'
    | 'A2'
    | 'A3'
    | 'A4'
    | 'A5'
    | 'A6'
    | 'A7'
    | 'A8'
    | 'A9'
    | 'A10'
    | 'B0'
    | 'B1'
    | 'B2'
    | 'B3'
    | 'B4'
    | 'B5'
    | 'B6'
    | 'B7'
    | 'B8'
    | 'B9'
    | 'B10'
    | 'EXECUTIVE'
    | 'FOLIO'
    | 'LEGAL'
    | 'LETTER'
    | 'TABLOID';
type PaperSize = {
    [key in Paper]: number[];
};
