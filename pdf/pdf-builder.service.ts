import pdfMake from 'pdfmake/build/pdfmake';

export class PDFBuilderService {
    constructor() {
        // フォント設定を行う
        pdfMake.fonts = {
            ipag: {
                normal: 'notosans_medium.ttf'
            }
        };
        //pdfMake.vfs = NOTOSANS_PDF_FONTS;
    }

    /**
     * 本家createPdfのラッパー、デフォルトフォントを設定
     * @param docDefinition any
     */
    createPdf(docDefinition: any) {
        docDefinition.defaultStyle = docDefinition.defaultStyle || {};
        docDefinition.defaultStyle.font = 'ipag';

        // noinspection TypeScriptUnresolvedFunction
        return pdfMake.createPdf(docDefinition);
    }

    /**
     * レイアウト情報からPDF作成
     * ダウンロードイベントは発生しない
     * @param docDefinition any レイアウト情報
     * @return PDFデータ返却
     */
    testPdfMake(docDefinition: any): any {
        docDefinition.defaultStyle = docDefinition.defaultStyle || {};
        // docDefinition.defaultStyle.font = 'msgothic';
        return pdfMake.createPdf(docDefinition);
    }

    /**
     * レイアウト情報からPDF作成
     * ダウンロードイベントを発生させる
     * @param docDefinition any レイアウト情報
     */
    pdfMakeForIE(docDefinition: any, name = 'outputpdf') {
        console.log(docDefinition);
        docDefinition.defaultStyle = docDefinition.defaultStyle || {};
        // docDefinition.defaultStyle.font = 'msgothic';
        pdfMake.createPdf(docDefinition).download(name + '.pdf');
    }

    // URLの取得にもラップをかける
    public async createPdfUrl(docDefinition: any): Promise<any> {
        const pdfDocGenerator = await this.createPdf(docDefinition);
        return await pdfDocGenerator.getDataUrl((url) => {
            return url;
        });
    }
}
