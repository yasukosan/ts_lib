export class TacksheetMakerService {
    /**
     * 印刷サイズ
     * A4を350dpiで出力する場合
     * 横2894px、縦4093px必要
     * 倍率は13.78095倍
     */
    private sheetSize: any = {
        /* width: 794,
        height: 1123 */
        width: 210,
        height: 297
    };
    /**
     * ラベルに関するサイズ、余白情報
     */
    private sheetSpec: any = {
        marginTop: 0, // ページ余白上
        marginLeft: 0, // ページ余白左
        cellWidth: 0, // セルの横幅
        cellHeight: 0, // セルの高さ
        cellMarginTop: 0, // セルの余白上
        cellMarginLeft: 0 // セルの余白左
    };
    /**
     * 文字デザイン
     */
    private textDesine: any = {
        fontSize: 30,
        fontDesine: 'MS PMincho',
        fontWeight: 'normal',
        textMargin: 2
    };
    /**
     * 印刷オプション
     */
    private printOption: any = {
        cellCount: 1,
        startPosition: 0,
        printCount: 1
    };

    private textLayout: any;

    private resulution = 1;

    private contents: any[] = [];
    private imageContents: any[] = [];

    private sheetImage = '';
    private prevewImage = '';

    /**
     * セルサイズの設定
     * @param spec object {marginTop, marginLeft, cellWidth,
     *                       cellHeight, cellMarginTop, cellMarginLeft}
     * @returns TacksheetMakerService
     */
    public setSheetSpec(spec: object): TacksheetMakerService {
        this.sheetSpec = Object.assign({
            ...this.sheetSpec,
            ...spec
        });
        return this;
    }
    /**
     * テキストフォーマット
     * @param desine object CSSスタイル設定
     * @returns
     */
    public setTextDesine(desine: object): TacksheetMakerService {
        this.textDesine = Object.assign({
            ...this.textDesine,
            ...desine
        });
        return this;
    }
    /**
     * 印刷設定
     * @param option object {cellCount, startPostion, printCount}
     * @returns
     *
     * cellCount: 同じ内容を何セル印刷するか
     * startPosition: 印刷開始セル
     * printCount： 何ページ印刷するか
     */
    public setPrintOption(option: any): TacksheetMakerService {
        this.printOption = Object.assign({
            ...this.printOption,
            ...option
        });
        return this;
    }
    /**
     * 出力文字列毎の設定（配置位置、フォントサイズ）
     * @param layout object
     * @returns
     */
    public setTextLayout(layout: any): TacksheetMakerService {
        this.textLayout = layout;
        return this;
    }
    /**
     * 出力文字列
     * @param contents object
     * @returns
     */
    public setContents(contents: any): TacksheetMakerService {
        this.contents = contents;
        return this;
    }
    public setImageContents(contents: any): TacksheetMakerService {
        this.imageContents = contents;
        return this;
    }
    public setResulution(magnification: any): TacksheetMakerService {
        this.resulution = magnification;
        return this;
    }

    public sheetMaker(): void {
        this.doEnlargement();

        const oc: HTMLCanvasElement = document.createElement('canvas');
        const ctx: CanvasRenderingContext2D | null = oc.getContext('2d');
        if (ctx === null) {
            return;
        }
        const sd = this.sheetSpec;
        const po = this.printOption;
        const rectPoints: any[] = [];

        oc.setAttribute('width', this.sheetSize.width.toString());
        oc.setAttribute('height', this.sheetSize.height.toString());
        ctx.fillStyle = 'rgb(255, 255, 255)';
        ctx.fillRect(0, 0, this.sheetSize.width, this.sheetSize.height);

        // console.log(ctx.font);
        ctx.textAlign = 'start';
        ctx.textBaseline = 'bottom';
        ctx.fillStyle = 'rgb(0, 0, 0)';

        let textCount = 0;
        let widthCount = 0;
        let heightCount = 0;
        let printCount = 1;

        for (let i = 0; i < po.cellCount; i++) {
            let X =
                sd.cellWidth * widthCount +
                sd.cellMarginLeft * widthCount +
                sd.marginLeft;
            if (X + sd.cellWidth > this.sheetSize.width) {
                widthCount = 0;
                heightCount++;
                X =
                    sd.cellWidth * widthCount +
                    sd.cellMarginLeft * widthCount +
                    sd.marginLeft;
            }
            let Y =
                sd.cellHeight * heightCount +
                sd.cellMarginTop * heightCount +
                sd.marginTop;

            widthCount++;
            rectPoints[i] = [X, Y, sd.cellWidth, sd.cellHeight];

            if (i >= po.startPosition && printCount <= po.printCount) {
                let lineCount = 0;
                for (const key in this.contents[textCount]) {
                    if (this.contents[textCount].hasOwnProperty(key)) {
                        console.log(this.contents[textCount][key]);
                        if (this.textLayout[lineCount] !== undefined) {
                            const realSize: any = this.getRealSize(
                                this.textLayout[lineCount]
                            );
                            ctx.font =
                                realSize.font +
                                'px "' +
                                this.textDesine.fontDesine +
                                '"';
                            const _Y = Y + realSize.height;
                            const _X = X + realSize.width;
                            ctx.fillText(this.contents[textCount][key], _X, _Y);
                        } else if (
                            this.imageContents[lineCount] !== undefined
                        ) {
                            console.log(this, this.imageContents[lineCount]);
                            ctx.drawImage(
                                this.imageContents[lineCount],
                                X,
                                Y,
                                this.sheetSize.width,
                                this.sheetSize.height
                            );
                        } else {
                            ctx.font =
                                this.textDesine.fontSize +
                                'px "' +
                                this.textDesine.fontDesine +
                                '"';
                            Y =
                                Y +
                                this.textDesine.fontSize +
                                this.textDesine.textMargin;
                            ctx.fillText(this.contents[textCount][key], X, Y);
                        }
                        lineCount++;
                    }
                }
                printCount++;
                textCount++;
            }
        }
        this.sheetImage = oc.toDataURL('image/jpg');

        for (const key in rectPoints) {
            if (rectPoints.hasOwnProperty(key)) {
                const rp = rectPoints[key];
                this.setRoundRect(ctx, rp[0], rp[1], rp[2], rp[3], 50);
                ctx.stroke();
            }
        }
        this.prevewImage = oc.toDataURL('image/jpg');
    }

    public async sheetMakerImage(): Promise<void> {
        this.doEnlargement();

        const oc: HTMLCanvasElement = document.createElement('canvas');
        const ctx: CanvasRenderingContext2D | null = oc.getContext('2d');
        if (ctx === null) {
            return;
        }
        const sd = this.sheetSpec;
        const po = this.printOption;
        const rectPoints: any[] = [];

        oc.setAttribute('width', this.sheetSize.width.toString());
        oc.setAttribute('height', this.sheetSize.height.toString());
        ctx.fillStyle = 'rgb(255, 255, 255)';
        ctx.fillRect(0, 0, this.sheetSize.width, this.sheetSize.height);

        let textCount = 0;
        let widthCount = 0;
        let heightCount = 0;
        let printCount = 1;

        for (let i = 0; i < po.cellCount; i++) {
            let X =
                sd.cellWidth * widthCount +
                sd.cellMarginLeft * widthCount +
                sd.marginLeft;
            if (X + sd.cellWidth > this.sheetSize.width) {
                widthCount = 0;
                heightCount++;
                X =
                    sd.cellWidth * widthCount +
                    sd.cellMarginLeft * widthCount +
                    sd.marginLeft;
            }
            const Y =
                sd.cellHeight * heightCount +
                sd.cellMarginTop * heightCount +
                sd.marginTop;

            widthCount++;
            rectPoints[i] = [X, Y, sd.cellWidth, sd.cellHeight];

            if (i >= po.startPosition && printCount <= po.printCount) {
                const im = await this.loadImage(this.imageContents[textCount]);
                ctx.drawImage(im, X, Y, 1246, 756);
                printCount++;
                textCount++;
            }
        }
        this.sheetImage = oc.toDataURL('image/jpg');

        for (const key in rectPoints) {
            if (rectPoints.hasOwnProperty(key)) {
                const rp = rectPoints[key];
                this.setRoundRect(ctx, rp[0], rp[1], rp[2], rp[3], 50);
                ctx.stroke();
            }
        }
        this.prevewImage = oc.toDataURL('image/jpg');
    }

    /**
     * 印刷サイズを設定
     * @return array 横　縦　倍率
     * シートサイズに倍率をかけた実寸サイズを返す
     */
    public getRealSize(layout: any): object {
        const _layout = { width: 0, height: 0, font: 0 };
        const wResult = this.sheetSpec.cellWidth / (200 * this.resulution);
        const hResult = this.sheetSpec.cellHeight / (100 * this.resulution);
        const fResult = (wResult + hResult) / 2;
        console.log(wResult + '::' + hResult + '::' + fResult);
        _layout.width = Math.round(layout[0] * this.resulution * wResult);
        _layout.height = Math.round(layout[1] * this.resulution * hResult);
        _layout.font = Math.round(layout[2] * this.resulution * fResult);

        return _layout;
    }

    getSheetImage(): string {
        return this.sheetImage;
    }
    getPreviewImage(): string {
        return this.prevewImage;
    }

    initialization(): TacksheetMakerService {
        this.sheetImage = '';
        this.sheetSize = {
            width: 210,
            height: 297
        };
        this.sheetSpec = {
            marginTop: 0,
            marginLeft: 0,
            cellWidth: 0,
            cellHeight: 0,
            cellMarginTop: 0,
            cellMarginLeft: 0
        };

        this.textDesine = {
            fontSize: 30,
            fontDesine: 'MS PMincho',
            fontWeight: 'normal',
            textMargin: 2
        };

        this.printOption = {
            cellCount: 1,
            startPosition: 0,
            printCount: 1
        };
        this.resulution = 1;
        this.contents = [];
        return this;
    }
    private async loadImage(img: string): Promise<HTMLImageElement> {
        return new Promise((resolve, reject) => {
            const _img = new Image();
            _img.onload = (e: any) => resolve(_img);
            _img.onerror = (e: any) => reject(e);
            _img.src = img;
        });
    }
    /**
     * シートの枠を描画
     * @param ctx canvasコンテキスト
     * @param x ｘ座標
     * @param y Y座標
     * @param h 高さ（ポジション指定の幅になる）
     * @param w 幅（ポジション指定の高さになる）
     * @param r 円弧の半径
     */
    private setRoundRect(
        ctx: any,
        x: number,
        y: number,
        h: number,
        w: number,
        r: number
    ): void {
        ctx.beginPath();
        ctx.moveTo(x, y + r);
        ctx.arc(x + r, y + w - r, r, Math.PI, Math.PI / 2, 1);
        ctx.arc(x + h - r, y + w - r, r, Math.PI / 2, 0, 1);
        ctx.arc(x + h - r, y + r, r, 0, (Math.PI * 3) / 2, 1);
        ctx.arc(x + r, y + r, r, (Math.PI * 3) / 2, Math.PI, 1);
        ctx.closePath();
    }
    /**
     * 指定倍率の応じたシートサイズの拡大
     */
    private doEnlargement(): TacksheetMakerService {
        for (const key in this.sheetSpec) {
            if (this.sheetSpec.hasOwnProperty(key)) {
                this.sheetSpec[key] = this.sheetSpec[key] * this.resulution;
            }
        }
        this.sheetSize.width = this.sheetSize.width * this.resulution;
        this.sheetSize.height = this.sheetSize.height * this.resulution;
        this.textDesine.fontSize =
            this.textDesine.fontSize * (this.resulution / 2);
        this.textDesine.textMargin =
            this.textDesine.textMargin * this.resulution;

        return this;
    }
}
