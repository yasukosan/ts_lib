
# UZAYA Auth

---
## HOW TO USE
---

#### 中間ファイル作成

実際のプロジェクトから呼び出す中間ファイルを作成。
```javascript
import {
    verify as verifyAPI,
    signOut as signOutAPI,
    signIn as signInAPI,
    signInVerify as signInVerifyAPI,
    setOptions
} from '../../_lib/uzaya_auth/uzaya_auth.service'

setOptions({
    // UZAYA認証サーバーのエンドポイント
    endpoint_url: 'http://localhost:3000',
    // ローカルシステムのログインページ
    login_url: 'http://localhost:5173/login',
    // ローカルシステムのデフォルトトップページ
    home_url: 'http://localhost:5173/',
    // 認証プロセスで使用する一時トークンのキー名
    keyId: 'uzaya-auth',
    // 認証完了後のUZAYAトークンを保存するキー名
    keyToken: 'uza-token'
})

// 認証確認で呼び出す
export const verify = async () => await verifyAPI()
// サインアウトで呼び出す
export const signOut = async () => await signOutAPI()
// サインインで呼び出す（必ずログインページ）
export const signIn = async () => await signInAPI()
// サインイン確認で呼び出す（必ずログインページ）
export const signInVerify = async () => await signInVerifyAPI()
```

#### 呼び出し


コンポーネントの先頭に関数の呼び出しを宣言
```typescript
import { signInVerify } from '../../_domain/_global/auth'
.....
    useEffect(() => {
        signInVerify()
    }, [])

```

SignIn処理は、ボタンに紐づけるか
イベント呼び出し先の関数内で呼び出す
* フォームボタンに紐づけ
```typescript
    <form className="space-y-4 md:space-y-6" action="#">
        <button
            type="button"
            className="
                w-full h-full text-white hover:text-gray-300
                font-medium rounded-lg text-[2em] text-center
                ring-1 ring-primary-600
                bg-gray-700 hover:bg-gray-900
                focus:ring-4 focus:outline-none focus:ring-primary-300
                px-5 py-2.5
                dark:bg-primary-600 dark:hover:bg-primary-700
                dark:focus:ring-primary-800
                "
            onClick={() => {
                signIn()
            }}>Sign in</button>
    </form>
```

* 外部関数内で呼び出し

```typescript
const pushButton = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
): void => {
    e.preventDefault()
    signIn()
}
```
