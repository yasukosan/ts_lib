
type OptionsType = {
    keyId: string,
    keyToken: string
}
let Options: OptionsType = {
    keyId: 'uzaya-auth',
    keyToken: 'uzaya-token'
}


export const setOptions = (options: Partial<OptionsType>): void => {
    Options = Object.assign(Options, options)
}


/**
 * 
 * UUID
 * 
 */

/**
 * UUIDが存在するか確認
 * @param uuid string
 * @return Promise<boolean>
 */
export const checkUUID = async (
    uuid: string
): Promise<boolean> => {
    const result = await searchKey(Options.keyId)
    return (result === uuid)
}

/**
 * UUIDを保存
 * @param uuid string
 * @return Promise<boolean>
 */
export const setUUID = async (
    uuid: string
): Promise<boolean> => {
    await save(Options.keyId, uuid)
    return true
}

/**
 * UUIDを削除
 * @return Promise<boolean>
 */
export const removeUUID = async (): Promise<boolean> => {
    await deleteKey(Options.keyId)
    return true
}


/**
 * 
 * JWT Token
 * 
 */

/**
 * Tokenを保存
 * @param token string
 * @return Promise<boolean>
 */
export const setToken = async (
    token: string
): Promise<boolean> => {
    await save(Options.keyToken, token)
    return true
}

/**
 * Tokenを取得
 * @return Promise<string>
 */
export const getToken = async (): Promise<string> => {
    const token = await searchKey(Options.keyToken)
    return (token === false) ? '' : token
}

/**
 * Tokenを削除
 * @return Promise<boolean>
 */
export const removeToken = async (): Promise<boolean> => {
    await deleteKey(Options.keyToken)
    return true
}


/**
 * 
 * Redirect URL
 * 
 */

/**
 * リダイレクト先URLを保存
 * @param url string
 * @return Promise<boolean>
 */
export const setRedirectHome = async (url: string): Promise<boolean> => {
    const r = await save('redirect-home', url)
    return r
}

/**
 * リダイレクト先URLを取得
 * @return Promise<string>
 */
export const getRedirectHome = async (): Promise<string> => {
    const r = await load('redirect-home')
    return r
}

/**
 * リダイレクト先URLを削除
 * @return Promise<boolean>
 */
export const removeRedirectHome = async (): Promise<boolean> => {
    await deleteKey('redirect-home')
    return true
}


/**
 * LocalStrageにデータ保存
 * 文字列データをKye Value形式で保存
 *
 * @param key string キー
 * @param data string 文字列データ
 * @return Promise<boolean>
 */
const save = async (
    key: string,
    data: string
): Promise<boolean> => {
    try {
        await localStorage.setItem(key, data)
        return true
    } catch (error) {
        return false
    }
}

/**
 * データ取得（文字列データ）
 * @param key string キー
 * @return Promise<string>
 */
const  load = async (
    key: string
): Promise<string> => {
    try {
        const st = await localStorage.getItem(key)
        return st ? st : ''
    } catch (error) {
        return ''
    }
}


/**
 * キーが存在した場合はデータを返す、存在しない場合はFalseを返す
 * @param key
 * @return string | boolean
 */
const searchKey = async (key: string): Promise<string | false> => {
    for (let i = 0; i < localStorage.length; i++) {
        if (localStorage.key(i) === key) {
            return await load(key)
        }
    }
    return false
}

/**
 * データ削除
 * @param key string キー
 * @return LocalStrageService
 */
const deleteKey = async (key: string): Promise<void> => {
    try {
        await localStorage.removeItem(key)
        return this
    } catch (error) {
        return this
    }
}



