
/**
 * GETパラメーター「stoken」「rtoken」を取得
 */
export const getURLParam = (): {
    stoken: string,
    rtoken: string,
} => {
    const url = new URL(window.location.href)
    return {
        stoken: url.searchParams.get('stoken') || '',
        rtoken: url.searchParams.get('rtoken') || '',
    }
}

/**
 * 現在のURLを取得
 */
export const getURL = (): string => {
    return window.location.href || ''
}