
type OptionsType = {
    endpoint_url: string,
    login_url: string,
    home_url: string
}

export type ResponseType = {
    code: number,
    token?: string | null,
    message: string
}

const Options: OptionsType = {
    endpoint_url: 'http://localhost:3000',
    login_url: 'http://localhost:5173/login',
    home_url: 'http://localhost:5173/'
}

export const setOptions = (options: Partial<OptionsType>): void => {
    Object.assign(Options, options)
}

export const signInAPI = async (tmp: string): Promise<ResponseType> => {
    const response = await fetch(`${Options.endpoint_url}/api/auth`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'UZA-TOKEN': tmp,
        },
        body: JSON.stringify({
            tmp: tmp
        })
    })
    const data = await response.json()
    return {
        code: response.status,
        token: response.headers.get('Uza-Token'),
        message: data.message
    }
}

export const verifyAPI = async (
    token: string
): Promise<ResponseType> => {
    const response = await fetch(`${Options.endpoint_url}/api/verify`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'UZA-TOKEN': token,
        },
    })
    
    return {
        code: response.status,
        token: response.headers.get('Uza-Token'),
        message: response.status === 200 ? 'verify success' : 'verify failed',
    }
}

/**
 * ログアウトAPI
 * 
 * @returns レスポンス
 * */
export const signOutAPI = async (): Promise<ResponseType> => {
    const response = await fetch(`${Options.endpoint_url}/api/signout`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
    })
    const data = await response.json()
    return {
        code: response.status,
        message: data.message.message[0]
    }
}


/**
 * 外部認証画面にリダイレクト
 * 
 * @param token - トークン文字列
 * @returns なし
 */
export const redirect = (token: string): void => {
    let back = window.location.href
    while (back.endsWith('#') || back.endsWith('?') || back.endsWith('&')) {
        back = back.slice(0, -1)
    }
    // console.log('Redirect to :', `${url}/sso/${token}/?back=${back}`)
    window.location.href = `${Options.endpoint_url}/sso/${token}/?back=${back}`
}

/**
 * トップページor指定URLにリダイレクト
 * 
 * @param url - リダイレクト先URL (デフォルトはトップページ)
 * @returns なし
 */
export const redirectHome = (url: string = ''): void => {
    if (url === '') window.location.href = Options.home_url
    else window.location.href = url
}

/**
 * ログイン画面にリダイレクト
 * 
 * @returns なし
 */
export const redirectLogin = (): void => {
    window.location.href = Options.login_url
}



/**
 * UUIDを取得
 * 
 * @returns UUID文字列
 *  */
export const getUUID = (): string  =>{
    // UUIDv4の各フィールドに対応するランダムな値を生成します
    function getRandomHex(digits: number): string {
        let result = ''
        for (let i = 0; i < digits; i++) {
            const randomByte = Math.floor(Math.random() * 16).toString(16);
            result += randomByte
        }
        return result
    }

    // UUIDの形式が「8-4-4-4-12」の形式であるため、各部分を適切に生成します
    const part1 = getRandomHex(8)
    const part2 = getRandomHex(4)
    // バージョン (4) フィールド
    const part3 = '4' + getRandomHex(3)
     // `8`, `9`, `a`, or `b`
    const part4 = (8 + Math.floor(Math.random() * 4)).toString(16) + getRandomHex(3)
    const part5 = getRandomHex(12)

    // 生成した各部分を組み合わせてUUIDv4を構築します
    return `${part1}-${part2}-${part3}-${part4}-${part5}`
}