import {
    getToken, setToken,
    setUUID, checkUUID, removeUUID,
    setOptions as setStrageOptions,
    setRedirectHome,
    getRedirectHome,
    removeRedirectHome
} from './lib/strage'

import {
    redirectLogin, redirectHome, redirect,
    verifyAPI, signOutAPI, signInAPI,
    getUUID,
    setOptions as setCallOptions,
    ResponseType
} from './lib/call'

import {
    getURLParam,
    getURL
} from './lib/param'

export const setOptions = (options: {
    endpoint_url: string,
    home_url: string,
    login_url: string,
    keyId: string,
    keyToken: string
}): void => {
    setStrageOptions(options)
    setCallOptions(options)
}

export const verify = async (): Promise<boolean> => {
    const token: string = await getToken()
    if (token === '')
        await redirectLogin()

    const r: ResponseType = await verifyAPI(token)

    if (r.code === 401) {
        await setRedirectHome(getURL())
        await redirectLogin()
    }

    if (r.code === 200) {
        console.log(r.message)
        return true
    }
    console.error('Error :', r.message)
    return false
}

export const signOut = async (): Promise<boolean> => {
    const r = await signOutAPI()
    console.log(r)
    if (r.code === 200) {
        await removeUUID()
        await redirectLogin()
        return true
    }
    console.error('Error :', r.message)
    return false
}

export const signIn = async (): Promise<void> => {
    const uuid = getUUID()
    console.log('Set CallBack ID :', uuid)
    await setUUID(uuid)
    redirect(uuid)
}

export const signInVerify = async(): Promise<boolean> => {
    const t = getURLParam()
    console.log(t)
    if (t.stoken === '' || t.rtoken === '') {
        return false
    }
    const callbackid: boolean = await checkUUID(t.rtoken)

    if (callbackid === true) {
        await removeUUID()
    }

    if (callbackid) {
        const r: ResponseType = await signInAPI(t.stoken)

        if (r.code === 200) {
            await setToken(r.token as string)
            const r_home = await getRedirectHome()
            await removeRedirectHome()
            await redirectHome(r_home)
            return true
        }

        if (r.code === 401) {
            await redirectLogin()
        }
        return false
    }

    await redirectLogin()
    return true
}

