/**
 * Node.jsでSlack APIを使用して、チャンネルのメッセージ履歴を取得するサンプル
 * 参考: https://api.slack.com/methods/conversations.history
 * 
 * 
 */
'use server'
import { WebClient } from '@slack/web-api';
import { MessageElement } from '@slack/web-api/dist/response/ConversationsHistoryResponse';

import {
    getHistory, getSlackHistory
} from './_helper/slack.helper'

type SuccessResponse = {
    status: true
    message: MessageElement[]
}
type ErrorResponse = {
    status: false
    message: string
}

/**
 * Slack APIから取得したメッセージのHistoryデータの型
 */
export type SlackHistoryType = MessageElement

/**
 * 
 * 
 * @returns Promise<SuccessResponse | ErrorResponse>
 */
export const fetchChannelHistory = async(

): Promise<SuccessResponse | ErrorResponse> => {
        
    const slackHistory = await getHistory()

    // const slackHistory = getSlackHistory()

    if (slackHistory.status === false) {
        return {
            status: false,
            message: slackHistory.message
        }
    }
    const results: {
        user: string
        text: string
        ts: string
    }[] = []
    console.log('slackHistory:', slackHistory)  
    try {
        if (slackHistory.message.length === 0) {
            console.error('No messages found in the channel')
            return {
                status: false,
                message: 'No messages found in the channel'
            }
        }
        slackHistory.message.forEach(message => {
            results.push({
                user: (message.user) ? message.user : 'Unknown',
                text: (message.text) ? message.text : 'No text',
                ts: (message.ts) ? message.ts : 'No timestamp'
            })
        })
        return {
            status: true,
            message: results
        }

    } catch (error) {
        console.error('Error fetching channel history:', error)
        return {
            status: false,
            message: error as string
        }
    }
}
