'use server'
import { WebClient } from '@slack/web-api'
import { MessageElement } from '@slack/web-api/dist/response/ConversationsHistoryResponse'
import { sleep } from '../../_helper/timer.helper'

type SuccessResponse = {
    status: true
    message: MessageElement[]
}
type ErrorResponse = {
    status: false
    message: string
}

/**
 * Slack APIから取得したメッセージのHistoryデータの型
 */
export type SlackHistoryType = MessageElement

// Slack APIトークンを設定（環境変数から取得することを推奨）
let token = process.env.SLACK_TOKEN
// チャンネルIDを設定
let channelId = process.env.SLACK_CHANNEL

let loopFlag = true
let loopLimit = 3
let loopCount = 0

let SlackClient: WebClient
let slackHistory: SlackHistoryType[] = []
let ReturnResult: SuccessResponse | ErrorResponse

export const setToken = (t: string) => token = t
export const setChannelId = (id: string) => channelId = id

export const getSlackHistory = () => slackHistory

/**
 * 初期化
 */
export const init = () => {
    if (!SlackClient) SlackClient = new WebClient(token)
        
    slackHistory = []
}

/**
 * 履歴を取得
 * @param filter_thread boolean スレッドをフィルタリングするかどうか
 * @returns Promise<SuccessResponse | ErrorResponse>
 */
export const getHistory = async (
    filter_thread: boolean = true
): Promise<SuccessResponse | ErrorResponse> => {

    loopFlag = true

    await init()

    if (!token) {
        console.error('Slack API token is not provided');
        return {
            status: false,
            message: 'Slack API token is not provided'
        }
    }

    if (!channelId) {
        console.error('Slack channel ID is not provided')
        //process.exit(1);
        return {
            status: false,
            message: 'Slack channel ID is not provided'
        }
    }
    while (loopFlag) {
        try {
            const result = await SlackClient.conversations.history({
                channel: channelId,
                limit: 20
            }) as {
                ok: boolean
                messages?: SlackHistoryType[]
            }

            // 戻りステータスがOKでない場合、ループカウントを増やしてリトライ
            if (!result.ok || !result.messages) {
                console.error('Failed to fetch channel history')
                checkErrorResponse('Failed to fetch channel history')
                continue
            }

            // メッセージがある場合、スレッドをフィルタリングしてslackHistoryに格納
            slackHistory = (filter_thread)
                            ? filterThread(result.messages)
                            : result.messages
            checkSuccessResponse(slackHistory)
        } catch (error) {
            console.error('Error fetching channel history:', error)
            checkErrorResponse('Error fetching channel history')
        }

        // ループカウントが上限に達している場合、エラーを返す
        if (loopCount > loopLimit) {
            console.error('Failed to fetch channel history')
            checkErrorResponse('Failed to fetch channel history')
        }
    }

    return ReturnResult
}

/**
 * スレッドを取得
 */
export const fetchThread = async (
    historys: SlackHistoryType[]
): Promise<any> => {
    for (const history of historys) {
        if (history.thread_ts) {

        }
    }
}

/**
 * thread_tsがあるメッセージを除去
 * @param historys 
 * @returns SlackHistoryType[]
 */
const filterThread = (
    historys: SlackHistoryType[]
): SlackHistoryType[] => {
    return historys.filter(history => {
        return !history.thread_ts
    })
}

const checkErrorResponse = (message: string): void => {
    if (loopCount > loopLimit) {
        loopCount = 0
        loopFlag = false
        ReturnResult = {
            status: false,
            message: message
        }
        return
    }
    loopCount++
    return
}

const checkSuccessResponse = (message: MessageElement[]): void => {
    loopCount = 0
    loopFlag = false
    ReturnResult = {
        status: true,
        message: message
    }
    return
}