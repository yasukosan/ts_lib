// Imports
import { Pool, PoolClient, QueryResult } from 'pg'

export type DBPropertiesType = {
    user: string
    host: string
    database: string
    password: string
    port: number
}

export class PGService {

    private static instance: PGService

    public static call(): PGService {
        if (!this.instance) {
            this.instance = new PGService()
        }
        return this.instance
    }

    private pool: Pool | undefined
    private client: PoolClient | undefined

    /**
     * @constructor
     */
    constructor() {
        if (!process.env.DB_USER) {
            return
        }
        this.connect()
    }

    destroy() {
        this.disconnect()
    }

    /**
     * Connect to the database
     */
    async connect(
        properties: Partial<DBPropertiesType> = {}
    ): Promise<boolean> {
        if (
            this.pool !== undefined
            && this.client !== undefined
        ) return true

        await this.disconnect()

        try {
            this.pool = new Pool({
                user: (process.env.DB_USER) ? process.env.DB_USER : properties.user,
                host: (process.env.DB_HOST) ? process.env.DB_HOST : properties.host,
                database: (process.env.DB_NAME) ? process.env.DB_NAME : properties.database,
                password: (process.env.DB_PASSWORD) ? process.env.DB_PASSWORD : properties.password,
                port: (process.env.DB_PORT) ? parseInt(process.env.DB_PORT) : properties.port,
                max: 2,
            })
            this.client = await this.pool.connect()
            return true
        } catch (error) {
            console.error(error)
            return false
        }
    }

    /**
     * Disconnect from the database
     */
    async disconnect() {
        if (this.pool === undefined) return
        if (this.client === undefined) return
        await this.pool.end()
        this.pool = undefined
        await this.client.release()
        this.client = undefined
    }

    /**
     * Execute a query
     * @param {string} query - The query text
     * @param {Array} params - The query parameters
     * @returns {Promise<QueryResult>}
     */
    async exec(
        query: string,
        params: Array<any>
    ): Promise<QueryResult | false> {
        if (this.client === undefined) {
            await this.connect()
            if (this.client === undefined) return false
        }

        try {
            const res = await this.client.query(query, params)
            return res
        } catch (error) {
            console.log(error)
            await this.disconnect()
            return false
        }
    }

    /**
     * Get a single row from the database
     * @param {string} query - The query text
     * @param {Array} params - The query parameters
     * @returns {Promise<Object>}
     */
    async getOne(
        query: string,
        params: Array<any>
    ): Promise<QueryResult['rows'] | false> {
        const res = await this.exec(query, params)
        return (res === false) ? false : res.rows[0]            
    }

    /**
     * Get multiple rows from the database
     * @param {string} query - The query text
     * @param {Array} params - The query parameters
     * @returns {Promise<Array>}
     */
    async getMany(
        query: string,
        params: Array<any>
    ): Promise<QueryResult['rows'] | false> {
        const res = await this.exec(query, params)
        return (res === false) ? false : res.rows
    }
}
