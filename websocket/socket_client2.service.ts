import { DataTypeHelper } from '../_helper/datatype.helper';

interface wsData {
    job: string;
    room?: string;
    client?: string;
    data: any;
}

type NextCallType = {
    [K in 'open' | 'message' | 'close' | 'error']: 
        ((e: Event | MessageEvent) => void) | false 
}

export class SocketClient2Service {
    private static instance: SocketClient2Service;

    private socket: WebSocket | undefined;
    private port = 3000;
    private url = 'ws://localhost';

    private onRoom = false;
    private onEvent = false;

    private Nexts: NextCallType = {
        open: false,
        message: false,
        close: false,
        error: false
    };

    // メッセージ受信後の処理
    private next: { [key: string]: Function } = {
        on: () => {
            return null;
        },
        room: () => {
            return null;
        }
    };

    public constructor(port = 0, url = '') {
        this.port = port > 0 ? port : this.port;
        this.url = url !== '' ? url : this.url;
        try {
            this.socket = new WebSocket(this.url + ':' + this.port);
        } catch (error) {
            console.error(error);
        }
    }

    public static call(
        port = 3000,
        url = 'http://localhost'
    ): SocketClient2Service {
        if (!SocketClient2Service.instance) {
            SocketClient2Service.instance = new SocketClient2Service(port, url);
        }
        return SocketClient2Service.instance;
    }

    /**
     * 簡易スタート
     */
    public start() {
        this.listen();
        //this.joinRoom();

        this.setNext(() => {
            this.joinRoom();
        }, 'open');
    }

    public setNext(
        next: (e: Event | MessageEvent) => void,
        target: 'open' | 'message' | 'close' | 'error'
    ): SocketClient2Service {
        this.Nexts[target] = next
        return this
    }

    /**
     * 待受開始
     */
    public listen(): SocketClient2Service {
        if (this.onEvent || this.socket === undefined) {
            return this;
        }

        this.socket.addEventListener('open', (e: Event) => {
            console.log('Open WebSocket', e);
            if (this.Nexts.open !== false) {
                this.Nexts.open(e);
            }
        });

        this.socket.addEventListener('message', (m: MessageEvent) => {
            const d = this.fromJson(m.data)
            DataTypeHelper.call().setFile(d.data).check()
            if (this.Nexts.message !== false) {
                this.Nexts.message({
                    ...d,
                    ...{ type: DataTypeHelper.call().getResult() }
                })
            }
        })
        this.socket.addEventListener('close', (e: Event) => {
            console.log('Close WebSocket', e)
            if (this.Nexts.close !== false) {
                this.Nexts.close(e)
            }
        })
        this.socket.addEventListener('error', (e: Event) => {
            console.log('Error::')
            console.error(e)
            if (this.Nexts.error !== false) {
                this.Nexts.error(e)
            }
        })
        this.onEvent = true

        return this
    }

    /**
     * メッセージの送信
     * @param data wsData
     * @returns
     */
    public send(data: wsData): SocketClient2Service {
        if (this.socket === undefined) return this
        this.socket.send(this.toJson(data))
        return this
    }

    public joinRoom(room = ''): SocketClient2Service {
        if (this.onRoom) {
            return this;
        }
        const _room = room === '' ? this.rand() : room;
        console.log(`room: ${_room}`);
        this.send({
            job: 'join',
            room: _room,
            data: {}
        });
        this.onRoom = true;

        return this;
    }

    private rand() {
        const S =
            'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        const N = 16
        return Array.from(Array(N))
            .map(() => S[Math.floor(Math.random() * S.length)])
            .join('')
    }

    private toJson(data: object): string {
        return JSON.stringify(data)
    }

    private fromJson(data: string): any {
        return JSON.parse(data)
    }
}
