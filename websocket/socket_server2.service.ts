import ws from 'ws';
import { RoomHelper } from './helper/room.helper';

export class SocketServer2Service {
    private static instance: SocketServer2Service;

    private server: ws.Server | any; // 待受中のサーバー
    private port = 3000; // 待受ポート

    private jobStack: any = {
        cast: [],
        room: [],
        client: []
    };

    // メッセージ受信後の処理
    private next: { [key: string]: Function } = {
        cast: () => {
            return null;
        },
        room: () => {
            return null;
        },
        client: () => {
            return null;
        }
    };

    public constructor() {}

    public static call(): SocketServer2Service {
        if (!SocketServer2Service.instance) {
            SocketServer2Service.instance = new SocketServer2Service();
        }
        return SocketServer2Service.instance;
    }

    /**
     * WebSocketの待受ポート設定
     * @param port number
     * @returns SocketServer2Service
     */
    public setPort(port: number): SocketServer2Service {
        this.port = typeof port === 'number' ? port : this.port;
        return this;
    }

    /**
     * メッセージ受信後の処理を設定
     * クライアントから送信された[message]を
     * そのまま受け取り処理する関数を任意で追加出来る
     * ※上書きではなく処理の追加なので注意
     *
     * @param next Function
     * @param target string 'cast' | 'room' | 'target'
     * @returns
     */
    public setNext(
        next: Function | string = (data: any) => {
            console.log(data);
        },
        target: 'cast' | 'room' | 'target'
    ): SocketServer2Service {
        if (typeof next === 'function') {
            this.next[target] = next;
        }
        return this;
    }

    /**
     * 接続中の全クライアントに送信
     * @param data any（文字列 or 配列 or オブジェクト）
     * @returns ScoketServer2Service
     */
    public sendAllClient(data: any): SocketServer2Service {
        this.server.clients.forEach((client: any) => {
            this.send(client, 'cast', data);
        });
        return this;
    }

    /**
     * クライアントを指定して送信
     * @param client string clientID
     * @param data any 送信データ（文字列 or 配列 or オブジェクト）
     * @returns SocketServer2Service
     */
    public sendToClient(client: string, data: any): SocketServer2Service {
        this.server.clients.forEach((c: any) => {
            if (c.id === client) {
                this.send(c, 'to_client', data);
            }
        });
        return this;
    }

    /**
     * 部屋を指定して送信
     * @param room string roomid
     * @param data any 送信データ（文字列 or 配列 or オブジェクト）
     * @returns SocketServer2Service
     */
    public sendToRoom(room: string, data: any): SocketServer2Service {
        this.server.clients.forEach((client: any) => {
            if (client.room === room) {
                this.send(client, 'to_room', data);
            }
            /**
            RoomHelper.call()
                .getRoomUsers(room)
                .then((result: any) => {
                    if (result.indexOf(client.id) >= 0) {
                        this.send(client, 'to_room', data);
                    }
                }); */
        });
        return this;
    }

    /**
     * websocketサーバーを起動
     * @returns
     */
    public start(): SocketServer2Service {
        this.server = new ws.Server({ port: this.port });
        console.log(`
        [Start Socket Server] Listen Port :: ${this.port}
        `);
        // サーバーアクション設定
        this.action();

        return this;
    }

    /**
     * 受信時の動作を登録
     * @returns
     */
    private action(): SocketServer2Service {
        this.server.on('connection', (s: any) => {
            // IDを付与
            s.ipAddress = s._socket.remoteAddress.replace(/^.*:/g, '');
            s.port = s._socket.remotePort;
            s.unique = this.buildId();
            s.id = `${s.ipAddress}:${s.port}:${s.unique}`;

            // 生成されたIDを返す
            s.send(
                JSON.stringify({
                    job: 'connection',
                    id: s.id
                })
            );

            // 待受開始
            s.on('message', async (message: any) => {
                const m = this.fromJson(message);
                await this.lisnHub(m, s);
                this.executeJob();
            });
        });

        return this;
    }

    /**
     * job毎に処理を振り分け
     * @param m any 受信データ
     * @param s any 接続ClientのSocket
     * @returns Promise<SocketServer2Service>
     */
    private async lisnHub(m: any, s: any): Promise<SocketServer2Service> {
        if (m.job === 'cast') {
            const job =
                this.next.cast() !== null
                    ? this.next.cast(m)
                    : { to: 'cast', data: m.data };
            this.addJob(job, 'cast');
        }

        if (m.job === 'to_room' && 'room' in m.data) {
            const job =
                this.next.room() !== null
                    ? this.next.room(m)
                    : { to: m.data.room, data: m.data };
            this.addJob(job, 'room');
        }

        if (m.job === 'to_client' && m.data.client) {
            const job =
                this.next.client() !== null
                    ? this.next.client(m)
                    : { to: m.data.client, data: m.data.data };
            this.addJob(job, 'client');
        }

        if (m.job === 'join' && 'room' in m.data) {
            await this.joinRoom(m.data.room, s.id);
            s.room = m.data.room;
            this.addJob(
                {
                    to: m.data.room,
                    data: {
                        job: 'join',
                        message: this.getClientList(m.data.room)
                    }
                },
                'room'
            );
        }

        if (m.job === 'leave' && m.room) {
            await this.leaveRoom(m.room, s.id);
            this.addJob(
                {
                    to: m.data.room,
                    data: { job: 'leave', message: this.getClientList() }
                },
                'room'
            );
        }

        if (m.job === 'get_member') {
            s.send(
                this.toJson({
                    job: 'member',
                    data: this.getClientList()
                })
            );
        }
        if (m.job === 'get_room_member') {
            s.send(
                this.toJson({
                    job: 'room_member',
                    data: this.getClientList()
                })
            );
        }

        return this;
    }

    /**
     * Jobスタックにデータを追加
     * @param job {to: string, data: any} 送信先と送信データ
     * @param send 'cast' | 'room' | 'client' 送信方式、全配信、部屋配信、指定配信
     * @returns SocketServer2Service
     */
    private addJob(
        job: { to: string; data: any },
        send: 'cast' | 'room' | 'client'
    ): SocketServer2Service {
        this.jobStack[send].push(job);
        return this;
    }

    /**
     * スタックされたジョブを実行
     * @returns SocketServer2Service
     */
    private executeJob(): SocketServer2Service {
        for (const key in this.jobStack) {
            if (Object.prototype.hasOwnProperty.call(this.jobStack, key)) {
                if (this.jobStack[key].length > 0) {
                    this.jobStack[key].map((job: any) => {
                        console.log(`
                        [send message] JOB: ${key}
                        ${JSON.stringify(job)}
                        `);
                        if (key === 'cast') {
                            this.sendAllClient(job.data);
                        } else if (key === 'room') {
                            this.sendToRoom(job.to, job.data);
                        } else if (key === 'client') {
                            this.sendToClient(job.to, job.data);
                        }
                    });
                    this.jobStack[key] = [];
                }
            }
        }
        return this;
    }

    /**
     * クライアントに送信
     * @param client any WebSocketオブジェクト
     * @param job string 送信処理種別
     * @param data any 送信データの中身（JSON文字列）
     * @returns SocketServer2Service
     */
    private send(client: any, job: string, data: any): SocketServer2Service {
        client.send(
            this.toJson({
                job: job,
                data: data
            })
        );
        return this;
    }

    /**
     * ClientIDを生成
     * @returns string ランダム生成されたClinetID
     */
    private buildId(): string {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    /**
     * 接続中のClientを一覧で返す
     * 部屋名の指定がある場合は、指定部屋内のユーザー一覧を返す
     * @param room string 部屋名
     * @returns [{socketid: string, name: string}...]
     */
    private getClientList(room = ''): object[] {
        const clients: object[] = [];
        this.server.clients.forEach((client: any) => {
            if (room !== '') {
                if (client.room === room) {
                    clients.push({
                        socketid: client.id,
                        name: client.name === undefined ? 'user' : client.name
                    });
                }
            } else {
                clients.push({
                    socketid: client.id,
                    name: client.name === undefined ? 'user' : client.name
                });
            }
        });
        console.log('clients:: ' + clients);
        return clients;
    }

    /**
     * Roomに入室
     * @param room string 部屋名
     * @param user string ユーザー名
     * @returns SocketServer2Service
     */
    private async joinRoom(
        room: string,
        user: string
    ): Promise<SocketServer2Service> {
        await RoomHelper.call().addUser(room, user);
        console.log(
            `[join to room]
                user    : ${user}
                room    : ${room}`
        );
        return this;
    }

    /**
     * Roomから退出
     * @param room string
     * @param user
     * @returns
     */
    private async leaveRoom(
        room: string,
        user: string
    ): Promise<SocketServer2Service> {
        await RoomHelper.call().delUser(user, room);
        console.log(
            `[leave to room]
                user    : ${user}
                room    : ${room}`
        );
        return this;
    }

    private toJson(data: any): string {
        return JSON.stringify(data);
    }

    private fromJson(data: string): any {
        return JSON.parse(data);
    }
}
