import express from 'express';
import { SocketServer2Service } from './socket_server2.service';

export class SocketHub2Service {
    private hubPort = 8080;
    private socketPort = 8081;

    private app: express.Express;
    private socket: SocketServer2Service;

    private origin: string[] = [];
    private methods: string[] = [];
    private headers: string[] = [];

    public constructor() {
        this.app = express();
        this.socket = SocketServer2Service.call();
    }

    public setNext(
        Next: Function,
        target: 'room' | 'target' | 'cast'
    ): SocketHub2Service {
        this.socket.setNext(Next, target);
        return this;
    }

    /**
     * 待受開始
     */
    public start(): SocketHub2Service {
        this.setupWebsocket();
        this.setupAPIHub();
        return this;
    }

    /**
     * Websocket起動
     */
    public setupWebsocket(): SocketHub2Service {
        // this.socket.start();
        this.socket.setPort(this.socketPort).start();
        // this.socket.sendMessageToRoom(this.socket.getRoom(), 'welcome');
        return this;
    }

    /**
     * WebSocket中継APIの起動
     *
     */
    public setupAPIHub() {
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));

        //CROS対応
        this.app.use(
            (
                req: express.Request,
                res: express.Response,
                next: express.NextFunction
            ) => {
                res.header(
                    'Access-Control-Allow-Origin',
                    this.getParam('origin')
                );
                res.header(
                    'Access-Control-Allow-Methods',
                    this.getParam('methods')
                );
                res.header(
                    'Access-Control-Allow-Headers',
                    this.getParam('headers')
                );
                next();
            }
        );

        this.app.listen(this.hubPort, () => {
            console.log('Start on port 8080.');
        });

        this.app.post(
            '/send',
            (req: express.Request, res: express.Response) => {
                console.log(req.body);
                res.status(200).send('ok');
                this.socket.sendAllClient(req.body);
                //this.socket.sendMessageToRoom(this.socket.getRoom(), req.body);
                //socket.sendMessage(req.body);
            }
        );
    }

    /**
     * アクセスコントロール情報を取得
     * @param target 'origin' | 'methods' | 'headers'
     * @returns string
     */
    public getParam(target: 'origin' | 'methods' | 'headers'): string {
        return this[target].length === 0 ? '*' : this[target].join(', ');
    }

    /**
     * 許可するアクセスコントロールを追加
     * @param target 'origin' | 'methods' | 'headers'
     * @param propatie string
     * @returns SocketHubService
     */
    public setAccessControl(
        target: 'origin' | 'methods' | 'headers',
        propatie: string
    ): SocketHub2Service {
        if (propatie === '*') {
            this[target] = [];
            return this;
        }
        this[target].push(propatie);
        return this;
    }
}
