import socketIo from 'socket.io';
import http from 'http';

// import Helper
import { RoomHelper } from './helper/room.helper';

export class SocketServerService {
    private static instance: SocketServerService;

    private io: socketIo.Server; // SocketIOサーバー本体
    private server: http.Server; // 待受中のサーバー

    private port = 3000; // 待受ポート

    private socket: any = null; // 接続中クライアントのSocketID
    private room = ''; // 接続中クライアントが入室した部屋名

    private sockets: {} = {};

    private next: Function = () => {}; // メッセージ受信後の処理

    private events: any = {
        to_server: 'to_server', // サーバーに送信
        to_client: 'to_client', // クライアントに送信
        join_room: 'join_to_room', // 部屋に入室
        leave_room: 'leave_to_room' // 部屋から退出
    };

    public constructor() {
        this.server = http.createServer();
        this.io = new socketIo.Server(this.server, {
            cors: {
                origin: '*'
            }
        });
    }

    public static call(): SocketServerService {
        if (!SocketServerService.instance) {
            SocketServerService.instance = new SocketServerService();
        }
        return SocketServerService.instance;
    }

    /**
     * WebSocketの待受ポート設定
     * @param port number
     * @returns SocketServerService
     */
    public setPort(port: number): SocketServerService {
        this.port = typeof port === 'number' ? port : this.port;
        return this;
    }

    public setRoomId(room: string): SocketServerService {
        this.room = room;
        return this;
    }

    /**
     * メッセージ受信後の処理を設定
     * @param next
     * @returns
     */
    public setNext(
        next: Function = () => {
            return;
        }
    ): SocketServerService {
        this.next = next;
        return this;
    }

    /**
     * 受信イベントを追加
     * @param event string イベント名
     * @param next Function　イベント取得後に実行する処理
     * @returns SocketServerService
     */
    public addListner(event: string, next: Function): SocketServerService {
        if (this.socket) {
            this.socket.on(event, (data: any) => next(data));
        }
        return this;
    }

    /**
     * 現在設定(入室中)されている部屋名を返す
     * @returns string
     */
    public getRoom(): string {
        return this.room;
    }

    /**
     * wensocketに設定されている部屋名すべてを返す
     * @returns string[]
     */
    public getRooms(): string[] {
        const rooms = this.io.of('/').adapter.rooms;
        const _rooms: string[] = [];
        rooms.forEach((room, key) => {
            _rooms.push(key);
        });
        return _rooms;
    }

    /**
     * 部屋に登録されているsocketid をすべて返す
     * @param room
     * @returns
     */
    public getClientByRoom(room: string): string[] {
        const rooms = this.io.of('/').adapter.rooms;
        if (room in rooms) {
            // return rooms.get(room);
        }
        return [];
    }

    /**
     * Socketにメッセージを送信
     * @param message any string | json
     * @returns void
     */
    public sendMessage(message: any, onself = false): void {
        if (!this.socket) return;

        if (onself) {
            this.socket.broadcast.emit(this.events.to_client, {
                body: message
            });
        } else {
            this.socket.emit(this.events.to_client, {
                body: message
            });
        }

        return;
    }

    /**
     * 部屋内にメッセージを送信
     * @param room string
     * @param message any string | json
     * @returns void
     */
    public sendMessageToRoom(room: string, message: any): void {
        if (!this.socket) {
            return;
        }
        console.log(room);
        this.io.to(room).emit(this.events.to_client, {
            body: message
        });
        return;
    }

    public sendMessageToClint(clinet: string, message: any) {}

    /**
     * sokcetid を指定してメッセージを送信
     * @param socket
     * @param message
     * @returns
     */
    public sendMessageToSocketId(socket: string, message: any) {
        if (!this.socket) {
            return;
        }
        console.log(socket);
        this.io.to(socket).emit(this.events.to_client, {
            body: message
        });
        return;
    }

    /**
     * websocketサーバーを起動
     * @returns
     */
    public start(): SocketServerService {
        // サーバーアクション設定
        this.action();

        // 送受信開始
        this.listen();
        return this;
    }

    /**
     * 受信時の動作を登録
     * @returns
     */
    private action(): SocketServerService {
        this.io.on('connection', (socket: socketIo.Socket) => {
            // socketを保持
            this.setSocket(socket);

            this.socket.on(this.events.join_room, (data: { room: string }) =>
                this.joinRoom(data)
            );
            this.socket.on(this.events.to_server, (data: { body: object }) =>
                this.next(data)
            );
            //this.sendMessage({unko: 'unko'});

            this.socket.on('disconnect', () => this.leaveRoom());
        });

        return this;
    }

    /**
     * WebSocket待受開始
     * @returns SocketServerService
     */
    private listen(): SocketServerService {
        this.server.listen(this.port, () => {
            console.log('Start WebSocket Server on Port ' + this.port);
        });
        return this;
    }

    /**
     * ソケットを保持
     * @param socket
     */
    private setSocket(socket: socketIo.Socket): void {
        this.socket = socket;
        return;
    }

    /**
     * Room名を保持
     * @param room
     * @returns
     */
    private setRoom(room = ''): void {
        this.room = room === '' ? this.room : room;
        return;
    }

    /**
     * Roomに入室
     * @param data { room: string }
     * @returns SocketServerService
     */
    private joinRoom(data: any): SocketServerService {
        data.room = 'test';
        this.setRoom(data.room);
        this.socket.join(data.room);

        console.log(
            `[join to room]
            socketId: ${this.socket.id}
            roomId: ${data.room}`
        );
        console.log(
            `[room member]
           ${this.getRooms()}
        `
        );

        this.sendMessageToRoom(this.room, { data: data.message });

        //RoomHelper.call().addUser(this.socket.id, data.room);
        return this;
    }

    private leaveRoom(): SocketServerService {
        this.socket.leave(this.room);
        this.reset();
        // RoomHelper.call().delUser(this.socket.id, this.room);
        return this;
    }

    private reset(): SocketServerService {
        this.socket = '';
        this.room = '';
        return this;
    }
}
