# README #

* WebSocket実装ライブラリ
* サポート機能
* * 中継APIを使用したストリーム配信
* * 接続Client全てに双方向の配信
* * Clientと1対1の配信
* * 部屋を作成し部屋内の配信

* サンプルプロジェクト「[Socket_Sample](https://bitbucket.org/yasukosan/socket_sample/src/master/)」
# How to use

## ■ 共通処理
## サーバー側は下記をnodejs上で起動
* POST待受は8080ポート
* Socket待ち受けは8081ポート

```javascript
import { SocketHub2Service } from './_lib/websocket/socket_hub2.service';

const www = new SocketHub2Service();
www.start();

```
---
## クライアント側は下記サンプルhookをComponent内で読み込む（サンプルはReact）
```javascript
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { SocketClient2Service } from '../../_lib/websocket/socket_client2.service';
import { SocketInterface, SocketPropsInterface } from '../../reducers/Socket';

export interface WebSocket {
    next    : string,
}

export const useWebsocketListner = (state: WebSocket) => {
    const s = useSelector((state: SocketPropsInterface) => state.Socket);
    const dispatch = useDispatch();

    const socket = SocketClient2Service.call(8081, 'ws://localhost');

    useEffect(() => {
        socket
            // メッセージ受け取り処理
            .setNext((data: any) => {
                // 初回接続時にClientsIDが返るので処理を分けるか
                // 受け先で分岐させる場合は下記分岐処理は不要
                const next = (data.job === 'connection') ? 'Socket/setMyId' : state.next;
                dispatch({
                    type    : next,
                    data    : data,
                });
            }, 'message')
            // 待受開始
            .listen();
    }, []);

    // 送信処理
    if (s && s.emit) {
        socket.send({
            job     : s.job,
            data    : s.message
        });
        // 送信データを保持するReducerをClear
        dispatch({
            type    : 'Socket/clearEmit'
        });
    }

    return {message: ''};
};


```

---
---
## Stream Mode (配信専用)

* サーバー側APIにPOST送信した内容をWebSocket経由で全クライアントに配信
### アクセスポイント [ http://[server_address]/send ]にPOSTでデータ送信


* Hookで受け取ったデータをそのまま表示する

```javascript
import React from 'react';
import { useSelector } from 'react-redux';
import { useWebsocketListner } from '../_hook/websocket';

// import reducer
import {
    DownStreamPropsInterface
} from '../../reducers/_Socket/DownStream'


const ShowDownStream = (() => {
    const d = useSelector((state: DownStreamPropsInterface) => {
                    return (state.DownStream) ? state.DownStream : {
                        message : ['unko'],
                        job     : 'unko'
                    }
                });
    // Client共通Hookで次にデータを投げる先を指定
    useWebsocketListner({
        next    : 'DownStreamAction/onHub',
    });

    return (
        <div className="container">
            <nav className="navbar">
                <h3>Down Stream Test</h3>
            </nav>
            <br></br>
            <div>
                { showMessages(d.message) }
            </div>
            <div>
                localhost:8080/send にpostでデータを送信
            </div>
        </div>
    );
})
```

---
---

## Mutual Mode （双方向通信）
WebSocket経由で、メッセージの送信、受信を行う

### 処理の流れ
#### ■ Client

* フォームからメッセージ送信
* Reducerにメッセージ情報を登録
* Reducerの更新でHookの内容が更新される
* 登録情報をHookからライブラリ経由で送信、Reducerをクリア
#### ■ Server
* サーバー側でmessageを受信
* job「cast（全接続クライアントに配信）」の処理を開始
* 全ての接続クライアントに送信されたメッセージ情報を送信
#### ■ Client
* Hookでmessageを受け取り
* Componentから指定された投げ先にDispachされる
* job[cast]を確認し受け取ったメッセージをReducerに登録
* 画面の内容を更新する

## Componentサンプル
```javascript
import { useDispatch, useSelector } from 'react-redux';
import { useWebsocketListner } from '../_hook/websocket';
// import component

// import reducer
import {
    MutualStreamPropsInterface, initialState
} from '../../reducers/_Socket/MutualStream'


const MutualForm = ((state: MutualStreamPropsInterface) => {
    const dispatch = useDispatch();
    const d = useSelector((state: MutualStreamPropsInterface) => {
                    return (state.MutualStream) ? state.MutualStream : {
                        message : ['unko'],
                        job     : 'unko'
                    }
                });
    // Client共通Hookで次にデータを投げる先を指定
    useWebsocketListner({
        next    : 'MutualAction/onHub',
    });

    return (
        <div className="container">
            <nav className="navbar">
                <h3>Mutual Stream Test</h3>
            </nav>
            <br></br>

            <div>
                <input
                    id='MutualForm'
                    type={'text'}
                />
                <button
                    type='button'
                    className="btn btn-secondary btn-sm"
                    data-toggle="modal" data-target='#show_qr'
                    onClick={
                        () => {
                            dispatch({
                                type    : 'SocketAction/Emit',
                                job     : 'cast',
                                data    : getMessage(),
                            });
                        }
                }>
                    送信
                </button>
            </div>
            <div>
                { showMessages(d.message) }
            </div>
            <div>
                接続中の全クライアントにメッセージを送信
            </div>
        </div>
    );
})
```
## Actionサンプル
```javascript
// Root Saga登録配列
export const RootMutualAction = [
    takeEvery('MutualAction/onHub', onHub),
];


export function* onHub(val): any
{
    console.log(val);
    if ( 'job' in val.data && val.data.job === 'cast' ) {
        yield put({
            type    : 'MutualStream/setMessage',
            message : val.data.data
        });
    }
    return;
}

```



---
---
## Client Mode （クライアント指定）

※ Clientを指定して送信

## 処理の流れ（Client一覧取得）
#### ■ Client
* サーバーにjob[get_member]を送信
#### ■ Server
* job[get_member]の処理を開始
* WebSocketに接続中のClientを全て取得
* job[member]とClient一覧をmessageに格納し返す
#### ■ Client
* Hookでmessageを受信
* Coponentから指定された投げ先にDispatchされる
* job[member]を確認し、Client一覧をReducerに登録
* 画面の内容を更新する
## 処理の流れ（メッセージ送信）
#### ■ Client
* ユーザー一覧から送信Clientを選択
* フォームからメッセージを入力
* 送信ボタンを押す
* Reducerにメッセージ情報を登録
* Reducerの更新でHookの内容が更新される
* 登録情報をHookからライブラリ経由で送信、Reducerをクリア

#### ■ Server
* サーバー側でmessageを受信
* job[to_client（Client指定配信）]の処理を開始
* 全ての接続クライアントに送信されたメッセージ情報を送信

#### ■ Client
* Hookでmessageを受け取り
* Componentから指定された投げ先にDispachされる
* job[to_client]を確認し受け取ったメッセージをReducerに登録
* 画面の内容を更新する

## Componentサンプル
```javascript
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useWebsocketListner } from '../_hook/websocket';

// import reducer
import {
    DirectionalityPropsInterface
} from '../../reducers/_Socket/Directionality'

import {
    SocketPropsInterface, initialState as SocketInitial
} from '../../reducers/Socket';


const Directionality = (() => {
    const dispatch = useDispatch();
    const d = useSelector((state: DirectionalityPropsInterface) => {
                    return (state.Directionality) ? state.Directionality : {
                        users   : [{user: 'unko', socket:'unko'}],
                        messages : [{user: 'unko', message:'unko'}],
                        job     : 'unko'
                    }
                });
    
    const s = useSelector((state: SocketPropsInterface) => {
                    return (state.Socket) ? state.Socket : SocketInitial
                });

    useWebsocketListner({
        next    : 'DirectionalityAction/onHub',
    });

    return (
        <div className="container">
            <nav className="navbar">
                <h3>Directionality Stream Test</h3>
                <h4>MyID: {s.myId}</h4>
            </nav>
            <br></br>
            <button
                    type='button'
                    className="btn btn-secondary btn-sm"
                    data-toggle="modal" data-target='#show_qr'
                    onClick={
                        () => {
                            dispatch({
                                type    : 'DirectionalityAction/emitHub',
                                task    : 'member',
                            });
                        }
                }>
                    接続(ユーザー一覧取得)
                </button>
            <div>
                <input
                    id='Username'
                    type={'text'}
                />
            </div>
            <div>
                <input
                    id='MessageForm'
                    type={'text'}
                />
                <button
                    type='button'
                    className="btn btn-secondary btn-sm"
                    data-toggle="modal" data-target='#show_qr'
                    onClick={
                        () => {
                            dispatch({
                                type    : 'DirectionalityAction/emitHub',
                                task    : 'message',
                                data    : buildMessage(),
                            });
                        }
                }>
                    送信
                </button>
                <div className="user-list">
                    { showUsers(d.users, dispatch) }
                </div>
            </div>
            <div>
                <table className='table'>
                    <tbody>
                    <tr>
                        <td>
                            <div className="message-list">
                                <ul className="list-group">
                                { showMessages(d.messages) }
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div>
                localhost:8080/send にpostでデータを送信
            </div>
        </div>
    );
})
```
## Actionサンプル
```javascript
// Root Saga登録配列
export const RootDirectionalityAction = [
    takeEvery('DirectionalityAction/onHub', onHub),
    takeEvery('DirectionalityAction/emitHub', emitHub),
    takeEvery('DirectionalityAction/getRoomMember', getRoomMember),
];


export function* onHub(val): any
{
    if ( 'job' in val.data && val.data.job === 'member' ) {
        yield put({
            type    : 'Directionality/setUser',
            users   : val.data.data
        });
    }
    if ( 'job' in val.data && val.data.job === 'to_client' ) {
        yield put({
            type    : 'Directionality/setMessage',
            message : {
                name    : 'user',
                message : val.data.data
            }
        });
    }
    return;
}

export function* emitHub(val): any
{
    console.log(val);
    if ( val.task === 'member' ) {
        yield put({
            type    : 'SocketAction/Emit',
            job     : 'get_member',
            data    : { room : 'test' }
        });
    }
    if ( val.task === 'message' ) {
        yield put({
            type    : 'SocketAction/Emit',
            job     : 'to_client',
            data    : {
                client  : val.data.user,
                data    : val.data.message,
            }
        });
    }
    return;
}
```

---
## Room Mode （部屋通信）
※ Roomを指定して送信

## 処理の流れ（Client一覧取得）
#### ■ Client
* 部屋名を入力
* 入室ボタンを押す
* 入室Actionが実行される
* job[join]とroom名が送信
#### ■ Server
* job[join]の処理を開始
* 接続中のSocketのプロパティにroom情報を追加
* 現在接続中のClinetsより、同一のRoomに属しているClientを抜き出す
* job[join]とClient一覧をmessageに格納し返す
#### ■ Client
* Hookでmessageを受信
* Coponentから指定された投げ先にDispatchされる
* job[join]を確認し、Client一覧をReducerに登録
* 画面の内容を更新する
## 処理の流れ（メッセージ送信）
#### ■ Client
* フォームからメッセージを入力
* 送信ボタンを押す
* Reducerにメッセージ情報を登録
* Reducerの更新でHookの内容が更新される
* 登録情報をHookからライブラリ経由で送信、Reducerをクリア

#### ■ Server
* サーバー側でmessageを受信
* job[to_room（Room指定配信）]の処理を開始
* 全ての接続クライアントに送信されたメッセージ情報を送信

#### ■ Client
* Hookでmessageを受け取り
* Componentから指定された投げ先にDispachされる
* job[to_room]を確認し受け取ったメッセージをReducerに登録
* 画面の内容を更新する
## Componentサンプル
```javascript
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useWebsocketListner } from '../_hook/websocket';
// import component

// import reducer
import {
    RoomPropsInterface
} from '../../reducers/_Socket/Room'

import {
    SocketPropsInterface, initialState as SocketInitial
} from '../../reducers/Socket';

const ChatRoom = (() => {
    const dispatch = useDispatch();
    const r = useSelector((state: RoomPropsInterface) => {
                    return (state.Room) ? state.Room : {
                        member   : [{user: 'unko', socket:'unko'}],
                        messages : [{user: 'unko', message:'unko'}],
                        room     : 'test'
                    }
                });
    
    const s = useSelector((state: SocketPropsInterface) => {
                    return (state.Socket) ? state.Socket : SocketInitial
                });

    useWebsocketListner({
        next    : 'RoomAction/onHub',
    });

    return (
        <div className="container">
            <nav className="navbar">
                <h3>Room Strem Test</h3>
                <h4>MyID: {s.myId}</h4>
            </nav>
            <br></br>
            <div>
                Room：
                <input
                        id='Roomname'
                        type={'text'}
                    />
                <button
                        type='button'
                        className="btn btn-secondary btn-sm"
                        data-toggle="modal" data-target='#show_qr'
                        onClick={
                            () => {
                                dispatch({
                                    type    : 'RoomAction/emitHub',
                                    task    : 'join',
                                    room    : getRoomName(),
                                });
                            }
                    }>
                        接続
                </button>
            </div>
            <br></br>
            <div>
                Mess：<input
                    id='MessageForm'
                    type={'text'}
                />
                <button
                    type='button'
                    className="btn btn-secondary btn-sm"
                    data-toggle="modal" data-target='#show_qr'
                    onClick={
                        () => {
                            dispatch({
                                type    : 'RoomAction/emitHub',
                                task    : 'message',
                                data    : {
                                    room    : r.room,
                                    message : getMessage(),
                                    myId    : s.myId
                                },
                            });
                        }
                }>
                    送信
                </button>
                <div className="user-list">
                    { showMember(r.member, dispatch) }
                </div>
            </div>
            <div>
                <table className='table'>
                    <tbody>
                    <tr>
                        <td>
                            <div className="message-list">
                                <ul className="list-group">
                                { showMessages(r.messages) }
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div>
                localhost:8080/send にpostでデータを送信
            </div>
        </div>
    );
})
```

## Actionサンプル
```javascript
export const RootRoomAction = [
    takeEvery('RoomAction/onHub', onHub),
    takeEvery('RoomAction/emitHub', emitHub),
];


export function* onHub(val): any
{
    if ( 'job' in val.data === false || val.data.job !== 'to_room' ) return;
    if ('job' in val.data.data === false) return;

    if (val.data.data.job === 'join') {
        yield put({
            type   : 'Room/setMember',
            member : val.data.data.message
        });
    }
    if (val.data.data.job === 'message') {
        yield put({
            type    : 'Room/setMessage',
            message : val.data.data.message
        });
    }

    return;
}

export function* emitHub(val): any
{
    console.log(val);
    if ( val.task === 'join' ) {
        yield put({
            type    : 'Room/setRoom',
            room    : val.room,
        });
        yield put({
            type    : 'SocketAction/Emit',
            job     : 'join',
            data    : { room : val.room }
        });
    }
    if ( val.task === 'message' ) {
        yield put({
            type    : 'SocketAction/Emit',
            job     : 'to_room',
            data    : {
                room    : val.data.room,
                message : {
                    message : val.data.message,
                    name    : val.data.myId,
                },
                job     : 'message'
            }
        });
    }
    return;
}
```

---
