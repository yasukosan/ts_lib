import { RedisService } from '../_lib/redis.service';

export class RoomHelper {
    private static instance: RoomHelper;
    private DB: RedisService;

    public constructor() {
        this.DB = RedisService.call().conn(6379, 'redis');
    }

    public static call(): RoomHelper {
        if (!RoomHelper.instance) {
            RoomHelper.instance = new RoomHelper();
        }
        return RoomHelper.instance;
    }

    public async getRoomUsers(room: string): Promise<any> {
        let users = '';
        if (await this.checkRoom(room)) {
            users = await RedisService.call().get(room);
        }
        return JSON.parse(users);
    }

    public async addRoom(room: string): Promise<RoomHelper> {
        if (await this.checkRoom(room)) {
            await RedisService.call().set(room, JSON.stringify([]));
        }
        return this;
    }

    public async delRoom(room: string): Promise<RoomHelper> {
        const _room = await RedisService.call().get(room);
        if (_room !== '') await RedisService.call().del(room);
        return this;
    }

    public async addUser(room: string, user: string): Promise<RoomHelper> {
        // 部屋が既にあるか確認
        if (await this.checkRoom(room)) {
            this.addRoom(room);
        }
        // 別の部屋に入室済みの場合消す
        const re = await this.checkRegistered(user);
        if (re) {
            await this.delUser(String(re), user);
        }

        // 部屋に登録
        const users = await RedisService.call().get(room);
        const _users = users !== null ? JSON.parse(users) : ['uiui'];
        _users.push(user);
        console.log(_users);

        await RedisService.call().del(room);
        await RedisService.call().set(room, JSON.stringify(_users));
        return this;
    }

    public async delUser(room: string, user: string): Promise<RoomHelper> {
        if (!this.checkRoom) {
            return this;
        }

        // this.Room = this.Room[room].filter((room: any) => (room.match(user)) == null );
        const _room = await RedisService.call().get(user);
        await RedisService.call().del(user);

        const users = JSON.parse(await RedisService.call().get(_room));
        console.log('users::' + users);
        if (users.indexOf(user) !== false) {
            await RedisService.call().del(room);
            await RedisService.call().set(
                room,
                users.splice(users.indexOf(user), 1)
            );
        }

        return this;
    }

    private async checkRoom(room: string): Promise<boolean> {
        const _room = await RedisService.call().get(room);
        return _room !== null ? true : false;
    }

    private async checkRegistered(user: string): Promise<boolean> {
        const _user = await RedisService.call().get(user);
        return _user !== null ? true : false;
    }
}
