import { RedisService } from '../_lib/redis.service';

export class DBHelper {
    private static instance: DBHelper;
    private db: any;

    public constructor() {
        this.db = RedisService.call().conn(6389, 'redis');
    }

    public static call(): DBHelper {
        if (!DBHelper.instance) {
            DBHelper.instance = new DBHelper();
        }
        return DBHelper.instance;
    }

    public saveUser(user: string, room: string, name: string): DBHelper {
        this.db.set(
            user,
            JSON.stringify({
                room: room,
                name: name
            })
        );
        return this;
    }

    public getUser(user: string): any {
        return JSON.parse(this.db.get(user));
    }

    public delUser(user: string): DBHelper {
        this.db.del(user);
        return this;
    }
}
