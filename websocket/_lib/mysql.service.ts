const mysql = require('mysql');

export class MysqlService {
    private static insance: MysqlService;

    private conn: any = null;

    public constructor() {}

    public static call(): MysqlService {
        if (!MysqlService.insance) {
            MysqlService.insance = new MysqlService();
        }
        return MysqlService.insance;
    }

    public query(query: string): any {
        this.conn.query(query, (error: any, result: any, fields: any) => {
            if (error) {
                console.error('Query Error : ' + error);
                return;
            }
            return result[0];
        });
    }

    /**
     * MysqlServerに接続
     * @param host      string ホスト名 (default:localhost)
     * @param user      string 接続ユーザー (default:root)
     * @param password  string パスワード (default:passwrod)
     * @param database  string DB名 (default:db)
     * @param charset   string 文字エンコード (default:UTF8_GENERAL_CI)
     * @returns
     */
    public connDB(
        host = 'localhost',
        user = 'root',
        password = 'password',
        database = 'db',
        charset = 'UTF8_GENERAL_CI'
    ): boolean {
        if (!this.conn) {
            this.conn = mysql.createConnection({
                host: host,
                user: user,
                password: password,
                database: database,
                charset: charset
            });
        }
        this.conn.connect((error: any) => {
            if (error) {
                console.error('Connection Error : ' + error.stack);
                return false;
            }
        });
        console.log('Mysql Connection');
        return true;
    }

    private disconnDB(): void {
        this.conn.end((error: any) => {
            if (error) {
                console.error('DisConnection Error : ' + error.stack);
            }
        });
    }
}
