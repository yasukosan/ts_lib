import Redis from 'ioredis';

export class RedisService {
    private static instance: RedisService;
    private redis: any;

    public constructor() {}

    public static call(): RedisService {
        if (!RedisService.instance) {
            RedisService.instance = new RedisService();
        }
        return RedisService.instance;
    }

    public conn(port = 6379, host = '127.0.0.1', db = 0): RedisService {
        const options = {
            port: port,
            host: host
        };
        if (db > 0) {
            this.redis = new Redis({ ...options, db: db });
        } else {
            this.redis = new Redis(options);
        }
        return this;
    }

    /**
     * データ登録
     * 既存のKeyに登録した場合上書き
     * @param key string Indexキー
     * @param val string 登録文字列
     * @param limit number データの有効期限
     * @returns Promise<RedisService>
     */
    public async set(key: string, val: any, limit = 0): Promise<RedisService> {
        this.redis.set(key, val);
        return await this;
    }

    /**
     * データ取得
     * @param key string Indexキー
     * @returns Promise<string>
     */
    public async get(key: string): Promise<string> {
        return await this.redis.get(key);
    }

    /**
     * データ削除
     * @param key string Indexキー
     * @returns Promise<RedisService>
     */
    public async del(key: string): Promise<RedisService> {
        this.redis.del(key);
        return await this;
    }
}
