const memcache = require('memcache');

export class MemcacheService {
    private static insance: MemcacheService;

    private conn: any = null;

    public constructor() {}

    public static call(): MemcacheService {
        if (!MemcacheService.insance) {
            MemcacheService.insance = new MemcacheService();
        }
        return MemcacheService.insance;
    }

    public get(key: string): any {
        this.conn.get(key, (err: any, val: any) => {
            if (!err) {
                return val;
            }
            console.error('Data Get ERROR :: ' + err);
            return false;
        });
    }

    public set(key: string, data: string): MemcacheService {
        this.conn.set(key, data, (err: any, val: any) => {
            if (err) {
                console.error('Data Set ERROR :: ' + err);
            }
            if (this.get(key) !== data) {
                console.error('ERROR :: Recorde Error');
            }
        });
        return this;
    }

    public del(key: string): MemcacheService {
        this.conn.del(key, (err: any) => {
            if (err) {
                console.error('Data Delete ERROR :: ' + err);
            }
        });
        return this;
    }

    /**
     * MysqlServerに接続
     * @returns
     */
    public connDB(): boolean {
        if (!this.conn) {
            this.conn = new memcache.Client(11211, 'localhost');
        }
        console.log('Memcache Connection');
        return true;
    }

    private reConnection(): void {}

    private disconnDB(): void {
        this.conn.end((error: any) => {
            if (error) {
                console.error('DisConnection Error : ' + error.stack);
            }
        });
    }
}
