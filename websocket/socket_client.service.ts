import { io } from 'socket.io-client';
import { DataTypeHelper } from '../_helper/datatype.helper';

export class SocketClientService {
    private static instance: SocketClientService;

    private socket: any;
    private port = 3000;
    private url = 'http://localhost';

    private onRoom = false;
    private onEvent = false;

    private events: any = {
        to_server: 'to_server', // サーバーに送信
        to_client: 'to_client', // クライアントに送信
        join_room: 'join_to_room', // 部屋に入室
        leave_room: 'leave_to_room' // 部屋から退出
    };

    private Nexts: any = {};

    public constructor(port = 0, url = '') {
        this.port = port > 0 ? port : this.port;
        this.url = url !== '' ? url : this.url;
        this.socket = io(this.url + ':' + this.port);
    }

    public static call(
        port = 3000,
        url = 'http://localhost'
    ): SocketClientService {
        if (!SocketClientService.instance) {
            SocketClientService.instance = new SocketClientService(port, url);
        }
        return SocketClientService.instance;
    }

    public callSocket(): any {
        return this.socket;
    }

    public getScheme(scheme: 'to_server' | 'to_client'): string {
        return this.events[scheme];
    }

    /**
     * 簡易スタート
     */
    public start() {
        this.connect();
        this.listen();
        this.joinRoom();
    }

    /**
     * サーバーに接続
     */
    public connect(): SocketClientService {
        this.socket.on('connect', () => {
            console.log('connect');
        });
        return this;
    }

    /**
     * 待受開始
     */
    public listen(
        next: Function = <T>(args: T) => {
            return;
        }
    ): SocketClientService {
        if (this.onEvent) {
            return this;
        }

        this.socket.on(this.events.to_client, (data: { body: object }) => {
            // console.log(data);
            console.log(JSON.stringify(data.body));

            next({
                ...data,
                type: DataTypeHelper.call()
                    .setFile(data.body)
                    .check()
                    .getResult()
            });
        });
        this.onEvent = true;

        return this;
    }

    public send(data: { job: string; body: any }): SocketClientService {
        this.socket.emit(this.events.to_server, data);
        // this.onEvent = true;

        return this;
    }

    public joinRoom(room = ''): SocketClientService {
        if (this.onRoom) {
            return this;
        }
        const _room = room === '' ? this.rand() : room;
        console.log(`room: ${_room}`);
        this.socket.emit('join_to_room', { room: _room });
        this.onRoom = true;

        return this;
    }

    private rand() {
        const S =
            'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        const N = 16;
        return Array.from(Array(N))
            .map(() => S[Math.floor(Math.random() * S.length)])
            .join('');
    }
}
