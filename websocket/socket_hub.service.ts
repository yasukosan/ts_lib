import express from 'express';
import { SocketClientService } from './socket_client.service';
import { SocketServerService } from './socket_server.service';

export class SocketHubService {
    private hubPort = 8080;
    private socketPort = 8081;

    private app: express.Express;
    private socket: SocketServerService;

    private origin: string[] = [];
    private methods: string[] = [];
    private headers: string[] = [];

    public constructor() {
        this.app = express();
        this.socket = SocketServerService.call();
    }

    /**
     * 待受開始
     */
    public start() {
        this.setupWebsocket();
        this.setupAPIHub();
    }

    /**
     * Websocket起動
     */
    private setupWebsocket() {
        this.socket
            .setNext((data: any) => {
                console.log(data);
                const s = SocketServerService.call();
                console.log(s.getRoom());
                if (data.job === 'cast') {
                    s.sendMessageToRoom(s.getRoom(), data.body);
                }
                if (data.job === 'get_users') {
                    s.sendMessageToRoom(
                        s.getRoom(),
                        s.getClientByRoom(s.getRoom())
                    );
                }
                if (data.job === 'get_rooms') {
                    s.sendMessageToRoom(s.getRoom(), s.getRooms());
                }
            })
            .setPort(this.socketPort)
            .start();
        this.socket.sendMessageToRoom(this.socket.getRoom(), 'welcome');
    }

    /**
     * WebSocket中継APIの起動
     *
     */
    private setupAPIHub() {
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));

        //CROS対応
        this.app.use(
            (
                req: express.Request,
                res: express.Response,
                next: express.NextFunction
            ) => {
                res.header(
                    'Access-Control-Allow-Origin',
                    this.getParam('origin')
                );
                res.header(
                    'Access-Control-Allow-Methods',
                    this.getParam('methods')
                );
                res.header(
                    'Access-Control-Allow-Headers',
                    this.getParam('headers')
                );
                next();
            }
        );

        this.app.listen(this.hubPort, () => {
            console.log('Start on port 8080.');
        });

        this.app.post(
            '/send',
            (req: express.Request, res: express.Response) => {
                console.log(req.body);
                res.status(200).send('ok');
                this.socket.sendMessageToRoom(this.socket.getRoom(), req.body);
                //socket.sendMessage(req.body);
            }
        );
    }

    /**
     * アクセスコントロール情報を取得
     * @param target 'origin' | 'methods' | 'headers'
     * @returns string
     */
    public getParam(target: 'origin' | 'methods' | 'headers'): string {
        return this[target].length === 0 ? '*' : this[target].join(', ');
    }

    /**
     * 許可するアクセスコントロールを追加
     * @param target 'origin' | 'methods' | 'headers'
     * @param propatie string
     * @returns SocketHubService
     */
    public setAccessControl(
        target: 'origin' | 'methods' | 'headers',
        propatie: string
    ): SocketHubService {
        if (propatie === '*') {
            this[target] = [];
            return this;
        }
        this[target].push(propatie);
        return this;
    }
}
