import { gql, GraphQLClient } from 'graphql-request';

export class GraphqlService {
    private static instance: GraphqlService;

    private endpoint: string = '';
    private header: string[] = [];

    private Client: GraphQLClient | undefined = undefined;

    public static call(): GraphqlService {
        if (!GraphqlService.instance) {
            GraphqlService.instance = new GraphqlService();
        }
        return GraphqlService.instance;
    }

    public setEndpoint(endpoint: string): GraphqlService {
        this.endpoint = endpoint;
        return this;
    }

    public setHeader(): GraphqlService {
        return this;
    }

    public getClient(): GraphQLClient {
        if (this.Client === undefined) {
            this.newClient();
        }
        return this.Client as GraphQLClient;
    }

    public async do(query: string, variables: {[key: string]: string}) {
        
        this.newClient();

        let result = false;

        if (this.Client !== undefined) {
            const _q = gql`${query}`
            result = await this.Client.request(_q, variables);
            //console.log(result);
        }

        return result;
    }

    private newClient() {
        if (this.Client === undefined) {
            this.Client = new GraphQLClient(this.endpoint, {
                headers: {},
                jsonSerializer: {
                    parse: JSON.parse,
                    stringify: JSON.stringify,
                },
            })
        }
    }
}