
export type CODEC_TYPE = 'H264' | 'VP8' | 'VP9' | 'AV1'

let remoteVideo: HTMLVideoElement | undefined = undefined

let peerConnection: RTCPeerConnection | undefined = undefined
let dataChannel: RTCDataChannel | undefined = undefined
let candidates: RTCIceCandidate[] = [];
let hasReceivedSdp = false;
let codec: CODEC_TYPE = 'H264'

let ws: WebSocket | undefined = undefined

// iceServer 
const iceServers = [
    { 'urls': 'stun:stun.l.google.com:19302' }
]
// peer connection configuration
const peerConnectionConfig = {
    'iceServers': iceServers
}

export const setRemoteVideo = (
    video: HTMLVideoElement,
    control: boolean
) => {
    remoteVideo = video
    remoteVideo.controls = control
}

export const setCodec = (codecType: CODEC_TYPE) => {
    codec = codecType
}

export const setWebSocket = (webSocket: WebSocket) => {
    ws = webSocket
}

export const getHasReceivedSdp = (): boolean => {
    return hasReceivedSdp
}

export const getDataChannel = (): RTCDataChannel | undefined => {
    return dataChannel
}

export const play = () => {
    if (remoteVideo) {
        remoteVideo.play()
    }
}

export const stop = () => {
    if (remoteVideo) {
        remoteVideo.pause()
        remoteVideo.srcObject = null
    }
}

export const sendData = (message: Uint8Array) => {
    if (dataChannel == null || dataChannel.readyState != "open") {
        return
    }
    if (dataChannel) {
        dataChannel.send(message)
    }
}

export const addCandidates = (candidate: RTCIceCandidate) => {
    if (hasReceivedSdp) addIceCandidate(candidate)
    else candidates.push(candidate)
}


const drainCandidate = () => {
    hasReceivedSdp = true;
    candidates.forEach((candidate) => addIceCandidate(candidate))
    candidates = [];
}

export const addIceCandidate = (
    candidate: RTCIceCandidate
): void => {
    if (peerConnection) peerConnection.addIceCandidate(candidate)
    else console.error('PeerConnection does not exist!')
}

const sendIceCandidate = (candidate: RTCIceCandidate) => {
    if (!ws) {
        console.error('ws DOES NOT exist!');
        return;
    }
    console.log('---sending ICE candidate ---')
    const message = JSON.stringify({ type: 'candidate', ice: candidate })
    console.log('sending candidate=' + message)
    ws.send(message)
}

/**
 * エレメント指定になっているのは、複数の映像データが来るパターンがありえるため
 * @param element HTMLVideoElement
 * @param stream MediaStream
 */
const playVideo = (
    element: HTMLVideoElement,
    stream: MediaStream
) => {
    element.srcObject = stream
}

const prepareNewConnection = async () => {
    const peer: RTCPeerConnection = new RTCPeerConnection(peerConnectionConfig)
    dataChannel = peer.createDataChannel("serial")
    //if ('ontrack' in peer) {
        if (isSafari()) {
            let tracks: MediaStreamTrack[] = [];
            peer.ontrack = (event: RTCTrackEvent) => {
                console.log('-- peer.ontrack()');
                tracks.push(event.track)
                // safari ã§å‹•ä½œã•ã›ã‚‹ãŸã‚ã«ã€ontrack ãŒç™ºç«ã™ã‚‹ãŸã³ã« MediaStream ã‚’ä½œæˆã™ã‚‹
                let mediaStream = new MediaStream(tracks);
                playVideo(remoteVideo as HTMLVideoElement, mediaStream);
            };
        } else {
            let mediaStream: MediaStream  = new MediaStream()
            playVideo(remoteVideo as HTMLVideoElement, mediaStream)
            peer.ontrack = (event: RTCTrackEvent) => {
                console.log('-- peer.ontrack()')
                mediaStream.addTrack(event.track)
            }
        }
    /* 古いAPI実装なのでおそらく使うことはない
    } else {
        peer.onaddstream = (event) => {
        console.log('-- peer.onaddstream()');
        playVideo(remoteVideo, event.stream);
        };
    }*/

    peer.onicecandidate = (event: RTCPeerConnectionIceEvent) => {
        console.log('-- peer.onicecandidate()')
        if (event.candidate) {
            console.log(event.candidate)
            sendIceCandidate(event.candidate)
        } else {
            console.log('empty ice event')
        }
    };

    peer.oniceconnectionstatechange = () => {
        console.log('-- peer.oniceconnectionstatechange()')
        console.log('ICE connection Status has changed to ' + peer.iceConnectionState)
        switch (peer.iceConnectionState) {
            case 'closed':
            case 'failed':
            case 'disconnected':
            break;
        }
    }
    peer.addTransceiver('video', {direction: 'recvonly'})
    peer.addTransceiver('audio', {direction: 'recvonly'})

    dataChannel.onmessage = function (event) {
        console.log("Got Data Channel Message:", new TextDecoder().decode(event.data))
    }
    
    return peer
}

const browser = () => {
    const ua = window.navigator.userAgent.toLocaleLowerCase();
    if (ua.indexOf('edge') !== -1) {
        return 'edge';
    }
    else if (ua.indexOf('chrome')  !== -1 && ua.indexOf('edge') === -1) {
        return 'chrome';
    }
    else if (ua.indexOf('safari')  !== -1 && ua.indexOf('chrome') === -1) {
        return 'safari';
    }
    else if (ua.indexOf('opera')   !== -1) {
        return 'opera';
    }
    else if (ua.indexOf('firefox') !== -1) {
        return 'firefox';
    }
    return ;
}

const isSafari = () => {
    return browser() === 'safari';
}

const sendSdp = (sessionDescription) => {
    if (!ws) {
        console.error('ws DOES NOT exist!');
        return;
    }
    console.log('---sending sdp ---');
    const message = JSON.stringify(sessionDescription);
    console.log('sending SDP=' + message);
    ws.send(message);
}

export const makeOffer = async () => {
    peerConnection = await prepareNewConnection();
    try {
        const sessionDescription = await peerConnection.createOffer({
            'offerToReceiveAudio': true,
            'offerToReceiveVideo': true
        })
        console.log('createOffer() success in promise, SDP=', sessionDescription.sdp);
        switch (codec) {
            case 'H264':
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'VP8');
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'VP9');
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'AV1');
                break;
            case 'VP8':
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'H264');
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'VP9');
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'AV1');
                break;
            case 'VP9':
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'H264');
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'VP8');
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'AV1');
                break;
            case 'AV1':
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'H264');
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'VP8');
                sessionDescription.sdp = removeCodec(sessionDescription.sdp, 'VP9');
                break;
        }
        await peerConnection.setLocalDescription(sessionDescription)
        console.log('setLocalDescription() success in promise')
        sendSdp(peerConnection.localDescription)
    } catch (error: any) {
        console.error('makeOffer() ERROR:', error)
    }
}

const makeAnswer = async () => {
    console.log('sending Answer. Creating remote session description...')
    if (!peerConnection) {
        console.error('peerConnection DOES NOT exist!')
        return
    }
    try {
        const sessionDescription = await peerConnection.createAnswer()
        console.log('createAnswer() success in promise')
        await peerConnection.setLocalDescription(sessionDescription)
        console.log('setLocalDescription() success in promise')
        sendSdp(peerConnection.localDescription)
        drainCandidate()
    } catch (error: any) {
        console.error('makeAnswer() ERROR:', error)
    }
}

// offer sdp
export const setOffer = async (sessionDescription: RTCSessionDescription) => {
    if (peerConnection === undefined) {
        console.error('peerConnection already exists!');
        peerConnection = await prepareNewConnection();
    }

    peerConnection.onnegotiationneeded = async () => {
        try{
            if (peerConnection === undefined) return
            await peerConnection.setRemoteDescription(sessionDescription);
            console.log('setRemoteDescription(offer) success in promise');
            makeAnswer();
        }catch(error) {
            console.error('setRemoteDescription(offer) ERROR: ', error);
        }
    }
}

export const setAnswer = async (sessionDescription: RTCSessionDescription) => {
    if (!peerConnection) {
        console.error('peerConnection DOES NOT exist!');
        return;
    }
    try {
        await peerConnection.setRemoteDescription(sessionDescription);
        console.log('setRemoteDescription(answer) success in promise');
        drainCandidate();
    } catch(error) {
        console.error('setRemoteDescription(answer) ERROR: ', error);
    }
}

/* getOffer() function is currently unused.
const getOffer = () => {
    initiator = false;
    createPeerConnection();
    sendXHR(
        ".GetOffer",
        JSON.stringify(peer_connection.localDescription),
        (respnse) => {
        peer_connection.setRemoteDescription(
            new RTCSessionDescription(respnse),
            function () {
            peer_connection.createAnswer(
                function (answer) {
                peer_connection.setLocalDescription(answer);
                }, function (e) { });
            }, function (e) {
            console.error(e);
            });
        }, true);
}
*/

const removeCodec = (
    orgsdp: string | undefined,
    codec: string
) => {
    const internalFunc = (sdp) => {
        const codecre = new RegExp('(a=rtpmap:(\\d*) ' + codec + '\/90000\\r\\n)');
        const rtpmaps = sdp.match(codecre);
        if (rtpmaps == null || rtpmaps.length <= 2) {
            return sdp;
        }
        const rtpmap = rtpmaps[2];
        let modsdp = sdp.replace(codecre, "");

        const rtcpre = new RegExp('(a=rtcp-fb:' + rtpmap + '.*\r\n)', 'g');
        modsdp = modsdp.replace(rtcpre, "");

        const fmtpre = new RegExp('(a=fmtp:' + rtpmap + '.*\r\n)', 'g');
        modsdp = modsdp.replace(fmtpre, "");

        const aptpre = new RegExp('(a=fmtp:(\\d*) apt=' + rtpmap + '\\r\\n)');
        const aptmaps = modsdp.match(aptpre);
        let fmtpmap = "";
        if (aptmaps != null && aptmaps.length >= 3) {
        fmtpmap = aptmaps[2];
        modsdp = modsdp.replace(aptpre, "");

        const rtppre = new RegExp('(a=rtpmap:' + fmtpmap + '.*\r\n)', 'g');
        modsdp = modsdp.replace(rtppre, "");
        }

        let videore = /(m=video.*\r\n)/;
        const videolines = modsdp.match(videore);
        if (videolines != null) {
        //If many m=video are found in SDP, this program doesn't work.
        let videoline = videolines[0].substring(0, videolines[0].length - 2);
        const videoelems = videoline.split(" ");
        let modvideoline = videoelems[0];
        videoelems.forEach((videoelem, index) => {
            if (index === 0) return;
            if (videoelem == rtpmap || videoelem == fmtpmap) {
            return;
            }
            modvideoline += " " + videoelem;
        })
        modvideoline += "\r\n";
        modsdp = modsdp.replace(videore, modvideoline);
        }
        return internalFunc(modsdp);
    }
    return internalFunc(orgsdp);
}

