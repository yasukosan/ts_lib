
export type SocketOptionsType = {
    reconnection: boolean
    reconnectionAttempts: number
    reconnectionDelay: number
    reconnectionDelayMax: number
    randomizationFactor: number
    timeout: number
    autoConnect: boolean
}

export type OptionsType = {
    isSSL: boolean
    host: string
    path: string
}

const SocketOptions: SocketOptionsType = {
    reconnection: true,
    reconnectionAttempts: 5,
    reconnectionDelay: 1000,
    reconnectionDelayMax: 5000,
    randomizationFactor: 0.5,
    timeout: 20000,
    autoConnect: true,
}

const Options: OptionsType = {
    isSSL: false,
    host: 'localhost',
    path: '/ws',
}

let ws: WebSocket | undefined = undefined

export const setOptions = (options: OptionsType) => {
    Object.assign(Options, options);
}

export const setSocketOptions = (options: SocketOptionsType) => {
    Object.assign(SocketOptions, options);
}

export const getWs = () => ws

export const init = (): boolean => {
    if (ws !== undefined && ws.readyState === ws.OPEN) {
        console.log('ws is already open');
        return true
    }

    // Options.isSSL = location.protocol === 'https:'
    const wsProtocol = Options.isSSL ? 'wss://' : 'ws://'

    try {
        console.log('ws init()', wsProtocol + Options.host + '/' + Options.path)
        ws = new WebSocket(wsProtocol + Options.host + '/' + Options.path)
        console.log('ws init() ws:', ws)
        // SocketOptionsの内容をwsに適用
        Object.assign(ws, SocketOptions)
        return true
    } catch (error) {
        console.error('ws init() ERROR:', error)
        return false
    }
}

export const setupListeners = (
    setOffer: (offer: RTCSessionDescription) => void,
    setAnswer: (answer: RTCSessionDescription) => void,
    getHasReceivedSdp: () => boolean,
    addIceCandidate: (candidate: RTCIceCandidate) => void,
    addCandidates: (candidate: RTCIceCandidate) => void,
) => {
    if (ws === undefined) {
        console.error('ws is undefined')
        return
    }
    ws.onopen = (e) => onWsOpen(e)
    ws.onerror = (e) => onWsError(e)
    ws.onmessage = (e) => onWsMessage(
        e,
        setOffer,
        setAnswer,
        getHasReceivedSdp,
        addIceCandidate,
        addCandidates,
    )
}

const onWsError = (error: Event) => {
    console.error('ws onerror() ERROR:', error);
}

const onWsOpen = (event: Event) => {
    console.log('ws open()');
}
const onWsMessage = (
    event: MessageEvent,
    setOffer: (offer: RTCSessionDescription) => void,
    setAnswer: (answer: RTCSessionDescription) => void,
    getHasReceivedSdp: () => boolean,
    addIceCandidate: (candidate: RTCIceCandidate) => void,
    addCandidates: (candidate: RTCIceCandidate) => void,
) => {
    console.log('ws onmessage() data:', event.data)
    const message = JSON.parse(event.data)
    if (message.type === 'offer') {
        console.log('Received offer ...')
        const offer = new RTCSessionDescription(message)
        console.log('offer: ', offer)
        setOffer(offer)
    }
    else if (message.type === 'answer') {
        console.log('Received answer ...')
        const answer = new RTCSessionDescription(message)
        console.log('answer: ', answer)
        setAnswer(answer)
    }
    else if (message.type === 'candidate') {
        console.log('Received ICE candidate ...')
        const candidate = new RTCIceCandidate(message.ice)
        console.log('candidate: ', candidate)
        if (getHasReceivedSdp()) {
            addIceCandidate(candidate)
        } else {
            addCandidates(candidate)
        }
    }
    else if (message.type === 'close') {
        console.log('peer connection is closed ...')
    }
}

