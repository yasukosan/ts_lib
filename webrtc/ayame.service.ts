import {
    connection,
} from "@open-ayame/ayame-web-sdk"
import { defaultOptions } from "@open-ayame/ayame-web-sdk"
import Connection from "@open-ayame/ayame-web-sdk/dist/connection"


export type ConnectionOptionsType = {
    signalingKey: string
    signalingUrl: string
    roomIdPrefix: string
    roomName: string
    clientId: string
    video: boolean
    audio: boolean
    simulcast: boolean
    videoCodecType: 'VP8' | 'VP9' | 'H264' | 'AV1' | 'H265'
    audioCodecType: 'OPUS' | 'ISAC' | 'G722' | 'PCMU' | 'PCMA' | 'G729' | 'ILBC' | 'AAC' | 'HEAAC'
    iceServers: RTCIceServer[]
}

const ConnectionOptions: ConnectionOptionsType = {
    signalingKey: 'iCQZqFUcJX6ttE3BAdSIW1SnMrkE4T8PgxTuiRMrD3wS7kXo',
    signalingUrl: 'wss://ayame-labo.shiguredo.app/signaling',
    roomIdPrefix: 'Yasuko@',
    roomName: 'ayame-labo-sample',
    clientId: crypto.randomUUID(),
    video: true,
    audio: true,
    simulcast: true,
    videoCodecType: "VP8",
    audioCodecType: "OPUS",
    iceServers: [
        {
            urls: "stun:stun.l.google.com:19302",
        },
    ],
}

export type StatusViewElementType = {
    roomId: HTMLSpanElement | string
    roomName: HTMLSpanElement | string
    clientId: HTMLSpanElement | string
    connectionState: HTMLSpanElement | string
    remoteVideo: HTMLVideoElement | string
    localVideo: HTMLVideoElement | string
}

const StatusViewElement = {
    roomId: '',
    roomName: '',
    clientId: '',
    connectionState: '',
    remoteVideo: 'video',
    localVideo: '',
}

const debug = true;
const options = defaultOptions;

let conn: Connection | null = null;

export const setOptions = (options: ConnectionOptionsType) => {
    Object.assign(ConnectionOptions, options);
}

export const setElements = (elements: StatusViewElementType) => {
    Object.assign(StatusViewElement, elements);
}



export const setStatusDOM = (
    roomId: string,
    roomName: string,
    clientId: string,
) => {
    const roomIdElement = document.querySelector(
        "#room-id",
    ) as HTMLSpanElement;
    if (roomIdElement) {
        roomIdElement.textContent = roomId;
    }

    const roomNameElement = document.querySelector(
        "#room-name",
    ) as HTMLSpanElement;
    if (roomNameElement) {
        roomNameElement.textContent = roomName;
    }

    const clientIdElement = document.querySelector(
        "#client-id",
    ) as HTMLSpanElement;
    if (clientIdElement) {
        clientIdElement.textContent = clientId;
    }
}

export const connect = () => {
    if (conn !== null) {
        console.log('Ayame connection is already established')
        return
    }

    options.clientId = ConnectionOptions.clientId
    options.signalingKey = ConnectionOptions.signalingKey

    const roomId = `${ConnectionOptions.roomIdPrefix}${ConnectionOptions.roomName}`
    conn = connection(ConnectionOptions.signalingUrl, roomId, options, debug)

    loadViewer()

    addListenner()
    addStream()
}

export const disconnect = () => {
    if (!conn) {
        console.log('Ayame connection is not established')
        return
    }
    conn.disconnect()
}

const addStream = () => {
    if (!conn) return
    
    conn.on("addstream", (event: any) => {
            const remoteVideoElement = document.getElementById(
                "video",
            ) as HTMLVideoElement;
            if (remoteVideoElement) {
                remoteVideoElement.srcObject = event.stream;
            }
        });
}


const addListenner = () => {
    if (!conn) return
    
    // WebRTC が確立したら connection-state に pc.connectionState 反映する
    conn.on("open", (event: Event) => {
        if (!conn) {
            return;
        }
        const pc = conn.peerConnection;
        if (pc) {
            pc.onconnectionstatechange = (event: Event) => {
            const connectionStateElement = document.getElementById(
                "connection-state",
            ) as HTMLSpanElement;
                if (connectionStateElement) {
                    // data- に connectionState を追加する
                    connectionStateElement.dataset.connectionState = pc.connectionState;
                }
            };
        }
        });
    
        conn.on("disconnect", (event: Event) => {
        if (!conn) {
            return;
        }
        conn = null;
    
        const remoteVideoElement = document.getElementById(
            "remote-video",
        ) as HTMLVideoElement;
            if (remoteVideoElement) {
                remoteVideoElement.srcObject = null;
            }
        });
    
        conn.connect(null);
}

const loadViewer = () => {
    for (const key in StatusViewElement) {
        if (typeof StatusViewElement[key] === 'string' && StatusViewElement[key] !== '') {
            StatusViewElement[key] = document.getElementById(StatusViewElement[key]) as HTMLSpanElement
        }
    }
}