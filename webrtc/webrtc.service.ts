import {
    init,
    setOptions,
    setupListeners,
    getWs,
} from './_helper/websocket.helper'

import {
    makeOffer, setWebSocket,
    setCodec as _setCodec,
    setRemoteVideo as _setRemoteVideo,
    setOffer, setAnswer, addIceCandidate, addCandidates,
    getDataChannel,
    play as _play, stop,
    sendData,
    getHasReceivedSdp
} from './_helper/webrtc.helper'

import {
    CODEC_TYPE
} from './_helper/webrtc.helper'

let peerConnection: RTCPeerConnection | undefined = undefined
let dataChannel: RTCDataChannel | undefined = undefined

let ws: WebSocket | undefined = undefined


export const setRemoteVideo = (
    videoElement: string | HTMLVideoElement = 'remote_video',
    control: boolean = true
): void => {
    const remoteVideo = typeof videoElement === 'string'
                    ? document.getElementById(videoElement) as HTMLVideoElement
                    : videoElement
    _setRemoteVideo(remoteVideo as HTMLVideoElement, control)
}

export const setCodec = (codecType: CODEC_TYPE): void => {
    _setCodec(codecType)
}

export const initialize = (
    url: string,
    path: string,
    isSSL: boolean = false,
) => {
    setOptions({
        host: url, path, isSSL})
    if (init()) {
        ws = getWs()
        setupListeners(setOffer, setAnswer, getHasReceivedSdp, addIceCandidate, addCandidates)
    }
}


export const connect = () => {
    if (ws === undefined) {
        console.error('websocket is not initialized.')
        return
    }

    console.group()
    if (!peerConnection) {
        console.log('make Offer')
        makeOffer()
    }
    else {
        console.warn('peer connection already exists.');
    }
    dataChannel = getDataChannel()
    console.groupEnd()
    setWebSocket(ws)
}

export const disconnect = () => {
    console.group();
    if (peerConnection !== undefined) {
        if (peerConnection.iceConnectionState !== 'closed') {
            peerConnection.close()
            peerConnection = undefined
            if (ws && ws.readyState === 1) {
                const message = JSON.stringify({ type: 'close' });
                ws.send(message)
            }
            console.log('sending close message')
            stop()
            return
        }
    }
    console.log('peerConnection is closed.')
    console.groupEnd()
}

export const play = () => {
    _play()
}

export const sendDataChannel = (message: any) => {
    if (message.length == 0) {
        return
    }
    sendData(message)
}