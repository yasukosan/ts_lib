import AjaxParseService from './ajax.parse.service'
import RequestService from './request.service'
export class AjaxService {
    private Request: RequestService = new RequestService()
    private APS: AjaxParseService = new AjaxParseService()
    private result: string | number | object = {}

    /**
     * URLを設定
     *
     * @param url string
     */
    public setURL(url: string): AjaxService {
        this.Request.setURL(url)
        return this
    }

    /**
     * メソッドを設定
     *
     * @param method 'GET' | 'POST' | 'DELETE' | 'UPDATE'
     */
    public setMethod(
        method: 'GET' | 'POST' | 'DELETE' | 'UPDATE'
    ): AjaxService {
        this.Request.setMethod(method)
        return this
    }

    /**
     * CSRFトークンを設定
     *
     * @param token string
     * @return AjaxService
     */
    public setCSRFToken(token: string): AjaxService {
        this.Request.setToken('csrf', token)
        return this
    }

    /**
     * Bearerトークンを設定
     *
     * @param token string
     * @returns AjaxService
     */
    public setBearerToken(token: string): AjaxService {
        this.Request.setToken('bearer', token);
        return this
    }

    /**
     * ヘッダーを設定
     *
     * @param header string
     */
    public setHeader(header: string): AjaxService {
        this.Request.setHeader(header);
        return this
    }

    /**
     * 送信データを設定
     *
     * @param body any
     */
    public setBody(
        body: string | string[] | {[key: string]: string}
    ): AjaxService {
        this.Request.setBody(body)
        return this
    }

    /**
     * ヘッダー、送信データをHttpオプション形式に変換
     */
    public buildRequestParam(type: 'api' | 'form' = 'api'): AjaxService {
        if (type === 'api') {
            this.Request.buildRequestParam()
        }
        if (type === 'form') {
            this.Request.buildFormParam()
        }
        return this
    }

    public getStatusCode(): number {
        return this.APS.getResponce().status;
    }

    /**
     * 通信結果の受け取り
     */
    public async getResult(

    ): Promise<string | number | object> {
        await this.run()
        return this.result
    }

    /**
     * AjaxParseServiceObjectを返す
     *
     * @return AjaxParseService
     */
    public callAjaxParseService(): AjaxParseService {
        return this.APS
    }

    /**
     * サーバーレスポンスを初期化
     *
     * @return this
     */
    public resetResult(): AjaxService {
        this.APS.allReset()
        return this
    }

    /**
     * サーバーリクエストを初期化
     *
     * @return this
     */
    public resetRequest(): AjaxService {
        this.Request.reset()
        return this
    }

    /**
     * サーバーにリクエストを投げる
     * @return Promise<any>
     */
    private async run(): Promise<void> {
        const url = this.Request.buildURL()
        // console.log(this.Request.buildOption())
        return await fetch(url, this.Request.buildOption())
            .then(async (res) => {
                // console.log(res);
                await this.APS.setResult(res)
                this.result = this.APS.check().getBody()
            })
            .catch(async (e) => {
                console.error(e)
                await this.APS.setResult(e)
                this.result = this.APS.check().getBody()
            })
    }
}
