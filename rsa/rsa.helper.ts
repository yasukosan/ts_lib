import RSAService from './rsa.service';
import { bufferToBase64url } from '../fido/converter.service';

export type Base64urlString = string;

export class RSAHelper {
    public constructor() {
        const rsa = new RSAService();

        rsa.generateKey();

        // const buffer = new Uint8Array(10);
        //const buffer = new Array(1000);
        const buffer = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';

        const enc2 = rsa.publicEncrypt(buffer);
        console.log('enc2=', enc2);

        const dec2 = rsa.privateDecrypt(enc2);
        console.log('OK? dec2=', dec2);

        const a = this.bufferToBase64url(dec2);
        const b = this.base64urlToBuffer(a);
        console.log(a);
        console.log(b);

        const pem1 = rsa.der2pem('pkcs1-private', rsa.getPrivateKey());
        console.log(pem1);

        const der1 = rsa.pem2der('pkcs1-private', pem1);
        console.log(der1);

        const pem2 = rsa.der2pem('pkcs1-public', rsa.getPublicKey());
        console.log(pem2);
        const der2 = rsa.pem2der('pkcs1-public', pem2);
        console.log(der2);

        const sig = rsa.sign(buffer);
        console.log('sig=', sig);

        const result = rsa.verify(buffer, sig);
        console.log('result=', result);
    }

    public bufferToBase64url(buffer: ArrayBuffer): Base64urlString {
        const byteView: any = new Uint8Array(buffer);
        let str = '';
        for (const charCode of byteView) {
            str += String.fromCharCode(charCode);
        }

        const base64String = btoa(str);

        const base64urlString = base64String
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=/g, '');
        return base64urlString;
    }

    public base64urlToBuffer(baseurl64String: string): ArrayBuffer {
        const padding = '=='.slice(0, (4 - (baseurl64String.length % 4)) % 4);
        const base64String =
            baseurl64String.replace(/-/g, '+').replace(/_/g, '/') + padding;

        const str = atob(base64String);

        const buffer = new ArrayBuffer(str.length);
        const byteView = new Uint8Array(buffer);
        for (let i = 0; i < str.length; i++) {
            byteView[i] = str.charCodeAt(i);
        }
        return buffer;
    }
}
