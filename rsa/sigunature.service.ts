import CryptService from './crypt.service';

export default class SigunatureService {
    private CS: any = {};
    public constructor() {
        this.CS = new CryptService();
    }

    public async getSigunature(message: string, pubkeys: string): Promise<any> {
        const hash = await this.getSHA256Hash(message);

        this.CS.setKeys({ public: pubkeys, private: '' });
        const sigunature: string = await this.CS.publicEncrypt(hash);

        return {
            hash: hash,
            sigunature: sigunature
        };
    }

    public async getSHA256Hash(message: string): Promise<string> {
        // encode as (utf-8) Uint8Array
        const msgUint8 = new TextEncoder().encode(message);
        // hash the message
        const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);
        // convert buffer to byte array
        const hashArray = Array.from(new Uint8Array(hashBuffer));
        // convert bytes to hex string
        const hashHex = hashArray
            .map((b) => b.toString(16).padStart(2, '0'))
            .join('');
        return hashHex;
    }

    public async getSHA512Hash(message: string): Promise<string> {
        const msgUint8 = new TextEncoder().encode(message);
        const hashBuffer = await crypto.subtle.digest('SHA-512', msgUint8);
        const hashArray = Array.from(new Uint8Array(hashBuffer));
        const hashHex = hashArray
            .map((b) => b.toString(16).padStart(2, '0'))
            .join('');
        return hashHex;
    }
}
