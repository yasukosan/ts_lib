const RSA = require('node-rsa');

/**
 * RSA鍵をブラウザ側で生成し、相互にやり取りする場合を想定
 * サーバー側で生成した鍵を使用する場合は
 * アルゴリズムの違いでエラーになるので注意
 */

export type Base64urlString = string;
export default class RSAService {
    private Key: any = {};
    private Priv = '';
    private Pub = '';

    /**
     * 鍵情報を取得
     * @returns
     */
    public getKeys(): object {
        return {
            public: this.Pub,
            private: this.Priv
        };
    }

    /**
     * 公開鍵を取得
     * @returns
     */
    public getPublicKey(): string {
        return this.Pub;
    }

    /**
     * 秘密鍵を取得
     * @returns
     */
    public getPrivateKey(): string {
        return this.Priv;
    }

    /**
     * 鍵情報をセット
     * @param keys
     * @returns
     */
    public setKeys(keys: any): RSAService {
        this.Priv = keys.private;
        this.Pub = keys.public;
        return this;
    }

    /**
     * RSA公開鍵ペア作成
     *
     * @param bits
     * @returns RSAService
     */
    public generateKey(bits = 1024): RSAService {
        this.Key = new RSA({ b: bits });

        this.Priv = this.Key.exportKey('pkcs1-private-der');
        this.Pub = this.Key.exportKey('pkcs1-public-der');

        return this;
    }

    /**
     * 鍵をPEM形式からDER形式に変換
     *
     * @param scheme
     * @param pem
     * @returns
     */
    public pem2der(scheme: any, pem: any): any {
        const rsa = new RSA(pem, scheme + '-pem');
        return rsa.exportKey(scheme + '-der');
    }

    /**
     * 鍵をDER形式からPEM形式に変換
     *
     * @param scheme
     * @param key
     * @returns
     */
    public der2pem(scheme: any, key: any): any {
        const der = Buffer.from(key);
        const rsa = new RSA(der, scheme + '-der');
        return rsa.exportKey(scheme + '-pem');
    }

    /**
     * DER形式からDER形式に変換
     * 鍵自体の形式をPKCS#8からPKCS#1に変換
     *
     * @param key
     * @param src_scheme
     * @param dest_scheme
     * @returns
     */
    public der2der(key: any, src_scheme: any, dest_scheme: any): any {
        const der = Buffer.from(key);
        const rsa = new RSA(der, src_scheme + '-der');
        return rsa.exportKey(dest_scheme + '-der');
    }

    /**
     * 公開鍵で暗号化
     *
     * @param buffer
     * @returns
     */
    public publicEncrypt(buffer: any): any {
        const input = Buffer.from(buffer);
        const der = Buffer.from(this.Pub);
        // const rsa = new RSA(der, 'pkcs1-public-der', { encryptionScheme : 'pkcs1_oaep' });
        const rsa = new RSA(der, 'pkcs8-public-der');
        const enc = rsa.encrypt(input);
        return enc;
    }

    /**
     * 秘密鍵で復号化
     *
     * @param buffer any 暗号データ
     * @returns any 復号データ
     */
    public privateDecrypt(buffer: any): any {
        const input = Buffer.from(buffer);
        const der = Buffer.from(this.Priv);
        const rsa = new RSA(der, 'pkcs1-private-der', {
            encryptionScheme: 'pkcs1_oaep'
        });
        const dec = rsa.decrypt(input);
        return dec;
    }

    /**
     * 秘密鍵で署名
     *
     * @param buffer
     * @returns
     */
    public sign(buffer: any): any {
        const input = Buffer.from(buffer);
        const der = Buffer.from(this.Priv);
        const rsa = new RSA(der, 'pkcs1-private-der');
        const sig = rsa.sign(input);
        return sig;
    }

    /**
     * 公開鍵で検証
     *
     * @param buffer
     * @param signature
     * @returns
     */
    public verify(buffer: any, signature: any): boolean {
        const input = Buffer.from(buffer);
        const der = Buffer.from(this.Pub);
        const sig = Buffer.from(signature);
        const rsa = new RSA(der, 'pkcs1-public-der');
        const result = rsa.verify(input, sig);
        return result;
    }

    public bufferToBase64url(buffer: ArrayBuffer): Base64urlString {
        const byteView: any = new Uint8Array(buffer);
        let str = '';
        for (const charCode of byteView) {
            str += String.fromCharCode(charCode);
        }

        const base64String = btoa(str);

        const base64urlString = base64String
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=/g, '');
        return base64urlString;
    }

    public base64urlToBuffer(baseurl64String: string): ArrayBuffer {
        const padding = '=='.slice(0, (4 - (baseurl64String.length % 4)) % 4);
        const base64String =
            baseurl64String.replace(/-/g, '+').replace(/_/g, '/') + padding;

        const str = atob(base64String);

        const buffer = new ArrayBuffer(str.length);
        const byteView = new Uint8Array(buffer);
        for (let i = 0; i < str.length; i++) {
            byteView[i] = str.charCodeAt(i);
        }
        return buffer;
    }
}
