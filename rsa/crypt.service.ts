import JSEncrypt from 'jsencrypt';
import CryptoJS from 'crypto-js';
export type Base64urlString = string;
export default class CryptService {
    private Priv = '';
    private Pub = '';
    private Crypt: any = {};

    public constructor() {
        this.Crypt = new JSEncrypt({});
    }

    /**
     * 鍵情報をセット
     * @param keys
     * @returns
     */
    public setKeys(keys: any): CryptService {
        this.Priv = keys.private;
        this.Pub = keys.public;
        return this;
    }

    /**
     * 公開鍵で暗号化
     *
     * @param buffer
     * @returns string
     */
    public publicEncrypt(buffer: any): string {
        this.Crypt.setPublicKey(this.Pub);
        return this.Crypt.encrypt(buffer);
    }

    /**
     * 秘密鍵で復号化
     *
     * @param buffer
     * @returns
     */
    public privateDecrypt(buffer: any): any {
        this.Crypt.setPrivateKey(this.Priv);
        return this.Crypt.decrypt(buffer);
    }

    /**
     * 署名（秘密鍵）
     * @param buffer string 署名文字列（文字列になっていれば何でも可）
     * @returns any | string 署名が返る
     */
    public sign(buffer: string): any {
        this.Crypt.setPrivateKey(this.Priv);
        return this.Crypt.sign(buffer, CryptoJS.SHA256, 'sha256');
    }

    /**
     * 署名の検証 (公開鍵)
     * @param buffer
     * @param sigunature
     * @returns
     */
    public verify(buffer: string, sigunature: string): boolean {
        this.Crypt.setPublicKey(this.Pub);
        return this.Crypt.verify(buffer, sigunature, CryptoJS.SHA256);
    }
}
