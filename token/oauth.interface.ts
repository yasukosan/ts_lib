export interface TokenInterface {
    access_token: string;
    refresh_token: string;
    expires_in: number;
    token_type: string;
    [key: string]: string | number;
}
/**
 * サーバー保管されているアクセストークン情報の型
 */
export interface AccessTokenInterface {
    client: {
        created_at: string;
        id: string;
        name: string;
        password_client: boolean;
        personal_access_client: boolean;
        provider: string | null;
        redirect: string;
        revoked: boolean;
        updated_at: string;
    };
    client_id: string;
    created_at: string;
    expires_at: string;
    id: string;
    name: string;
    revoked: boolean;
    scopes: object;
    updated_at: string;
    user_id: number;
}

export interface ClientsInterface {
    [key: string]: string | number;
}

export interface GrantInterface {
    grant_type: string;
    client_id: string;
    client_secret: string;
    username: string;
    password: string;
    scope: string;
    [key: string]: string;
}

export interface ServerInterface {
    bearer?: string;
    csrf?: string;
    url: string;
    method: string;
    header: Array<string>;
    body: any;
    [key: string]: any;
}

export const initialStateToken = {
    access_token: '',
    refresh_token: '',
    expires_in: 1800,
    token_type: ''
};

export const initialStateAccessToken = {
    client: {
        created_at: '',
        id: '',
        name: '',
        password_client: false,
        personal_access_client: false,
        provider: '',
        redirect: '',
        revoked: false,
        updated_at: ''
    },
    client_id: '',
    created_at: '',
    expires_at: '',
    id: '',
    name: '',
    revoked: false,
    scopes: {},
    updated_at: '',
    user_id: 0
};

export const initialStateGrant = {
    grant_type: 'password',
    client_id: '',
    client_secret: '',
    username: '',
    password: '',
    scope: ''
};

export const initialStateServer = {
    bearer: '',
    csrf: '',
    url: 'https://yasukosan.dip.jp/laravel-2fa-fido/public/',
    method: 'POST',
    header: ['Content-type: application/json', 'Accept: application/json'],
    body: ''
};
