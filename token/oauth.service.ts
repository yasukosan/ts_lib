/*
import {
    // Interface読み込み
    TokenInterface, GrantInterface,
    // イニシャルステートの読み込み
    initialStateGrant, initialStateToken,
} from './oauth.interface';*/

import AccessTokenService from './manage/access_token.service';
import CallPersonalAccessTokenService from './call/call_personal_access_token.service';
import CallClientsService from './call/call_clients.service';
import CallTokenService from './call/call_token.service';
import TokenService from './token.service';
import SaveLoadService from './helper/save_load.service';

export default class OauthService {
    // public constructor() {}

    /**
     * PersonalTokenServiceを返す
     *
     * @return AccessTokenService
     */
    public callAccessTokenObj(): AccessTokenService {
        return new AccessTokenService();
    }

    public callPersonalAccessTokenService(): CallPersonalAccessTokenService {
        return new CallPersonalAccessTokenService();
    }

    /**
     * ClientServiceを返す
     *
     * @return CallClientsService
     *
     * @method getClients() CallClientsService
     * @method addClients() CallClientsService
     * @method updateClients(id: number) CallClientsService
     * @method delClients(id: number) CallClientsService
     */
    public callClientsService(): CallClientsService {
        return new CallClientsService();
    }

    public callTokenService(): CallTokenService {
        return new CallTokenService();
    }

    /**
     * TokenServiceを返す
     *
     * @return TokenService
     */
    public callTokenObj(): TokenService {
        return new TokenService();
    }

    /**
     * SaveLoadSerivceを返す
     */
    public callSaveLoadObj(): SaveLoadService {
        return new SaveLoadService();
    }

    public saveToken() {}
}
