import SaveLoadService from './helper/save_load.service';
import { initialStateToken, TokenInterface } from './oauth.interface';
import { Token } from './manage/local_token.service';
import StateusService from '../status/status.service';
import StatusService from '../status/status.service';

/**
 *
 * 取得済み、ローカル保管中のToken管理サービス
 *
 *
 *
 */
export default class TokenService {
    private SLS: SaveLoadService;
    private SS: StateusService;

    private TOKEN: Token = new Token();

    public constructor(token: TokenInterface = initialStateToken) {
        this.SLS = new SaveLoadService();
        this.SS = new StatusService();
        if (token !== initialStateToken) {
            this.TOKEN.setToken(token);
        }
    }

    /**
     * 成否ステータス管理オブジェクトを返す
     *
     * @return StatusService
     */
    public getStatusObj(): StateusService {
        return this.SS;
    }

    /**
     * トークンを設定
     *
     * @param token TokenInterface
     * @return this TokenService
     */
    public setToken(token: TokenInterface): TokenService {
        this.TOKEN.setToken(token);
        return this;
    }

    /**
     * TokenServiceオブジェクトを返す
     * @return Token
     */
    public callToken(): Token {
        return this.TOKEN;
    }

    /**
     * Tokenオブジェクトを返す
     * @return Token
     */
    public getToken(): TokenInterface {
        return this.TOKEN.getToken();
    }

    /**
     * トークンを保存（先にトークンが渡されていること）
     *
     * @return Promise<TokenService>
     */
    public async saveToken(): Promise<TokenService> {
        await this.SLS.setToken(this.TOKEN).saveToken();
        if (this.SLS.getStatusObj().checkError()) {
            console.error('保存失敗しとるがな');
            this.SS.setError('Token Save Error');
        }
        return this;
    }

    /**
     * 保存済みトークンの呼び出し
     * 保存データが無い、欠損している場合initilaStateTokenで初期化された
     * Tokenオブジェクトが返る
     *
     * @return Promise<Token>
     */
    public async loadToken(): Promise<TokenInterface> {
        this.TOKEN.setToken(await this.SLS.loadToken());
        if (this.SLS.getStatusObj().checkError()) {
            console.error('読み込み失敗しとるがな');
            return initialStateToken;
        }
        return this.TOKEN.getToken();
    }
}
