import AjaxHelper from '../../http/ajax.service';
import {
    // Interface読み込み
    AccessTokenInterface
} from '../oauth.interface';

import { Param, Paths } from '../conf/config.service';
import { buildParam } from '../helper/helper.service';

export default class CallClientsService {
    private aj: AjaxHelper;
    private PARAM: Param;
    private AccessTokens: Array<AccessTokenInterface> = [];
    private Clients: { [key: number]: AccessTokenInterface } = {};

    public constructor() {
        this.PARAM = new Param();
        this.aj = new AjaxHelper();
    }

    /**
     * API呼び出し用サーバーオプションの設定
     *
     * @param data
     * @return AccessTokenSetvice
     */
    public setServerParam(data: any): CallClientsService {
        this.PARAM.setParam(data);
        return this;
    }

    /**
     * トークン情報を設定
     *
     * @param tokens
     * @return AccessTokenSetvice
     */
    public setPersonalToken(
        tokens: Array<AccessTokenInterface>
    ): CallClientsService {
        this.AccessTokens = tokens;
        return this;
    }

    /**
     * 認証ユーザーの全てのクライアントを取得
     */
    public async getClients() {
        const p: { [key: string]: any } = buildParam(this.PARAM);
        const op = Paths.clients_get;

        this.Clients = await this.aj
            .setURL(p.url + op.path)
            .setMethod(op.method)
            .setHeader(p.header)
            .buildRequestParam()
            .getResult();
        return this.Clients;
    }

    /**
     * 新規にクライアントを作成
     */
    public async addClients() {
        const p: { [key: string]: any } = buildParam(this.PARAM);
        const op = Paths.clients_add;

        this.Clients = await this.aj
            .setURL(p.url + op.path)
            .setMethod(op.method)
            .setHeader(p.header)
            .setBody(p.body)
            .buildRequestParam()
            .getResult();
        return this.Clients;
    }

    /**
     * クライアントを更新
     *
     * @param id number クライアントID
     */
    public async updateClients(id: string) {
        const p: { [key: string]: any } = buildParam(this.PARAM);
        const op = Paths.clients_update;

        this.Clients = await this.aj
            .setURL(p.url + op.path + '/' + id)
            .setMethod(op.method)
            .setHeader(p.header)
            .setBody(p.body)
            .buildRequestParam()
            .getResult();
        return this.Clients;
    }

    /**
     * クライアントを削除
     *
     * @param id string クライアントID
     */
    public async deleteClients(id: string) {
        const p: { [key: string]: any } = buildParam(this.PARAM);
        console.log(p);
        const op = Paths.clients_delete;

        this.Clients = await this.aj
            .setURL(p.url + op.path + '/' + id)
            .setMethod(op.method)
            .setHeader(p.header)
            .setBody(p.body)
            .buildRequestParam()
            .getResult();
        return this.Clients;
    }
}
