import AjaxHelper from '../../http/ajax.service';
import {
    // Interface読み込み
    AccessTokenInterface
} from '../oauth.interface';

import { Param, Paths } from '../conf/config.service';
import { buildParam } from '../helper/helper.service';

export default class CallTokenService {
    private aj: AjaxHelper;
    private PARAM: Param;
    private Tokens: { [key: number]: AccessTokenInterface } = {};

    public constructor() {
        this.PARAM = new Param();
        this.aj = new AjaxHelper();
    }

    /**
     * API呼び出し用サーバーオプションの設定
     *
     * @param data
     * @return AccessTokenSetvice
     */
    public setServerParam(data: any): CallTokenService {
        this.PARAM.setParam(data);
        return this;
    }
    /**
     * 認証ユーザーの全てのクライアントを取得
     */
    public async getTokens() {
        const p: { [key: string]: any } = buildParam(this.PARAM);
        const op = Paths.token_get;

        this.Tokens = await this.aj
            .setURL(p.url + op.path)
            .setMethod(op.method)
            .setHeader(p.header)
            .buildRequestParam()
            .getResult();
        return this.Tokens;
    }

    /**
     * 新規にクライアントを作成
     */
    public addClients() {}

    /**
     * クライアントを更新
     *
     * @param id number クライアントID
     */
    public updateClients(id: number) {}

    /**
     * クライアントを削除
     *
     * @param id number クライアントID
     */
    public delClients(id: number) {}
}
