import AjaxHelper from '../../http/ajax.service';
import {
    // Interface読み込み
    AccessTokenInterface,
    TokenInterface
} from '../oauth.interface';

import { Param, Paths } from '../conf/config.service';
import { buildParam } from '../helper/helper.service';

export default class CallScopeService {
    private aj: AjaxHelper;
    private PARAM: Param;
    private Clients: { [key: number]: AccessTokenInterface } = {};

    public constructor() {
        this.PARAM = new Param();
        this.aj = new AjaxHelper();
    }
    /**
     * 認証ユーザーの全てのクライアントを取得
     */
    public getClients() {}

    /**
     * 新規にクライアントを作成
     */
    public addClients() {}

    /**
     * クライアントを更新
     *
     * @param id number クライアントID
     */
    public updateClients(id: number) {}

    /**
     * クライアントを削除
     *
     * @param id number クライアントID
     */
    public delClients(id: number) {}
}
