import AjaxHelper from '../../http/ajax.service';
import {
    // Interface読み込み
    AccessTokenInterface
} from '../oauth.interface';

import { Param, Paths } from '../conf/config.service';
import { buildParam } from '../helper/helper.service';

export default class CallPersonalAccessTokenService {
    private aj: AjaxHelper;
    private PARAM: Param;
    private AccessTokens: Array<AccessTokenInterface> = [];
    private Clients: { [key: number]: AccessTokenInterface } = {};

    public constructor() {
        this.PARAM = new Param();
        this.aj = new AjaxHelper();
    }

    /**
     * API呼び出し用サーバーオプションの設定
     *
     * @param data
     * @return AccessTokenSetvice
     */
    public setServerParam(data: any): CallPersonalAccessTokenService {
        this.PARAM.setParam(data);
        return this;
    }

    /**
     * トークン情報を設定
     *
     * @param tokens
     * @return AccessTokenSetvice
     */
    public setPersonalToken(
        tokens: Array<AccessTokenInterface>
    ): CallPersonalAccessTokenService {
        this.AccessTokens = tokens;
        return this;
    }

    /**
     * 作成済みトークン情報の一覧を取得
     *
     * @return Promise<Array<AccessTokenInterface>>
     */
    public async getPersonalAccessTokens(): Promise<
        Array<AccessTokenInterface>
        > {
        const p: { [key: string]: any } = buildParam(this.PARAM);
        const op = Paths.personaltoken_get;

        this.AccessTokens = await this.aj
            .setURL(p.url + op.path)
            .setMethod(op.method)
            .setHeader(p.header)
            .buildRequestParam()
            .getResult();
        return this.AccessTokens;
    }

    /**
     * PersonalTokenを新規に発行
     *
     * POSTプロパティ{
     *  name: string
     *  scope: []
     * }
     * @return Promise<AccessTokenInterface>
     */
    public async addPersonalAccessToken(): Promise<AccessTokenInterface> {
        const p: { [key: string]: any } = buildParam(this.PARAM);
        const op = Paths.personaltoken_add;

        return await this.aj
            .setURL(p.url + op.path)
            .setMethod(op.method)
            .setHeader(p.header)
            .setBody(p.body)
            .buildRequestParam()
            .getResult();
    }

    /**
     */
    public async delPersonalAccessToken(id: number): Promise<void> {
        const p: { [key: string]: any } = buildParam(this.PARAM);
        const op = Paths.personaltoken_delete;

        return await this.aj
            .setURL(p.url + op.path + '/' + id)
            .setMethod(op.method)
            .setHeader(p.header)
            .buildRequestParam()
            .getResult();
    }
}
