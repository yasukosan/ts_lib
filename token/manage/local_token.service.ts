import { initialStateToken, TokenInterface } from '../oauth.interface';
import { checkExpired } from '../helper/helper.service';

/**
 *
 * 取得したトークンを成形し、内部保持、管理するサービス
 *
 *
 */
export class Token {
    private TOKEN: TokenInterface = initialStateToken;

    public constructor(token: TokenInterface = initialStateToken) {
        if (token !== initialStateToken) {
            this.TOKEN = token;
        }
    }

    /**
     * トークンデータを設定
     * 必要なプロパティーに抜けがある場合は
     * TokenInterface型に変換
     *
     * @param token TokenInterface
     * @return Token
     */
    public setToken(token: TokenInterface): Token {
        if (!this.checkToken(token)) {
            this.TOKEN = this.moldingToken(token);
        } else {
            this.TOKEN = token;
        }
        return this;
    }

    /**
     * トークンデータを返す
     *
     * @return TokenInterface
     */
    public getToken(): TokenInterface {
        return this.TOKEN;
    }

    /**
     * Accessトークンを返す
     * 設定されていない場合は「""」が返る
     *
     * @return string トークン文字列
     */
    public getAccessToken(): string {
        return this.TOKEN.access_token;
    }

    /**
     * Refreshトークンを返す
     * 設定されていない場合は「""」が返る
     *
     * @return string トークン文字列
     */
    public getRefreshToken(): string {
        return this.TOKEN.refresh_token;
    }

    /**
     * トークンが有効か確認
     * 他所でも使用する関係で実装は「helper.service」
     *
     * @return boolean 有効：true、無効：false
     */
    public checkExpired(): boolean {
        return checkExpired(this.TOKEN);
    }

    /**
     * initialStateTokenと一致しているか判定
     *
     * @return boolean 一致：true、相違：false
     */
    public checkDefault(): boolean {
        return this.TOKEN === initialStateToken ? true : false;
    }

    /**
     * 与えられたトークンが必要なプロパティーを
     * 持っているか確認
     *
     * @param token any 検証データ
     * @return boolean
     */
    public checkToken(token: any): boolean {
        let checker = true;
        for (const key in initialStateToken) {
            if (Object.prototype.hasOwnProperty.call(token, key)) {
                checker = false;
            }
        }
        return checker;
    }

    /**
     * トークンをJSON文字列に変換したものを返す
     *
     * @return string
     */
    public tokenToJson(): string {
        return JSON.stringify(this.TOKEN);
    }

    /**
     * Json文字列をTokenに復元し返す
     * ・復元後のオブジェクトに必要なプロパティーが含まれない
     * ・JSON.parse処理で例外が出る
     * 場合にはinitilaStateTokenを返す
     *
     * @param token JSON文字列
     * @return TokenInterface
     */
    public jsonToToken(token: string): TokenInterface {
        try {
            const _token = JSON.parse(token);
            return this.checkToken(_token) ? _token : initialStateToken;
        } catch (error) {
            return initialStateToken;
        }
    }

    /**
     * トークン情報をTokenPropsInterface型に成形する
     * 無いデータは「''」になる
     *
     * @return TokenInterface
     */
    public moldingToken(token: any): TokenInterface {
        return {
            access_token: token['access_token'] ? token['access_token'] : '',
            refresh_token: token['refresh_token'] ? token['refresh_token'] : '',
            expires_in: token['expires_in'] ? token['expires_in'] : '',
            token_type: token['token_type'] ? token['token_type'] : ''
        };
    }
}
