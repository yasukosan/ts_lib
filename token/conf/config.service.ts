import {
    GrantInterface,
    initialStateGrant,
    ServerInterface,
    initialStateServer
} from '../oauth.interface';

export const Paths: { [key: string]: any } = {
    token_get: { path: 'oauth/token', method: 'POST', id: false },
    token_refresh: { path: 'oauth/token/refresh', method: 'POST', id: false },
    token_info: { path: 'oauth/tokens', method: 'GET', id: true },
    token_delete: { path: 'oauth/tokens', method: 'DELETE', id: true },
    personaltoken_get: {
        path: 'oauth/personal-access-tokens',
        method: 'GET',
        id: false
    },
    personaltoken_delete: {
        path: 'oauth/personal-access-tokens',
        method: 'DELETE',
        id: true
    },
    personaltoken_add: {
        path: 'oauth/personal-access-tokens',
        method: 'POST',
        id: false
    },
    clients_get: { path: 'oauth/clients', method: 'GET', id: false },
    clients_add: { path: 'oauth/clients', method: 'POST', id: false },
    clients_delete: { path: 'oauth/clients', method: 'DELETE', id: true },
    clients_update: { path: 'oauth/clients', method: 'PUT', id: true },
    scopes_get: { path: 'oauth/scopes', method: 'GET', id: true },
    authorize_start: { path: 'redirect', method: 'POST', id: false },
    authorize_delete: { path: 'redirect', method: 'POST', id: false }
};

export const TokenIndex = {
    token: { key: 'token' },
    personalToken: { key: 'personalToken' },
    clients: { key: 'clients' }
};

export class Param {
    // GrantToken呼び出しオプション
    private GRANT: GrantInterface = initialStateGrant;
    // APIコールオプション
    private PARAM: ServerInterface = initialStateServer;

    private Path: { [key: string]: string | boolean } = {};
    private _ORG: any = {};

    /**
     * GrantToken要求オプションを全て返す
     *
     * @return GrantInterface
     */
    public getGrant(): GrantInterface {
        return this.GRANT;
    }

    /**
     * APIコールオプションを全て返す
     *
     * @return
     */
    public getParam(): ServerInterface {
        return this.PARAM;
    }

    /**
     * API接続先パスを全て返す
     *
     * @return {[key: string]: string | boolean}
     */
    public getPath(): { [key: string]: string | boolean } {
        return this.Path;
    }

    /**
     * configに保存するデータを設定
     * 内容を問わず一時保存させる
     *
     * @param raw any
     * @return Param
     */
    public setRawData(raw: any): Param {
        this._ORG = raw;
        return this;
    }

    /**
     * PasswordGrantToken要求オプションを設定
     * GrantInterfaceで設定されている項目のみ設定
     *
     * @param data GrantInterface
     * @return Param
     */
    public setGrant(data: any): Param {
        for (const key in this.GRANT) {
            if (Object.prototype.hasOwnProperty.call(this.GRANT, key)) {
                if (key in data) {
                    this.GRANT[key] = data[key];
                }
            }
        }
        return this;
    }

    /**
     * パラメータをセットする
     * 雛形パラメータに合致するキーのみ更新
     *
     * @param data
     */
    public setParam(data: any): Param {
        this.Path = 'job' in data ? Paths[data['job']] : {};
        this.setRawData(data);

        for (const key in this.PARAM) {
            if (Object.prototype.hasOwnProperty.call(this.PARAM, key)) {
                this.overrideParam(key);
            }
        }

        return this;
    }

    /**
     * パラメータの上書き
     * Path情報を優先して上書き
     *
     * @param key
     */
    private overrideParam(key: string): void {
        if (key in this.Path) {
            this.PARAM[key] = this.Path[key];
        } else if (key in this._ORG) {
            this.PARAM[key] = this._ORG[key];
        }
    }
}
