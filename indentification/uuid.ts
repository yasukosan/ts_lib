import * as uuid from 'uuid';
/**
 * https://github.com/uuidjs/uuid
 * 
 * 
 */

export class uuidService
{
    private static instance: uuidService;

    public static call() {
        if (!uuidService.instance) {
            uuidService.instance = new uuidService();
        }
        return uuidService.instance;
    }

    /**
     * 0 ベースのUUID作成
     * 例：'00000000-0000-0000-0000-000000000000'
     * @returns string
     */
    public getNIL(): string
    {
        return uuid.NIL;
    }

    /**
     * Version4のUUID作成
     * @returns string
     */
    public getUUIDv4(): string
    {
        return uuid.v4();
    }

    /**
     * Version5のUUID作成
     * 
     * nameがURLの場合、namespace判定となりURLNameSpaceでUUIDを作成
     * @param name string 
     * @param namespace string
     * @returns string
     */
    public getUUIDv5(name: string, namespace: string): string
    {
        const check = (n: string): boolean => {
            try {
                new URL(n);
                return true;
            } catch (error) {
                return false;
            }
        };

        return check(name) ? uuid.v5(name, uuid.v5.URL) : uuid.v5(name, namespace);
    }

    /**
     * UUIDのバージョン情報を返す
     * @param id string UUID v1 | v4 | v5
     * @returns number 
     */
    public checkVersion(id: string): number
    {
        return uuid.version(id);
    }

    /**
     * UUIDをUint8Arrayに変換
     * @param id string UUID v1 | v4 | v5
     * @returns Uint8Array
     */
    public stringToUint8Array(id: string): Uint8Array
    {
        return uuid.parse(id) as Uint8Array;
    }

    /**
     * Uint8ArrayをStringに変換
     * @param id Uint8Array
     * @returns string UUID
     */
    public uint8ArrayToString(id: Uint8Array): string
    {
        return uuid.stringify(id);
    }
}