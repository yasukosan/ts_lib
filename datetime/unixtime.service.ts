export type addType =
    | 'day'
    | 'week'
    | 'month'
    | 'year'
    | 'hour'
    | 'minute'
    | 'second';

export type dateFormat = {
    date: string;
    datetime: string;
    time: string;
    [key: string]: string;
};

export class UnixTime {
    private FORMAT_LIST: dateFormat = {
        date: 'YYYY/MM/DD',
        datetime: 'YYY/MM/DD HH:mm:ss',
        time: 'HH:mm:ss'
    };

    private FORMAT = 'date';
    private UNIXTIME = 0;

    public constructor(now = 0) {
        if (!now) {
            const _now = new Date();
            this.UNIXTIME = Math.floor(_now.getTime() / 1000);
        } else {
            this.UNIXTIME = now;
        }
    }

    public getUnixTime(): number {
        return this.UNIXTIME;
    }
}
