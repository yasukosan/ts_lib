import { CalcHelper } from './helper/calc.henlper';

/*
// 部位ごとの座標配列、格納配列数は部位により可変
annotations
	leftCheek, leftEyeIris, silhouette,
    lipsUpperOuter, lipsLowerOuter, lipsUpperInner, lipsLowerInner
	rightEyeUpper0, rightEyeLower0, rightEyeUpper1, rightEyeLower1, rightEyeUpper2
	rightEyeLower2, rightEyeLower3, rightEyebrowUpper, rightEyebrowLower, rightEyeIris
	leftEyeUpper0, leftEyeLower0, leftEyeUpper1, leftEyeLower1, leftEyeUpper2
	leftEyeLower2, leftEyeLower3, leftEyebrowUpper, leftEyebrowLower, leftEyeIris
	midwayBetweenEyes
	noseTip, noseBottom, noseRightCorner, noseLeftCorner
	rightCheek, leftCheek

// 検出した顔座標
boundingBox
 bottomRight: array
 topLeft: array
 faceInViewConfidence: number

// メッシュ座標の配列
mesh        // 顔切り抜き後の相対座標
scaledMesh  // オブジェクトの絶対座標

*/

export class FaceMatchService {
    private static instance: FaceMatchService;

    // 顔のメッシュデータ
    private faceMesh: any[] = [];
    // 比較元の比率データ
    private baseRation: any = {};

    // メッシュデータから計算した部位ごとの距離データ
    private faceGeometric: any = {};
    // 距離データから計算した部位ごとの比率データ
    private faceRatio: any = {};
    // 比率データから比較した顔の一致率
    private faceThreshold = 0;

    // 一致しているとみなすしきい値 0～1
    private matchThreshold = 0.94;
    // 一致しているとみなす回数
    private matchOver = 5;
    // 一致検証カウンター
    private matchCount = 0;

    private result: any = {
        match: false,
        history: []
    };

    private faceGeometricPoints: any = {
        face_width: [177, 401],
        face_height: [51, 199],
        forehead_width: [63, 293],
        left_eye_width: [362, 263],
        left_eye_height: [386, 374],
        right_eye_width: [33, 133],
        right_eye_height: [159, 145],
        top_of_eye: [133, 362],
        nose_height: [6, 1],
        nose_width1: [174, 399],
        nose_width2: [134, 363],
        nose_head_width1: [102, 331],
        nose_head_width2: [240, 460],
        nose_head_width: [238, 458],
        nose_head_height: [1, 94],
        lips_nose: [2, 0],
        lips_width: [61, 291],
        lips_height: [0, 17],
        cheek_width: [215, 435],
        right_eye_cheek: [33, 192],
        left_eye_cheek: [263, 416],
        jaw_width: [32, 262],
        jaw_height: [18, 199]
    };

    private faceRatioMap: any = {
        left_eye: ['left_eye_width', 'left_eye_height'],
        right_eye: ['right_eye_width', 'right_eye_height'],
        nose1: ['nose_width1', 'nose_height'],
        nose2: ['nose_width2', 'nose_height'],
        nose_head1: ['nose_head_width', 'nose_head_height'],
        nose_head2: ['nose_head_width1', 'nose_head_height'],
        nose_head3: ['nose_head_width2', 'nose_head_height'],
        lips: ['lips_width', 'lips_height'],
        jaw: ['jaw_width', 'jaw_height'],
        face: ['face_width', 'face_height'],
        forehead_eye: ['forehead_width', 'top_of_eye'],
        cheek_jaw: ['cheek_width', 'jaw_width'],
        mouse_nose: ['lips_height', 'lips_nose']
    };

    public static call() {
        if (!FaceMatchService.instance) {
            FaceMatchService.instance = new FaceMatchService();
        }
        return FaceMatchService.instance;
    }

    /**
     * 一致とみなす、しきい値超えの回数を設定
     * @param num number
     * @returns
     */
    public setMatchOver(num: number): FaceMatchService {
        this.matchOver = num;
        return this;
    }

    /**
     * 比較元の顔率データを設定
     * @param ration
     * @returns
     */
    public setBaseFaceRation(ration: number[]): FaceMatchService {
        this.baseRation = ration;
        return this;
    }
    /**
     * 顔メッシュデータ設定
     * @param mesh
     * @returns
     */
    public setFaceMesh(mesh: number[]): FaceMatchService {
        // this.faceMesh = this.convHash(mesh);
        this.faceMesh = mesh;
        return this;
    }

    /**
     * 顔の比率データ取得
     * @returns object
     */
    public getFaceRation(): object {
        this.calcGeo();
        this.calcRatio();
        return this.faceRatio;
    }

    /**
     * 比率から求めた顔一致率を返す
     * @returns number
     */
    public getRate(): number {
        return this.faceThreshold;
    }

    /**
     * ・顔の一致判定結果
     * ・一致率の履歴
     * が格納されたオブジェクトを返す
     * @returns object {match: boolean, history: number[]}
     */
    public getResult(): object {
        return this.result;
    }

    /**
     * 一致率がしきい値を超えているか判定
     *
     * @returns number 0~2
     * 0 しきい値以下
     * 1 しきい値以上
     * 2 一致判定回数到達
     */
    public check(): number {
        if (this.faceThreshold > this.matchThreshold) {
            this.matchCount++;
            return 1;
        }
        if (this.matchCount >= this.matchOver) {
            this.result.match = true;
            this.reset();
            return 2;
        }
        return 0;
    }

    public do(): FaceMatchService {
        // 標準偏差で判定
        // return this.calcMesh();
        // 顔の部位の距離から判定
        this.consistency_rate();
        return this;
    }

    /**
     * パーツごとの距離比率から一致率を算出
     * 額縦：151：168
    額横：63：293
    左目横：362：263
    左目縦：386：374
    右目横：33：133
    右目縦：159：145
    目頭　133:362
    鼻長さ：6：1
    鼻横１：174：399
    鼻横２：134：363
    鼻頭横１：102：331
    鼻頭横２：240：460
    鼻頭横：238：458
    鼻頭縦：1：94
    唇鼻：2：0
    唇横：61：291
    唇縦：0：17
    頬横：215：435
    顎縦：18：199
    顎横：32：262
    右目尻頬：33：192
    左目尻頬：263：416
     * @returns number
     */
    private consistency_rate(): FaceMatchService {
        let _geos = 0;
        this.calcGeo();
        this.calcRatio();
        for (const key in this.baseRation) {
            if (Object.prototype.hasOwnProperty.call(this.baseRation, key)) {
                const r = this.consistencyRate(
                    this.baseRation[key],
                    this.faceRatio[key]
                );
                // console.log(r);
                /*if (r >= 0.98) {
                    _geos++;
                }*/
                _geos = _geos + r;
            }
        }
        this.faceThreshold = _geos / Object.keys(this.baseRation).length;
        this.result.history.push(this.faceThreshold);
        return this;
    }

    /**
     * r1とr2の比率を計算
     * 100分率にしたいので、1を超えている場合は2を引いた結果を返す
     * @param r1 number
     * @param r2 number
     * @returns number
     */
    private consistencyRate(r1: number, r2: number): number {
        let _r = r1 / r2;
        if (_r > 1) {
            _r = 2 - _r;
        }
        return _r;
    }

    /**
     * 顔比較用座標情報計算
     */
    private calcGeo(): void {
        for (const key in this.faceGeometricPoints) {
            if (
                Object.prototype.hasOwnProperty.call(
                    this.faceGeometricPoints,
                    key
                )
            ) {
                const fg = this.faceGeometricPoints[key];
                // 座標間の3次元座標を計算
                this.faceGeometric[key] = CalcHelper.calc3DDistance(
                    this.faceMesh[fg[0]],
                    this.faceMesh[fg[1]]
                );
            }
        }
        // console.log(this.faceGeometric);
    }

    /**
     * 比率計算
     * @returns void
     */
    private calcRatio(): void {
        for (const key in this.faceRatioMap) {
            if (Object.prototype.hasOwnProperty.call(this.faceRatioMap, key)) {
                const frm = this.faceRatioMap[key];
                this.faceRatio[key] =
                    this.faceGeometric[frm[0]] / this.faceGeometric[frm[1]];
            }
        }
    }

    private reset(): FaceMatchService {
        this.matchCount = 0;
        this.result.match = false;
        this.result.history = [];
        return this;
    }

    /**
     * 試験用の比率計算済みデータ
     */
    private baseRatio: any = {
        left_eye: 2.937313122140009,
        right_eye: 2.396713059940689,
        nose1: 0.46874619955157326,
        nose2: 0.5191841293456321,
        nose_head1: 1.0004255594196514,
        nose_head2: 3.435692436796517,
        nose_head3: 2.60743993434883,
        lips: 2.922789737120373,
        jaw: 2.894713497837406,
        face: 1.6191380353679,
        forehead_eye: 2.3581135168857217,
        cheek_jaw: 2.7622065754922627,
        mouse_nose: 1.1207467590925027
    };
    /**
     * 試験用の部位ごとの距離データ
     */
    private baseGeo: any = {
        forehead_width: 70.44005509426164,
        left_eye_width: 17.897101347687148,
        left_eye_height: 6.09301787160098,
        right_eye_width: 18.736761078278356,
        right_eye_height: 7.817690566071365,
        top_of_eye: 29.871358859470583,
        nose_height: 31.587471531722418,
        nose_width1: 14.806507233938396,
        nose_width2: 16.39971390542724,
        nose_head_width1: 31.504658739704183,
        nose_head_width2: 23.909737797289857,
        nose_head_width: 9.17371575710124,
        nose_head_height: 9.169813456608335,
        lips_nose: 12.096967546858759,
        lips_width: 39.626122788814314,
        lips_height: 13.557637172989136,
        cheek_width: 94.23927819882768,
        right_eye_cheek: 50.785686916761556,
        left_eye_cheek: 48.71175256258147,
        jaw_width: 34.11738971117067,
        jaw_height: 11.786102402417104,
        face_width: 99.2064570614418,
        face_height: 61.27115470973427
    };
}
