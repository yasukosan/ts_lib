# 顔検知

・TensorflowJS向けにコンバートされているプロジェクト色々
[人体系プロジェクト](https://github.com/tensorflow/tfjs-models)
・今回使っているプロジェクト
[face-landmarks-detection](https://github.com/tensorflow/tfjs-models/tree/master/face-landmarks-detection)


## 必要パッケージ
- @tensorflow-models/face-landmarks-detection
- @tensorflow/tfjs-backend-webgl
- @tensorflow/tfjs-converter
- @tensorflow/tfjs-core

## 関連ライブラリ
- mediaDevice   ( カメラ操作に使用
- ImageStreamingService ( カメラから画像の切り出しに使用


# ファイル構成
- face-detect.service.ts
    画像、画像タグ、映像タグから人の顔の検出

- face-match.service.ts
    顔同士の一致率計算、顔データから算術用のヒリデータへの変換

- face-orientation.service.ts
    顔の向き計算、上下左右のどの方向に真正面を0%、真横を100%として計算

- faceclipping.setvice.ts
    顔部分のみを切り抜き、外周を透過させたPNGを生成

- face-gaze.service.ts
    視線を算出し、視線方向、カメラ目線になっているか？を計算

- face.service.ts
    連続して顔の検知、比較を行う、全てのサービスのラッパーを兼ねる

- helper
    - asyst.helper.ts
        顔枠の表示、正面を向くための修正情報の表示、3Dメッシュの出力等

各機能の単独使用の場合は
「face-detects.service.ts」で顔検知を行い
その結果を「face-match.service.ts」「face-orientation.service.ts」に
渡す格好で処理を行う

連続的に処理を行う場合は
大本の「face.service.ts」を呼び出し
「face.service.ts」から他のサービスを呼び出す


# FLASK 的使い方
 * hookで顔検知をループさせる
 * 顔検知後、結果をActionにスロー
 * Actionで検知結果を要件毎に処理
 * 処理結果をReducerにスロー
 * Reducerの結果をCopmonentで表示



# 例 カメラから顔検出

```javascript

// 利用可能なMediaAPIを検索、カメラを起動

const ms = new MediaService();
await ms.getLocalStream(
    // MediaDeviceの起動オプション作成インスタンスを呼び出し
    ms.callStreamModeService()
        .onVideo()          // ビデオを有効化
        .getStreamMode(),   // オプションを取得
        'getUserMedia'      // 使用するAPIを指定
    );
// 映像を再生するVideoタグを指定
await ms.setVideoTarget('ターゲットビデオタグのID')
    .playVideo();           // 再生開始

const fds = new FaceDetectService();
await fds.setImageData(face);
// 初回読みとTernsorFlowの初期化に時間がかかる
await fds.loadModel();
return await fds.detect();

```

# 例 画像から顔検出

```javascript

// 利用可能なMediaAPIを検索カメラを起動

const ms = new MediaService();
await ms.getLocalStream(
    // MediaDeviceの起動オプション作成インスタンスを呼び出し
    ms.callStreamModeService()
        .onVideo()          // ビデオを有効化
        .getStreamMode(),   // オプションを取得
        'getUserMedia'          // 使用するAPIを指定
    );

await ms.setVideoTarget('ターゲットビデオタグのID') // 映像を再生するVideoタグを指定
    .playVideo();                                 // 再生開始

const is = new ImageStreamService()

const faceShot = is.setVideoTarget('facevideo')
    .getImageToStreamMulti(9, 200);

const fds = new FaceDetectService();
await fds.setImageData(faceShot);
await fds.loadModel();      // 初回読みとTernsorFlowの初期化に時間がかかる
return await fds.detect();

```

# 例 カメラから比較用メッシュデータを生成

```javascript

// 利用可能なMediaAPIを検索カメラを起動

const ms = new MediaService();
await ms.getLocalStream(
    // MediaDeviceの起動オプション作成インスタンスを呼び出し
    ms.callStreamModeService()
        .onVideo()          // ビデオを有効化
        .getStreamMode(),   // オプションを取得
        'getUserMedia'          // 使用するAPIを指定
    );

await ms.setVideoTarget('ターゲットビデオタグのID') // 映像を再生するVideoタグを指定
    .playVideo();                                 // 再生開始

const fds = new FaceDetectService();
await fds.setImageData(face);
await fds.loadModel();      // 初回読みとTernsorFlowの初期化に時間がかかる
const detect =  await fds.detect();


const fms = new FaceMeshService();
return fms.setFaceMesh(detect.mesh).getFaceRation();

```


# 例 カメラ映像から顔比較

```javascript

// 利用可能なMediaAPIを検索カメラを起動

const ms = new MediaService();
await ms.getLocalStream(
    // MediaDeviceの起動オプション作成インスタンスを呼び出し
    ms.callStreamModeService()
        .onVideo()          // ビデオを有効化
        .getStreamMode(),   // オプションを取得
        'getUserMedia'      // 使用するAPIを指定
    );

await ms.setVideoTarget('ターゲットビデオタグのID') // 映像を再生するVideoタグを指定
    .playVideo();                                 // 再生開始

// 顔データ用意
const face = // どっかから持ってきた顔メッシュデータ

const fds = new FaceService();
// 引数未指定の場合VideoタグIDに「facevideo」が指定されているものを探す
await fds.callDetectService().setStream();
// 初回読みとTernsorFlowの初期化に時間がかかるので注意
await fds.callDetectService().loadModel();
// 顔一致判定有効化
await fds.onFaceMatch();
// 計測開始（デフォルト10回カメラから顔を検出し、比較元の顔データと検証を行う）
return fds.faceMatch(face);
/*
平均して90％前後の一致率が必ず出てくるため
複数回検証し、94％以上の数値が一定回数出たものを正とする処理が理想
*/

```

# 例　顔切り抜き
```javascript
/**
 * Test６の内容
 * 視線検出
 */

const v = new MediaService();
const fds = new FaceService()
await v.getLocalStream(
            MediaService.call() // メディアの再生設定作成
                .callStreamModeService() // 配信モード作成インスタンス取得
                .onVideo()
                .getStreamMode(),        // MediaAPIに渡す再生設定を取得
            'getUserMedia'
        );

v.setVideoTarget('facetest6') // カメラとvideoタグを接続
 .playVideo();   // カメラ映像を再生

// 映像ストリームを設定
fds.callDetectService()
    .setVideoTarget(v.getVideoTarget());

// 顔検知ループ開始
await fds.faceWatch();

// 検知結果受け取り
const f = fds.getResult();
const im =  await v.setVideoTarget('facetest4')
            .getImageToStream();

// 顔切り抜きサービスに切り抜くカメラ映像と、検出された顔座標を渡す
await FaceClippingService.call()
    .setBaseImage(im)
    .setFaceMesh(f['annotations']['silhouette'])
    .do();

// 切り抜き後のイメージをCanvasに出力
await FaceClippingService.call().outCanvas('facetest5');

```

# 例　視線検出
```javascript
/**
 * Test６の内容
 * 視線検出
 */

const v = new MediaService();
const fds = new FaceService()
await v.getLocalStream(
            MediaService.call() // メディアの再生設定作成
                .callStreamModeService() // 配信モード作成インスタンス取得
                .onVideo()
                .getStreamMode(),        // MediaAPIに渡す再生設定を取得
            'getUserMedia'
        );

v.setVideoTarget('facetest6') // カメラとvideoタグを接続
 .playVideo();   // カメラ映像を再生

// 映像ストリームを設定
fds.callDetectService()
    .setVideoTarget(v.getVideoTarget());

// 顔検知ループ開始
await fds.faceWatch();

// 検知結果受け取り
const f = fds.getResult();

// 視線計算サービスに顔情報と虹彩情報を渡す
await FaceGazeService.call()
    .setEyeMesh(f['scaledMesh'])
    .setIris(f['annotations'])
    .do();
// 視線情報を受け取る
console.log(FaceGazeService.call().getResult());
// カメラ目線になっているかを判定
console.log(FaceGazeService.call().checkLook());

```