import '@tensorflow/tfjs-core';
import '@tensorflow/tfjs-converter';
import '@tensorflow/tfjs-backend-webgl';
import * as faceapi from '@vladmandic/face-api';

export class tfjsService {
    private hideoTarget: HTMLVideoElement | any;
    private canvasTarget: HTMLCanvasElement | any;

    /**
     * 映像ストリーム、出力ターゲットを設定
     * @param video string videoタグID
     * @param canvas string cvnasタグID
     * @returns
     */
    public setStream(video = 'facevudei', canvas = 'facecanvas'): tfjsService {
        this.hideoTarget = <HTMLVideoElement>document.getElementById(video);
        this.canvasTarget = <HTMLCanvasElement>document.getElementById(canvas);
        this.canvasTarget.width = this.hideoTarget.videoWidth;
        this.canvasTarget.height = this.hideoTarget.videoHeight;
        return this;
    }

    /**
     * Tensorflowが使用するモデルデータ読み込み
     * @returns
     */
    public async loadModel() {
        await faceapi.loadFaceExpressionModel('/models');
        await faceapi.loadFaceRecognitionModel('/models');
        await faceapi.loadFaceLandmarkModel('/models');
        await faceapi.loadSsdMobilenetv1Model('/models');
        return;
    }

    /**
     * 顔検出
     * @returns
     */
    public async detect() {
        const displaySize = {
            width: this.hideoTarget.videoWidth,
            height: this.hideoTarget.videoHeight
        };

        return new Promise(() => {
            faceapi.matchDimensions(this.canvasTarget, displaySize);
            const read = async () => {
                const detections = await faceapi
                    //.detectAllFaces(this.hideoTarget, new faceapi.TinyFaceDetectorOptions())
                    .detectAllFaces(
                        this.hideoTarget,
                        new faceapi.SsdMobilenetv1Options()
                    )
                    .withFaceLandmarks()
                    .withFaceExpressions();

                const resizedDetections = faceapi.resizeResults(
                    detections,
                    displaySize
                );
                this.canvasTarget
                    .getContext('2d')
                    .clearRect(
                        0,
                        0,
                        this.canvasTarget.width,
                        this.canvasTarget.height
                    );
                faceapi.draw.drawDetections(
                    this.canvasTarget,
                    resizedDetections
                );
                //faceapi.draw.drawFaceLandmarks(this.canvasTarget, resizedDetections);
                //faceapi.draw.drawFaceExpressions(this.canvasTarget, resizedDetections);
                setTimeout(() => {
                    read();
                }, 200);
            };
            read();
        });
    }

    /**
     * 顔の一致判定
     *
     * 5回トライし、一致率が0.35（65％一致）しない場合はFalse
     * 1度でもしきい値を超えた場合はTrueを返す
     *
     * @return boolean
     */
    public async Match() {
        const img = await this.loadMatchingImage();
        let checkCount = 0;

        const displaySize = {
            width: this.hideoTarget.videoWidth,
            height: this.hideoTarget.videoHeight
        };
        return new Promise((resolve, reject) => {
            //faceapi.matchDimensions(this.canvasTarget, displaySize);
            const read = async () => {
                const detection: any = await faceapi
                    .detectSingleFace(
                        this.hideoTarget,
                        new faceapi.SsdMobilenetv1Options()
                    )
                    .withFaceLandmarks()
                    .withFaceDescriptor();

                if (detection) {
                    const faceMatcher = new faceapi.FaceMatcher(img, 0.6);
                    const result: any = faceMatcher.findBestMatch(
                        detection.descriptor
                    );
                    console.log(result);
                    if (result._distance <= 0.35) {
                        resolve(true);
                    }
                    if (checkCount > 4) {
                        reject(false);
                    }
                    checkCount++;
                    /*
                    const resizedDetection = faceapi.resizeResults(detection, displaySize);
                    this.canvasTarget.getContext("2d").clearRect(0, 0, this.canvasTarget.width, this.canvasTarget.height);
                    faceapi.draw.drawDetections(this.canvasTarget, resizedDetection);
                    const box = resizedDetection.detection.box;
                    const drawBox = new faceapi.draw.DrawBox(box, {
                        label: result.toString()
                    });
                    faceapi.matchDimensions(this.canvasTarget, displaySize);
                    drawBox.draw(this.canvasTarget);*/
                }

                setTimeout(() => {
                    read();
                }, 10);
            };
            read();
        });
    }

    /**
     * ローカル上にあるマスターイメージを読み込む
     *
     * ※ サーバーから受け取る形式に変える
     * @returns
     */
    private async loadMatchingImage() {
        const image = await faceapi.fetchImage('/models/sc_001.png');
        const detection: any = await faceapi
            .detectSingleFace(image)
            .withFaceLandmarks()
            .withFaceDescriptor();

        return new faceapi.LabeledFaceDescriptors('UFO', [
            detection.descriptor
        ]);
    }
}
