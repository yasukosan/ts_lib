import '@tensorflow/tfjs-core'
import '@tensorflow/tfjs-converter'
import '@tensorflow/tfjs-backend-webgl'
import '@tensorflow-models/face-landmarks-detection'
const faceLandmarksDetection: any = 'face-landmarks-detection'

/**
 * faceLandmarksDetection
 */
export class FaceDetectService {
    private static instance: FaceDetectService;

    private model: any;
    // private detector    : any;
    /*private parts       : any = {
        leftEyebrow     : {},
        rightEyebrow    : {},
        leftEye     : {},
        rightEye    : {},
        faceOval    : {},
        lips        : {},
    }*/

    private input = 'video'; // カメラ、画像、どちらで判定か
    private hideoTarget: HTMLVideoElement | any; // カメラ判定用DOM
    private imageTarget: HTMLImageElement | any; // 画像判定用DOM

    private faceResult: any; // 検出された顔データ

    private Porcess: any = []; // ループ中のプロセス置き場
    private LOOP = true;
    private loopTimes = 0;
    private loopCounter = 0;

    public static call(reset = false): FaceDetectService {
        if (!FaceDetectService.instance) {
            FaceDetectService.instance = new FaceDetectService();
        }
        if (reset) {
            this.reset();
        }
        return FaceDetectService.instance;
    }

    /**
     * ビデオタグを取り込む
     * @param videotag string default[faceviode] videoタグID
     * @returns FaceDetectService
     */
    public setStream(videotag = 'facevideo'): FaceDetectService {
        this.input = 'video';
        this.hideoTarget = document.getElementById(
            videotag
        ) as HTMLVideoElement;
        return this;
    }

    public setVideoTarget(target: HTMLVideoElement): FaceDetectService {
        this.input = 'video'
        console.log(target)
        this.hideoTarget = target
        return this
    }

    /**
     * Imgタグから取り込む
     * @param tag
     * @param canvastag
     * @returns
     */
    public setImage(tag = 'faceimage'): FaceDetectService {
        this.input = 'image';
        this.imageTarget = document.getElementById(tag);
        return this;
    }

    /**
     * 画像データから取り込む
     * @param image: string
     * @param canvastag
     * @returns Promise<tfjs.service>
     */
    public setImageData(image: string): Promise<FaceDetectService> {
        this.input = 'image';
        return new Promise((resolve, reject) => {
            const _img = new Image();
            _img.onload = (e: any) => {
                console.log(e);
                this.imageTarget = _img;
                resolve(this);
            };
            _img.onerror = (e: any) => reject(e);
            _img.src = image;
        });
    }

    /**
     * 検出された顔データを返す
     * @returns
     */
    public getResult(): any {
        return this.faceResult;
    }

    /**
     * ビデオタグの縦横を返す
     * @returns object {width: number, height: number}
     */
    public getVideoScale(): any {
        return {
            width: this.hideoTarget.videoWidth,
            height: this.hideoTarget.videoHeight
        };
    }

    /**
     * モデルデータ読み込み
     */
    public async loadModel(options: any = {}): Promise<FaceDetectService> {
        if (this.model) {
            return this;
        }
        console.log(faceLandmarksDetection)
        this.model = await faceLandmarksDetection.load(
            faceLandmarksDetection.SupportedPackages.mediapipeFacemesh
        );
        if ('preload' in options) {
            const oc: HTMLCanvasElement = document.createElement('canvas');
            await this.setImageData(oc.toDataURL('image/jpg'));
            this.detect();
        }
        return this;
        /*try {
            this.model = await faceLandmarksDetection.SupportedModels.MediaPipeFaceMesh;
            const detectorConfig = {
                runtime: 'mediapipe',
                solutionPath: 'https://cdn.jsdelivr.net/npm/@mediapipe/face_mesh',
            }
            this.detector = await faceLandmarksDetection.createDetector(
                                        this.model,
                                        detectorConfig as faceLandmarksDetection.MediaPipeFaceMeshMediaPipeModelConfig
                                    );
        } catch (error) {
            console.log(`
            [Model Load Error] ${error}
            `)
        } finally{


            if ('preload' in options) {
                const oc: HTMLCanvasElement = document.createElement('canvas');
                await this.setImageData(oc.toDataURL('image/jpg'));
                this.detect();
            }
            return this;
        }*/
    }

    /**
     * 顔検出
     * @returns Promise<object>
     *
     * return object
     *  annotations: Object: 顔の部位ごとのメッシュ座標
     *  boundingBox: Object: 顔が検出された範囲の座標
     *  faceInViwConfidence: Number: 検出された顔の数
     *  kind: String: ?
     *  mesh: Object: 顔全体のメッシュ座標（絶対座標）
     *  scaledMesh: Object: 顔全体のメッシュ座標（相対座標）
     */
    public async detect(): Promise<object> {
        await this.loadModel();
        // console.log(this.model)
        const predictions = await this.model.estimateFaces({
            input: this.input === 'video' ? this.hideoTarget : this.imageTarget
        });
        /*const predictions = await this.detector.estimateFaces(
            (this.input === 'video') ? this.hideoTarget : this.imageTarget,
            {}
        )*/
        // console.log(predictions);
        // 顔が複数検知された場合は、複数の配列がここに入る
        if (predictions.length) {
            // 0番を問答無用で格納しているので複数人いる場合は運が絡む
            this.faceResult = predictions[0];
            return predictions[0];
        }
        return {};
    }

    /**
     * 動画から連続して顔を検出
     * @param back boolean default false 無限ループするか(標準　しない) trueで無限ループ
     * @param interval number default 100 顔検知後再実行するまでの時間（標準 100ミリ秒）
     * @returns
     */
    public async faceWatch(
        back = false,
        interval = 100,
        next: Function
    ): Promise<boolean> {
        await this.loadModel();
        this.resetDetect(); // 検出系データを初期化

        return new Promise(async (resolve, reject) => {
            const doRead = async () => {
                this.clearProcess();
                if (!this.LOOP) {
                    this.LOOP = true;
                    resolve(true);
                    return;
                }

                try {
                    this.faceResult = await this.detect();
                } catch (error) {
                    // 顔検出でエラーになった場合、コンソールに出力し続行
                    console.error(error);
                }

                if ('mesh' in this.faceResult) {
                    //if ('keypoints' in this.faceResult) {
                    // ループチェック、無限ループの場合Falseが返る
                    if (!this.checkLoop(back)) {
                        resolve(true);
                    }
                    next(this.faceResult);
                }
                this.Porcess.push(setTimeout(doRead, interval));
            };
            await doRead();
        });
    }

    /**
     *
     * @param back boolean trueの場合無限ループ
     * @returns
     */
    private checkLoop(back: boolean): boolean {
        // 無限ループ有効の場合、Promiseだけ返す
        if (back) {
            return false;
        }
        // ループカウンターチェック
        if (this.loopTimes === this.loopCounter) {
            this.LOOP = false;
        }
        this.loopCounter++;
        return true;
    }

    /**
     * ループ処理中のプロセスを全て止める
     */
    public clearProcess(): void {
        this.Porcess.forEach((val: any) => {
            clearTimeout(val);
        });
    }

    private static reset(): void {
        FaceDetectService.instance.input = 'video';
        FaceDetectService.instance.hideoTarget = new HTMLVideoElement();
        FaceDetectService.instance.imageTarget = new HTMLImageElement();

        FaceDetectService.instance.resetDetect();
        return;
    }

    private resetDetect(): void {
        this.faceResult = {};
    }
}
