// import { Face } from "three";
import { CalcHelper } from './helper/calc.henlper';
import { AssystHelper } from './helper/assyst.helper';

export class FaceGazeService {
    private static instance: FaceGazeService;

    // 顔のメッシュデータ
    private eyes: any = {
        left_width: 0,
        left_height: 0,
        left_center: [0, 0, 0],
        right_width: 0,
        right_height: 0,
        right_center: [0, 0, 0]
    };

    // 比較元の比率データ
    private iris: any = {
        left: [0, 0, 0],
        right: [0, 0, 0]
    };

    private gaze: any = {
        direction: 0,
        distance: 0
    };

    private gazeHistory: number[] = [];
    private insightHistory: number[] = [];

    public static call() {
        if (!FaceGazeService.instance) {
            FaceGazeService.instance = new FaceGazeService();
        }
        return FaceGazeService.instance;
    }

    /**
     * 目頭と涙袋の座標から目の情報を設定
     * @param left number[]
     * @param right number[]
     * @returns
     */
    public setEyeMesh(mesh: number[][]): FaceGazeService {
        const right = [mesh[33], mesh[133], mesh[159], mesh[145]];
        const left = [mesh[362], mesh[263], mesh[386], mesh[374]];
        this.eyes = {
            ...this.calcEye(left, 'left'),
            ...this.calcEye(right, 'right')
        };
        return this;
    }

    /**
     * 虹彩の中心座標を設定
     * @param iris object [annotations オブジェクト]
     * @returns FaceGazeSErvice
     */
    public setIris(iris: any): FaceGazeService {
        this.iris = {
            left: iris['leftEyeIris'][0],
            right: iris['rightEyeIris'][0]
        };
        return this;
    }

    public setCanvasTarget(canvas: HTMLCanvasElement): FaceGazeService {
        AssystHelper.call().setCanvas(canvas);
        return this;
    }

    /**
     * 計算結果の角度と距離を返す
     * @returns object {direction: number, distance: number}
     */
    public getGaze(): { direction: number; distance: number } {
        return this.gaze;
    }

    /**
     * 目のステータス、左右の目の寸法（縦、横、中央幅）を返す
     * @returns object
     */
    public getEyes(): object {
        return this.eyes;
    }

    /**
     * 目の大きさが平均値よりどれだけ乖離しているか判定
     * @returns number
     */
    public checkInsight(): number {
        const now = (this.eyes.left_height + this.eyes.right_height) / 2;
        if (this.insightHistory.length < 20) {
            this.insightHistory.push(now);
        }
        return CalcHelper.calcDivergence(this.insightHistory, now);
    }

    /**
     * カメラ目線判定
     * @param bound カメラ目線とみなす半径
     * @returns boolean
     */
    public checkLook(bound = 5): boolean {
        const l1 =
            (this.iris.left[0] - this.eyes.left_center[0]) ^
            (2 + (this.iris.left[1] - this.eyes.left_center[1])) ^
            (2 - bound) ^
            2;
        const l2 =
            (this.iris.right[0] - this.eyes.right_center[0]) ^
            (2 + (this.iris.right[1] - this.eyes.right_center[1])) ^
            (2 - bound) ^
            2;

        if (l1 > 0 && l2 > 0) {
            return false;
        }
        if (l1 <= 0 && l2 <= 0) {
            return true;
        }
        return false; // カメレオン？
    }

    /**
     * 視線の分散を計算
     * 眼底と目の中点の距離の分散を過去10ステップ分で計算
     * 1点集中している時、5以下
     * 視線が分散している時　20以上
     * の数値に収まる
     * @returns number
     */
    public checkDispersion(): number {
        // 1000を超えていたら減らす
        if (this.gazeHistory.length > 10) {
            this.gazeHistory.shift();
        }
        this.gazeHistory.push(this.gaze.distance);
        return CalcHelper.calcDispersion(this.gazeHistory);
    }

    /**
     * 視線の角度、距離を計算
     */
    public calcGaze(): void {
        const _direction1 = CalcHelper.calcInclination(
            this.iris.left,
            this.eyes.left_center
        );
        const _direction2 = CalcHelper.calcInclination(
            this.iris.right,
            this.eyes.right_center
        );
        const _distance1 = CalcHelper.calc2DDistance(
            this.iris.left,
            this.eyes.left_center
        );
        const _distance2 = CalcHelper.calc2DDistance(
            this.iris.right,
            this.eyes.right_center
        );

        this.gaze = {
            direction: (_direction1 + _direction2) / 2,
            distance: (_distance1 + _distance2) / 2
        };

        return;
    }

    /**
     * 眼球の直径、眼底の仮座標の計算
     * @param eye number[] メッシュ座標
     * @param witch 'left' | 'right' 右目か左目か
     * @returns {'**_width': number, '**'_center: [x, y, z] }
     */
    private calcEye(eye: number[][], witch: 'left' | 'right'): object {
        const _cx = (eye[1][0] - eye[0][0]) / 2 + eye[0][0];
        const _cy = (eye[1][1] + eye[0][1]) / 2;
        const _cz = eye[0][2] - (eye[1][0] - eye[0][0]) / 2;

        /*return {
            [witch + '_width']  : eye[1][0] - eye[0][0],
            [witch + '_height'] : eye[1][0] - eye[0][0],
            [witch + '_center'] : [_cx, _cy, _cz],
        };*/
        const dw = CalcHelper.calc3DDistance(eye[0], eye[1]);
        const dh = CalcHelper.calc3DDistance(eye[2], eye[3]);

        //const _cx2 = (dw / 2) + eye[0][0];
        //const _cy2 = dh / 2 + eye[1][1];
        //const _cz2 = eye[0][2] - (dw / 2);

        return {
            [witch + '_width']: dw,
            [witch + '_height']: dh,
            [witch + '_center']: [_cx, _cy, _cz]
        };
    }

    private calcDsitance(
        p1: number[],
        p2: number[],
        type: '2d' | '3d'
    ): number {
        const dx = (p1[0] - p2[0]) ** 2;
        const dy = (p1[1] - p2[1]) ** 2;
        if (type === '3d') {
            const dz = (p1[2] - p2[2]) ** 2;
            return Math.sqrt(dx + dy + dz);
        }
        if (type === '2d') {
            return Math.sqrt(dx + dy);
        }
        return 0;
    }
}
