export class FaceClippingService {
    private static instance: FaceClippingService;

    // 顔のメッシュデータ
    private faceMesh: any[] = [];
    private baseImage: any = '';
    private _baseImage: any = '';

    private resultCanvas: HTMLCanvasElement | any;
    private result: any = '';

    private positions: any = {
        x: 0,
        y: 0,
        width: 0,
        height: 0,
        base_width: 0,
        base_height: 0
    };

    // 顔枠の座標[x左上,y左上,x右下,y右下]
    private frame: any = {
        min: [0, 0],
        max: [0, 0]
    };

    public static call() {
        if (!FaceClippingService.instance) {
            FaceClippingService.instance = new FaceClippingService();
        }
        return FaceClippingService.instance;
    }

    /**
     * 顔メッシュデータ設定
     * @param mesh
     * @returns
     */
    public setFaceMesh(mesh: number[]): FaceClippingService {
        // this.faceMesh = this.convHash(mesh);
        this.faceMesh = mesh;
        this.calcFrame();
        return this;
    }

    /**
     * 切り抜かれる顔を含んだ画像
     * @param image
     * @returns
     */
    public setBaseImage(image: string): FaceClippingService {
        this.baseImage = image;
        return this;
    }

    /**
     * 切り抜き後の画像を取得
     * @returns
     */
    public getResult(): string {
        return this.result;
    }

    public getResultCanvas(): HTMLCanvasElement {
        return this.resultCanvas;
    }

    public getPositions(): object {
        return this.positions;
    }

    public getPoint(): number[] {
        return this.frame;
    }

    /**
     * 切り抜き開始
     *
     * @returns Promsie<FaceClippingService>
     */
    public async do(): Promise<FaceClippingService> {
        await this.clip();
        return this;
    }

    /**
     * 顔画像をCanvasに出力
     * @param canvas string CanvasタグID
     * @returns Promise<FaceClippingService>
     */
    public async outCanvas(canvas: string): Promise<FaceClippingService> {
        const c = document.getElementById(canvas) as HTMLCanvasElement;
        c.width = this.frame.max[0] - this.frame.min[0];
        c.height = this.frame.max[1] - this.frame.min[1];
        const ctx: any = c.getContext('2d');
        ctx.drawImage(this.resultCanvas, 0, 0);
        return this;
    }

    /**
     * メッシュ座標から顔部分を切り抜くマスクを作成し
     * 顔部分のみの透過レイヤーを作り、切り取る
     */
    private async clip() {
        await this.calcPostion();
        const oc = this.getNewCanvas(
            this.positions.width,
            this.positions.height
        );
        const ctx: any = oc.getContext('2d');

        ctx.beginPath();
        for (const key in this.faceMesh) {
            if (Object.prototype.hasOwnProperty.call(this.faceMesh, key)) {
                if (key === '0') {
                    ctx.moveTo(
                        this.faceMesh[key][0] - this.frame.min[0],
                        this.faceMesh[key][1] - this.frame.min[1]
                    );
                }
                ctx.lineTo(
                    this.faceMesh[key][0] - this.frame.min[0],
                    this.faceMesh[key][1] - this.frame.min[1]
                );
            }
        }
        ctx.closePath();
        ctx.stroke();
        ctx.clip();

        ctx.drawImage(
            this._baseImage,
            this.positions.x,
            this.positions.y,
            this.positions.width,
            this.positions.height,
            0,
            0,
            this.positions.width,
            this.positions.height
        );
        this.resultCanvas = oc;
        this.result = oc.toDataURL('image/png');
    }

    private async calcPostion(): Promise<void> {
        this._baseImage = await this.loadImage(this.baseImage);
        this.positions.x = this.frame.min[0];
        this.positions.y = this.frame.min[1];
        this.positions.width = this.frame.max[0] - this.frame.min[0];
        this.positions.height = this.frame.max[1] - this.frame.min[1];
        this.positions.base_width = this._baseImage.width;
        this.positions.base_height = this._baseImage.height;
        return;
    }

    /**
     * 顔座標のxy座標から縦横幅を計算
     * @returns FaceClippingService
     */
    private calcFrame(): FaceClippingService {
        const min = [1000, 1000];
        const max = [0, 0];
        for (const key in this.faceMesh) {
            if (Object.prototype.hasOwnProperty.call(this.faceMesh, key)) {
                min[0] =
                    this.faceMesh[key][0] < min[0]
                        ? this.faceMesh[key][0]
                        : min[0];
                min[1] =
                    this.faceMesh[key][1] < min[1]
                        ? this.faceMesh[key][1]
                        : min[1];
                max[0] =
                    this.faceMesh[key][0] > max[0]
                        ? this.faceMesh[key][0]
                        : max[0];
                max[1] =
                    this.faceMesh[key][1] > max[1]
                        ? this.faceMesh[key][1]
                        : max[1];
            }
        }
        this.frame = { min: min, max: max };
        return this;
    }

    /**
     * 作業用HTMLCanvasElementを作成
     * @param width
     * @param height
     * @returns
     */
    private getNewCanvas(width: number, height: number): HTMLCanvasElement {
        const oc: HTMLCanvasElement = document.createElement('canvas');
        oc.setAttribute('width', width.toString());
        oc.setAttribute('height', height.toString());

        return oc;
    }

    /**
     * 計算データを初期化
     * @returns FaceClippingService
     */
    private reset(): FaceClippingService {
        this.result.match = false;
        this.result.history = [];
        return this;
    }
    /**
     * 画像データをHtmlImageElementに変換
     * @param img string
     * @returns Promise<HTMLImageElement>
     */
    private async loadImage(img: string): Promise<HTMLImageElement> {
        return new Promise((resolve, reject) => {
            const _img = new Image();
            _img.onload = (e: any) => resolve(_img);
            _img.onerror = (e: any) => reject(e);
            _img.src = img;
        });
    }
}
