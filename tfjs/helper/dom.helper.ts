export class DomHelper {
    private static instance: DomHelper;
    private Parent: HTMLDivElement | any;

    private VideoElements: any;
    private CanvasElements: any;

    /**
     * AssystHelperオブジェクトを返す
     * @param canvas HTMLCanvasElement
     * @returns AssystHelper
     */
    public static call(dom: HTMLDivElement): DomHelper {
        if (!DomHelper.instance) {
            DomHelper.instance = new DomHelper();
        }
        DomHelper.instance.Parent = dom;
        return DomHelper.instance;
    }

    /**
     * ビデオタグを新規追加
     * @returns
     */
    public addVideoElement(): HTMLVideoElement {
        const video = new HTMLVideoElement();
        this.VideoElements.push(video);
        return video;
    }
}
