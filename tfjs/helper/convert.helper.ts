export class ConvertHelper {
    private static instance: ConvertHelper;

    public static call(): ConvertHelper {
        if (!ConvertHelper.instance) {
            ConvertHelper.instance = new ConvertHelper();
        }
        return ConvertHelper.instance;
    }

    public toJson(val: any): string {
        return JSON.stringify(val);
    }

    public toUint8(val: any): Uint8Array {
        return new Uint8Array(val);
    }

    public toFloat64(val: any): Float64Array {
        return new Float64Array(val);
    }

    public clone(val: any): any {
        return JSON.parse(JSON.stringify(val));
    }
}
