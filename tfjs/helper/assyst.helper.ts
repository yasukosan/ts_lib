export class AssystHelper {
    private static instance: AssystHelper;
    private Canvas: HTMLCanvasElement | any;

    /**
     * AssystHelperオブジェクトを返す
     * @param canvas HTMLCanvasElement
     * @returns AssystHelper
     */
    public static call(): AssystHelper {
        if (!AssystHelper.instance) {
            AssystHelper.instance = new AssystHelper();
        }
        return AssystHelper.instance;
    }

    /**
     * アシスト情報出力先のCanvasタグを設定
     * @param canvas HTMLCanvasElement
     * @returns AssystHelper
     */
    public setCanvas(
        canvas: HTMLCanvasElement,
        scale: any = null
    ): AssystHelper {
        this.Canvas = canvas;
        if (scale) {
            this.Canvas.width = scale.width;
            this.Canvas.width = scale.height;
        }
        return this;
    }

    /**
     * 撮影枠の表示
     * @returns AssystHelper
     */
    public showFaceAssistanceLine(): AssystHelper {
        const w = this.Canvas.width;
        const h = this.Canvas.height;
        if (this.Canvas.getContext) {
            const ctx = this.Canvas.getContext('2d');
            console.log(w);
            const rs = { x: w * 0.75, y: h * 0.2 }; //始点
            const rc = { x: w * 1, y: h * 0.5 }; //制御点
            const re = { x: w * 0.75, y: h * 0.8 }; //終点
            const ls = { x: w * 0.25, y: h * 0.2 }; //始点
            const lc = { x: w * 0.0, y: h * 0.5 }; //制御点
            const le = { x: w * 0.25, y: h * 0.8 }; //終点

            ctx.beginPath();
            ctx.moveTo(rs.x, rs.y); // 始点まで移動
            ctx.quadraticCurveTo(rc.x, rc.y, re.x, re.y); // 2次ベジュ曲線を描画
            ctx.strokeStyle = 'Orange'; // 線の色
            ctx.lineWidth = 5; // 線の太さ
            ctx.stroke();

            ctx.beginPath();
            ctx.moveTo(ls.x, ls.y); // 始点まで移動
            ctx.quadraticCurveTo(lc.x, lc.y, le.x, le.y); // 2次ベジュ曲線を描画
            ctx.strokeStyle = 'Orange'; // 線の色
            ctx.lineWidth = 5; // 線の太さ
            ctx.stroke();

            ctx.font = '28px sans-serif';
            ctx.fillStyle = '#ffffff';
            ctx.fillText('枠内に顔を入れて下さい', w * 0.2, h * 0.95);
        }
        return this;
    }

    /**
     * 検出された顔座標全てにドットを表示する
     * @param mesh number[][] [[x,y,z]....]
     * @returns AssystHelper
     */
    public showFaceDotMesh(mesh: number[][]): AssystHelper {
        if (this.Canvas.getContext) {
            const ctx = this.Canvas.getContext('2d');
            ctx.clearRect(0, 0, this.Canvas.width, this.Canvas.height);
            for (const key in mesh) {
                if (Object.prototype.hasOwnProperty.call(mesh, key)) {
                    ctx.beginPath();
                    ctx.arc(
                        mesh[key][0],
                        mesh[key][1],
                        1,
                        (0 * Math.PI) / 180,
                        (360 * Math.PI) / 180,
                        false
                    );
                    ctx.fillStyle = 'rgba(0, 0, 0, 1)';
                    ctx.fill();
                }
            }
        }
        return this;
    }

    /**
     * 顔を中央に向ける補助を表示する
     * @param rote number[]
     * @returns AssystHelper
     */
    public showOrientationCorrection(rote: number[]): AssystHelper {
        const th = 0.18;
        let check = false;
        if ('getContext' in this.Canvas) {
            const w = this.Canvas.width;
            const h = this.Canvas.height;
            const ctx = this.Canvas.getContext('2d');
            ctx.clearRect(0, 0, w, h);
            ctx.font = '28px sans-serif';
            ctx.fillStyle = '#000000';

            if (rote[0] > th) {
                ctx.fillText('少し下を向いて下さい', w * 0.2, h * 0.95);
                check = true;
            }
            if (rote[1] > th) {
                ctx.fillText('少し上を向いて下さい', w * 0.2, h * 0.95);
                check = true;
            }
            if (rote[2] > th) {
                ctx.fillText('少し右を向いて下さい', w * 0.2, h * 0.9);
                check = true;
            }
            if (rote[3] > th) {
                ctx.fillText('少し左を向いて下さい', w * 0.2, h * 0.9);
                check = true;
            }
            if (!check) {
                ctx.fillText('そのまま！', w * 0.2, h * 0.95);
            }
        }
        return this;
    }

    /**
     * 視線位置の情報表示
     * @param rote number[]
     * @returns AssystHelper
     */
    public showLooking(f: number): AssystHelper {
        const th = 0.18;
        let check = false;
        if ('getContext' in this.Canvas) {
            const w = this.Canvas.width;
            const h = this.Canvas.height;
            const ctx = this.Canvas.getContext('2d');
            ctx.clearRect(0, 0, w, h);
            ctx.font = '28px sans-serif';
            ctx.fillStyle = '#000000';

            if (f > th) {
                ctx.fillText('こっちみんなぁ', w * 0.2, h * 0.95);
                check = true;
            }
            if (f > th) {
                ctx.fillText('少し上を向いて下さい', w * 0.2, h * 0.95);
                check = true;
            }
            if (f > th) {
                ctx.fillText('少し右を向いて下さい', w * 0.2, h * 0.9);
                check = true;
            }
            if (f > th) {
                ctx.fillText('少し左を向いて下さい', w * 0.2, h * 0.9);
                check = true;
            }
            if (!check) {
                ctx.fillText('そのまま！', w * 0.2, h * 0.95);
            }
        }
        return this;
    }

    /**
     * Canvasをクリアする
     * @returns
     */
    public clearCanvas(): AssystHelper {
        if ('getContext' in this.Canvas) {
            const ctx = this.Canvas.getContext('2d');
            ctx.clearRect(0, 0, this.Canvas.width, this.Canvas.height);
        }
        return this;
    }
}
