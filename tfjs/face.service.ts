import '@tensorflow/tfjs-core';
import '@tensorflow/tfjs-converter';
import '@tensorflow/tfjs-backend-webgl';
// import * as faceLandmarksDetection from "@tensorflow-models/face-landmarks-detection";
import { FaceDetectService } from './face-detect.service';
import { FaceMatchService } from './face-match.service';
import { FaceOrientationService } from './face-orientation.service';
import { FaceClippingService } from './face-clipping.service';
import { AssystHelper } from './helper/assyst.helper';
/**
 * faceLandmarksDetection
 */
export class FaceService {
    private static instance: FaceService;

    private FDS: FaceDetectService | null = null;
    private FMS: FaceMatchService | null = null;
    private FOS: FaceOrientationService | null = null;
    private FCS: FaceClippingService | null = null;
    private AH: AssystHelper | null = null;

    private faceResult: any; // 検出された顔データ

    private LOOP = true; // 顔検出ループ継続フラグ
    private loopTimes = 10;
    private loopCounter = 0;

    private flags: any = {
        matching: false,
        orientation: false
    };
    private Porcess: any = []; // ループ中のプロセス置き場
    private showSupport = false;

    public static call(reset = false): FaceService {
        if (!FaceService.instance) {
            FaceService.instance = new FaceService();
        }
        if (reset) {
            this.reset();
        }
        return FaceService.instance;
    }

    /**
     * 顔検知サービスを返す
     * @returns FaceDetectService
     */
    public callDetectService(): FaceDetectService {
        if (!this.FDS) {
            this.FDS = new FaceDetectService();
        }
        return this.FDS;
    }

    /**
     * 顔一致判定サービスを返す
     * @returns FaceMatchService
     */
    public callMatchService(): FaceMatchService {
        if (!this.FMS) {
            this.FMS = new FaceMatchService();
        }
        return this.FMS;
    }

    /**
     * 顔の向き計算サービスを返す
     * @returns FaceOrientationService
     */
    public callOrientationService(): FaceOrientationService {
        if (!this.FOS) {
            this.FOS = new FaceOrientationService();
        }
        return this.FOS;
    }

    public callClippingService(): FaceClippingService {
        if (!this.FCS) {
            this.FCS = new FaceClippingService();
        }
        return this.FCS;
    }

    public callAsystService(): AssystHelper {
        if (!this.AH) {
            this.AH = new AssystHelper();
        }
        return this.AH;
    }

    /**
     * 補助ライン表示用のCanvasターゲットを設定
     * @param canvastag string Canvasタグ名
     * @returns
     */
    public setCanvasTarget(canvastag = 'facecanvas'): FaceService {
        console.log(this.callDetectService().getVideoScale());
        this.callAsystService().setCanvas(
            document.getElementById(canvastag) as HTMLCanvasElement,
            this.callDetectService().getVideoScale()
        );
        return this;
    }

    /**
     * 顔検知を実行する回数を設定
     * ※初期値：10
     * @param num number
     * @returns
     */
    public setFaceLoopCounter(num: number): FaceService {
        this.loopTimes = num;
        return this;
    }

    /**
     * 検出された顔データを返す
     * @returns
     */
    public getResult(): any {
        return this.callDetectService().getResult();
    }
    /**
     * 顔の向きを返す
     * @returns number[] [上:number、下:number、左:number、右:number]
     */
    public getOrientation(): any {
        return this.callOrientationService().getRote();
    }

    /**
     * 顔の一致判定結果を返す
     * @returns object [match: boolean, history: number[]]
     * match 一致：true　不一致：false
     * history　検出時の一致率配列 [0.9..., 0.8..., etc.....]
     */
    public getMatching(): any {
        return this.callMatchService().getResult();
    }

    public stopLoop(): FaceService {
        this.LOOP = false;
        return this;
    }

    public onFaceMatch(): FaceService {
        this.flags.matching = true;
        return this;
    }

    public onFaceOrientation(): FaceService {
        this.flags.orientation = true;
        return this;
    }

    public onSupport(): FaceService {
        this.showSupport = true;
        return this;
    }

    /**
     * 指定時間処理を止める
     * @param time number
     * @returns Promise<boolean> true only
     */
    public async sleep(time = 1000): Promise<boolean> {
        return new Promise((result) => {
            const sleep = async (val = false) => {
                if (val) {
                    result(true);
                    return;
                }
                setTimeout(sleep, time, true);
            };
            sleep();
        });
    }

    /**
     * 動画から連続して顔を検出
     * @param back boolean default false 無限ループするか(標準　しない) trueで無限ループ
     * @param interval number default 100 顔検知後再実行するまでの時間（標準 100ミリ秒）
     * @returns
     */
    public async faceWatch(back = false, interval = 100): Promise<boolean> {
        await this.callDetectService().loadModel();
        this.resetDetect(); // 検出系データを初期化

        return new Promise(async (resolve, reject) => {
            const doRead = async () => {
                this.clearProcess();
                if (!this.LOOP) {
                    this.LOOP = true;
                    resolve(true);
                    return;
                }

                try {
                    this.faceResult = await this.callDetectService().detect();
                } catch (error) {
                    // 顔検出でエラーになった場合、コンソールに出力し続行
                    console.error(error);
                }

                if ('mesh' in this.faceResult) {
                    //if ('keypoints' in this.faceResult) {
                    // ループチェック、無限ループの場合Falseが返る
                    if (!this.checkLoop(back)) {
                        resolve(true);
                    }
                    this.doFaceProcess();
                }
                this.Porcess.push(setTimeout(doRead, interval));
            };
            await doRead();
        });
    }

    /**
     *
     * @param back boolean trueの場合無限ループ
     * @returns
     */
    private checkLoop(back: boolean): boolean {
        // 無限ループ有効の場合、Promiseだけ返す
        if (back) {
            return false;
        }
        // ループカウンターチェック
        if (this.loopTimes === this.loopCounter) {
            this.stopLoop();
        }
        this.loopCounter++;
        return true;
    }

    /**
     * 検知された顔データをフラグに合わせて処理
     * @returns
     */
    private doFaceProcess(): void {
        this.clearProcess();
        // 傾き計算
        if (this.flags.orientation) {
            this.doFaceOrientation();
        }
        // 一致判定
        if (this.flags.matching) {
            this.doFaceMath();
        }
        return;
    }

    /**
     * 顔の向き検知
     * @returns void
     */
    private doFaceOrientation(): void {
        const rote = this.callOrientationService()
            .setFaceMesh(this.faceResult.mesh)
            // .setFaceMesh(this.faceResult.keypoints)
            .do()
            .getRote();
        console.log(
            '上:' +
                rote[0] +
                ',下:' +
                rote[1] +
                ',左:' +
                rote[2] +
                ',右:' +
                rote[3]
        );
        if (this.showSupport) {
            this.showFaceOrientation();
        }
    }

    /**
     * 比較元と顔が一致しているか判定
     * @returns
     */
    private doFaceMath(): number {
        // 顔が中央にあるか判定
        const center = this.callOrientationService()
            .setFaceMesh(this.faceResult.mesh)
            //.setFaceMesh(this.faceResult.keypoints)
            .do()
            .checkCenter();

        if (center) {
            // 中央を向いている場合、顔が一致している判定
            // 0 不一致、 1 一致、2 所定回数一致達成
            const m = this.callMatchService()
                .setFaceMesh(this.faceResult.mesh)
                // .setFaceMesh(this.faceResult.keypoints)
                .do()
                .check();
            // 顔一致率取得
            const r: number = this.callMatchService().getRate();
            console.log('Match :' + r);
            if (m === 2) {
                this.stopLoop();
            }
        }
        if (this.showSupport) {
            this.showFaceAssistanceLine();
        }

        return 0;
    }

    /**
     * 撮影枠の表示
     * @returns FaceService
     */
    private showFaceAssistanceLine(): FaceService {
        this.callAsystService().showFaceAssistanceLine();
        return this;
    }

    /**
     * 検出ポイントをドット表記
     * @returns FaceService
     */
    public showFaceDotMesh(): FaceService {
        if ('mesh' in this.faceResult) {
            // if ('keypoints' in this.faceResult) {
            this.callAsystService().showFaceDotMesh(this.faceResult.mesh);
            // .showFaceDotMesh(this.faceResult.keypoints);
        }

        return this;
    }

    /**
     * 顔の向き補正情報を表示
     * どっちを向けば正面になるかを文字で表示
     *
     * @returns FaceService
     */
    private showFaceOrientation(): FaceService {
        this.callAsystService().showOrientationCorrection(
            this.callOrientationService().getRote()
        );
        return this;
    }

    /**
     * ループ処理中のプロセスを全て止める
     */
    public clearProcess(): void {
        this.Porcess.forEach((val: any) => {
            clearTimeout(val);
        });
    }

    private static reset(): void {
        FaceService.instance.flags = {
            matching: false,
            orientation: false
        };
        FaceService.instance.showSupport = false;
        FaceService.instance.resetDetect();
        return;
    }

    private resetDetect(): void {
        this.faceResult = {};

        this.LOOP = true;

        this.loopTimes = 10;
        this.loopCounter = 0;
    }
}
