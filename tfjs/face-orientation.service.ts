import { CalcHelper } from './helper/calc.henlper';

export class FaceOrientationService {
    private static instance: FaceOrientationService;

    private faceMesh: any[] = []; // 計算元の顔メッシュ
    private vertical: number[] = [0, 0]; // 顔の上幅、下幅
    private horizontal: number[] = [0, 0]; // 顔の左幅、右幅
    private rote: number[] = [0, 0, 0, 0]; // 傾き強度「上,下,左,右」1方が0以上の時、対角は0になる
    private th = 0.18;

    public static call() {
        if (!FaceOrientationService.instance) {
            FaceOrientationService.instance = new FaceOrientationService();
        }
        return FaceOrientationService.instance;
    }

    public setThreshold(num: number): FaceOrientationService {
        if (num > 0 && num < 1) {
            this.th = num;
        }
        return this;
    }

    /**
     * 顔メッシュデータ設定
     * @param mesh
     * @returns
     */
    public setFaceMesh(mesh: number[]): FaceOrientationService {
        this.reset();
        this.faceMesh = mesh;
        return this;
    }

    /**
     * ?
     * @returns
     */
    public getOrientation(): number[] {
        return this.vertical.concat(this.horizontal);
    }

    /**
     * 傾き計算開始
     * @returns
     */
    public do(): FaceOrientationService {
        this.calc_rate();
        return this;
    }

    /**
     * 向きの計測結果を返す
     * @returns number[] [上、下、左、右]
     */
    public getRote(): number[] {
        return this.rote;
    }

    /**
     * 顔が中央を向いているか判定
     * 振れ幅18%以内ならTrue
     * @returns boolean
     */
    public checkCenter(): boolean {
        if (this.rote.reduce((sum, el) => sum + el, 0) === 0) {
            return false;
        }

        const result = this.rote.map((val) => {
            return val < this.th ? 'OK' : 'out';
        });
        return result.indexOf('out') === -1 ? true : false;
    }

    /**
     * パーツごとの距離比率から一致率を算出
     *
     * // 気分で決めた座標
     * 1 - 9
     * 1 - 200
     * 1 - 137
     * 1 - 366
     * // 顔の重心から端点
     * 6 - 10
     * 6 - 200
     * 6 - 34
     * 6 - 264
     * @returns number
     */
    private calc_rate(): number {
        this.vertical[0] = CalcHelper.calc2DDistance2(
            this.faceMesh[1],
            this.faceMesh[9]
        );
        this.vertical[1] = CalcHelper.calc2DDistance2(
            this.faceMesh[1],
            this.faceMesh[200]
        );
        this.horizontal[0] = CalcHelper.calc2DDistance2(
            this.faceMesh[1],
            this.faceMesh[137]
        );
        this.horizontal[1] = CalcHelper.calc2DDistance2(
            this.faceMesh[1],
            this.faceMesh[366]
        );
        // const role = this.calcInclination(this.faceMesh[200], this.faceMesh[9]);

        this.calcOrientation();
        return 0;
    }

    /**
     * 傾き計算
     * 端末のスペック不足で座標情報が正しく取得できない場合
     * 誤差が大きくなるので注意
     * @param point1 number[] [x,y,z]
     * @param point2 number[] [x,y,z]
     * @returns number ラジアン角
     */
    private calcInclination(point1: number[], point2: number[]): number {
        const bottom = Math.abs(point1[1] - point2[1]);
        const height = Math.abs(point1[0] - point2[0]);
        const tan = Math.tan(height / bottom) / (Math.PI / 180);

        if (point1[0] > point2[0]) {
            return -1 * tan;
        }
        return tan;
    }

    /**
     * 顔の向きと、強度を計算（正面：0%、真横：100%）
     */
    private calcOrientation(): FaceOrientationService {
        // 初期化
        this.rote = [0, 0, 0, 0];
        this.rote = CalcHelper.calcOrientation(this.horizontal, this.vertical);
        return this;
    }

    private reset(): FaceOrientationService {
        this.faceMesh = [];
        this.vertical = [0, 0];
        this.horizontal = [0, 0];
        this.rote = [0, 0, 0, 0];
        return this;
    }
}
