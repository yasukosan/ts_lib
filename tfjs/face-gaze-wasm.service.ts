import { EyeService, LookService, GazeService } from 'wasm-lib';

// import { Face } from "three";
import { CalcHelper } from './helper/calc.henlper';
import { AssystHelper } from './helper/assyst.helper';
import { ConvertHelper } from './helper/convert.helper';

type wasm = {
    look: Function;
    gand: Function;
    eye: Function;
};
export class FaceGazeWasmService {
    private static instance: FaceGazeWasmService;

    // 目のステータス
    private eyes: any = {
        left_width: 0,
        left_height: 0,
        left_center: [0, 0, 0],
        right_width: 0,
        right_height: 0,
        right_center: [0, 0, 0]
    };

    // 比較元の比率データ
    private iris: any = {
        left: [0, 0, 0],
        right: [0, 0, 0]
    };

    private gaze: any = {
        direction: 0,
        distance: 0
    };

    private gazeHistory: number[] = [];
    private insightHistory: number[] = [];

    private eyeService: EyeService;
    private gazeService: GazeService;
    private lookService: LookService;

    public static call() {
        if (!FaceGazeWasmService.instance) {
            FaceGazeWasmService.instance = new FaceGazeWasmService();
        }
        return FaceGazeWasmService.instance;
    }

    public constructor() {
        this.eyeService = new EyeService();
        this.gazeService = new GazeService();
        this.lookService = new LookService();
    }

    /**
     * 目頭と涙袋の座標から目の情報を設定
     * @param left number[]
     * @param right number[]
     * @returns
     */
    public async setEyeMesh(mesh: number[]): Promise<FaceGazeWasmService> {
        const right = [mesh[33], mesh[133], mesh[159], mesh[145]];
        const left = [mesh[362], mesh[263], mesh[386], mesh[374]];
        this.eyes = {
            ...(await this.calcEye(left, 'left')),
            ...(await this.calcEye(right, 'right'))
        };
        return this;
    }

    /**
     * 虹彩の中心座標を設定
     * @param iris object [annotations オブジェクト]
     * @returns FaceGazeSErvice
     */
    public setIris(iris: any): FaceGazeWasmService {
        this.iris = {
            left: iris['leftEyeIris'][0],
            right: iris['rightEyeIris'][0]
        };
        return this;
    }

    public setCanvasTarget(canvas: HTMLCanvasElement): FaceGazeWasmService {
        AssystHelper.call().setCanvas(canvas);
        return this;
    }

    /**
     * 計算結果の角度と距離を返す
     * @returns object {direction: number, distance: number}
     */
    public getGaze(): { direction: number; distance: number } {
        return this.gaze;
    }

    /**
     * 目のステータス、左右の目の寸法（縦、横、中央幅）を返す
     * @returns object
     */
    public getEyes(): object {
        return this.eyes;
    }

    /**
     * 目の大きさが平均値よりどれだけ乖離しているか判定
     * @returns number
     */
    public checkInsight(): number {
        const now = (this.eyes.left_height + this.eyes.right_height) / 2;
        if (this.insightHistory.length < 20) {
            this.insightHistory.push(now);
        }
        return CalcHelper.calcDivergence(this.insightHistory, now);
    }

    /**
     * カメラ目線になっているか判定
     * @param bound カメラ目線とみなす半径
     * @returns boolean
     */
    public checkLook(bound = 5): boolean {
        this.lookService.left = ConvertHelper.call().toJson(this.iris.left);
        this.lookService.right = ConvertHelper.call().toJson(this.iris.right);
        this.lookService.center_left = ConvertHelper.call().toJson(
            this.eyes.left_center
        );
        this.lookService.center_right = ConvertHelper.call().toJson(
            this.eyes.right_center
        );
        this.lookService.bound = bound;

        this.lookService.look();

        return this.lookService.result;
    }

    /**
     * 視線の分散を計算
     * 眼底と目の中点の距離の分散を過去10ステップ分で計算
     * 1点集中している時、5以下
     * 視線が分散している時　20以上
     * の数値に収まる(目分量)
     * @returns number
     */
    public checkDispersion(): number {
        // 10を超えていたら減らす
        if (this.gazeHistory.length > 10) {
            this.gazeHistory.shift();
        }
        this.gazeHistory.push(this.gaze.distance);
        return CalcHelper.calcDispersion(this.gazeHistory);
    }

    /**
     * 視線の角度、距離を計算
     */
    public calcGaze(): void {
        // console.log(this.iris.left);
        // const g = new GazeService()

        this.gazeService.left = ConvertHelper.call().toJson(this.iris.left);
        this.gazeService.right = ConvertHelper.call().toJson(this.iris.right);
        this.gazeService.center_left = ConvertHelper.call().toJson(
            this.eyes.left_center
        );
        this.gazeService.center_right = ConvertHelper.call().toJson(
            this.eyes.right_center
        );

        this.gazeService.gaze();

        this.gaze = {
            direction: this.gazeService.direction,
            distance: this.gazeService.distance
        };
        return;
    }

    /**
     * 眼球の直径、眼底の仮座標の計算
     * @param eye number[] メッシュ座標
     * @param witch 'left' | 'right' 右目か左目か
     * @returns {'**_width': number, '**'_center: [x, y, z] }
     */
    private calcEye(eye: number[], witch: 'left' | 'right'): object {
        // console.log(JSON.stringify(eye));
        this.eyeService.eye = JSON.stringify(eye);
        this.eyeService.cacl_eye();
        return {
            [witch + '_width']: this.eyeService.width,
            [witch + '_height']: this.eyeService.height,
            [witch + '_center']: JSON.parse(this.eyeService.center)
        };
    }

    private calcDsitance(
        p1: number[],
        p2: number[],
        type: '2d' | '3d'
    ): number {
        const dx = (p1[0] - p2[0]) ** 2;
        const dy = (p1[1] - p2[1]) ** 2;
        if (type === '3d') {
            const dz = (p1[2] - p2[2]) ** 2;
            return Math.sqrt(dx + dy + dz);
        }
        if (type === '2d') {
            return Math.sqrt(dx + dy);
        }
        return 0;
    }
}
