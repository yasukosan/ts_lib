import { FaceLandmarker, FilesetResolver, DrawingUtils } from '@mediapipe/tasks-vision'
const faceLandmarksDetection: any = 'face-landmarks-detection'

/**
 * faceLandmarksDetection
 */
export class FaceDetectService {
    private static instance: FaceDetectService

    private faceLandmarker: any
    private runnningMode: 'IMAGE' | 'VIDEO' = 'VIDEO'
    private webcamRunning = false
    private model: any;
    // private detector    : any;
    /*private parts       : any = {
        leftEyebrow     : {},
        rightEyebrow    : {},
        leftEye     : {},
        rightEye    : {},
        faceOval    : {},
        lips        : {},
    }*/

    private hideoTarget: HTMLVideoElement | any // カメラ判定用DOM
    private imageTarget: HTMLImageElement | any // 画像判定用DOM
    private canvasTarget: HTMLCanvasElement | any // 結果描画用DOM

    private faceResult: any // 検出された顔データ

    private Porcess: any = [] // ループ中のプロセス置き場
    private LOOP = true
    private loopTimes = 0
    private loopCounter = 0

    public static call(reset = false): FaceDetectService {
        if (!FaceDetectService.instance) {
            FaceDetectService.instance = new FaceDetectService();
        }
        if (reset) {
            this.reset()
        }
        return FaceDetectService.instance
    }

    /**
     * ビデオをタグ情報からを取り込む
     * @param videotag string default[faceviode] videoタグID
     * @returns FaceDetectService
     */
    public setStream(videotag = 'facevideo'): FaceDetectService {
        this.runnningMode = 'VIDEO'
        this.hideoTarget = document.getElementById(
            videotag
        ) as HTMLVideoElement
        return this
    }

    /**
     * ビデオエレメントを取り込む
     * @param target HTMLVideoElement
     * @returns this
     */
    public setVideoTarget(target: HTMLVideoElement): FaceDetectService {
        this.runnningMode = 'VIDEO'
        this.hideoTarget = target
        return this
    }

    public setCanvasTarget(target: HTMLCanvasElement): FaceDetectService {
        this.canvasTarget = target
        return this
    }

    /**
     * Imgタグから取り込む
     * @param tag
     * @param canvastag
     * @returns
     */
    public setImage(tag = 'faceimage'): FaceDetectService {
        this.runnningMode = 'IMAGE'
        this.imageTarget = document.getElementById(tag)
        return this
    }

    /**
     * 画像データから取り込む
     * @param image: string
     * @param canvastag
     * @returns Promise<tfjs.service>
     */
    public setImageData(image: string): Promise<FaceDetectService> {
        this.runnningMode = 'IMAGE'
        return new Promise((resolve, reject) => {
            const _img = new Image()
            _img.onload = (e: any) => {
                console.log(e)
                this.imageTarget = _img
                resolve(this)
            }
            _img.onerror = (e: any) => reject(e)
            _img.src = image
        });
    }

    /**
     * 検出された顔データを返す
     * @returns
     */
    public getResult(): any {
        return this.faceResult
    }

    /**
     * ビデオタグの縦横を返す
     * @returns object {width: number, height: number}
     */
    public getVideoScale(): any {
        return {
            width: this.hideoTarget.videoWidth,
            height: this.hideoTarget.videoHeight
        }
    }

    /**
     * モデルデータ読み込み
     */
    public async loadModel(options: any = {}): Promise<FaceDetectService> {
        if (this.model) {
            return this;
        }
        const modelAssetPath: string = "https://storage.googleapis.com/mediapipe-models/face_landmarker/face_landmarker/float16/1/face_landmarker.task"
        const fileResolver = await FilesetResolver.forVisionTasks(
            "https://cdn.jsdelivr.net/npm/@mediapipe/tasks-vision@latest/wasm"
        )
        this.faceLandmarker = await FaceLandmarker.createFromOptions(
            fileResolver,
            {
                baseOptions: {
                    modelAssetPath: modelAssetPath,
                },
                outputFaceBlendshapes: true,
                runningMode: this.runnningMode,
                numFaces: 1,
            }
        )

        return this
    }

    /**
     * 顔検出
     * @returns Promise<object>
     *
     * return object
     *  annotations: Object: 顔の部位ごとのメッシュ座標
     *  boundingBox: Object: 顔が検出された範囲の座標
     *  faceInViwConfidence: Number: 検出された顔の数
     *  kind: String: ?
     *  mesh: Object: 顔全体のメッシュ座標（絶対座標）
     *  scaledMesh: Object: 顔全体のメッシュ座標（相対座標）
     */
    public async detect(): Promise<object> {
        await this.loadModel()

        this.faceLandmarker.setOptions({
            runnningMode    : this.runnningMode
        })
        const results = this.faceLandmarker.detectForVideo(this.hideoTarget, performance.now())
        // console.log(results)
        if ('faceLandmarks' in results) {
            this.faceResult = results.faceLandmarks[0]
            return this.faceResult
        }
        return {};
    }

    /**
     * 動画から連続して顔を検出
     * @param back boolean default false 無限ループするか(標準　しない) trueで無限ループ
     * @param interval number default 100 顔検知後再実行するまでの時間（標準 100ミリ秒）
     * @returns
     */
    public async faceWatch(
        back = false,
        interval = 100,
        next: Function
    ): Promise<boolean> {
        await this.loadModel();
        this.resetDetect(); // 検出系データを初期化

        return new Promise(async (resolve, reject) => {
            const doRead = async () => {
                this.clearProcess();
                if (!this.LOOP) {
                    this.LOOP = true;
                    resolve(true);
                    return;
                }

                try {
                    this.faceResult = await this.detect();
                } catch (error) {
                    // 顔検出でエラーになった場合、コンソールに出力し続行
                    console.error(error);
                }

                if (this.faceResult.length > 300) {
                    // ループチェック、無限ループの場合Falseが返る
                    if (!this.checkLoop(back)) {
                        resolve(true);
                    }
                    next(this.faceResult);
                }
                this.Porcess.push(setTimeout(doRead, interval));
            };
            await doRead();
        });
    }

    /**
     *
     * @param back boolean trueの場合無限ループ
     * @returns
     */
    private checkLoop(back: boolean): boolean {
        // 無限ループ有効の場合、Promiseだけ返す
        if (back) {
            return false;
        }
        // ループカウンターチェック
        if (this.loopTimes === this.loopCounter) {
            this.LOOP = false;
        }
        this.loopCounter++;
        return true;
    }

    /**
     * ループ処理中のプロセスを全て止める
     */
    public clearProcess(): void {
        this.Porcess.forEach((val: any) => {
            clearTimeout(val);
        });
    }

    private static reset(): void {
        FaceDetectService.instance.runnningMode = 'VIDEO'
        FaceDetectService.instance.hideoTarget = new HTMLVideoElement()
        FaceDetectService.instance.imageTarget = new HTMLImageElement()

        FaceDetectService.instance.resetDetect()
        return;
    }

    private resetDetect(): void {
        this.faceResult = {};
    }
}
