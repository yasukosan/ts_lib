/**
 * オブジェクトを内包した配列に、新要素を追加
 *
 * initial指定時、オブジェクトの最初の要素とinitialが一致している場合
 * addの内容でobjを初期化
 *
 * @param obj any[]
 * @param add object
 * @param initial any[]
 * @returns any
 */
export const add = <T extends string | number | object> (
    obj: { [key: string]: T },
    val: T,
    key: string,
    initial?: object
): { [key: string]: T } => {
    if (initial !== undefined && JSON.stringify(initial) === JSON.stringify(obj)) {
        return {[key]: val};
    }
    const _obj = duplicator<{ [key:string]: T }>(obj);
    _obj[key] = val;
    return _obj;
};

/**
 * オブジェクトを内包した配列から、指定要素を削除
 * keyがobj内に存在しない場合
 * remと同じ内容を検索し削除
 * @param obj object
 * @param key string
 * @param rem object
 * @returns any[]
 */
export const del = <T extends string | number | object> (
    obj: {[key: string]: T },
    key: string,
    rem: T | undefined = undefined
): { [key: string]: T } => {
    if (key in obj) {
        if ('object' === typeof obj) delete obj[key];
    } else if (rem !== undefined) {
        for (const key in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, key)) {
                if (JSON.stringify(obj[key]) === JSON.stringify(rem)) {
                    delete obj[key];
                    break;
                }
            }
        }
    }
    return obj;
};

/**
 * オブジェクトを内包した配列から、指定要素を更新
 * @param obj any[]
 * @param up object
 * @param index number
 * @returns any[]
 */
export const update = <T extends string | number | object> (
    obj: {[key: string]: T},
    key: string,
    up: T
): {[key: string]: T} => {
    const _obj = duplicator(obj);
    if (key in obj) {
        _obj[key] = up;
    }
    return _obj;
};

/**
 * オブジェクトが指定のキーを持ち
 * キーの値がsearch文字列と一致するIndexを返す
 * @param obj {[key: string | number]: object} | T[]
 * @param key string
 * @param search string
 * @returns string | number
 */
export const search = 
<T extends string | number | object>
(
    obj: any,
    key: string,
    search: T
): T => {
    if (typeof obj === 'object') {
        Object.keys(obj).map((k: string | number) => {
            if (typeof k === 'string'
                || typeof k === 'number'
                && key in obj[k]
                && JSON.stringify(obj[k][key]) === JSON.stringify(search))
            {
                return k
            }
        })
    }
    if (Array.isArray(obj)) {
        for (let i = 0; i <= obj.length; i++) {
            if (key in obj[i]
                && JSON.stringify(obj[i][key]) === JSON.stringify(search)
            ) return i as T
        }
    }

    return 0 as T
}

/**
 * 配列、オブジェクトを複製
 * @param org Array<string | number | object> | object
 * @returns Array<string | number | object> | object
 */
export const duplicator = 
<T extends Array<string | number | object> | { [key:string]: string | number | object }>
(
    org: T
): T => {
    if (Array.isArray(org)) {
        return window.structuredClone(org)
        // return org.concat() as T;
    }
    return window.structuredClone(org)
    // return Object.assign({}, JSON.parse(JSON.stringify(org)));;
}