import { add, del, update, search, duplicator } from '../KeyTo'

describe('オブジェクト操作ライブラリのテスト', () => {
    test('add関数: 新しい要素を追加する', () => {
        const obj = { a: 1, b: 2 };
        const result = add(obj, 3, 'c');
        expect(result).toEqual({ a: 1, b: 2, c: 3 });
    });

    test('add関数: initialが一致する場合にオブジェクトを初期化する', () => {
        const obj = { a: 1, b: 2 };
        const initial = { a: 1, b: 2 };
        const result = add(obj, 3, 'c', initial);
        expect(result).toEqual({ c: 3 });
    });

    test('del関数: 指定したキーの要素を削除する', () => {
        const obj = { a: 1, b: 2, c: 3 };
        const result = del(obj, 'b');
        expect(result).toEqual({ a: 1, c: 3 });
    });

    test('del関数: 指定した要素を削除する', () => {
        const obj = { a: 1, b: 2, c: 3 };
        const result = del(obj, 'd', 2);
        expect(result).toEqual({ a: 1, c: 3 });
    });

    test('update関数: 指定したキーの要素を更新する', () => {
        const obj = { a: 1, b: 2 };
        const result = update(obj, 'b', 3);
        expect(result).toEqual({ a: 1, b: 3 });
    });

    test('search関数: 指定したキーと値を持つ要素のインデックスを返す', () => {
        const obj = [{ id: 1, name: 'Alice' }, { id: 2, name: 'Bob' }];
        const result = search(obj, 'name', 'Bob');
        expect(result).toBe(1);
    });

    test('duplicator関数: オブジェクトを複製する', () => {
        const obj = { a: 1, b: 2 };
        const result = duplicator(obj);
        expect(result).toEqual(obj);
        expect(result).not.toBe(obj); // 複製されたオブジェクトは元のオブジェクトと異なるインスタンスであることを確認
    });

    test('duplicator関数: 配列を複製する', () => {
        const arr = [1, 2, 3]
        const result = duplicator(arr)
        expect(result).toEqual(arr)
        expect(result).not.toBe(arr); // 複製された配列は元の配列と異なるインスタンスであることを確認
    })
})