import { add, del, update, duplicator } from '../ArrayTo'

describe('配列操作ライブラリのテスト', () => {
    test('add関数: 新しい要素を追加する', () => {
        const arr = [1, 2];
        const result = add(arr, 3);
        expect(result).toEqual([1, 2, 3]);
    });

    test('add関数: initialが一致する場合に配列を初期化する', () => {
        const arr = [1];
        const initial = [1];
        const result = add(arr, 2, initial);
        expect(result).toEqual([2]);
    });

    test('del関数: 指定したインデックスの要素を削除する', () => {
        const arr = [1, 2, 3];
        const result = del(arr, 1);
        expect(result).toEqual([1, 3]);
    });

    test('del関数: 指定した要素を削除する', () => {
        const arr = [1, 2, 3];
        const result = del(arr, -1, 2);
        expect(result).toEqual([1, 3]);
    });

    test('update関数: 指定したインデックスの要素を更新する', () => {
        const arr = [1, 2, 3];
        const result = update(arr, 1, 4);
        expect(result).toEqual([1, 4, 3]);
    });

    test('duplicator関数: 配列を複製する', () => {
        const arr = [1, 2, 3];
        const result = duplicator(arr);
        expect(result).toEqual(arr);
        expect(result).not.toBe(arr); // 複製された配列は元の配列と異なるインスタンスであることを確認
    });
});