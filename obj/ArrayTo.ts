/**
 *
 * @param arr Array<any>
 * @param add any
 * @param initial Array<any> Default:[]
 * @returns Array<any>
 */
export const add = <T extends string | number | object> (
    arr: Array<T>,
    add: T,
    initial: Array<T> = []
): Array<T> => {
    const _arr = duplicator(arr);
    const i = _arr.length;
    if (i === 1 && JSON.stringify(_arr) === JSON.stringify(initial)) {
        _arr[0] = add;
    } else {
        _arr[i] = add;
    }
    return _arr;
};

/**
 *
 * @param arr Array<any>
 * @param index number
 * @param rem any
 * @returns Array<any>
 */
export const del =  <T extends string | number | object> (
    arr: Array<T>,
    index: number,
    rem: T | undefined = undefined
): Array<T> => {
    if (index > -1) {
        if (Array.isArray(arr)) arr.splice(index as number, 1)
    } else {
        if (rem === undefined) return arr
        
        for (let i = 0; i < arr.length; i++) {
            if (JSON.stringify(arr[i]) === JSON.stringify(rem)) {
                arr.splice(i, 1)
                break
            }
        }
    }
    return arr
}

/**
 *
 * @param arr Array<any>
 * @param index number
 * @param up any
 * @returns Array<any>
 */
export const update = <T extends string | number | object> (
    arr: Array<T>,
    index: number,
    up: T
): Array<T> => {
    const _arr = duplicator(arr);
    if (Array.isArray(arr)) _arr[index] = up;

    return _arr;
};



/**
 * 配列、オブジェクトを複製
 * @param org Array<string | number | object> | object
 * @returns Array<string | number | object> | object
 */
export const duplicator = 
<T extends Array<string | number | object> | { [key:string]: string | number | object }>
(
    org: T
): T => {
    if (Array.isArray(org)) {
        return structuredClone(org);
        // return org.concat() as T;
    }
    return structuredClone(org);
    // return Object.assign({}, JSON.parse(JSON.stringify(org)));
}

