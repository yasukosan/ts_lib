
/**
 * @property windowWidth number
 * @property windowHeight number
 * @property contentWidth number
 * @property contentHeight number
 * @property contentX number
 * @property contentY number
 */
export interface ScaleInterface {
    windowWidth     : number,
    windowHeight    : number,
    contentWidth    : number,
    contentHeight   : number,
    contentX        : number,
    contentY        : number,
}

export class RulerService {
    private static instance: RulerService;

    private Scales: ScaleInterface = {
        windowWidth     : 0,
        windowHeight    : 0,
        contentWidth    : 0,
        contentHeight   : 0,
        contentX        : 0,
        contentY        : 0,
    }

    private Content: HTMLElement = document.createElement('div');
    private Rect: DOMRect | any;

    public static call(): RulerService {
        if (!RulerService.instance) {
            RulerService.instance = new RulerService();
        }
        return RulerService.instance;
    }

    /**
     * 計測するタグのIDをセット
     * @param target string
     * @returns RulerService
     */
    public set(target: string): RulerService {
        const t = document.getElementById(target);
        if (t) {
            this.Content = t;
            this.Rect = t.getBoundingClientRect();
        } else {
            throw new Error("Ruler Exception Element not found");
        }
        return this;
    }

    /**
     * 座標情報を計算
     * @returns RulerService
     */
    public check(): RulerService {
        this.checkWindow();
        this.checkContent();
        return this;
    }

    /**
     * 座標情報を返す
     * @returns ScaleInterface
     */
    public get(): ScaleInterface {
        return this.Scales;
    }


    private checkWindow() {
        this.Scales.windowWidth = window.innerWidth;
        this.Scales.windowHeight = window.innerHeight;
    }

    private checkContent() {
        this.Scales.contentWidth = this.Content.offsetWidth;
        this.Scales.contentHeight = this.Content.offsetHeight;
        this.Scales.contentX = this.Rect.left;
        this.Scales.contentY = this.Rect.top;
    }
}