//import mat4 from "gl-matrix";

/**
 * Geometry Serviceに移動しているので
 * これ自体は、使っていたシステムが動かなくなるのを防ぐためのコード残骸のみ
 * WebGL関連は「../geometry」以下を参照
 *
 */

export class WebGL {
    private static instance: WebGL;

    private vsSource = `
        attribute vec4 aVertexPosition;

        uniform mat4 uModelViewMatrix;
        uniform mat4 uProjectionMatrix;

        void main() {
            gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
        }
    `;

    private fsSource = `
        void main() {
            gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
        }
    `;

    public static call(): WebGL {
        if (!WebGL.instance) {
            WebGL.instance = new WebGL();
        }
        return WebGL.instance;
    }

    public initShaderProgram() {
        const c: HTMLCanvasElement = document.querySelector(
            '#webgltest'
        ) as HTMLCanvasElement;

        const gl = c.getContext('webgl');
        if (gl === null) {
            alert('WebGL初期化失敗');
            return;
        }
        // クリアカラーを黒に設定し、完全に不透明にします
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        // 指定されたクリアカラーでカラーバッファをクリアします
        gl.clear(gl.COLOR_BUFFER_BIT);

        const vertexShader = this.loadShader(
            gl,
            gl.VERTEX_SHADER,
            this.vsSource
        );
        const fragmentShader = this.loadShader(
            gl,
            gl.FRAGMENT_SHADER,
            this.fsSource
        );

        // Create the shader program

        const shaderProgram: any = gl.createProgram();
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);

        // If creating the shader program failed, alert

        if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
            alert(
                'Unable to initialize the shader program: ' +
                    gl.getProgramInfoLog(shaderProgram)
            );
            return null;
        }

        const programInfo = {
            program: shaderProgram,
            attribLocations: {
                vertexPosition: gl.getAttribLocation(
                    shaderProgram,
                    'aVertexPosition'
                )
            },
            uniformLocations: {
                projectionMatrix: gl.getUniformLocation(
                    shaderProgram,
                    'uProjectionMatrix'
                ),
                modelViewMatrix: gl.getUniformLocation(
                    shaderProgram,
                    'uModelViewMatrix'
                )
            }
        };
        return shaderProgram;
    }

    public loadShader(gl: any, type: any, source: any) {
        // 新しいシェーダーを作成
        const shader = gl.createShader(type);

        // シェーダープログラムをレンダリングターゲットと一緒にセット
        gl.shaderSource(shader, source);

        // シェーダープログラムをコンパイル
        gl.compileShader(shader);

        // See if it compiled successfully
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            alert(
                'An error occurred compiling the shaders: ' +
                    gl.getShaderInfoLog(shader)
            );
            gl.deleteShader(shader);
            return null;
        }

        return shader;
    }

    private initBuffers(gl: any) {
        // Create a buffer for the square's positions.

        const positionBuffer = gl.createBuffer();

        // Select the positionBuffer as the one to apply buffer
        // operations to from here out.

        gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

        // Now create an array of positions for the square.

        const positions = [-1.0, 1.0, 1.0, 1.0, -1.0, -1.0, 1.0, -1.0];

        // Now pass the list of positions into WebGL to build the
        // shape. We do this by creating a Float32Array from the
        // JavaScript array, then use it to fill the current buffer.

        gl.bufferData(
            gl.ARRAY_BUFFER,
            new Float32Array(positions),
            gl.STATIC_DRAW
        );

        return {
            position: positionBuffer
        };
    }

    public test1() {
        const c: HTMLCanvasElement = document.querySelector(
            '#webgltest'
        ) as HTMLCanvasElement;

        const gl = c.getContext('webgl');
        if (gl === null) {
            alert('WebGL初期化失敗');
            return;
        }
        // クリアカラーを黒に設定し、完全に不透明にします
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        // 指定されたクリアカラーでカラーバッファをクリアします
        gl.clear(gl.COLOR_BUFFER_BIT);
    }
}
