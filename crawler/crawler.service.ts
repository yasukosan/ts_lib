import * as cheerio from 'cheerio'

export type CrawlerType = {
    url: string,
    title: string,
    text: string
}

export type ReturnSuccess = {
    status: true
    message: CrawlerType
}

export type ReturnError = {
    status: false
    message: string
}

let loopLimit = 3
let loopCount = 0

export const setLoopLimit = (limit: number) => loopLimit = limit
export const getLoopLimit = () => loopLimit

/**
 * URLからリンク先のWebページのタイトルと本文を取得する
 * - loopLimit回までリトライする（デフォルト3回）
 * 
 * @param urls string
 * @returns Promise<>
 */
export const crawler = async (
    url: string
): Promise<ReturnSuccess | ReturnError> => {

    while (true) {
        // URLからHTMLを取得
        const $ = await fetchURL(url)
        
        if ($ !== false) {
            // テキスト本文を取得（例として、<p>タグ内のテキストを取得）
            const textContent = extractText($)
            loopCount = 0
            return {
                status: true,
                message: {
                    url: url,
                    title: $('title').text(),
                    text: textContent
                }
            }
        }
        
        if (loopCount <= loopLimit) {
            console.log('Retry Fetch URL', url)
            loopCount++
            await sleep(5)

        } else {
            console.error('Failed to fetch URL', url)
            loopCount = 0
            return {
                status: false,
                message: 'Failed to fetch URL'
            }
        }
    }
}

/**
 * URLからHTMLを取得し、cheerioオブジェクトを返す
 * @param url string
 * @returns Promise<cheerio.CheerioAPI | false>
 */
const fetchURL = async (
    url: string
): Promise<cheerio.CheerioAPI | false> => {
    try {
        const r = await fetch(url, {
            method: 'GET'
        })
        if (r.status == 200) {
            const html = await r.text()
            return cheerio.load(html)
        }
        console.error('Failed to fetch URL', url)
        return false
    } catch (e) {
        // console.error(e)
        console.error('Fetch Exception Error', e)
        return false
    }
}

/**
 * cheerioオブジェクトからテキストを抽出する
 * @param $ cheerio.CheerioAPI
 * @returns string
 */
const extractText = (
    $: cheerio.CheerioAPI
): string => {
    return $('p').map((i, el) => $(el).text()).get().join('\n')
}

/**
 * 指定秒間待機する
 */
export const sleep = (sec: number): Promise<void> => {
    return new Promise(resolve => setTimeout(resolve, sec * 1000))
}
