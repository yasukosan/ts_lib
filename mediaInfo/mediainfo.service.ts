

export type MediaInfo = {
    name    : string
    type    : string
    duration: number
    size    : number
    width?  : number
    height? : number
}

const MimeTypes: {[key: string]: string} = {
    'mp3'   : 'audio/mpeg',
    'wav'   : 'audio/wav',
    'ogg'   : 'audio/ogg',
    'webm'  : 'audio/webm',
    'mp4'   : 'video/mp4',
    'ogv'   : 'video/ogg',
}


export class MediaInfoService {
    private static instance: MediaInfoService

    private virtualVideo: HTMLVideoElement | any
    private virtualAudio: HTMLAudioElement | any

    private mediaData: string = ''

    private mediaInfo: MediaInfo = {
        name    : '',
        type    : '',
        duration: 0,
        size    : 0,
    }

    public static call() {
        if (!MediaInfoService.instance) {
            MediaInfoService.instance = new MediaInfoService()
        }
        return MediaInfoService.instance;
    }

    /**
     * Base64文字列のメディアデータを変数に格納
     */
    public async setup(
        data: {
            data: string | ArrayBuffer,
            name: string,
            size: number,
            type: string,
        },
    ): Promise<MediaInfoService> {
        // 登録可能な情報を先に取得
        this.mediaData = typeof data.data === 'string'
                            ? data.data
                            : await this.arrayBufferToBase64(data.data, data.name)
        this.mediaInfo.name = data.name
        this.mediaInfo.size = data.size

        if (this.isVideo())
            await this.createVirtualVideo()
        else
            await this.createVirtualAudio()

        return this
    }

    /**
     * MediaInfoを取得
     */
    public getMediaInfo(): MediaInfo {
        return this.mediaInfo
    }

    /**
     * base64文字列のメディアデータの拡張子からデータがビデオかオーディオかを判定
     */
    private isVideo(): boolean {
        const extension = this.getExtension()
        if (extension !== 'audio') {
            return true
        }
        return false
    }

    /**
     * base64文字列のデータから拡張子を取得
     */
    private getExtension(): string {
        const extension = this.mediaData.split(';')[0].split('/')[0]
        this.mediaInfo.type = extension
        return extension
    }

    /**
     * 仮想videoタグを作成し、base64文字列のメディアデータを紐づける
     */
    private async createVirtualVideo(): Promise<void> {
        this.virtualVideo = document.createElement('video')
        return new Promise((resolve, reject) => {
            this.virtualVideo.onloadedmetadata = () => {
                // videoタグからmediaInfoの情報を取得
                this.mediaInfo.duration = this.virtualVideo.duration
                this.mediaInfo.width = this.virtualVideo.videoWidth
                this.mediaInfo.height = this.virtualVideo.videoHeight

                resolve()
            }
            this.virtualVideo.onerror = () => {
                reject()
            }
            this.virtualVideo.src = this.mediaData
        })
    }

    /**
     * 仮想audioタグを作成し、base64文字列のメディアデータを紐づける
     */
    private async createVirtualAudio(): Promise<void> {
        this.virtualAudio = document.createElement('audio')
        return new Promise((resolve, reject) => {
            this.virtualAudio.onloadedmetadata = () => {
                // audioタグからmediaInfoの情報を取得
                this.mediaInfo.duration = this.virtualAudio.duration
                resolve()
            }
            this.virtualAudio.onerror = () => {
                reject()
            }
            this.virtualAudio.src = this.mediaData
        })
    }

    private async arrayBufferToBase64(
        buffer: ArrayBuffer,
        filename: string
    ): Promise<string> {

        const extension: string = filename.split('.').pop() as string
        
        return new Promise((resolve, reject) => {
            const blob = new Blob([buffer])
            const file = new File([blob], filename, { type: MimeTypes[extension] })
            const reader = new FileReader()
            reader.onload = () => resolve(reader.result as string)
            reader.onerror = error => reject(error)
            reader.readAsDataURL(file)
        })
    }
}