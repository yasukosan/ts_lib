/**
 * 情報、エラーの集約
 *
 * setStatus, setError　：情報の設定を行う
 * getStatus, setStatus　：未読情報の取得、既読情報はバックアップ
 *
 */
export default class StatusService {
    private STATUS: Array<any> = [];
    private ERROR: Array<any> = [];
    private _STATUS: Array<any> = [];
    private _ERROR: Array<any> = [];
    private ST_CHECK = false;
    private ER_CHECK = false;

    /**
     * Statusに情報を追加、title指定がある場合は指定titleにて追加
     * 未指定の場合は'State'をtitleにして追加
     *
     * @param state any 保存ステータス
     * @param title string タイトル
     * @return StatusService
     */
    public setStatus(state: any, title = ''): StatusService {
        if (title === '') {
            this.STATUS.push({ title: 'State', message: state });
        } else {
            this.STATUS.push({ title: title, message: state });
        }
        this.onCheck('state');

        return this;
    }

    /**
     * Errorに情報を追加、title指定がある場合は指定titleにて追加
     * 未指定の場合は'Error'をtitleにして追加
     *
     * @param error any　保存エラー内容
     * @param title string　タイトル
     * @return StatusService
     */
    public setError(error: any, title = ''): StatusService {
        if (title === '') {
            console.error('Error Message: ' + error);
            this.ERROR.push({ title: 'Error', message: error });
        } else {
            console.error('Error Message: ' + title + ': ' + error);
            this.ERROR.push({ title: title, message: error });
        }
        this.onCheck('error');

        return this;
    }

    /**
     * Statusを返す
     * 取得済みステータスは
     *
     * @return Array<any>
     */
    public getStatus(): Array<any> {
        const st = this.STATUS;
        this.moveState('state');
        return st;
    }

    /**
     * Errorを返す
     *
     * @return Array<any>
     */
    public getError(): Array<any> {
        // const er = this.ERROR;
        this.moveState('error');
        return this.ERROR;
    }

    /**
     * 未読Statusがあるか返す
     * ある：true ない：false
     *
     * @return boolean
     */
    public checkStatus(): boolean {
        this.offCheck('state');
        if (this.ST_CHECK) {
            return true;
        }
        return false;
    }

    /**
     * 未読Errorがあるか返す
     * ある：true ない：false
     *
     * @return boolean
     */
    public checkError(): boolean {
        this.offCheck('error');
        if (this.ER_CHECK) {
            return true;
        }
        return false;
    }

    /**
     * Statusを初期化
     *
     * @return StatusService
     */
    public resetState(): StatusService {
        this.STATUS = [];
        this._STATUS = [];
        return this;
    }

    /**
     * Errorを初期化
     *
     * @return StatusService
     */
    public resetError(): StatusService {
        this.ERROR = [];
        this._ERROR = [];
        return this;
    }

    /**
     * StatusとErrorを初期化
     *
     * @return StatusService
     */
    public resetAll(): StatusService {
        this.resetState().resetError();
        return this;
    }

    /**
     * ステータス情報を退避させ
     * 情報をリセットする
     *
     * @param tag string 'state' or 'error'
     * @return StatusService
     */
    private moveState(tag: string): StatusService {
        if (tag === 'state') {
            this._STATUS = [...this._STATUS, ...this.STATUS];
            this.resetState();
        }
        if (tag === 'error') {
            this._ERROR = [...this._ERROR, ...this.ERROR];
            this.resetError();
        }
        return this;
    }

    /**
     * 未読フラグを立てる
     *
     * @param tag string 'state' or 'error'
     * @return StatusService
     */
    private onCheck(tag: string): StatusService {
        if (tag === 'state') this.ST_CHECK = true;
        if (tag === 'error') this.ER_CHECK = true;
        return this;
    }

    /**
     * 未読フラグを下ろす
     *
     * @param tag string 'state' or 'error'
     * @return StatusService
     */
    private offCheck(tag: string): StatusService {
        if (tag === 'state') this.ST_CHECK = false;
        if (tag === 'error') this.ER_CHECK = false;
        return this;
    }
}
