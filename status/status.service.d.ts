/**
 * 情報、エラーの集約
 *
 */
export declare class StatusService {
    /**
     * Statusに情報を追加、title指定がある場合は指定titleにて追加
     * 未指定の場合は'State'をtitleにして追加
     *
     * @param state any 保存ステータス
     * @param title string タイトル
     * @return StatusService
     */
    public setStatus(state: any, title: string): StatusService;

    /**
     * Errorに情報を追加、title指定がある場合は指定titleにて追加
     * 未指定の場合は'Error'をtitleにして追加
     *
     * @param error any　保存エラー内容
     * @param title string　タイトル
     * @return StatusService
     */
    public setError(error: any, title: string): StatusService;

    /**
     * 未読Statusを返す
     * 取得済みステータスは返らない
     *
     * @return Array<any>
     */
    public getStatus(): Array<any>;

    /**
     * 未読Errorを返す
     * 取得済みステータスは返らない
     *
     * @return Array<any>
     */
    public getError(): Array<any>;

    /**
     * 未読Statusがあるか返す
     * ある：true ない：false
     *
     * @return boolean
     */
    public checkStatus(): boolean;

    /**
     * 未読Errorがあるか返す
     * ある：true ない：false
     *
     * @return boolean
     */
    public checkError(): boolean;

    /**
     * Statusを初期化
     *
     * @return StatusService
     */
    public resetState(): StatusService;

    /**
     * Errorを初期化
     *
     * @return StatusService
     */
    public resetError(): StatusService;

    /**
     * StatusとErrorを初期化
     *
     * @return StatusService
     */
    public resetAll(): StatusService;

    /**
     * ステータス情報を退避させ
     * 情報をリセットする
     *
     * @param tag string 'state' or 'error'
     * @return StatusService
     */
    private moveState(tag: string): StatusService;

    /**
     * 未読フラグを立てる
     *
     * @param tag string 'state' or 'error'
     * @return StatusService
     */
    private onCheck(tag: string): StatusService;

    /**
     * 未読フラグを下ろす
     *
     * @param tag string 'state' or 'error'
     * @return StatusService
     */
    private offCheck(tag: string): StatusService;
}
