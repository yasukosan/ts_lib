import { requestI2CAccess } from "node-web-i2c"
import MPU6050 from "@chirimen/mpu6050"
import ADT7410 from "@chirimen/adt7410"



export const measure = async () => {
    const i2cAccess = await requestI2CAccess();
    const i2c1 = i2cAccess.ports.get(1);
    const adt7410 = new ADT7410(i2c1, 0x48);
    await adt7410.init();
    const temperature = await adt7410.read();
    console.log(`Temperature: ${temperature} ℃`);
}

measure();