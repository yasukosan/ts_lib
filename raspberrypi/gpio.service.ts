import { GPIOAccess, requestGPIOAccess } from "node-web-gpio";

const sleep = require("util").promisify(setTimeout);

let gpioAccess: GPIOAccess | undefined = undefined
const IO_PORTS = new Map<number, any>();

export const initGpio = async (ports: number[]) => {
    if (gpioAccess !== undefined) {
        return;
    }
    gpioAccess = await requestGPIOAccess();

    for (const port of ports) {
        const portInstance = gpioAccess.ports.get(port);
        if (portInstance === undefined) {
            throw new Error(`Port ${port} not found`);
        }
        IO_PORTS.set(port, portInstance);
        await portInstance.export("out");
    }
}

export const writeGpio = async (port: number, value: number) => {
    const portInstance = IO_PORTS.get(port);
    if (portInstance === undefined) {
        throw new Error(`Port ${port} not found`);
    }
    await portInstance.write(value);
}

export const sleepGpio = async (ms: number) => {
    await sleep(ms);
}
