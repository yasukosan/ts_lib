import { Gpio } from 'pigpio'
import { ReturnError, ReturnSuccess } from '../definisions'
//const Gpio = require('pigpio').Gpio

export type PortsType = {
    port: number,
    mode: "out" | "in"
    pullUpDown?: "up" | "down"
    alert?: boolean
}

const Ports: PortsType[] = [
    { port: 0, mode: "out" }
]

const GpioPorts: { [key:string]: Gpio } = []

export const addPort = (port: PortsType) => {
    if (Ports[0].port === 0) {
        Ports.pop()
    }
    if (Ports.find(p => p.port === port.port)) {
        console.log(`Port ${port.port} already exi sts`)
    }
    Ports.push(port)
}

export const removePort = (port: number) => {
    const index = Ports.findIndex(p => p.port === port)
    if (index !== -1) {
        Ports.splice(index, 1)
    }
}

export const initGpio = () => {

    if (Object.keys(GpioPorts).length > 0) {
        return {
            status: false,
            message: 'GPIO already initialized'
        }
    }

    try {
        for (const port of Ports) {
            GpioPorts[port['port']] = (
                new Gpio(port.port, {
                    mode: port.mode === 'in' ? Gpio.INPUT : Gpio.OUTPUT,
                    pullUpDown: port.pullUpDown === "up" ? Gpio.PUD_UP : Gpio.PUD_DOWN,
                    alert: port.alert
                })
            )
        }
        return {
            status: true,
            message: 'GPIO Initialized'
        }
    } catch (error: any) {
        return {
            status: false,
            message: error.message
        }
    }

}

export const write = (
    port: number,
    value: number
): ReturnSuccess<string> | ReturnError => {
    try {
        if (GpioPorts[port]) {
            GpioPorts[port].digitalWrite(value)
        }
        return {
            status: true,
            message: `Port ${port} value set to ${value}`
        }
    } catch (error: any) {
        return {
            status: false,
            message: error.message
        }
    }
}

export const pwmWrite = (
    port: number,
    value: number
): ReturnSuccess<string> | ReturnError => {
    try {
        if (GpioPorts[port]) {
            GpioPorts[port].pwmWrite(value)
        }
        return {
            status: true,
            message: `Port ${port} value set to ${value}`
        }
    } catch (error: any) {
        return {
            status: false,
            message: error.message
        }
    }
}
