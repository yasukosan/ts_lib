export class TextValidateService {
    public static isMail(mail: string): boolean {
        if (typeof mail === 'string') {
            return mail.match(
                /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/
            )
                ? true
                : false;
        }
        return false;
    }

    public static isNumber(txt: string | number): boolean {
        if (typeof txt === 'number') {
            return true;
        }
        if (typeof txt === 'string') {
            return txt.match(/^\d*$/) ? true : false;
        }
        return false;
    }

    public static isPhone(phone: string | number): boolean {
        if (typeof phone === 'string') {
            return phone.match(/^\d*$/) ? true : false;
        }
        if (
            (typeof phone === 'number' && String(phone).length === 10) ||
            String(phone).length === 11
        ) {
            return true;
        }
        return false;
    }
}
