import * as fs from 'fs'
import * as jwt from 'jsonwebtoken'

export class JWTService {
    private static instance: JWTService;

    private PrivateKey: string = ''
    private PublicKey: string = ''
    private ExpiresIn: number = 300

    public static call() {
        if (!JWTService.instance) {
            JWTService.instance = new JWTService()
        }
        return JWTService.instance;
    }

    /**
     * Sets the expiration time for the JWT.
     * 
     * @param expiresIn - The expiration time in seconds.
     * @returns The updated JWTService instance.
     */
    public setExpiresIn(
        expiresIn: number
    ): JWTService {
        this.ExpiresIn = expiresIn
        return this
    }

    /**
     * Sets the private and public keys for the JWT service.
     * 
     * @param privateKey - The path to the private key file.
     * @param publicKey - The path to the public key file.
     * @returns A promise that resolves when the keys are set.
     */
    public async setKeys(
        privateKey: string,
        publicKey: string
    ): Promise<void> {
        if (!fs.existsSync(privateKey) || !fs.existsSync(publicKey)) {
            this.PrivateKey = privateKey
            this.PublicKey = publicKey
        } else {
            this.PrivateKey = await fs.readFileSync(privateKey, 'utf8')
            this.PublicKey = await fs.readFileSync(publicKey, 'utf8')
        }
    }

    /**
     * Retrieves the public key.
     * 
     * @returns A promise that resolves to the public key as a string.
     */
    public async getPublicKey(): Promise<string> {
        console.log(`Public Key ::`, this.PublicKey)
        return this.PublicKey
    }

    /**
     * Signs a payload using the provided private key and returns a JWT token.
     * @param payload - The payload to be signed.
     * @returns A promise that resolves to the JWT token.
     */
    public async sign(payload: {
        [key: string]: any}
    ): Promise<string> {
        return await jwt.sign(payload, this.PrivateKey, {
            expiresIn: this.ExpiresIn,
            algorithm: 'RS256',
        });
    }

    /**
     * Verifies the given JWT token.
     * 
     * @param token - The JWT token to verify.
     * @returns A promise that resolves to the decoded payload of the JWT token or a string if the token is invalid.
     */
    public async verify(
        token: string
    ): Promise<jwt.JwtPayload | string> {
        try {
            const v = await jwt.verify(token, this.PrivateKey)
            return v
        } catch (error: any) {
            console.error('JWT Verify Error:', error)
            return error.message
        }
        //return jwt.verify(token, this.PrivateKey)
    }
} 
