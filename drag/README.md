
## Drag and Drop イベントで渡されたファイルを受け取り返す

### Drag Start

*  「target」にはDragイベントが設定されたタグのIDを渡す
```javascript
public async dragStart(target: string = 'dragtarget'): Promise<FileHelper>
{
    await DragService.call().setTarget(target);
    return this;
}
```


### Drag End


```javascript
 public async dragEnd(e: any, target: string = 'file'): Promise<FileHelper>
 {
     await DragService.call().onDrop(e);
     this.file = (target === 'file')
         ? DragService.call().getFile()
         : DragService.call().getImage();
     return this;
 }
```