import {
    FileService,
    FileTypes, ImageTypes, ReadMode
} from './file.service';

export type FileType = FileTypes
export type ImageType = ImageTypes

export class DragService {
    private static instance: DragService;

    private onDrag: boolean = false
    private target: HTMLElement | undefined = undefined
    private binary = true
    private drag    : 'drag' | 'select' = 'drag'

    private FS: FileService;

    public constructor() {
        this.FS = new FileService();
    }

    public static call(): DragService {
        if (!DragService.instance) {
            DragService.instance = new DragService();
        }
        return DragService.instance;
    }

    /**
     * ファイルの読み込みモードを変える
     * @ true(デフォルト) バイナリデータ読み込み
     * @ false テキストデータ読み込み
     * @param mode boolean
     * @returns
     */
    public setMode(mode: boolean): DragService {
        this.binary = mode;
        return this;
    }

    /**
     * ファイルをどの形式で読み込むかを指定
     * @param mode ReadMode("dataURL" | "binaryString" | "arrayBuffer" | "text") 
     * @returns 
     */
    public setReadMode(mode: ReadMode): DragService {
        this.FS.setReadMode(mode);
        return this;
    }

    public setFileGetOption(drag: 'drag' | 'select' = 'drag'): DragService {
        this.drag = drag;
        return this;
    }

    /**
     * イベントを監視するタグを指定
     * @param target string タグのID
     * @returns DragService
     */
    public setTarget(target: string): DragService {
        this.reset();
        this.target = document.querySelector('#' + target) as HTMLElement;
        return this;
    }

    /**
     * ターゲットタグにイベントリスナーを登録
     * @returns
     */
    public setEvenetListener(): DragService {
    
        if (this.target === undefined) {
            console.error('Target Element Error')
            return this
        }
        
        this.target.addEventListener('dragover', (e: DragEvent) => {
            this.onDragOver(e)
        })
        this.target.addEventListener('dragleave', (e: DragEvent) => {
            this.onDragLeave(e)
        })

        return this;
    }

    /**
     * ドラッグされたファイルをすべて受け取る
     * @returns FileTypes[]
     */
    public getFile(): FileTypes[] {
        return this.FS.getAllFile()
    }

    /**
     * ドラッグされた画像をすべて受け取る
     * @returns ImageTypes[]
     */
    public getImage(): ImageTypes[] {
        return this.FS.getAllImage()
    }

    public getFileToString(): FileTypes[] {
        const files: FileTypes[] = this.FS.getAllFile()
        for (const key in files) {
            const f = files[key]['data']
            if (typeof f === 'string')
                files[key]['data'] = btoa(f)
        }
        return files
    }

    /**
     *
     * @param event ドラッグイベント
     */
    public onDragOver(
        event: React.DragEvent | DragEvent
    ): DragService {
        event.preventDefault()
        this.onDrag = true
        this.unko(this.onDrag)
        return this
    }
    public onDragLeave(
        event: React.DragEvent | DragEvent
    ): DragService {
        event.stopPropagation()
        this.onDrag = false
        return this
    }
    /**
     * ファイルドロップイベント
     * @param event ドラッグイベント情報
     * @param next コールバック関数
     * @returns Promise<DragService>
     */
    public async onDrop(
        event: React.DragEvent | DragEvent,
        next: ((file: FileTypes[]) => void ) | null = null
    ): Promise<DragService> {
        this.stopScreenEvent(event)

        // イベントからファイル情報を取得
        await this.FS.setFile(event, this.drag, this.binary)

        // this.clearEvent();
        this.startScreenEvent(event)
        event.stopPropagation()

        if (next !== null) {
            next(this.getFile())
        }
        return this
    }

    /**
     * ブラウザのデフォルトイベントを止める
     * @param e Event
     * @returns
     */
    public stopScreenEvent(
        e: React.DragEvent | DragEvent
    ): DragService {
        e.preventDefault()
        return this
    }

    /**
     * ブラウザのデフォルトイベント再開
     * @param e Event
     * @returns
     */
    public startScreenEvent(
        e: React.DragEvent | DragEvent
    ): DragService {
        e.stopPropagation()
        return this
    }

    /**
     * タグに指定されたイベントリスナーを止める
     * @returns
     */
    public clearEvent(): DragService {

        if (this.target === undefined) {
            console.error('Target Element Error')
            return this
        }
        
        this.target.removeEventListener('dragover', (e) => {
            this.onDragOver(e)
        })
        this.target.removeEventListener('dragleave', (e) => {
            this.onDragLeave(e)
        })
        
        return this
    }

    private unko(drag: boolean): void {
        this.onDrag = drag
    }

    private reset(): void {
        this.target = undefined
        this.binary = true
        this.drag = 'drag'
        this.onDrag = false
    }
}
