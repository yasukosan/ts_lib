/**
 * ファイルタイプ
 * data: ファイルデータ
 * type: ファイルタイプ
 * name: ファイル名
 * size: ファイル容量
 * date: ファイル更新日
 */
export interface FileTypes {
    data: string | Uint8Array | ArrayBuffer
    type: string
    name: string
    size: number
    date: number
}
export interface ImageTypes {
    image: string | Uint8Array | ArrayBuffer
    type: string
    name: string
    size: number
    date: number
}

export type ReadMode = 'dataURL' | 'binaryString' | 'arrayBuffer' | 'text'

export class FileService {
    private static instance: FileService

    // 画像ファイルリスト
    private images: ImageTypes[] = []
    // ファイルリスト
    private files: FileTypes[] = []
    // 送信用ファイルの一時保存配列
    // private sendStack = []
    // ファイルAPI
    private Reader: FileReader | undefined = undefined
    // ファイルサイズリミット byte表記
    // private sizeLimit = 5000000;
    private sizeLimit = 250000000

    private readMode: ReadMode = 'dataURL'

    public static call(): FileService {
        if (!FileService.instance) {
            FileService.instance = new FileService()
        }
        return FileService.instance
    }

    /**
     * イベントからファイル情報を登録
     *
     * @param e any ドラッグ・selectイベント
     * @param event string [drag, select]イベント種別
     * @returns boolean
     */
    public async setFile(
        e: DragEvent | React.DragEvent | { target: { files: FileList } },
        event: 'drag' | 'select' = 'drag',
        binary = true
    ): Promise<boolean> {
        let file: FileList | undefined = undefined
        
        console.log(event)

        if (
            'dataTransfer' in e
            && e.dataTransfer !== null
            && e.dataTransfer.files !== null
        )
            file = e.dataTransfer.files
        
        if (
            e.target !== null
            && 'files' in e.target
            && e.target.files !== null
        )
            file = e.target.files
        

        if (file === undefined) {
            console.error('File is not found')
            return false
        }
        return await this.Analysis(file, binary)
    }

    public setReadMode(mode: ReadMode): FileService {
        this.readMode = mode
        return this
    }

    /**
     * ファイルデータを配列に追加
     *
     * @param file any ファイルデータ（base64化されている場合多し）
     * @param type string ファイルタイプ
     * @param name string 取得時のファイル名
     * @param size number ファイル容量
     */
    public addFile(
        file: string | Uint8Array | ArrayBuffer,
        type: string,
        name: string,
        size: number,
        date: number
    ): void {
        const _file: FileTypes = {
            data: file,
            type: type,
            name: name,
            size: size,
            date: date
        }
        this.files = [...this.files, ...[_file]]
    }

    /**
     * 画像データを追加
     * @param image any 画像データ（大体base64文字列）
     * @param type stirng 拡張子
     * @param name string ファイル名
     * @param size number サイズ
     */
    public addImage(
        image: string | Uint8Array | ArrayBuffer,
        type: string,
        name: string,
        size: number,
        date: number
    ): void {
        const _file: ImageTypes = {
            image: image,
            type: type,
            name: name,
            size: size,
            date: date
        }
        this.images = [...this.images, ...[_file]]
    }

    /**
     * 画像以外のファイルを全て取得
     * @returns FileTypes[]
     */
    public getAllFile(): FileTypes[] {
        return this.files
    }

    /**
     * index番号からファイルを取得（画像は取得できない）
     * @param index number 配列番号
     * @returns FileTypes
     */
    public getFileByIndex(index: number): FileTypes {
        const file = this.files.filter((n: FileTypes, i: number) => {
            if (i === index) {
                return n
            }
            return false
        })
        return file[0]
    }

    /**
     * 拡張子からファイルを取得（画像は取得できない）
     * @param type string 拡張子
     * @returns FileTypes[]
     */
    public getFileByType(type: string): FileTypes[] {
        const files = this.files.filter((n: FileTypes) => {
            if (n.type === type) {
                return n
            }
            return false
        })
        return files
    }

    /**
     * 画像データを全て取得
     * @returns ImageTypes[]
     */
    public getAllImage(): ImageTypes[] {
        return this.images
    }

    /**
     * Index番号から画像データを取得
     * @param index number Index番号
     * @returns ImageTypes
     */
    public getImageByIndex(index: number): ImageTypes {
        const image = this.images.filter((n: ImageTypes, i: number) => {
            if (i === index) {
                return n
            }
            return false
        })
        return image[0]
    }

    /**
     * 拡張子から画像データを取得
     * @param type string 拡張子
     * @returns ImageTypes[]
     */
    public getImageByType(type: string): ImageTypes[] {
        const image = this.images.filter((n: ImageTypes) => {
            if (n.type === type) {
                return n
            }
            return false
        })
        return image
    }

    public reset() {
        this.images = []
        this.files = []
    }

    /**
     * ファイルタイプを調べる
     * @param files
     * @returns Promise<boolean>
     */
    private async Analysis(
        files: FileList,
        binary: boolean
    ): Promise<boolean> {
        this.reset()

        const count = files.length
        return new Promise((resolve) => {
            let counter = 0
            const chCounter = () => {
                counter++
                if (count === counter) {
                    resolve(true)
                }
            }

            for (const key in files) {
                if (key in files === false) continue
                if (!this.checkFileSize(files[key])) continue
                // console.log(files[key])
                if (binary) {
                    this.ReadFile(files[key]).then(() => chCounter())
                } else {
                    this.ReadTextFile(files[key]).then(() => chCounter())
                }
            }
        })
    }

    /**
     * ファイルを読み込み
     * @param file
     * @returns
     */
    private async ReadFile(file: File): Promise<boolean> {
        this.Reader = new FileReader()
        return new Promise((resolve) => {
            try {
                if (this.Reader === undefined) {
                    console.log('FileReader is not defined')
                    resolve(false)
                    return
                }
                this.Reader.onloadend = (e: ProgressEvent<FileReader>) => {
                    if (e.target === null || e.target.result === null) return

                    const type = this.checkFileType(file)
                    const _file = e.target.result

                    this.checkImage(type)
                        ? this.addImage(_file, type, file.name, file.size, file.lastModified)
                        : this.addFile(_file, type, file.name, file.size, file.lastModified)

                    resolve(true)
                }

                switch (this.readMode) {
                    case 'binaryString': this.Reader.readAsBinaryString(file); break
                    case 'arrayBuffer': this.Reader.readAsArrayBuffer(file); break
                    case 'text': this.Reader.readAsText(file); break
                    case 'dataURL': this.Reader.readAsDataURL(file); break
                    default: break
                }

            } catch (error) {
                console.log('File Read ERROR ::' + error)
                resolve(false)
            }
        })
    }

    private async ReadTextFile(file: File): Promise<boolean> {
        this.Reader = new FileReader()
        return new Promise((resolve) => {
            try {
                if (this.Reader === undefined) {
                    console.log('FileReader is not defined')
                    resolve(false)
                    return
                }
                const result: string[][] = []
                this.Reader.onloadend = () => {
                    if (this.Reader === undefined || this.Reader.result === null) return

                    const body = this.Reader.result.toString().split('\n');
                    for (let j = 0; j < body.length; j++) {
                        result[j] = body[j].split(',')
                    }
                    const jb = JSON.stringify(result)
                    this.addFile(JSON.parse(jb), 'csv', file.name, file.size, file.lastModified)
                    resolve(true)
                }
                this.Reader.readAsText(file, 'UTF-8')
            } catch (error) {
                console.log('File Read ERROR ::' + error)
                resolve(false)
            }
        })
    }

    /**
     * 画像ファイルか判定
     * @param type
     * @returns
     */
    private checkImage(type: string): boolean {
        return (
            type === 'jpg' ||
            type === 'jpeg' ||
            type === 'gif' ||
            type === 'png'
        )
        ? true
        : false
    }

    /**
     * 拡張子を調べる
     * @param file
     * @returns
     */
    private checkFileType(file: File): string {
        if (file.type.indexOf('jpeg') > 0)
            return 'jpeg'
        if (file.type.indexOf('gif') > 0)
            return 'gif'
        if (file.type.indexOf('png') > 0)
            return 'png'
        if (file.type.indexOf('x-zip-compressed') > 0)
            return 'zip'
        if (file.type.indexOf('x-msdownload') > 0)
            return 'exe'
        if (file.type.indexOf('pdf') > 0)
            return 'pdf'
        if (file.type.indexOf('text') > 0)
            return 'txt'
        if (file.type.indexOf('spreadsheetml') > 0)
            return 'xlsx'
        if (file.type.indexOf('vnd.ms-excel') > 0)
            return 'xls'
        if (file.type.indexOf('wordprocessingml') > 0)
            return 'docx'
        if (file.type.indexOf('msword') > 0)
            return 'doc'
        if (file.type.indexOf('powerpointml') > 0)
            return 'pptx'
        if (file.type.indexOf('vnd.ms-powerpoint') > 0)
            return 'ppt'
        if (file.type.indexOf('json') > 0)
            return 'json'
        if (file.type.indexOf('csv') > 0)
            return 'csv'
        if (file.type.indexOf('html') > 0)
            return 'html'
        if (file.type.indexOf('xml') > 0)
            return 'xml'
        if (file.type.indexOf('javascript') > 0)
            return 'js'
        if (file.type.indexOf('css') > 0)
            return 'css'
        if (file.type.indexOf('php') > 0)
            return 'php'
        if (file.type.indexOf('svg') > 0)
            return 'svg'
        if (file.type.indexOf('wav') > 0)
            return 'wav'
        if (file.type.indexOf('wave') > 0)
            return 'wave'
        if (file.type.indexOf('mp3') > 0)
            return 'mp3'
        return 'file'
    }

    /**
     * ファイルサイズがサイズ制限以下であることを確認
     * 0～制限サイズまで ： True
     * 制限サイズ以上    ： False
     *
     * @param file FileObject
     * @returns boolean
     */
    private checkFileSize(file: File): boolean {
        return (this.sizeLimit === 0 || file.size <= this.sizeLimit) ? true : false
    }
}
