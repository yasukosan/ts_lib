import { CalcService } from '../calc.service';

/**
 * 平均の計算
 */
test('Calc Average ', () => {
    const t1 = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    expect(CalcService.average(t1)).toEqual(5);
});

/**
 * 小数点を含む、平均の計算
 */
test('Calc Average Decimal', () => {
    const t1 = [1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9];

    expect(CalcService.average(t1)).toEqual(5.5);
    expect(CalcService.average(t1, true)).toEqual(5);
});

/**
 * 分散計算
 */
test('Calc Disersion ', () => {
    const t1 = [1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9];

    expect(CalcService.disersion(t1)).toEqual(8.07);
});
