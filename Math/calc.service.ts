export class CalcService {
    /**
     * 1次元配列の平均を求める
     * @param num number[]
     * @param floor boolean (default: false) 切り捨てを行うか
     * @returns number
     */
    public static average(num: number[], floor = false): number {
        const av = num.reduce((sum: number, e: number) => {
            return sum + e;
        }, 0);
        return floor ? Math.floor(av / num.length) : av / num.length;
    }

    /**
     * 1次元配列の分散を求める
     *
     *
     * @param num number[] 分散を求める数値の配列
     * @param n number default 2 四捨五入桁数
     * @returns number 分散
     */
    public static disersion(num: number[], n = 2): number {
        let d = 0;
        const av = CalcService.average(num);
        num.forEach((x, i) => (d += (x - av) ** 2));
        return Math.round((d / num.length) * Math.pow(10, n)) / Math.pow(10, n);
    }
}

/**
 * コサイン類似度を計算して返す
 */
export const cosineSimilarity = (
    val1: number[],
    val2: number[]
): number => {
    const dot = (val1: number[], val2: number[]) => {
        let sum = 0;
        for (let i = 0; i < val1.length; i++) {
            sum += val1[i] * val2[i];
        }
        return sum;
    }

    // ノルムを計算する
    const norm = (val: number[]) => {
        let sum = 0;
        for (let i = 0; i < val.length; i++) {
            sum += val[i] * val[i];
        }
        return Math.sqrt(sum);
    };

    return dot(val1, val2) / (norm(val1) * norm(val2))
}