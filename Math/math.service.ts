export class MathService {
    /**
     * 2次元座標間の距離を計算
     * x, y座標から距離計算
     * @param point1 number[] [x, y, z]
     * @param point2  number[] [x, y, z]
     * @returns number 距離
     */
    public static calc2DDistance(point1: number[], point2: number[]): number {
        const _x = (point1[0] - point2[0]) ** 2;
        const _y = (point1[1] - point2[1]) ** 2;
        return Math.sqrt(_x + _y);
    }

    /**
     * 3次元座標間の距離を計算
     * @param point1 number[] [x, y, z]
     * @param point2  number[] [x, y, z]
     * @returns number 距離
     */
    public static calc3DDistance(point1: number[], point2: number[]): number {
        const _x = (point1[0] - point2[0]) ** 2;
        const _y = (point1[1] - point2[1]) ** 2;
        const _z = (point1[2] - point2[2]) ** 2;
        return Math.sqrt(_x + _y + _z);
    }

    /**
     * 3次元座標から角度を計算
     * @param point1 number[] 座標1 [x:number, y:number, z:number]
     * @param point2 number[] 座標2 [x:number, y:number, z:number]
     * @param point3 number[] 座標3 [x:number, y:number, z:number]
     * @returns number cos値
     */
    public static calc3DAnglar(
        point1: number[],
        point2: number[],
        point3: number[]
    ): number {
        const b1 = [
            point2[0] - point1[0],
            point2[1] - point1[1],
            point2[1] - point1[1]
        ];
        const b2 = [
            point2[0] - point3[0],
            point2[1] - point3[1],
            point2[1] - point3[1]
        ];
        const t1 = b1[0] * b2[0] + b1[1] * b2[1] + b1[2] * b2[2];
        const t2 = Math.sqrt(b1[0] ** 2 + b1[1] ** 2 + b1[2] ** 2);
        const t3 = Math.sqrt(b2[0] ** 2 + b2[1] ** 2 + b2[2] ** 2);
        return t1 / (t2 * t3);
    }

    /**
     * 2次元の座標間距離を斜辺とし
     * x座標の差を底辺としてpoint1 point2の角度を返す
     * @param point1 number[] 座標1 [x:number, y:number]
     * @param point2 number[] 座標2 [x:number, y:number]
     * @return number ラジアン角
     */
    public static calc2DAngle(point1: number[], point2: number[]): number {
        const c = MathService.calc2DDistance(point1, point2);
        const a = point1[0] - point2[0];
        return Math.acos(a / c);
    }

    /**
     * 3点座標から角度を計算
     * point1 → point2 → point3が成す角度になるように座標を渡す
     * @param point1 number[] [x, y]
     * @param point2  number[] [x, y]
     * @param point3  number[] [x, y]
     * @returns number
     */
    public static calc3PointAnglar(
        point1: number[],
        point2: number[],
        point3: number[]
    ): number {
        const ba = [point1[0] - point2[0], point1[1] - point2[1]];
        const bc = [point3[0] - point2[0], point3[1] - point2[1]];
        const babc = ba[0] * bc[0] + ba[1] * bc[1];
        const ban = ba[0] * ba[0] + ba[1] * ba[1];
        const bcn = bc[0] * bc[0] + bc[1] * bc[1];
        const radian = Math.acos(babc / Math.sqrt(ban * bcn));
        return (radian * 180) / Math.PI;
    }

    /**
     * 傾き計算
     * 端末のスペック不足で座標情報が正しく取得できない場合
     * 誤差が大きくなるので注意
     * @param point1 number[] [x,y,z] 基準点
     * @param point2 number[] [x,y,z] 動点
     * @returns number ラジアン角
     */
    public static calcInclination(point1: number[], point2: number[]): number {
        const bottom = Math.abs(point1[1] - point2[1]);
        const height = Math.abs(point1[0] - point2[0]);
        const tan = Math.tan(height / bottom) / (Math.PI / 180);

        if (point1[0] > point2[0]) {
            return -1 * tan;
        }
        return tan;
    }

    /**
     * 顔の向きと、強度を計算（正面：0%、真横：100%）
     *
     * @param horizontal number[] 顔の左右幅
     * @param vertical nmber[] 顔の上下幅
     * @returns number[] 上下左右の傾き割合
     *
     */
    public static calcOrientation(
        horizontal: number[],
        vertical: number[]
    ): number[] {
        // 初期化
        const rote = [0, 0, 0, 0];

        const truncation = (num: number): number => {
            return Math.floor(num * Math.pow(10, 4)) / Math.pow(10, 4);
        };

        // 左右計算
        const _ho = horizontal[0] - horizontal[1];
        if (_ho > 0) {
            rote[2] = truncation(_ho / horizontal[0]);
        } else {
            rote[3] = -1 * truncation(_ho / horizontal[1]);
        }

        // 上下計算
        const _ve = vertical[0] - vertical[1];
        if (_ve > 0) {
            rote[1] = truncation(_ve / vertical[0]);
        } else {
            rote[0] = -1 * truncation(_ve / vertical[1]);
        }
        return rote;
    }

    /**
     * 座標の分散を計算
     * @param p
     * @param history
     * @param ref
     * @returns
     */
    public static calcDispersion(history: number[]): number {
        const average = (numbers: number[]): number => {
            const reducer = (
                accumulator: number,
                currentValue: number,
                _: number,
                { length }: { length: number }
            ) => accumulator + currentValue / length;
            return Math.floor(numbers.reduce(reducer, 0));
        };

        const avr_x = average(history);
        //const avr_y = average(history.y);

        let dis_x = 0;
        // let dis_y = 0;
        history.forEach(
            (val: number) => (dis_x += (val - avr_x) ** 2)
        );
        //history.y.forEach((val: number, i: number) => dis_y += (val - avr_y) ** 2);
        return dis_x;
    }

    /**
     * 平均から最終値がどれだけ乖離しているか割合を返す
     * マイナスの場合は負の方向に乖離
     * プラスの場合は正の方向に乖離
     * @param history number[]
     * @returns number
     */
    public static calcDivergence(history: number[], now: number): number {
        const av = MathService.calcAverage(history);
        return now / av;
    }

    public static calcAverage(numbers: number[]): number {
        const reducer = (
            accumulator: number,
            currentValue: number,
            _: number,
            { length }: { length: number }
        ) => accumulator + currentValue / length;
        return Math.floor(numbers.reduce(reducer, 0));
    }
}
