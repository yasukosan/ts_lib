export class StopWatchService {
    private static instance: StopWatchService;

    private target: any = {};

    public static call(): StopWatchService {
        if (!StopWatchService.instance) {
            StopWatchService.instance = new StopWatchService();
        }
        return StopWatchService.instance;
    }

    public set(target: string): StopWatchService {
        this.target[target] = this.getInitial();
        return this;
    }

    public reset(target: string): StopWatchService {
        this.set(target);
        return this;
    }

    public check(target: string): number {
        if (target in this.target) {
            const now = new Date().getTime();
            return now - this.target[target]['base'];
        }
        return 0;
    }

    public lap(target: string): number {
        if (target in this.target) {
            const now = new Date().getTime();
            this.target[target]['lap'] = now - this.target[target]['lap'];
            return this.target[target]['lap'];
        }
        return 0;
    }

    private getInitial() {
        return {
            base: new Date().getTime(),
            lap: 0
        };
    }
}
