import {
    PushDictOptionsType,
} from "./__helper/definisions"

let endpoint = 'http://localhost:50021'

export type UserDictType =
{
    [key: string]: {                    // Key（UUID）
        surface: string,                // 表記
        priority: number,               // 優先度
        context_id: number,             // コンテキストID (デフォルト1348)
        part_of_speech: string,         // 品詞
        part_of_speech_detail_1: string,    // 品詞分類1
        part_of_speech_detail_2: string,    // 品詞分類2
        part_of_speech_detail_3: string,    // 品詞分類3
        inflectional_type: string,      // 活用型
        inflectional_form: string,      // 活用形
        stem: string,                   // 原形
        yomi: string,                   // 読み
        pronunciation: string,          // 発音
        accent_type: number,            // アクセント型
        mora_count: number,             // モーラ数
        accent_associative_rule: string // アクセント結合規則
    }[]
}

export type UserDictWordType = {
    surface: string,        // 表記
    pronunciation: string,  // 読み(カタカナ)
    accent_type: number,    // アクセント(アクセントの場所を番号で指定)
    // PROPER_NOUN（固有名詞）、COMMON_NOUN（普通名詞）
    // VERB（動詞）、ADJECTIVE（形容詞）、SUFFIX（語尾）
    word_type: "PROPER_NOUN" | "COMMON_NOUN" | "VERB" | "ADJECTIVE" | "SUFFIX"
    priority: number,       // 優先度0~10まで指定可能 (1~9を推奨)
}


const PushDictOptions: PushDictOptionsType = {
    url: endpoint + '/user_dict_word',
    surface: 'こんにちは',
    pronunciation: 'コンニチハ',
    accent_type: 1,
}

export const setPushDictOptions = (options: Partial<PushDictOptionsType>) => {
    Object.assign(PushDictOptions, options)
}

export const getPushDictOptions = () => {
    return PushDictOptions
}

export const setEndpoint = (url: string) => {
    endpoint = url
    PushDictOptions.url = endpoint + '/user_dict_word'
}


export const getDictionary = async () => {
    const response = await fetch(endpoint + '/user_dict', {
        method: 'GET',
    })

    if (response.status !== 200) {
        console.error('Failed to get dictionary:', response.statusText)
        return {
            status: false,
            message: response.statusText
        }
    }
    return {
        status: true,
        message: await response.json()
    }
}


export const addDictionary = async (
    word: UserDictWordType
) => {
    const response = await fetch(
        endpoint + '/user_dict_word?'
        + 'surface=' + word.surface
        + '&pronunciation=' + word.pronunciation
        + '&accent_type=' + word.accent_type
        + '&word_type=' + word.word_type
        + '&priority=' + word.priority
        , {
        method: 'POST'
    })

    if (response.status !== 200) {
        console.error('Failed to add dictionary:', response.statusText)
        return {
            status: false,
            message: response.statusText
        }
    }
    return {
        status: true,
        message: 'Success'
    }
}

export const updateDictionary = async (
    id: string,
    word: UserDictWordType
) => {
    const response = await fetch(
        endpoint + '/user_dict_word/' + id + '?'
        + 'surface=' + word.surface
        + '&pronunciation=' + word.pronunciation
        + '&accent_type=' + word.accent_type
        + '&word_type=' + word.word_type
        + '&priority=' + word.priority
        , {
        method: 'PUT'
    })

    if (response.status !== 200) {
        console.error('Failed to update dictionary:', response.statusText)
        return {
            status: false,
            message: response.statusText
        }
    }
    return {
        status: true,
        message: 'Success'
    }
}

export const deleteDictionary = async (
    id: string
) => {
    const response = await fetch(endpoint + '/user_dict_word/' + id, {
        method: 'DELETE',
    })

    if (response.status !== 200) {
        console.error('Failed to delete dictionary:', response.statusText)
        return {
            status: false,
            message: response.statusText
        }
    }
    return {
        status: true,
        message: 'Success'
    }
}

export const importDictionary = async (
    dict: UserDictType
) => {
    const response = await fetch(endpoint + '/import_user_dict', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(dict),
    })

    if (response.status !== 200) {
        console.error('Failed to import dictionary:', response.statusText)
        return {
            status: false,
            message: response.statusText
        }
    }
    return {
        status: true,
        message: 'Success'
    }
}