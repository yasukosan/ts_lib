import { blobToUint8Array } from "../_helper/convert.helper"
import {
    PushOptionsType,
    PushSynthesisOptionsType,
    QueryOptionsType
} from "./__helper/definisions"

let endpoint = 'http://localhost:50021'

const PushOptions: PushOptionsType = {
    url: endpoint + '/audio_query',
    text: 'Hello, World!',
    speaker: 1,
}

const PushSynthesisOptions: PushSynthesisOptionsType = {
    url: endpoint + '/synthesis',
    speaker: 1,
    query: {
        accent_phrase: [
            {
                moras: [
                    {
                        text: 'コ',
                        consonant: 'k',
                        consonant_length: 1,
                        vowel: 'o',
                        vowel_length: 1,
                        pitch: 0
                    },
                ],
                accent: 2,
                pause_mora: 0,
                is_interrogative: false
            }
        ],
        speedScale: 1.0,
        pitchScale: 1.0,
        intonationScale: 1.0,
        volumeScale: 1.0,
        prePhonemeLength: 0,
        postPhonemeLength: 0,
        pauseLength: null,
        pauseLengthScale: 1.0,
        outputSamplingRate: 24000,
        outputStereo: false,
        kana: 'コンニチハ'
    },
    filename: 'test.wav',
    audioData: null,
    playtime: 0,
}

export const setPushOptions = (options: Partial<PushOptionsType>) => {
    Object.assign(PushOptions, options)
}

export const setQueryOptions = (options: Partial<QueryOptionsType>) => {
    Object.assign(PushSynthesisOptions.query, options)
}

export const setEndpoint = (url: string) => {
    endpoint = url
    PushOptions.url = endpoint + '/audio_query'
    PushSynthesisOptions.url = endpoint + '/synthesis'
}

export const getQueryOptions = () => {
    return PushSynthesisOptions.query
}

/**
 * テキストを送信し、音声合成用クエリーを取得
 * @returns 
 */
export const callAudioQuery = async () => {
    const response = await fetch(
        PushOptions.url +
        '?speaker=' + PushOptions.speaker +
        '&text=' + PushOptions.text,
    {
        method: 'POST'
    })

    if (!response.ok) {
        console.error('Failed to fetch audio query:', response.statusText)
        return null
    }

    PushSynthesisOptions.query = await response.json()

    // console.log('Result Query', PushSynthesisOptions.query)
    return PushSynthesisOptions.query
}

/**
 * 合成用クエリーを送信し、音声データを取得
 * @param returnFormat 
 * @returns Promise<{
 *   name: string
 *   audio: T
 *  time: number
 * } | false>
 */
export const callSynthesisQuery = async<T extends Uint8Array | Blob> (
    returnFormat: 'blob' | 'uint8array' = 'blob'
): Promise<{
    name: string
    audio: T
    time: number
} | false> => {
    const response = await fetch(
        PushSynthesisOptions.url +
        '?speaker=' + PushSynthesisOptions.speaker,
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(PushSynthesisOptions.query)
    })

    if (!response.ok) {
        console.error('Failed to fetch synthesis query:', response.statusText)
        return false
    }
    // console.log('VoiceVox Audio Response :: ', response)
    // オーディオデータをBlob形式で取得
    if (returnFormat === 'uint8array') {
        // Blobを複製
        const blob = new Blob([await response.blob()]) 
        PushSynthesisOptions.audioData = await blobToUint8Array(blob)
    } else {
        PushSynthesisOptions.audioData = await response.blob()
    }
    // オーディオデータの再生時間を取得
    PushSynthesisOptions.playtime = await getWavDurationFromBlob(PushSynthesisOptions.audioData)

    // console.log('Result Synthesis', PushSynthesisOptions.audioData)
    return {
        name: PushSynthesisOptions.filename,
        audio: PushSynthesisOptions.audioData as T,
        time: PushSynthesisOptions.playtime
    }
}

/**
 * オーディオデータをダウンロード
 */
export const downloadFileData = () => {
    if (PushSynthesisOptions.audioData) {
        const blob = new Blob([PushSynthesisOptions.audioData], { type: 'audio/wav' })
        const url = URL.createObjectURL(blob)
        const a = document.createElement('a')
        a.href = url
        a.download = PushSynthesisOptions.filename
        document.body.appendChild(a)
        a.click()
    }
}

/**
 * オーディオデータの再生時間を取得し返す
 * @param blob Blob
 * @returns 
 */
const getWavDurationFromBlob = async (
    data: Blob | Uint8Array
): Promise<number> => {

    // dataがblobの場合、Uint8Arrayに変換
    const _data = data instanceof Blob ? await blobToUint8Array(data) : data

    return new Promise((resolve, reject) => {
        const audioContext = new (window.AudioContext || (window as any).webkitAudioContext)();
        audioContext.decodeAudioData(_data.buffer, (audioBuffer) => {
            resolve(audioBuffer.duration);
        }, (error) => {
            reject(error);
        });
    });
}