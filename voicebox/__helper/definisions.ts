export type PushOptionsType = {
    url: string
    text: string
    speaker: number
}

export type PushSynthesisOptionsType = {
    url: string
    speaker: number
    query: QueryOptionsType
    filename: string
    audioData: Uint8Array | Blob | null
    playtime: number
}

export type PushDictOptionsType = {
    url: string
    surface: string
    pronunciation: string
    accent_type: number
}

export type QueryOptionsType = {
    accent_phrase: {
        moras: {
            text: string
            consonant: string | null
            consonant_length: number | null
            vowel: string | null
            vowel_length: number | null
            pitch: number | null
        }[]
        accent: number
        pause_mora: number | null
        is_interrogative: boolean
    }[]
    speedScale: number
    pitchScale: number
    intonationScale: number
    volumeScale: number
    prePhonemeLength: number
    postPhonemeLength: number
    pauseLength: number | null
    pauseLengthScale: number
    outputSamplingRate: number
    outputStereo: boolean
    kana: string
}
