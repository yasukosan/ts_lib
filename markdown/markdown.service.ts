import { marked } from 'marked'

let target: HTMLElement | undefined = undefined
let markdown = ''

export const setTarget = (
    tag: HTMLElement | string
) => {
    if (typeof tag === 'string') {
        const element = document.getElementById(tag) as HTMLElement
        if (element === null) {
            throw new Error('Element not found')
            return
        }
        target = element
    }
    else {
        target = tag
    }
}


export const showMarkdown = async (
    markdown: string
): Promise<void> => {
    if (target !== undefined) {
        target.innerHTML = await marked.parse(sanitizeString(markdown))
    }
}

export const sanitizeString = (
    input: string | undefined | null | unknown
): string => {
    if (typeof input === 'string') {
        return input
    }
    return ''
}