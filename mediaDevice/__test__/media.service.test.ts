import { describe, it, expect, vi } from 'vitest'
import { MediaService } from '../media.service'
import { StreamModeService, StreamModeInterface } from '../stream_mode.service'
import { DeviceListService } from '../device_list.service'
import { MediaAPIListService } from '../mediaapi_list.service'

describe('MediaService', () => {
    let mediaService: MediaService

    beforeEach(() => {
        mediaService = MediaService.call()
    })

    it('インスタンスがシングルトンであることを確認する', () => {
        const anotherInstance = MediaService.call()
        expect(mediaService).toBe(anotherInstance)
    })

    it('StreamModeServiceを返す', () => {
        const streamModeService = mediaService.callStreamModeService()
        expect(streamModeService).toBeInstanceOf(StreamModeService)
    })

    it('DeviceListServiceを返す', () => {
        const deviceListService = mediaService.callDeviceListService()
        expect(deviceListService).toBeInstanceOf(DeviceListService)
    })

    it('MediaAPIListServiceを返す', () => {
        const mediaAPIListService = mediaService.callMediaAPIListService()
        expect(mediaAPIListService).toBeInstanceOf(MediaAPIListService)
    })

    it('取得したMediaStreamを返す', () => {
        const mockStream = {} as MediaStream
        mediaService.setStream(mockStream)
        expect(mediaService.getStream()).toBe(mockStream)
    })

    it('MediaStreamと接続したHTMLVideoElementを返す', () => {
        const mockVideoElement = document.createElement('video')
        mediaService.setVideoTarget(mockVideoElement)
        expect(mediaService.getVideoTarget()).toBe(mockVideoElement)
    })

    it('ローカルストリームを取得する', async () => {
        const mockStream = {} as MediaStream
        const mockGetUserMedia = vi.fn().mockResolvedValue(mockStream)
        Object.defineProperty(navigator, 'mediaDevices', {
            value: { getUserMedia: mockGetUserMedia },
            configurable: true
        })
        const result = await mediaService.getLocalStream({ video: true, audio: true }, 'getUserMedia')
        expect(result).toBe(true)
        expect(mediaService.getStream()).toBe(mockStream)
    })

    it('ビデオ再生を行う', async () => {
        const mockStream = {} as MediaStream
        const mockVideoElement = document.createElement('video')
        mediaService.setStream(mockStream)
        mediaService.setVideoTarget(mockVideoElement)

        // playメソッドをスパイして、Promiseを返すようにする
        // 本来はawaitが必要だが、この後にonloadedmetadataイベントをトリガーするため
        // イベントの発生を待たずに処理が進むように、awaitを省略している
        const playSpy = vi.spyOn(mockVideoElement, 'play').mockImplementation(() => Promise.resolve())
        mediaService.playVideo()

        // onloadedmetadataイベントを手動でトリガーする
        const loadedMetadataEvent = new Event('loadedmetadata')
        mockVideoElement.dispatchEvent(loadedMetadataEvent)

        expect(playSpy).toHaveBeenCalled()
        expect(mockVideoElement.srcObject).toBe(mockStream)
    })

    it('ビデオ停止を行う', () => {
        const mockStream = {
            getVideoTracks: vi.fn().mockReturnValue([{ stop: vi.fn() }]),
            getAudioTracks: vi.fn().mockReturnValue([{ stop: vi.fn() }])
        } as unknown as MediaStream
        const mockVideoElement = document.createElement('video')
        mediaService.setStream(mockStream)
        mediaService.setVideoTarget(mockVideoElement)

        // pauseメソッドをスパイしてモックする
        const pauseSpy = vi.spyOn(mockVideoElement, 'pause').mockImplementation(() => {})

        mediaService.stopVideo()
        expect(mockStream.getVideoTracks()[0].stop).toHaveBeenCalled()
        expect(mockStream.getAudioTracks()[0].stop).toHaveBeenCalled()
        expect(mediaService.getStream()).toBeUndefined()
        expect(mediaService.getVideoTarget()).toBe(mockVideoElement)
        expect(mockVideoElement.srcObject).toBeNull()
        expect(pauseSpy).toHaveBeenCalled()
    })

    it('全リセットを行う', () => {
        const resetDeviceListServiceSpy = vi.spyOn(DeviceListService.prototype, 'reset')
        const resetMediaAPIListServiceSpy = vi.spyOn(MediaAPIListService.prototype, 'reset')
        mediaService.reset()
        expect(mediaService.getStream()).toBeUndefined()
        expect(mediaService.getVideoTarget()).toBe(false)
        expect(resetDeviceListServiceSpy).toHaveBeenCalled()
        expect(resetMediaAPIListServiceSpy).toHaveBeenCalled()
    })
})