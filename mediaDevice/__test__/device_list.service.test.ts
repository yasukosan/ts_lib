import { describe, it, expect, vi } from 'vitest'
import { DeviceListService, DeviceInterface } from '../device_list.service'

// モックデバイスデータ
const mockDevices: DeviceInterface[] = [
    { deviceId: '1', groupId: 'group1', kind: 'videoinput', label: 'Webcam' },
    { deviceId: '2', groupId: 'group1', kind: 'audioinput', label: 'Microphone' },
    { deviceId: '3', groupId: 'group2', kind: 'audiooutput', label: 'Speakers' },
];

// navigator.mediaDevices.enumerateDevicesのモック
vi.stubGlobal('navigator', {
    mediaDevices: {
        enumerateDevices: vi.fn().mockResolvedValue(mockDevices),
    },
});

describe('DeviceListServiceのテスト', () => {
    it('インスタンスが正しく作成されること', () => {
        const service = DeviceListService.call();
        expect(service).toBeInstanceOf(DeviceListService);
    });

    it('映像入力デバイスが正しく取得されること', async () => {
        const service = DeviceListService.call();
        await service.SearchDeviceList();
        const videoDevices = service.getVideoInputDevices();
        expect(videoDevices).toEqual([mockDevices[0]]);
    });

    it('音声入力デバイスが正しく取得されること', async () => {
        const service = DeviceListService.call();
        await service.SearchDeviceList();
        const audioInputDevices = service.getAudioInputDevices();
        expect(audioInputDevices).toEqual([mockDevices[1]]);
    });

    it('音声出力デバイスが正しく取得されること', async () => {
        const service = DeviceListService.call();
        await service.SearchDeviceList();
        const audioOutputDevices = service.getAudioOutputDevices();
        expect(audioOutputDevices).toEqual([mockDevices[2]]);
    });

    it('デバイスリストがリセットされること', async () => {
        const service = DeviceListService.call();
        await service.SearchDeviceList();
        service.reset();
        expect(service.getVideoInputDevices()).toEqual([]);
        expect(service.getAudioInputDevices()).toEqual([]);
        expect(service.getAudioOutputDevices()).toEqual([]);
    });
});