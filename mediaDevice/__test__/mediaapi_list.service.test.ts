import { describe, it, expect, vi, beforeEach } from 'vitest'
import { MediaAPIListService } from '../mediaapi_list.service'

describe('MediaAPIListService', () => {
    let mediaAPIListService: MediaAPIListService

    beforeEach(() => {
        mediaAPIListService = MediaAPIListService.call()
        mediaAPIListService.reset()
    })

    it('インスタンスがシングルトンであることを確認する', () => {
        const anotherInstance = MediaAPIListService.call()
        expect(mediaAPIListService).toBe(anotherInstance)
    })

    it('指定のMediaAPIが利用可能か判定する', () => {
        const mockNavigator = {
            mediaDevices: {
                getUserMedia: vi.fn()
            }
        }
        global.navigator = mockNavigator as unknown as Navigator

        expect(mediaAPIListService.checkMedia('getUserMedia')).toBe(true)
        expect(mediaAPIListService.checkMedia('getDisplayMedia')).toBe(false)
    })

    it('使用可能なMediaAPI一覧を取得する', () => {
        const mockNavigator = {
            mediaDevices: {
                getUserMedia: vi.fn(),
                getDisplayMedia: vi.fn()
            }
        }
        global.navigator = mockNavigator as unknown as Navigator

        const mediaAPIs = mediaAPIListService
                            .checkAvailableUserMedia()
                            .getMdiaAPI()
        expect(mediaAPIs).toContain('getUserMedia')
        expect(mediaAPIs).toContain('getDisplayMedia')
    })

    it('利用可能なMediaAPI機能を検索する', () => {
        const mockNavigator = {
            mediaDevices: {
                getUserMedia: vi.fn(),
                getDisplayMedia: vi.fn()
            }
        }
        global.navigator = mockNavigator as unknown as Navigator

        mediaAPIListService.checkAvailableUserMedia()
        const mediaAPIs = mediaAPIListService.getMdiaAPI()
        expect(mediaAPIs).toContain('getUserMedia')
        expect(mediaAPIs).toContain('getDisplayMedia')
    })

    it('MediaAPIリストを初期化する', () => {
        const mockNavigator = {
            mediaDevices: {
                getUserMedia: vi.fn(),
                getDisplayMedia: vi.fn()
            }
        }
        global.navigator = mockNavigator as unknown as Navigator

        mediaAPIListService.checkAvailableUserMedia()
        mediaAPIListService.reset()
        expect(mediaAPIListService.getMdiaAPI()).toEqual([])
    })
})