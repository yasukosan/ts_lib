
# ファイル参照構造
```
media.service
    |
    |- device_list.service
    |- mediaapi_list.service
    |- stream_mode.service
```

# サンプル


## 使用可能なMediaAPIの一覧を取得

```javascript
const ms = new MediaService();
const hoge = ms.callMediaAPIListService().getMdiaAPI();

console.log(hoge);

```

## 使用可能なデバイスの一覧を取得

```javascript
const ms = new MediaService();
const result = await ms
                .callDeviceListService()
                .SearchDeviceList();

console.log(result)

```

## カメラ映像を取得、再生

```javascript
const ms = new MediaService();
const result = await ms.getLocalStream(
                    ms.callStreamModeService()
                        .onVideo()
                        .getStreamMode(),
                    'getUserMedia'
                );

console.log(result)

ms.setVideoTarget('ターゲットビデオタグのID')
    .playVideo();

```

## スクリーンシェアでデスクトップを共有

```javascript
const ms = new MediaService();
const result = await ms.getLocalStream(
                    ms.callStreamModeService().getStreamMode(),
                    'getDisplayMedia'
                );

console.log(result)

ms.setVideoTarget('ターゲットビデオタグのID')
    .playVideo();

```

