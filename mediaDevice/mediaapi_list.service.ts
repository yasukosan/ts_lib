export class MediaAPIListService {
    private static instance: MediaAPIListService

    private mediaAPI: string[] = []

    public static call(): MediaAPIListService {
        if (!MediaAPIListService.instance) {
            MediaAPIListService.instance = new MediaAPIListService()
        }
        return MediaAPIListService.instance
    }

    /**
     * 指定のMediaAPIが利用可能か判定
     * @param media
     * @returns boolean
     */
    public checkMedia(media = 'getUserMedia'): boolean {
        if (this.mediaAPI.length === 0) {
            this.checkAvailableUserMedia()
        }
        return this.mediaAPI.indexOf(media) === -1 ? false : true;
    }

    /**
     * 取得したMediaAPIリストを返す
     * @returns string[]
     */
    public getMdiaAPI(): string[] {
        return this.mediaAPI
    }

    /**
     * 利用可能なMediaAPI機能を検索
     *
     * 検索対象MediaAPI
     * [getUserMedia, getDisplayMedia, webkitGetUserMedia, mozGetUserMedia]
     *
     * @returns MediaHelperService
     */
    public checkAvailableUserMedia(): MediaAPIListService {
        if (this.mediaAPI.length > 0) {
            return this
        }

        const medias: string[] = []

        if (
            'getUserMedia' in navigator.mediaDevices &&
            typeof navigator.mediaDevices['getUserMedia'] === 'function'
        ) {
            // console.log('Support Media API : getUserMedia');
            medias.push('getUserMedia')
        }
        if (
            'getDisplayMedia' in navigator.mediaDevices &&
            typeof navigator.mediaDevices['getDisplayMedia'] === 'function'
        ) {
            // console.log('Support Media API : getDisplayMedia');
            medias.push('getDisplayMedia')
        }

        if (medias.length === 0) {
            this.mediaAPI = []
        } else {
            this.mediaAPI = medias
        }
        return this
    }

    /**
     * MediaAPIリストを初期化
     * @returns void
     */
    public reset(): void {
        this.mediaAPI = []
        return
    }
}
