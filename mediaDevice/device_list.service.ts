export interface DeviceInterface {
    deviceId: string; // デバイス毎の固有ID
    groupId: string; // ヘッドセット、Webカメラ等の同一デバイスグループ
    kind: string; // 種別（videoinput, audioinput, audiooutput...等）
    label: string; // 製品名
}

export class DeviceListService {
    private static instance: DeviceListService;

    private VideoInputDevices: DeviceInterface[] = [];
    private AudioInputDevices: DeviceInterface[] = [];
    private AudioOutputDevices: DeviceInterface[] = [];

    public static call(): DeviceListService {
        if (!DeviceListService.instance) {
            DeviceListService.instance = new DeviceListService();
        }
        return DeviceListService.instance;
    }

    public constructor() {
        // インスタンス作成時にデバイス検索を一応行う
        this.SearchDeviceList();
    }

    /**
     * 映像入力可能なデバイス一覧を取得
     *
     * @returns DeviceInterface[]
     */
    public getVideoInputDevices(): DeviceInterface[] {
        return this.VideoInputDevices;
    }

    /**
     * 音声入力可能なデバイス一覧を取得
     *
     * @returns DeviceInterface[]
     */
    public getAudioInputDevices(): DeviceInterface[] {
        return this.AudioInputDevices;
    }

    /**
     * 音声出力可能なデバイス一覧を取得（クロスブラウザ問題有り）
     *
     * @returns DeviceInterface[]
     */
    public getAudioOutputDevices(): DeviceInterface[] {
        return this.AudioOutputDevices;
    }

    /**
     * 選択可能なデバイスリストを取得
     *
     * @returns Promise<boolean>
     */
    public async SearchDeviceList(): Promise<boolean> {
        // mediaDevicesから情報が取得できるか確認
        if (
            !navigator.mediaDevices ||
            !navigator.mediaDevices.enumerateDevices
        ) {
            console.log('enumerateDevices() not supported.');
            return false;
        }
        // 取得可能なデバイス一覧を取得
        try {
            // console.log('Media Devices ', navigator.mediaDevices);
            const devices = await navigator.mediaDevices.enumerateDevices();
            devices.forEach((device) => {
                this.SortDevice(device);
            });
            return true;
        } catch (e) {
            return false
        }
    }

    /**
     * リストを初期化
     * @returns void
     */
    public reset(): void {
        this.VideoInputDevices = [];
        this.AudioInputDevices = [];
        this.AudioOutputDevices = [];
    }
    /**
     * 取得したデバイス一覧を種類ごとに振り分け
     * @param device DeviceInterface
     */
    private SortDevice(device: DeviceInterface): void {
        // console.log(device);
        if (device.kind === 'videoinput') {
            this.AddVideoInput(device);
        } else if (device.kind === 'audioinput') {
            this.AddAudioInput(device);
        } else if (device.kind === 'audiooutput') {
            this.AddAudioOutput(device);
        }
    }

    /**
     * ビデオ入力追加
     * @param device DeviceInterface
     * @returns DeviceListService
     */
    private AddVideoInput(device: DeviceInterface): DeviceListService {
        if (!this.checkKnownDevice(this.VideoInputDevices, device.deviceId)) {
            this.VideoInputDevices.push(device);
        }
        return this;
    }

    /**
     * オーディオ入力追加
     * @param device DeviceInterface
     * @returns DeviceListService
     */
    private AddAudioInput(device: DeviceInterface): DeviceListService {
        if (!this.checkKnownDevice(this.AudioInputDevices, device.deviceId)) {
            this.AudioInputDevices.push(device);
        }
        return this;
    }

    /**
     * オーディオ出力追加
     * @param device DeviceInterface
     * @returns DeviceListService
     */
    private AddAudioOutput(device: DeviceInterface): DeviceListService {
        if (!this.checkKnownDevice(this.AudioOutputDevices, device.deviceId)) {
            this.AudioOutputDevices.push(device);
        }
        return this;
    }

    /**
     * 登録済みのデイバイスIDと一致しているか確認
     *
     * default device, USB Headset 等単一のデバイスが
     * 都合上複数回別デバイスとして返ってくる場合があるので、その対応
     * @param list DeviceInterface[]
     * @param id string
     * @returns boolean
     */
    private checkKnownDevice(list: DeviceInterface[], id: string): boolean {
        let r = false;
        for (const key in list) {
            if (Object.prototype.hasOwnProperty.call(list, key)) {
                r = list[key]['deviceId'] === id ? true : false;
            }
        }
        return r;
    }
}
