import { StreamModeService, StreamModeInterface } from './stream_mode.service'
import { DeviceListService } from './device_list.service'
import { MediaAPIListService } from './mediaapi_list.service'

export class MediaService {
    private static instance: MediaService

    private LocalVideoTarget: HTMLVideoElement | undefined = undefined
    private LocalStream: MediaStream | undefined = undefined

    public static call(): MediaService {
        if (!MediaService.instance) {
            MediaService.instance = new MediaService()
        }
        return MediaService.instance
    }

    public callStreamModeService(): StreamModeService {
        return StreamModeService.call()
    }

    /**
     * DeviceListServiceを返す
     *
     * @returns DeviceListService
     */
    public callDeviceListService(): DeviceListService {
        return DeviceListService.call()
    }

    /**
     * MediaAPIListServiceを返す
     *
     * @returns MediaAPIListService
     */
    public callMediaAPIListService(): MediaAPIListService {
        return MediaAPIListService.call()
    }

    /**
     * 取得したMediaStreamを返す
     * @returns
     */
    public getStream(): MediaStream {
        return this.LocalStream as MediaStream
    }

    /**
     * MediaStreamと接続したHTMLVideoElementを返す
     * @returns HTMLVideoElement
     */
    public getVideoTarget(): HTMLVideoElement | false {
        return (this.LocalVideoTarget === undefined) ? false : this.LocalVideoTarget
    }

    /**
     * ローカルストリームを取得
     *
     * @param contribute StreamModeInterface ストリーム設定
     * @param media string mediaAPIの指定
     * @returns Promise<boolean>
     */
    public async getLocalStream(
        contribute: StreamModeInterface = {},
        media:
            | 'enumerateDevices'
            | 'getSupportedConstraints'
            | 'getDisplayMedia'
            | 'getUserMedia' = 'getUserMedia'
    ): Promise<boolean> {
        // 配信モード未設定の場合、video audioをtrueで初期化
        // getDisplayMediaの場合も強制的に指定
        if (!this.checkStreamMode(contribute) || media === 'getDisplayMedia') {
            contribute = { video: true, audio: true }
        }

        // mediaDeviceの取得オプション文字列取得
        const check = MediaAPIListService.call().checkMedia(media)

        if (!check) return false;

        // メディアデバイスに接続
        const stream: MediaStream = (await navigator.mediaDevices[media](
            contribute
        )) as MediaStream
        if (stream) {
            // console.log('Set Local Stream')
            this.setStream(stream)
            return true
        }
        console.error(stream)
        return false
    }

    /**
     * 配信モードの確認
     *
     * @mode any
     * @return boolean
     */
    private checkStreamMode(mode: object): boolean {
        if (Object.keys(mode).length > 1) {
            return true
        }
        return false
    }

    /**
     * ビデオタグのDOMオブジェクトを登録
     *
     * @param local string | DomElement
     * @return MediaService
     */
    public setVideoTarget(target: HTMLVideoElement | string): MediaService {
        if (typeof target === 'string') {
            this.LocalVideoTarget = document.querySelector(
                '#' + target
            ) as HTMLVideoElement
        } else {
            this.LocalVideoTarget = target
        }
        return this
    }

    /**
     * ストリームデータを変数に格納
     *
     * @param target ローカルかリモートか
     * @param stream 映像ストリーム
     * @return MediaService
     */
    public setStream(stream: MediaStream): MediaService {
        this.LocalStream = stream
        return this
    }

    /**
     * メディアデバイスを閉じる
     *
     * @returns MediaService
     */
    public closeStream(): MediaService {
        if (this.LocalStream === undefined) return this
        this.LocalStream.getTracks().forEach(
            (track: MediaStreamTrack) => track.stop()
        )
        return this
    }

    /**
     * ビデオ再生
     */
    public async playVideo(): Promise<MediaService> {
        if (this.LocalVideoTarget === undefined) return this
        if (this.LocalStream === undefined) return this

        const lvt = this.LocalVideoTarget
        lvt.srcObject = this.LocalStream
        lvt.volume = 0
        // ストリーム再生
        return new Promise((resolve) => {
            lvt.onloadedmetadata = () => {
                lvt.play()
                resolve(this)
            }
        })
    }

    /**
     * ビデオ停止
     * @returns MediaService
     */
    public stopVideo(): MediaService {
        if (this.LocalVideoTarget === undefined) return this
        if (this.LocalStream === undefined) return this

        this.LocalStream.getVideoTracks().forEach(
            (track: MediaStreamTrack) => track.stop()
        )
        this.LocalStream.getAudioTracks().forEach(
            (track: MediaStreamTrack) => track.stop()
        )

        this.LocalStream = undefined
        this.LocalVideoTarget.pause()
        this.LocalVideoTarget.srcObject = null
        this.LocalVideoTarget.volume = 0

        return this
    }

    /**
     * 全リセット
     * @returns MediaService
     */
    public reset(): MediaService {
        this.stopVideo()
        this.LocalVideoTarget = undefined
        this.callDeviceListService().reset()
        this.callMediaAPIListService().reset()
        return this
    }
}
