/**
 * Mediaデバイスの配信を設定、管理
 */

type AudioType = boolean
                | { deviceId: string }
type VideoType = boolean
            | {
                width: number | { min: number, ideal: number, max: number },
                height: number | { min: number, ideal: number, max: number }
                facingMode: string | { exact: string },
                deviceId: string,
            }

export interface StreamModeInterface {
    // 音の再生、取得があるか
    audio?: AudioType
    // ビデオの再生、取得があるか
    video?: VideoType
    // 画面の取得（ディスプレイ単位 or アプリ単位）
    screen?: boolean
}

export class StreamModeService {
    private static instance: StreamModeService;

    private mode: StreamModeInterface = {
        video: false,
        audio: false,
        screen: false
    };

    public static call(): StreamModeService {
        if (!StreamModeService.instance) {
            StreamModeService.instance = new StreamModeService()
        }
        return StreamModeService.instance
    }

    /**
     * StreamModeを返す
     *
     * @returns StreamModeInterface
     */
    public getStreamMode(): StreamModeInterface {
        return this.mode
    }

    /**
     * インカメラ、アウトカメラの設定
     * ※ 主にスマホ用
     *
     * @param mode string [in, out]
     * @returns StreamModeService
     */
    public setFacingMode(mode: 'in' | 'out' = 'in'): StreamModeService {
        if (mode === 'in') {
            this.mode.video = Object.assign({}, this.mode.video, {
                facingMode: 'user'
            })
        } else if (mode === 'out') {
            this.mode.video = Object.assign({}, this.mode.video, {
                facingMode: { exact: 'environment' }
            })
        }

        return this
    }

    /**
     * DeviceIdから使用するカメラを指定
     *
     * @param id string
     * @returns StreamModeService
     */
    public setDeviceId(id: string): StreamModeService {
        this.mode.video = Object.assign({}, this.mode.video, { deviceId: id })

        return this
    }

    /**
     * カメラのサイズ指定
     *
     * ※ 詳細指定時、idealを指定した場合、idealを基準に
     * 　 最も近い映像サイズを自動で選択する
     *
     * @param width numner |  { min: number, ideal: number, max: number }
     * @param height numner |  { min: number, ideal: number, max: number }
     * @returns
     */
    public setWidthHeight(
        width: number | { min?: number; ideal?: number; max?: number },
        height: number | { min?: number; ideal?: number; max?: number }
    ): StreamModeService {
        this.mode.video = Object.assign({}, this.mode.video, {
            width: width,
            height: height
        })
        return this
    }

    /**
     * まとめて指定する場合用ショートカットメソッド
     *
     * @param video
     * @param audio
     * @param screen
     * @returns
     */
    public setDeviceSuppot(
        video: VideoType,
        audio: AudioType,
        screen: boolean
    ): StreamModeService {
        this.mode = { video: video, audio: audio, screen: screen }
        return this
    }

    /**
     * videoを有効化
     *
     * @returns StreamModeService
     */
    public onVideo(): StreamModeService {
        this.mode.video = true
        return this
    }

    /**
     * videoを無効化
     *
     * @returns StreamModeService
     */
    public offVideo(): StreamModeService {
        this.mode.video = false
        return this
    }

    /**
     * audioを有効化
     *
     * @returns StreamModeService
     */
    public onAudio(): StreamModeService {
        this.mode.audio = true
        return this
    }

    /**
     * audioを無効化
     *
     * @returns StreamModeService
     */
    public offAudio(): StreamModeService {
        this.mode.audio = false
        return this
    }

    /**
     * screenを有効化
     *
     * @returns StreamModeService
     */
    public onScreen(): StreamModeService {
        this.mode.screen = true
        return this
    }

    /**
     * screenを無効化
     *
     * @returns StreamModeService
     */
    public offScreen(): StreamModeService {
        this.mode.screen = false
        return this
    }
}
