
export type ChatModelsType = 
    'gpt-4o-mini' | 
    'gpt-4o' |
    'gpt-4o-latest' |
    'gpt-4o-audio-preview' |
    'o1-preview' | 'o1-mini' |
    'gpt-4-turbo' | 
    'gpt-3.5-turbo'



/**
 * モデルに指示を出す開発者メッセージの型
 */
export type DeveloperMessageType = {
    content: string,
    role: 'developer'
    name?: string,
}

/**
 * モデルに指示を出すシステムメッセージの型
 */
export type SystemMessageType = {
    content: string,
    role: 'system'
    name?: string,
}

export type UserMessageType = {
    content: [{
        type: 'text',
        text: string,
    } | {
        type: 'image_url',
        image_url: {
            url: string,    // 画像のURL or base64文字列
            detail: string,
        }
    } | {
        type: 'input_audio',
        input_audio: {
            data: string,    // base64文字列
            format: 'wav' | 'mp3',
        }
    }],
    role: 'user'
    name?: string,
}

/**
 * モデルから応答されるメッセージの型
 */
export type AssistantMessageType = {
    content: string | {
        type: string,
        text: string,
    } [] | {
        type: string,
        refusal: string,
    } [],
    refusal?: string | null,
    role: 'assistant'
    name?: string,
    audio?: {
        id: string,
    } | null,
    tool_calls?: {
        id: string,
        type: 'function',
        function: {
            name: string,
            arguments: string,
        }
    },
    function_call?: {
        arguments: string,
        name: string,
    }
}

/**
 * アシスタントコール他、モデルによって呼び出されるツールメッセージの型
 */
export type ToolMessageType = {
    role: 'tool',
    content: {
        type: 'text',
        text: string,
    }[] | string,
    tool_call_id: string,
}

export type ChatMessagesType = 
        DeveloperMessageType | SystemMessageType | UserMessageType |
        AssistantMessageType | ToolMessageType



export type ToolType = {
    type: 'function',
    function: {
        description?: string,
        name: string,
        parameters: {
            type: 'object',
            properties: {
                [key: string]: {
                    type: 'string',
                    description: string,
                },
            },
            required: string[],
            additionalProperties: false,
        },
        strict?: boolean | null,        // default: false
    }
}

export const initialToolType: ToolType['function'] = {
    name: '',
    description: '',
    parameters: {
        type: 'object',
        properties: {},
        required: [],
        additionalProperties: false,
    },
    strict: false,
}

export type ChatOptionsType = {
    messages            : ChatMessagesType[],
    model               : ChatModelsType,
    // モデルの蒸留で使用するために、出力を保存するか (default: false)
    store?              : boolean,
    // o1モデルのみ、推論の労力を指定 (default: 'medium')
    reasoning_effort?   : 'medium' | 'low' | 'high',
    // 補完のフィルタリングに使用するメタデータオブジェクト、詳細不明 (default: null)
    metadata?           : object | null,
    // -2.0～2.0までの数値、正の値は新しいトークンにペナルティーを与え、同じ内容を繰り返す可能性が上がる
    frequency_penalty?  : number,                       // default: 0
    // -100～100までの数値を、トークンIDとのMapで指定することで、特定のトークンにバイアスを与える
    // 例: {50256: 2.0} で、トークンID50256に2.0のバイアスを与える
    logit_bias?         : {
        [key: number]: number
    } | null,
    // 出力トークンのLog確率を返すか (default: false)
    logprobs?           : boolean | null,
    // logprobsがtrueの場合限定、0～20の範囲で最も可能性の高いトークンの数を指定する
    top_logprobs?       : number | null,
    // 応答の完了までに生成できるトークンの上限（表示と内部推論トークンの合計）
    max_completion_tokens?: number | null,
    // 生成するチャットの数 (default: 1)
    n?                  : number,
    // モデルに出力させる出力のタイプ、gpt-4o-audio-previewのみ 'audio' が使用可能
    // 例: ['text'] | ['text', 'audio'] | null (default: ['text'])
    modalities?         : ['text'] | ['audio'] | ['text', 'audio'] | null,
    // 生成される応答が事前に予測できる内容の場合に使用
    // 登録しておくことで、トークンの消費を抑えることができる
    prediction?         : {
        type: 'content',
        content: string | {
            type: string,
            text: string,
        }[],
    },
    // modalitiesに 'audio' が含まれる場合、入力音声のフォーマットを指定が必要
    audio?              : {
        voice: 'ash' | 'ballad' | 'coral' | 'sage' | 'verse' | 'alloy' | 'echo' | 'shimmer',
        format: 'wav' | 'mp3' | 'flac' | 'opus' |'pcm16',
    } | null,
    // -2.0～2.0の数値で、トークンにペナルティーを与える
    // 正の値は、新しいトークンが生成される可能性を上げ、会話を別方向に進める
    presence_penalty?   : number | null,
    // 戻りデータ構造を指定する。
    // モデルは戻りデータが、指定されたjson構造と一致することを保証する
    response_format?    : {
        type: 'text'
    } | {
        type: 'json_object'
    } | {
        type: 'json_schema',
        json_schema: {
            description?: string,
            name: string,
            schema: object,
            strict?: boolean | null,       // default: false
        }
    },
    // 
    seed?               : number | null,
    // 自動スケーリングサービスを使用しているユーザーのみ対象
    service_tier?       : 'auto' | 'default' | null,        // default: auto
    // トークンの生成を停止する最大4つの条件を指定
    stop?               : string | Array<string> | null,    // default: null
    // 設定すると、随時部分的なメッセージの断片が送信される
    stream?             : boolean | null,       // default: null
    // ストリームが有効に設定されている場合のみ使用可能
    // ストリームが送信される前に、トークンの使用統計が送信される
    stream_options?     : {
        include_usage  : boolean,
    } | null,        // default: null
    // 0～2の実数、0.8等の高い数値ではランダム性が増し、0.2等の低い数値ではランダム性が減る
    temperature?        : number | null,        // default: 1
    // 0～1の実数、0.1の場合、上位10%の確率のトークンが選択される
    top_p?              : number | null,        // default: 1
    // モデルが呼び出す可能性があるツールのリスト、最大128個
    // 現時点でFunctionのみサポート
    tools?              : ToolType[],        // default: none
    tool_choice?        : 'auto' | 'none' | 'required' | {
        type: 'function',
        function: {
            name: string
        }
    },
    // 関数の同時呼び出しを許可するか
    parallel_tool_calls?: boolean,              // default: true
    user?               : string,
}

export const initialChatOption: ChatOptionsType = {
    model               : 'gpt-4o',
    messages            : [],
    temperature         : 0.7,
    top_p               : 1,
    n                   : 1,
    stream              : false,
    stop                : undefined,

    presence_penalty    : 0,
    frequency_penalty   : 0,
    user                : '',
}

export type ChatReturnType = {
    choices: {
        finish_reason: string,
        index: number,
        logprobs: any,
        message: {
            content: string,
            role: string
        }
    }[],
    created: number,
    id: string,
    model: string
    object: string
    system_fingerprint: string
}

export const ChatParser = (data: any): ChatMessagesType[] => {
    const messages: ChatMessagesType[] = []

    if (data.result.hasOwnProperty('choices')) {
        data.result.choices.map((val: any) => {
            messages.push({
                role: val.message.role,
                content: val.message.content,
            })
        })
    }
    return messages
}