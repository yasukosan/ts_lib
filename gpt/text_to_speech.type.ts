export type Model = 'tts-1' | 'tts-1-hd'
export type Voice = 'alloy' | 'echo' | 'fable' | 'onyx' | 'nova' | 'shimmer'
export type ResponceFormat = 'mp3' | 'opus' | 'aac' | 'flac'

/**
 * VisionAPI呼び出しオプションの型
 * @property model VisionAPIのモデル名
 * @property messages VisionAPIに送るメッセージ
 * @property messages.role メッセージの送信者
 * @property messages.content メッセージの内容
 * @property messages.content.type メッセージのタイプ
 * @property messages.content.text メッセージのテキスト
 * @property messages.content.image_url メッセージの画像URL
 */
export type TextToSpeechOptions = {
    model   : Model,
    voice   : Voice,
    input   : string,
    response_format? : ResponceFormat,
    speed?  : number,
}

export const initialTextToSpeechOptions: TextToSpeechOptions = {
    model: 'tts-1',
    voice: 'alloy',
    input: 'Hello!',
    response_format: 'mp3',
    speed: 1,
}
