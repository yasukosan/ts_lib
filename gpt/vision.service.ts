import { GptHelper } from "./_helper/gpt.helper"
import { VisionOptions, initialVisionOption } from "./vision.type"

export class VisionService {
    private static instance: VisionService;

    private Options: VisionOptions = initialVisionOption
    public static call(): VisionService {
        if (!VisionService.instance)
            VisionService.instance = new VisionService()
        return VisionService.instance
    }

    /**
     * APIKeyを設定
     * @param key string
     * @returns 
     */
    public setAPIKey(key: string): VisionService {
        GptHelper.call().setAPIKey(key)
        return this
    }

    /**
     * ChatOptionsを設定
     * @param options ChatOptions
     * @returns VisionService
     */
    public setOptions(options: VisionOptions
        ): VisionService {
        this.Options = Object.assign(this.Options, options);
        return this;
    }

    public setImage(
        image: string,
        text: string,
    ): VisionService {
        this.Options.messages = [{
            role        : 'user',
            content     : [
                {
                    type        : 'text',
                    text        : text
                },
                {
                    type        : 'image_url',
                    image_url   : {
                        url : image
                    }
                },
            ],
        }]

        return this
    }
    /**
     * ApiKeyが登録されているか確認
     * @returns boolean
     */
    public checkApi(): boolean {
        return GptHelper.call().checkApiKey()
    }

    /**
     * API呼び出し結果を返す
     * @returns any
     */
    public getResult(): any {
        return GptHelper.call().getResult()
    }

    public getInfo(): {type: string, message: string} {
        return GptHelper.call().getInfo()
    }

    /**
     * API呼び出し実行
     * @returns Promise<void>
     */
    public async do(): Promise<void> {
        GptHelper.call().ini()
        console.log(this.Options.messages[0])
        await GptHelper.call().doVision(this.Options)
        return
    }

    public reset(): void {
        this.Options = initialVisionOption
    }
}