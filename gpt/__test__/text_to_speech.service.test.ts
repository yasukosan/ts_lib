import { describe, it, expect, beforeEach, vi } from 'vitest'
import { TextToSpeechService } from '../text_to_speech.service'
import { GptHelper } from '../_helper/gpt.helper'
import { Voice, ResponceFormat, initialTextToSpeechOptions } from '../text_to_speech.type'

describe('TextToSpeechService', () => {
    let service: TextToSpeechService;

    beforeEach(() => {
        service = TextToSpeechService.call()
        service.reset()
    })

    it('should set API key correctly', () => {
        const setAPIKeySpy = vi.spyOn(GptHelper.call(), 'setAPIKey')
        service.setAPIKey('test-key');
        expect(setAPIKeySpy).toHaveBeenCalledWith('test-key')
    })

    it('should set options correctly', () => {
        const voice: Voice = 'alloy'
        const responseFormat: ResponceFormat = 'mp3'
        const speed = 1.0;

        service.setOptions(voice, responseFormat, speed);

        expect(service['Options']).toEqual({
            ...initialTextToSpeechOptions,
            voice,
            response_format: responseFormat,
            speed
        });
    });

    it('should set text correctly', () => {
        const text = 'Hello, world!';
        service.setText(text);
        expect(service['Options'].input).toBe(text);
    });

    it('should check API key correctly', () => {
        const checkApiKeySpy = vi.spyOn(GptHelper.call(), 'checkApiKey').mockReturnValue(true);
        const result = service.checkApi();
        expect(checkApiKeySpy).toHaveBeenCalled();
        expect(result).toBe(true);
    });

    it('should get result correctly', () => {
        const getResultSpy = vi.spyOn(GptHelper.call(), 'getResult').mockReturnValue('test-result');
        const result = service.getResult();
        expect(getResultSpy).toHaveBeenCalled();
        expect(result).toBe('test-result');
    });

    it('should get info correctly', () => {
        const getInfoSpy = vi.spyOn(GptHelper.call(), 'getInfo').mockReturnValue({ type: 'info', message: 'test-message' });
        const result = service.getInfo();
        expect(getInfoSpy).toHaveBeenCalled();
        expect(result).toEqual({ type: 'info', message: 'test-message' });
    });

    it('should call doTextToSpeech with correct options', async () => {
        const iniSpy = vi.spyOn(GptHelper.call(), 'ini');
        const doTextToSpeechSpy = vi.spyOn(GptHelper.call(), 'doTextToSpeech').mockResolvedValue();

        await service.do();

        expect(iniSpy).toHaveBeenCalled();
        expect(doTextToSpeechSpy).toHaveBeenCalledWith(service['Options']);
    });

    it('should reset options correctly', () => {
        service.setOptions('alloy', 'mp3', 1.0);
        service.reset();
        expect(service['Options']).toEqual(initialTextToSpeechOptions);
    });
});