import { 
    ChatParser,
    ChatMessagesType,
 } from "./_helper/chat.helper";

import { EmbedParser } from "./_helper/embed.helper";
import { ImageParser, ImageResultType } from "./_helper/image.helper";
import { TextToSpeechParser } from "./_helper/speech.helper";
import { VisionMessages, VisionParser } from "./_helper/vision.helper";
import { WhisperParser } from "./_helper/whisper.helper"

export class ParseService {
    private static instance: ParseService

    public static call(): ParseService {
        if (!ParseService.instance)
            ParseService.instance = new ParseService()
        return ParseService.instance;
    }

    public chat(result: any): ChatMessagesType[] {
        return ChatParser(result)
    }

    public whisper(result: any): string {
        return WhisperParser(result)
    }

    public image(result: any): ImageResultType {
        return ImageParser(result)
    }

    public speech(result: any): string {
        return TextToSpeechParser(result)
    }

    public vision(result: any): VisionMessages | false {
        return VisionParser(result)
    }

    public embed(result: any): number[] {
        return EmbedParser(result)
    }
}