import { GptHelper } from "./_helper/gpt.helper"
import {
    ImageOptionsType, initialImageOption,
    ImageEditOptionsType, initialImageEditOption,
    ImageChangeOptionsType, initialImageChangeOption
} from "./_helper/image.helper"
import { base64ToBlob, blobToFile } from "../_helper/convert.helper"

export class ImageService {
    private static instance: ImageService;

    private Options: ImageOptionsType | ImageEditOptionsType | ImageChangeOptionsType = initialImageOption
    private JOB: 'generate' | 'edit' | 'change' = 'generate'

    public static call(): ImageService {
        if (!ImageService.instance) {
            ImageService.instance = new ImageService()
        }
        return ImageService.instance
    }

    /**
     * APIKeyを設定
     * @param key string
     * @returns 
     */
    public setAPIKey(key: string): ImageService {
        GptHelper.call().setAPIKey(key);
        return this
    }

    /**
     * 処理ジョブを設定
     * @param job 'generate' | 'edit' | 'change'
     * @returns ImageService
     */
    public setJob(job: 'generate' | 'edit' | 'change'): ImageService {
        this.JOB = job
        if (job === 'generate') this.Options = initialImageOption
        if (job === 'edit') this.Options = initialImageEditOption
        if (job === 'change') this.Options = initialImageChangeOption
        return this
    }

    /**
     * 生成用のオプションを設定
     * @param Options any
     * @returns ImageService
     */
    public async setOptions(
        options: Partial<ImageOptionsType> | Partial<ImageEditOptionsType> | Partial<ImageChangeOptionsType>,
    ): Promise<ImageService> {
        if (this.JOB === 'generate') this.setGenerateOptions(options as ImageOptionsType)
        if (this.JOB === 'edit') await this.setEditOptions(options as ImageEditOptionsType)
        if (this.JOB === 'change') await this.setChangeOptions(options as ImageChangeOptionsType)
        return this
    }

    /**
     * 生成結果を返す
     * @returns 
     */
    public getResult(): any {
        return GptHelper.call().getResult()
    }

    public getInfo(): {type: string, message: string} {
        return GptHelper.call().getInfo()
    }

    public ini(): boolean | {type: string, message: string} {
        return GptHelper.call().ini()
    }

    /**
     * API呼び出しを開始
     * @returns 
     */
    public async do(): Promise<void> {
        if (this.JOB === 'generate') 
            await GptHelper.call().doImageCompletion(this.Options as ImageOptionsType)
        if (this.JOB === 'edit')
            await GptHelper.call().doImageEdit(this.Options as ImageEditOptionsType)
        if (this.JOB === 'change')
            await GptHelper.call().doImageChange(this.Options as ImageChangeOptionsType)
        return
    }

    /**
     * 画像生成用オプションを設定
     * @param options ImageOptions
     * @returns ImageService
     */
    private setGenerateOptions(options: ImageOptionsType): ImageService {
        this.Options = Object.assign({}, this.Options, options)
        return this
    }

    /**
     * 画像編集用オプションを設定
     * @param options 
     * @returns 
     */
    private async setEditOptions(options: ImageEditOptionsType): Promise<ImageService> {
        this.Options = {
            image   : await blobToFile(base64ToBlob(options.image_base64.split(',')[1])),
            mask    : await blobToFile(base64ToBlob(options.mask_base64.split(',')[1])),
            model   : (options.model !== undefined) ? options.model : this.Options.model,
            prompt  : (options.prompt !== undefined) ? options.prompt : this.Options.prompt,
            size    : (options.size !== undefined) ? options.size : this.Options.size,
            response_format : (options.response_format) ? options.response_format : this.Options.response_format,
            n       : (options.n) ? options.n : this.Options.n
        } as ImageEditOptionsType
        return this
    }

    /**
     * 画像変換用オプションを設定
     * @param options 
     * @returns 
     */
    private async setChangeOptions(options: ImageChangeOptionsType): Promise<ImageService> {
        this.Options = {
            image   : await blobToFile(base64ToBlob(options.image_base64.split(',')[1])),
            model   : (options.model !== undefined) ? options.model : this.Options.model,
            size    : (options.size !== undefined) ? options.size : this.Options.size,
            //prompt  : (options.prompt !== undefined) ? options.prompt : this.Options.prompt,
            response_format : (options.response_format !== undefined) ? options.response_format : this.Options.response_format,
            n       : (options.n) ? options.n : this.Options.n
        } as ImageChangeOptionsType
        return this
    }
}