import { GptHelper } from "./_helper/gpt.helper"
import {
    EmbedOptionsType,
    initialEmbedOptions as initialEmbedOptionsORG,
    EmbedReturnType,
 } from "./_helper/embed.helper"

export type EmbedOptions = EmbedOptionsType
export type EmbedReturn = EmbedReturnType
export const initialEmbedOptions = initialEmbedOptionsORG

export class EmbedService {
    private static instance: EmbedService

    private Options: EmbedOptions = initialEmbedOptions

    public static call(): EmbedService {
        if (!EmbedService.instance) {
            EmbedService.instance = new EmbedService()
        }
        return EmbedService.instance;
    }

    /**
     * APIKeyを設定
     * @param key string
     * @returns 
     */
    public setAPIKey(key: string): EmbedService {
        GptHelper.call().setAPIKey(key)
        return this
    }

    public setInput(
        input: string,
    ): EmbedService {
        this.Options.input = input
        return this
    }

    public setEncodeFormat(
        encoding_format: 'float' | 'base64',
    ): EmbedService {
        this.Options.encoding_format = encoding_format
        return this
    }

    public getResult(): EmbedReturnType {
        return GptHelper.call().getResult()
    }

    public getInfo(): {type: string, message: string} {
        return GptHelper.call().getInfo()
    }

    public checkApiKey(): boolean | {type: string, message: string} {
        return GptHelper.call().checkApiKey()
    }

    public ini(): boolean | {type: string, message: string} {
        return GptHelper.call().ini()
    }

    public async do(): Promise<void> {
        GptHelper.call().ini()
        await GptHelper.call().doEmbed(this.Options)
        return
    }

    public reset(): EmbedService {
        this.Options = initialEmbedOptions
        return this
    }
}