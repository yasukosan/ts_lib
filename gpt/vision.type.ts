

export type Role = 'user' | 'system' | 'assistant'

/**
 * VisionAPI呼び出しオプションの型
 * @property model VisionAPIのモデル名
 * @property messages VisionAPIに送るメッセージ
 * @property messages.role メッセージの送信者
 * @property messages.content メッセージの内容
 * @property messages.content.type メッセージのタイプ
 * @property messages.content.text メッセージのテキスト
 * @property messages.content.image_url メッセージの画像URL
 */
export type VisionOptions = {
    model: 'gpt-4o',
    messages: {
        role: Role,
        content: {
            type: 'text' | 'image_url',
            text?: string,
            image_url?: {
                url: string
            },
        }[]
    }[],
    stream?: boolean,
    max_tokens?: number, // デフォルト状態のトークンが10しか無いので必ず上げる
}

export const initialVisionOption: VisionOptions = {
    model: 'gpt-4o',
    messages: [
        {
            role: 'user',
            content: [
                {
                    type: 'text',
                    text: 'Hello!'
                }
            ]
        }
    ],
    stream: false,
    max_tokens: 4000,
}