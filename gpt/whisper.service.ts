import { GptHelper } from "./_helper/gpt.helper"

export type WhisperOptions = {
    audio: File | undefined,
    model: 'whisper-1',
    prompt?: string,
    output_format?: 'txt' | 'json' | 'srt' | 'vtt' | 'verbose_json',
    response_format?: 'text' | 'json' | 'srt' | 'vtt' | 'verbose_json',
    temperature?: number,
    language?: string,
}
export class WhisperService {
    private static instance: WhisperService;

    private Options: WhisperOptions = {
        audio: undefined,
        model: 'whisper-1',
        // response_format: 'vtt',
        response_format: 'text',
        temperature: 0.5,
        language: 'ja',
    }

    public static call(): WhisperService {
        if (!WhisperService.instance) {
            WhisperService.instance = new WhisperService()
        }
        return WhisperService.instance;
    }

    /**
     * APIKeyを設定
     * @param key string
     * @returns 
     */
    public setAPIKey(key: string): WhisperService {
        GptHelper.call().setAPIKey(key);
        return this;
    }

    /**
     * 音声ファイルを保持
     * @param audio File | string
     * @returns Promise<WhisperService>
     */
    public async setAudioFile(
        audio: File | string
    ): Promise<WhisperService> {
        if (typeof audio === 'string') {
            // console.log(audio)
            this.Options.audio = await this.base64ToMp3File(audio);
        } else {
            //this.Audio = audio;
            this.Options.audio = audio
        }
        return this;
    }

    /**
     * Promptを設定
     * 前段で処理した結果や、独特の言い回しを設定する
     * @param prompt string
     * @returns 
     */
    public setPrompt(prompt: string): WhisperService {
        this.Options.prompt = prompt;
        return this;
    }

    /**
     * ResponseFormatを設定
     * @param format 'text' | 'json' | 'srt' | 'vtt' | 'verbose_json'
     * @returns 
     */
    public setResponseFormat(
        format: 'text' | 'json' | 'srt' | 'vtt' | 'verbose_json'
    ): WhisperService {
        this.Options.response_format = format;
        return this;
    }

    public setTemperature(temperature: number): WhisperService {
        this.Options.temperature = temperature;
        return this;
    }

    /**
     * ApiKeyが登録されているか確認
     * @returns boolean
     */
    public checkApi(): boolean {
        return GptHelper.call().checkApiKey()
    }

    /**
     * API処理結果を返す
     * @returns any
     */
    public getResult(): any {
        return GptHelper.call().getResult();
    }

    /**
     * Info情報を返す
     * @returns {type : string, message: string}
     */
    public getInfo(): {type: string, message: string} {
        return GptHelper.call().getInfo();
    }

    /**
     * Apiをコール
     * @returns Promise<void>
     */
    public async do(): Promise<void> {
        if (this.Options.audio === undefined) return;
        
        GptHelper.call().ini();
        await GptHelper.call().doTranscription(this.Options);
        return;
    }

    private async base64ToMp3File(file: string): Promise<File>
    {
        const response = await fetch(`data:audio/mpeg;base64,${file}`)
        const blob = await response.blob()
        return new File([blob], 'tmp.mp3', { type: 'audio/mpeg' })
    }
}