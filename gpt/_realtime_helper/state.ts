import { ItemType, OptionsType } from "./definitions";

export const initialSessionOptions: OptionsType  = {
    modalities: ['text'],
    model: 'gpt-4o-realtime-preview-2024-12-17',
    instructions: 'Write a haiku about code',
    voice: 'coral',
    input_audio_format: 'pcm16',
    output_audio_format: 'pcm16',
    input_audio_transcription: {
        model: 'whisper-1'
    },
    turn_detection: {
        type: 'server_vad',
        threshold: 0.5,
        prefix_padding_ms: 300,
        silence_duration_ms: 500,
        create_response: true
    },
    tools: [],
    tool_choice: 'auto',
    temperature: 0.8,
    max_response_output_tokens: 'inf'
}


export const initialItem: ItemType = {
    id: null,
    type: 'message',
    status: 'completed',
    role: 'user',
    content: [{
        type: 'input_text',
        text: 'Hello, world!',
    }]
}