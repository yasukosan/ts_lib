import {
    APICallOptionType,
} from './definitions'

export const RealTimeOptions: APICallOptionType = {
    url: "https://api.openai.com/v1/realtime",
    key_server_url: "https://ephemeral-server.vercel.app/api/ephemeral",
    model: 'gpt-4o-mini-realtime-preview-2024-12-17',
    EPHEMERAL_KEY: '',
    sdp: '',
}

let Peer: RTCPeerConnection | undefined
let Data: RTCDataChannel | undefined

export const setRealTimeOptions = (
    options: Partial<APICallOptionType>
): APICallOptionType => Object.assign(RealTimeOptions, options)
export const getRealTimeOptions = (): APICallOptionType => RealTimeOptions

export const init = async(
    key: string,
    userMedia: MediaStream | null = null,
    id: string | null = null
) => {
    await getEphemeralKey(key)
    // await refalense()
    await addAudioTrack(userMedia, id)
    await addDataChannel()
    await initPeerConnection(key)
}

export const closeRealTimeConnection = () => {
    if (Peer === undefined) return
    Peer.close()
    Peer = undefined
    Data = undefined
}

export const addAudioTrack = async (
    userMedia: MediaStream | null = null,
    id: string | null = null
) => {
    if (Peer === undefined) {
        console.log('Peer connection not ready')
        return
    }

    const speacker = 
            (id === null)
            ? document.createElement('audio')
            : document.getElementById(id) as HTMLAudioElement

    speacker.autoplay = true
    Peer.ontrack = (e: any) => speacker.srcObject = e.streams[0]


    const microphone = 
            (userMedia === null)
            ? await navigator.mediaDevices.getUserMedia({audio: true})
            : userMedia
    Peer.addTrack(microphone.getTracks()[0])
}


export const addEventListener = (
    type: string,
    listener: (e: any) => void
): void => {
    if (Data === undefined) {
        console.log('Data channel not ready')
        return
    }
    Data.addEventListener(type, listener)
}

export const send = (
    message: any
): void => {
    console.log('Send Message :: ', message)    
    if (Data === undefined) {
        console.log('Data channel not ready')
        return
    }
    Data.send(JSON.stringify(message))
}


const getEphemeralKey = async(
    key: string
) => {
    const tokenResponse = await fetch(RealTimeOptions.key_server_url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            message: 'unkopannmanhyahhou',
            key: key
        })
    })
    console.log('Token Response :: ', tokenResponse)
    const data = await tokenResponse.json()
    console.log('Ephemetal Key :: ', data)
    RealTimeOptions.EPHEMERAL_KEY = data.message.key.client_secret.value

    Peer = await new RTCPeerConnection()
}

const initPeerConnection = async(
    key: string
) => {
    if (Peer === undefined) {
        console.log('Peer connection not ready')
        return
    }
    const offer = await Peer.createOffer()
    await Peer.setLocalDescription(offer)

    console.log('Created Offer :: ', offer)

    const sdpResponse = await fetch(
        `${RealTimeOptions.url}?model=${RealTimeOptions.model}`,
        {
            method: 'POST',
            body: offer.sdp,
            headers: {
                Authorization: `Bearer ${RealTimeOptions.EPHEMERAL_KEY}`,
                //Authorization: `Bearer ${key}`,
                'Content-Type': 'application/sdp'
                //'Content-Type': 'application/json'
            }
        }
    )

    console.log('SDP Response :: ', sdpResponse)

    const answer = {
        type: 'answer',
        sdp: await sdpResponse.text()
    } as RTCSessionDescriptionInit
    console.log('Created Answer :: ', answer)
    await Peer.setRemoteDescription(answer)
}

const addDataChannel = (
    channel: string = 'oai-events'
) => {
    if (Peer === undefined) {
        console.log('Peer connection not ready')
        return
    }

    console.log('Data Channel Created :: ', channel)

    Data = Peer.createDataChannel(channel)
    Data.addEventListener('message', (e: any) => {
        const realtimeEvent = JSON.parse(e.data)
        console.log(realtimeEvent)
    })
    // エラーイベント
    Data.addEventListener('error', (e: any) => {
        console.error('RealTime API Error', e)
    })
    // 新規セッション作成イベント
    Data.addEventListener('session.created', (e: any) => {
        console.log('New Session Created :: ', e)
    })
    // セッション更新イベント
    Data.addEventListener('session.updated', (e: any) => {
        console.log('Session Updated :: ', e)
    })
}

