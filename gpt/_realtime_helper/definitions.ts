export type APICallOptionType = {
    url: string,
    key_server_url: string,
    model: Models,
    EPHEMERAL_KEY: string,
    sdp: string,
}

export type Models = 
    'gpt-4o-realtime-preview-2024-12-17' |
    'gpt-4o-mini-realtime-preview-2024-12-17'


export type OptionsType = {
    modalities: ['audio'] | ['text'] | ['audio', 'text']
    model: Models
    // モデルに応答内容を指示する。
    // 例: 「非常に簡潔にする」、「フレンドリーに振る舞う」、「良い例は次のとおりです」 応答」）
    // および音声動作（例：「早く話す」、「感情を注入する」） 「声に出して」、「よく笑いましょう」）
    instructions: string
    voice: 'alloy' | 'ash' | 'ballad' | 'coral' | 'echo' | 'sage' | 'shimmer' | 'verse'
    input_audio_format: 'pcm16' | 'g711_ulaw' | 'g711_alaw',
    output_audio_format: 'pcm16' | 'g711_ulaw' | 'g711_alaw',
    // 現時点ではWhisper-1のみ (default: null) 
    // 何をするオプションなのか不明、nullでいい
    input_audio_transcription: {
        model: 'whisper-1'
    } | null,
    // 会話ターンの検出 (default: null）
    turn_detection: {
        // ターンの検出タイプ
        type: 'server_vad' | string // (default: 'server_vad')
        // アクティブにするための音量しきい値、騒がしい環境では高めに設定すると良い
        threshold: number // 0.0~1.0 (default: 0.5)
        // VADが何ミリ秒以上会話が続くとターン開始とみなすか
        prefix_padding_ms: number // (default: 300ms)
        // 会話終了とみなすための無音の長さ（ミリ秒）
        silence_duration_ms: number // (default: 500ms)
        create_response?: boolean // (default: true)
    },
    tools: {
        type: 'function',
        // 関数の名前
        name: string,
        // 関数の説明
        description: string,
        // 関数のパラメータ、JSONスキーマに従う
        parameters: {[key: string]: any}
    }[],
    tool_choice: 'auto' | 'none' | 'required',
    temperature: number // 0.6~1.2 (default: 0.8)
    // 出力トークンの最大数
    max_response_output_tokens: number | 'inf' // 1~4096 (default: inf（無制限）)
}    

export type SessionType = {
    // 必ず一意である必要がある
    id: string,
} & OptionsType


export type ItemType = {
    // 会話アイテムを管理するID、指定がない場合は自動生成
    id: string | null,
    type: 'message' | 'function_call' | 'function_call_output',
    object?: string | 'realtime.item',
    status: 'incomplete' | 'completed',
    role: 'system' | 'user' | 'assistant',
    // メッセージの内容
    // roleがsystemの場合、input_textのみがサポートされる
    // roleがuserの場合、input_text、input_audioがサポートされる
    // roleがassistantの場合、textがサポートされる
    content: {
        type: 'input_text' | 'input_audio' | 'item_reference' | 'text',
        text?: string,       // type: 'input_text' | 'text' の場合
        id?: string,         // type: 'item_reference' の場合
        audio?: string,      // type: 'input_audio' の場合
        transcript?: string, // type: 'input_audio' の場合
    }[],
    // function_callの呼び出しID
    call_id?: string,
    // 関数の名前
    name?: string,
    // 関数の引数
    arguments?: string,
    // 関数の出力
    output?: string
}

export type ResponseType = {
    id: string,
    object: string | 'realtime.response',
    status: 'completed' | 'incomplete' | 'failed' | 'cancelled',
    status_details:{
        type: 'completed' | 'incomplete' | 'failed' | 'cancelled',
        // 応答が完了しなかった理由
        // turn_detected 新しい会話を検知
        // client_cancelled 区アイアントがキャンセルイベントを送信、など
        reason: string,
        error: {
            type: string,
            // エラーコード、エラーがない場合はnull
            code: string | null,
        }
    } | null,
    // 応答の内容 
    output: ItemType[],
    // 応答に関連付けされている、ユーザーが提供したメタデータ
    metadata: {[key: string]: any},
    // トークンの使用状況
    usage: {
        // 入力と出力のトークンの合計数
        total_tokens: number,
        // 入力トークンの数
        input_tokens: number,
        // 出力トークンの数
        output_tokens: number,
        input_token_details: {
            cached_tokens: number,
            text_tokens: number,
            audio_tokens: number
        },
        output_token_details: {
            text_tokens: number,
            audio_tokens: number
        }
    } | null,
}