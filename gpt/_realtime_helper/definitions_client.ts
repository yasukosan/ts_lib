import {
    OptionsType,
    ItemType,
} from './definitions'

/*
APIリファレンス
https://platform.openai.com/docs/api-reference/realtime-server-events/conversation/item/truncated
*/

export type RequestOptionsType = OptionsType

export type ClientOfferType = 
        'session.update' | 'input_audio_buffer.append'
        | 'input_audio_buffer.commit' | 'input_audio_buffer.clear'
        | 'conversation.item.create' | 'conversation.item.truncate'
        | 'conversation.item.delete' | 'response.cancel'

/**
 * セッション情報を更新するイベント
 */
export type SessionUpdateEventType = {
    event_id: string,
    type: 'session.update',
    session: RequestOptionsType,
}

// オーディオ入力イベント

/**
 * オーディオ入力を追加するイベント
 * オーディオバッファはコミット可能な一時ストレージとして機能する
 * 
 */
export type InputAudioBufferAppendEventType = {
    event_id: string,
    type: 'input_audio_buffer.append',
    audio: string, // base64
}
/**
 * オーディオ入力の完了を通知するイベント
 * ※ 通常、自動で処理されるので送信する必要はない
 * ※ 送信した場合、以降のオーディオ入力は新しいセッションとして扱われる
 * イベントを送信した場合、応答は返ってこないが
 * input_audio_buffer.committed イベントがサーバーから送信される
 */
export type InputAudioBufferCommitEventType = {
    event_id: string,
    type: 'input_audio_buffer.commit',
}
/**
 * サーバー側のオーディオバッファをクリアするイベント
 * input_audio_buffer.cleared イベントがサーバーから送信される
 */
export type InputAudioBufferClearEventType = {
    event_id: string,
    type: 'input_audio_buffer.clear',
}

// 会話イベント

/**
 * 会話アイテムを作成するイベント
 */
export type ConversationItemCreateEventType = {
    event_id: string,
    type: 'conversation.item.create',
    // 前の会話アイテムのID、ない場合はnull
    // 指定がある場合、会話の途中に割り込むことができる
    previous_item_id: string | null,
    item: ItemType
}

/**
 * 会話アイテムを削除するイベント
 */
export type ConversationItemTruncateEventType = {
    event_id: string,
    type: 'conversation.item.truncate',
    item_id: string,
    content_index: number,
    // 会話終了までの期間（ミリ秒）
    // 実際の音声の長さより、長い場合は、エラーが返される
    audio_end_ms: number,
}

/**
 * 会話アイテムを削除するイベント
 */
export type ConversationItemDeleteEventType = {
    event_id: string,
    type: 'conversation.item.delete',
    item_id: string,
}




export type ResponseCreateEventType = {
    event_id: string,
    type: 'response.create',
    response: {
        modalities: OptionsType['modalities'],
        instructions: OptionsType['instructions'],
        voice: OptionsType['voice'],
        output_audio_format: OptionsType['output_audio_format'],
        tools: OptionsType['tools'],
        tool_choice: OptionsType['tool_choice'],
        temperature: OptionsType['temperature'],
        max_response_output_tokens: OptionsType['max_response_output_tokens'],
        conversatoin: string,
        metadata: {[key: string]: string}, // 任意のメタデータ 最大16個
        input: ItemType[]
    },
}

export type ResponseCancelEventType = {
    event_id: string,
    type: 'response.cancel',
    response_id: string,
}