import {
    SessionType,
    ItemType,
    ResponseType
} from './definitions'

/*
APIリファレンス
https://platform.openai.com/docs/api-reference/realtime-server-events/conversation/item/truncated
*/


export type ResponseFunctionKeysType =
                'response.created' | 'response.done'
                | 'response.output.item.added' | 'response.output.item.done'
                | 'response.content.part.added' | 'response.content.part.done'
                | 'response.text.delta' | 'response.text.done'
                | 'response.audio.delta' | 'response.audio.done'
                | 'response.audio.transcript.done' | 'response.audio.transcript.delta'
                | 'response.function_call.arguments.delta' | 'response.function_call.arguments.done'

/****************************************************
 * 
 * ：：エラーイベント
 * 
 ****************************************************/

/**
 * クライアント、サーバーの問題でエラーが発生した場合に送信されるイベント
 */
export type ErrorEventType = {
    event_id: string,
    type: 'error',
    error: {
        type: 'invalid_request_error' | 'server_error' | string,
        code: string | null,
        message: string,
        param: string | null,
        // エラー原因になったクライアントイベントのID
        event_id: string | null,
    }
}

/****************************************************
 * 
 * ：：セッションイベント
 * 
 ****************************************************/

/**
 * クライアントがサーバーに接続したときに送信されるイベント
 */
export type SessionCreatedEventType = {
    event_id: string,
    type: 'session.created',
    session: SessionType
}

/**
 * クライアントがセッションを更新したときに送信されるイベント
 */
export type SessionUpdateEventType = {
    event_id: string,
    type: 'session.updated',
    session: SessionType
}

/****************************************************
 * 
 * ：：会話イベント
 * 
 ****************************************************/

/**
 * 会話が作成されたときに送信されるイベント
 */
export type ConversationCreatedEventType = {
    event_id: string,
    type: 'conversation.created',
    conversation: {
        id: string,
        object: 'realtime.conversation' | string,
    }
}

/**
 * 会話アイテムが作成されたときに送信されるイベント
 */
export type ConversationItemCreatedEventType = {
    event_id: string,
    type: 'conversation.item.created',
    // 前の会話アイテムのID
    previous_item_id: string,
    item: ItemType
}

/**
 * オーディオ入力が完了し、転記が完了したときに送信されるイベント
 */
export type ConversationItemInputAudioTranscriptionCompletedEventType = {
    event_id: string,
    type: 'conversation.item.input_audio_transcription.completed',
    // 会話アイテムのID
    item_id: string,
    content_index: number,
    // 文字起こしされたテキスト
    transcript: string,
}

/**
 * オーディオ入力、転記、会話アイテムの作成が失敗したときに送信されるイベント
 */
export type ConversationItemInputAudioTranscriptionFailedEventType = {
    event_id: string,
    type: 'conversation.item.input_audio_transcription.failed',
    item_id: string,
    content_index: number,
    error: {
        type: string,
        code: string,
        message: string,
        param: string,
    }
}

/**
 * 会話アイテムが切り詰められたときに送信されるイベント
 */
export type ConversationItemTruncatedEventType = {
    event_id: string,
    type: 'conversation.item.truncated',
    // 切り詰められた会話アイテムのID
    item_id: string,
    // 切り詰められたコンテンツのインデックス
    content_index: number,
    // 切り詰められるまでの期間（ミリ秒）
    audio_end_ms: number,
}

/**
 * 会話アイテムが削除されたときに送信されるイベント
 * ※ サーバーによる削除ではなく、クライアントの指示による削除
 */
export type ConversationItemDeletedEventType = {
    event_id: string,
    type: 'conversation.item.deleted',
    // 削除された会話アイテムのID
    item_id: string,
}


/****************************************************
 * 
 * ：：オーディオバッファイベント
 * 
 ****************************************************/

/**
 * オーディオバッファがコミットされたときに送信されるイベント
 */
export type InputAudioBufferCommittedEventType = {
    event_id: string,
    type: 'input_audio_buffer.committed',
    // 前のアイテムのID
    previous_item_id: string,
    // 今回作成されたアイテムのID
    item_id: string,
}

/**
 * オーディオバッファがクリアされたときに送信されるイベント
 * ※ サーバーによるクリアではなく、クライアントの指示によるクリア
 */
export type InputAudioBufferClearedEventType = {
    event_id: string,
    type: 'input_audio_buffer.cleared'
}

/**
 * スピーチが開始されたときに送信されるイベント
 */
export type InputAudioBufferSpeechStartedEventType = {
    event_id: string,
    type: 'input_audio_buffer.speech_started',
    // 書き込まれたバッファが指定時間以上になった場合に、スピーチ開始とみなす時間
    // prefix_padding_msと同義
    audio_start_ms: number,
    item_id: string,
}

/**
 * スピーチが停止されたときに送信されるイベント
 */
export type InputAudioBufferSpeechStoppedEventType = {
    event_id: string,
    type: 'input_audio_buffer.speech_stopped',
    // 最後に書き込まれたバッファから、指定時間スピーチが停止している場合に、スピーチ停止とみなす時間
    // silence_duration_msと同義
    audio_end_ms: number,
    item_id: string,
}

/****************************************************
 * 
 * ：：応答イベント
 * 
 ****************************************************/

/**
 * 応答が作成されたときに送信されるイベント
 */
export type ResponseCreatedEventType = {
    event_id: string,
    type: 'response.created',
    response: ResponseType
}

/**
 * 応答が完了したときに送信されるイベント
 */
export type ResponseDoneEventType = {
    event_id: string,
    type: 'response.done',
    response: ResponseType
}

// OutputItem

/**
 * 応答の生成中に新しい応答が作成されたときに送信されるイベント
 */
export type ResponseOutputItemAddedEventType = {
    event_id: string,
    type: 'response.output_item.added',
    // この応答が属する応答のID
    response_id: string,
    output_index: number,
    item: ItemType
}

/**
 * 応答のストリーミングが完了したときに送信されるイベント
 * ※ 応答が中断、不完全、キャンセルの場合も送信される
 */
export type ResponseOutputItemDoneEventType = {
    event_id: string,
    type: 'response.output_item.done',
    // この応答が属する応答のID
    response_id: string,
    output_index: number,
    item: ItemType
}

// ContentPart

/**
 * 新しい応答のコンテンツパートが追加されたときに送信されるイベント
 */
export type ResponseContentPartAddedEventType = {
    event_id: string,
    type: 'response.content_part.added',
    response_id: string,
    // コンテンツが追加された応答のアイテムID
    item_id: string,
    output_index: number,
    content_index: number,
    part: {
        type: 'text' | 'audio',
        text: string,       // type: 'text' の場合
        audio: string,      // type: 'audio' の場合
        transcript: string, // type: 'audio' の場合
    }
}

/**
 * コンテンツ部分のストリーミングが完了したときに送信されるイベント
 * ※ コンテンツ部分が中断、不完全、キャンセルの場合も送信される
 */
export type ResponseContentPartDoneEventType = {
    event_id: string,
    type: 'response.content_part.done',
    response_id: string,
    // アイテムのID
    item_id: string,
    output_index: number,
    content_index: number,
    part: {
        type: 'text' | 'audio',
        text: string,       // type: 'text' の場合
        audio: string,      // type: 'audio' の場合
        transcript: string, // type: 'audio' の場合
    }
}

// Text

/**
 * テキストコンテンツ部分の、テキスト値が更新されたときに送信されるイベント
 */
export type ResponseTextDeltaEventType = {
    event_id: string,
    type: 'response.text.delta',
    response_id: string,
    // アイテムのID
    item_id: string,
    output_index: number,
    content_index: number,
    delta: string
}

/**
 * テキストコンテンツ部分のストリーミングが完了したときに送信されるイベント
 * ※ テキストコンテンツ部分が中断、不完全、キャンセルの場合も送信される
 */
export type ResponseTextDoneEventType = {
    event_id: string,
    type: 'response.text.done',
    response_id: string,
    item_id: string,
    output_index: number,
    content_index: number,
    text: string
}

// Audio

/**
 * オーディオ出力のトランスクリプションが、更新されたときに送信されるイベント
 */
export type ResponseAudioTranscriptDeltaEventType = {
    event_id: string,
    type: 'response.audio_transcript.delta',
    response_id: string,
    item_id: string,
    output_index: number,
    content_index: number,
    delta: string
}

/**
 * オーディオ出力のトランスクリプションが、ストリーミングが完了したときに送信されるイベント
 * ※ オーディオ出力のトランスクリプションが中断、不完全、キャンセルの場合も送信される
 */
export type ResponseAudioTranscriptDoneEventType = {
    event_id: string,
    type: 'response.audio_transcript.done',
    response_id: string,
    item_id: string,
    output_index: number,
    content_index: number,
    // トランスクリプションされたテキスト
    transcript: string
}

/**
 * オーディオが更新されたときに送信されるイベント
 */
export type ResponseAudioDeltaEventType = {
    event_id: string,
    type: 'response.audio.delta',
    response_id: string,
    item_id: string,
    output_index: number,
    content_index: number,
    // base64でエンコードされたオーディオデータ
    delta: string
}

/**
 * オーディオがストリーミングが完了したときに送信されるイベント
 * ※ オーディオが中断、不完全、キャンセルの場合も送信される
 */
export type ResponseAudioDoneEventType = {
    event_id: string,
    type: 'response.audio.done',
    response_id: string,
    item_id: string,
    output_index: number,
    content_index: number,
}

// FunctionCall

/**
 * 関数呼び出しの引数が更新されたときに送信されるイベント
 */
export type ResponseFunctionCallArgumentsDeltaEventType = {
    event_id: string,
    type: 'response.function_call.arguments.delta',
    response_id: string,
    item_id: string,
    output_index: number,
    call_id: string,
    delta: string       // JSON文字列
}

/**
 * 関数呼び出しの引数がストリーミングが完了したときに送信されるイベント
 * ※ 関数呼び出しの引数が中断、不完全、キャンセルの場合も送信される
 */
export type ResponseFunctionCallArgumentsDoneEventType = {
    event_id: string,
    type: 'response.function_call.arguments.done',
    response_id: string,
    item_id: string,
    output_index: number,
    call_id: string,
    arguments: string       // JSON文字列
}





/**
 * 応答の戦闘で発行される、レート制限を表すイベント
 * レスポンスが生成されると、一部のトークンが出力用に「予約」される
 * ここに表示されるレート制限はその予約を反映しています
 */
export type RateLimitsUpdatedEventType = {
    event_id: string,
    type: 'rate_limits.updated',
    rate_limits: {
        name: string | 'requests'| 'tokens',
        limit: number,          // 制限値
        remaining: number,      // 残りのトークン数
        reset_seconds: number,  // リセットまでの秒数
    }[]
}