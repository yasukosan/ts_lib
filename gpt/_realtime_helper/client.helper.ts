import {
    OptionsType,
    ItemType
} from './definitions'
import {
    InputAudioBufferAppendEventType,
    InputAudioBufferCommitEventType,
    InputAudioBufferClearEventType,
    SessionUpdateEventType,
    ConversationItemCreateEventType,
    ConversationItemTruncateEventType,
    ConversationItemDeleteEventType,
    ResponseCancelEventType,
} from './definitions_client'
import {
    send
} from './realtime.helper'
import { initialSessionOptions } from './state'

const RequestOptions: OptionsType = initialSessionOptions

export const setRequestOptions = (options: Partial<OptionsType>) => Object.assign(RequestOptions, options)
export const getRequestOptions = () => RequestOptions

/**
 * セッションの更新を呼び出す
 */
export const callSessionUpdate = (
    options: Partial<OptionsType> = {}
) => {
    if (Object.keys(options).length > 0) {
        setRequestOptions(options)
    }
    const sessionUpdate: SessionUpdateEventType = {
        event_id: 'event_' + randomString(),
        type: 'session.update',
        session: RequestOptions
    }

    send(sessionUpdate)
}

/**
 * オーディオバッファを追加する
 * @param audioBuffer string
 */
export const callAddAudioBuffer = (audioBuffer: string) => {
    const addAudioBuffer: InputAudioBufferAppendEventType = {
        event_id: 'event_' + randomString(),
        type: 'input_audio_buffer.append',
        audio: audioBuffer
    }

    send(addAudioBuffer)
}

export const callCommitAudioBuffer = () => {
    const audioBufferCommit: InputAudioBufferCommitEventType = {
        event_id: 'event_' + randomString(),
        type: 'input_audio_buffer.commit'
    }

    send(audioBufferCommit)
}

export const callClearAudioBuffer = () => {
    const clearAudioBuffer: InputAudioBufferClearEventType = {
        event_id: 'event_' + randomString(),
        type: 'input_audio_buffer.clear'
    }

    send(clearAudioBuffer)
}

export const callCreateConversationItem = (item: ItemType) => {
    const createConversationItem: ConversationItemCreateEventType = {
        event_id: 'event_' + randomString(),
        type: 'conversation.item.create',
        previous_item_id: null,
        item
    }

    send(createConversationItem)
}

export const callTruncateConversationItem = (item_id: string) => {
    const createConversationItem: ConversationItemTruncateEventType = {
        event_id: 'event_' + randomString(),
        type: 'conversation.item.truncate',
        item_id: item_id,
        content_index: 0,
        audio_end_ms: 0
    }

    send(createConversationItem)
}

export const callDeleteConversationItem = (item_id: string) => {
    const createConversationItem: ConversationItemDeleteEventType = {
        event_id: 'event_' + randomString(),
        type: 'conversation.item.delete',
        item_id: item_id
    }

    send(createConversationItem)
}

export const callResponseCancel = (response_id: string) => {
    const responseCancel: ResponseCancelEventType = {
        event_id: 'event_' + randomString(),
        type: 'response.cancel',
        response_id
    }

    send(responseCancel)
}

// ランダムな8桁の文字列を生成
const randomString = () => Math.random().toString(36).slice(-8)