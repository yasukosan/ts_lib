import {
    ResponseFunctionKeysType,
    ErrorEventType,
    SessionCreatedEventType,
    SessionUpdateEventType,
    ConversationCreatedEventType,
    ConversationItemCreatedEventType,
    ConversationItemInputAudioTranscriptionCompletedEventType,
    ConversationItemInputAudioTranscriptionFailedEventType,
    ConversationItemTruncatedEventType,
    ConversationItemDeletedEventType,
    InputAudioBufferCommittedEventType,
    InputAudioBufferClearedEventType,
    InputAudioBufferSpeechStartedEventType,
    InputAudioBufferSpeechStoppedEventType,
    ResponseCreatedEventType,
    ResponseDoneEventType,
    ResponseOutputItemAddedEventType,
    ResponseOutputItemDoneEventType,
    ResponseContentPartAddedEventType,
    ResponseContentPartDoneEventType,
    ResponseTextDeltaEventType,
    ResponseTextDoneEventType,
    ResponseAudioTranscriptDeltaEventType,
    ResponseAudioTranscriptDoneEventType,
    ResponseAudioDeltaEventType,
    ResponseAudioDoneEventType,
    ResponseFunctionCallArgumentsDeltaEventType,
    ResponseFunctionCallArgumentsDoneEventType,
    RateLimitsUpdatedEventType
} from './definitions_server'

import {
    addEventListener
} from './realtime.helper'


type ResponseFunctionStatusType = {
    on: boolean,
    call_function: (e: any) => void
}

type ResponseNextType = {
    [K in ResponseFunctionKeysType]: ResponseFunctionStatusType
}

const AudioStack: string[] = []
const TextStack: string[] = []

const ResponseNext: ResponseNextType = {
    'response.created': {
        on: false,
        call_function: (e: ResponseCreatedEventType) => {},
    },
    'response.done': {
        on: false,
        call_function: (e: ResponseDoneEventType) => {},
    },
    'response.output.item.added': {
        on: false,
        call_function: (e: ResponseOutputItemAddedEventType) => {},
    },
    'response.output.item.done': {
        on: false,
        call_function: (e: ResponseOutputItemDoneEventType) => {},
    },
    'response.content.part.added': {
        on: false,
        call_function: (e: ResponseContentPartAddedEventType) => {},
    },
    'response.content.part.done': {
        on: false,
        call_function: (e: ResponseContentPartDoneEventType) => {},
    },
    'response.text.delta': {
        on: false,
        call_function: (e: ResponseTextDeltaEventType) => {},
    },
    'response.text.done': {
        on: false,
        call_function: (e: ResponseTextDoneEventType) => {},
    },
    'response.audio.transcript.delta': {
        on: false,
        call_function: (e: ResponseAudioTranscriptDeltaEventType) => {},
    },
    'response.audio.transcript.done': {
        on: false,
        call_function: (e: ResponseAudioTranscriptDoneEventType) => {},
    },
    'response.audio.delta': {
        on: false,
        call_function: (e: ResponseAudioDeltaEventType) => {},
    },
    'response.audio.done': {
        on: false,
        call_function: (e: ResponseAudioDoneEventType) => {},
    },
    'response.function_call.arguments.delta': {
        on: false,
        call_function: (e: ResponseFunctionCallArgumentsDeltaEventType) => {},
    },
    'response.function_call.arguments.done': {
        on: false,
        call_function: (e: ResponseFunctionCallArgumentsDoneEventType) => {},
    },
}

/**
 * イベント受診後の処理を設定する
 * @param key ResponseFunctionKeysType
 * @param on boolean
 * @param call_function (e: any) => void リターン関数
 */
export const setReturnNext = (
    key: ResponseFunctionKeysType,
    on: boolean,
    call_function: (e: any) => void
) => ResponseNext[key] = {on, call_function}

export const getReturnNext = () => ResponseNext

export const addConversationListener = () => {
    addEventListener('conversation.item.input_audio_transcription.completed', (event: ConversationItemInputAudioTranscriptionCompletedEventType) => {
        console.log('conversation item input audio transcription completed', event)
    })
    addEventListener('conversation.item.input_audio_transcription.failed', (event: ConversationItemInputAudioTranscriptionFailedEventType) => {
        console.log('conversation item input audio transcription failed', event)
    })
    addEventListener('conversation.item.truncated', (event: ConversationItemTruncatedEventType) => {
        console.log('conversation item truncated', event)
    })
    addEventListener('conversation.item.deleted', (event: ConversationItemDeletedEventType) => {
        console.log('conversation item deleted', event)
    })
}

export const addInputAudioBufferListener = () => {
    addEventListener('input.audio_buffer.committed', (event: InputAudioBufferCommittedEventType) => {
        console.log('input audio buffer committed', event)
    })
    addEventListener('input.audio_buffer.cleared', (event: InputAudioBufferClearedEventType) => {
        console.log('input audio buffer cleared', event)
    })
    addEventListener('input.audio_buffer.speech.started', (event: InputAudioBufferSpeechStartedEventType) => {
        console.log('input audio buffer speech started', event)
    })
    addEventListener('input.audio_buffer.speech.stopped', (event: InputAudioBufferSpeechStoppedEventType) => {
        console.log('input audio buffer speech stopped', event)
    })
}

export const addResponseListener = () => {
    addEventListener('response.created', (event: ResponseCreatedEventType) => {
        (ResponseNext['response.created'].on) 
            ? ResponseNext['response.created'].call_function(event)
            : console.log('response created', event)
    })
    addEventListener('response.done', (event: ResponseDoneEventType) => {
        (ResponseNext['response.done'].on) 
            ? ResponseNext['response.done'].call_function(event)
            : console.log('response done', event)
    })
    addEventListener('response.output.item.added', (event: ResponseOutputItemAddedEventType) => {
        (ResponseNext['response.output.item.added'].on) 
            ? ResponseNext['response.output.item.added'].call_function(event)
            : console.log('response output item added', event)
    })
    addEventListener('response.output.item.done', (event: ResponseOutputItemDoneEventType) => {
        (ResponseNext['response.output.item.done'].on) 
            ? ResponseNext['response.output.item.done'].call_function(event)
            : console.log('response output item done', event)
    })
    addEventListener('response.content.part.added', (event: ResponseContentPartAddedEventType) => {
        (ResponseNext['response.content.part.added'].on) 
            ? ResponseNext['response.content.part.added'].call_function(event)
            : console.log('response content part added', event)
    })
    addEventListener('response.content.part.done', (event: ResponseContentPartDoneEventType) => {
        (ResponseNext['response.content.part.done'].on) 
            ? ResponseNext['response.content.part.done'].call_function(event)
            : console.log('response content part done', event)
    })
    addEventListener('response.text.delta', (event: ResponseTextDeltaEventType) => {
        (ResponseNext['response.text.delta'].on) 
            ? ResponseNext['response.text.delta'].call_function(event)
            : console.log('response text delta', event)
    })
    addEventListener('response.text.done', (event: ResponseTextDoneEventType) => {
        (ResponseNext['response.text.done'].on) 
            ? ResponseNext['response.text.done'].call_function(event)
            : console.log('response text done', event)
    })
    addEventListener('response.audio.transcript.delta', (event: ResponseAudioTranscriptDeltaEventType) => {
        (ResponseNext['response.audio.transcript.delta'].on) 
            ? ResponseNext['response.audio.transcript.delta'].call_function(event)
            : console.log('response audio transcript delta', event)
    })
    addEventListener('response.audio.transcript.done', (event: ResponseAudioTranscriptDoneEventType) => {
        (ResponseNext['response.audio.transcript.done'].on) 
            ? ResponseNext['response.audio.transcript.done'].call_function(event)
            : console.log('response audio transcript done', event)
    })
    addEventListener('response.audio.delta', (event: ResponseAudioDeltaEventType) => {
        (ResponseNext['response.audio.delta'].on) 
            ? ResponseNext['response.audio.delta'].call_function(event)
            : console.log('response audio delta', event)
    })
    addEventListener('response.audio.done', (event: ResponseAudioDoneEventType) => {
        (ResponseNext['response.audio.done'].on) 
            ? ResponseNext['response.audio.done'].call_function(event)
            : console.log('response audio done', event)
    })
    addEventListener('response.function_call.arguments.delta', (event: ResponseFunctionCallArgumentsDeltaEventType) => {
        (ResponseNext['response.function_call.arguments.delta'].on) 
            ? ResponseNext['response.function_call.arguments.delta'].call_function(event)
            : console.log('response function call arguments delta', event)
    })
    addEventListener('response.function_call.arguments.done', (event: ResponseFunctionCallArgumentsDoneEventType) => {
        (ResponseNext['response.function_call.arguments.done'].on) 
            ? ResponseNext['response.function_call.arguments.done'].call_function(event)
            : console.log('response function call arguments done', event)
    })
}
    