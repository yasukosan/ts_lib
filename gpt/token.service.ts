
export type TokenResultType = {
    completion_tokens: number,
    completion_tokens_details: {
        accepted_prediction_tokens: number,
        audio_tokens: number,
        reasoning_tokens: number,
    },
    prompt_tokens: number,
    prompt_tokens_details: {
        audio_tokens: number,
        cached_tokens: number,
    },
    total_tokens: number,
}

export const initialTokenResult: TokenResultType = {
    completion_tokens: 0,
    completion_tokens_details: {
        accepted_prediction_tokens: 0,
        audio_tokens: 0,
        reasoning_tokens: 0,
    },
    prompt_tokens: 0,
    prompt_tokens_details: {
        audio_tokens: 0,
        cached_tokens: 0,
    },
    total_tokens: 0,
}

let TokenResult: TokenResultType = initialTokenResult
const TokenResultHistory: TokenResultType[] = []

/**
 * トークンの使用状況を返す
 * @returns TokenResultType
 */
export const getTokenResult = (): TokenResultType => {
    return TokenResult
}

/**
 * トークンの使用履歴を返す
 * @returns TokenResultType[]
 */
export const getTokenResultHistory = (): TokenResultType[] => {
    return TokenResultHistory
}

/**
 * トークンの使用状況を設定
 * @param result TokenResultType
 */
export const setTokenResult = (result: TokenResultType): void => {
    TokenResultHistory.push(result)
    sumTokenResult()
}

/**
 * トークンの使用状況をクリア
 */
export const clearTokenResult = (): void => {
    TokenResultHistory.length = 0
}

/**
 * トークンの使用履歴を合算する
 */
const sumTokenResult = (): void => {
    const result = JSON.parse(JSON.stringify(TokenResult))

    const recursiveSum = (target: any, source: any) => {
        for (const key in source) {
            if (typeof source[key] === 'number') {
                target[key] += source[key]
            } else if (typeof source[key] === 'object' && source[key] !== null) {
                recursiveSum(target[key], source[key])
            }
        }
    }

    TokenResultHistory.forEach((history) => {
        recursiveSum(result, history)
    })

    TokenResult = result
}
