import {
    APICallOptionType,
    ItemType,
    OptionsType
} from './_realtime_helper/definitions'

import {
    setRequestOptions, getRequestOptions,
    callSessionUpdate,
    callAddAudioBuffer,
    callCommitAudioBuffer,
    callClearAudioBuffer,
    callCreateConversationItem,
    callTruncateConversationItem,
    callDeleteConversationItem,
    callResponseCancel,
} from './_realtime_helper/client.helper'
import {
    ClientOfferType,
} from './_realtime_helper/definitions_client'
import {
    setRealTimeOptions,
    init,
    send,
    closeRealTimeConnection
} from './_realtime_helper/realtime.helper'
import {
    addConversationListener,
    addInputAudioBufferListener,
    addResponseListener,
    setReturnNext
} from './_realtime_helper/server_event.helper'
import { ResponseFunctionKeysType } from './_realtime_helper/definitions_server'


// サーバークライアント間の応答イベントを有効化
export const enableConversationListener = () => addConversationListener()
// サーバークライアント間のオーディオ入力イベントを有効化
export const enableInputAudioListener = () => addInputAudioBufferListener()
// サーバークライアント間のレスポンスイベントを有効化
export const enableResponseListener = () => addResponseListener()

/**
 * サーバーレスポンスイベントのリターン関数を設定する
 * @param target 
 * @param on 
 * @param call_function 
 * @returns 
 */
export const addReturnFunction = (
    target: ResponseFunctionKeysType,
    on: boolean,
    call_function: (e: any) => void
) => setReturnNext(target, on, call_function)

/**
 * WebRTCセッションを開始する
 * @param key string
 * @param options Partial<APICallOptionType>
 * @param userMedia MediaStream | null
 * @param id string | null
 */
export const conn = async (
    key: string,
    options: Partial<APICallOptionType> = {},
    userMedia: MediaStream | null = null,
    id: string | null = null
) => {
    setRealTimeOptions(options)
    await init(key, userMedia, id)
}

/**
 * WebRTCセッションを終了する
 * @returns void
 */
export const close = () => closeRealTimeConnection()

/**
 * データを送信する
 * @param message any
 * @returns void
 */
export const push = (message: any) => send(JSON.stringify(message))

// セッション系処理

/**
 * WebRTCセッションの更新
 * @param options Partial<OptionsType>
 * @returns void
 */
export const sessionUpdate = (options: Partial<OptionsType> = {}) => callSessionUpdate(options)

// オーディオバッファ系処理

/**
 * オーディオバッファを追加する
 * @param audioBuffer string
 * @returns void
 */
export const addAudioBuffer = (audioBuffer: string) => callAddAudioBuffer(audioBuffer)
/**
 * オーディオバッファをコミットする
 * @returns 
 */
export const commitAudioBuffer = () => callCommitAudioBuffer()
/**
 * オーディオバッファをクリアする
 * @returns 
 */
export const clearAudioBuffer = () => callClearAudioBuffer()

// アイテム系処理

/**
 * アイテムを追加する
 * @param item itemType
 * @returns 
 */
export const addItem = (item: ItemType) => callCreateConversationItem(item)
/**
 * アイテムを切り詰める
 * 会話中に、指定のアイテムを削除・停止する
 * @param item_id string
 * @returns 
 */
export const truncateItem = (item_id: string) => callTruncateConversationItem(item_id)
/**
 * アイテムを削除する
 * @param item_id string
 * @returns 
 */
export const deleteItem = (item_id: string) => callDeleteConversationItem(item_id)

// レスポンス系処理

/**
 * レスポンスをキャンセルする
 * @param response_id string
 * @returns 
 */ 
export const cancel = (response_id: string) => callResponseCancel(response_id)

