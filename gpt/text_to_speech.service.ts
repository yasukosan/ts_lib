import { GptHelper } from "./_helper/gpt.helper"
import {
    ResponceFormat, TextToSpeechOptions, Voice, initialTextToSpeechOptions
} from "./text_to_speech.type"

export class TextToSpeechService {
    private static instance: TextToSpeechService;

    private Options: TextToSpeechOptions = initialTextToSpeechOptions
    public static call(): TextToSpeechService {
        if (!TextToSpeechService.instance)
        TextToSpeechService.instance = new TextToSpeechService()
        return TextToSpeechService.instance
    }

    /**
     * APIKeyを設定
     * @param key string
     * @returns 
     */
    public setAPIKey(key: string): TextToSpeechService {
        GptHelper.call().setAPIKey(key)
        return this
    }

    /**
     * ChatOptionsを設定
     * @param options ChatOptions
     * @returns VisionService
     */
    public setOptions(
        voice: Voice,
        response_format: ResponceFormat,
        speed: number,
    ): TextToSpeechService {
        this.Options = Object.assign(this.Options, {
            voice,
            response_format,
            speed
        })
        return this
    }

    public setText(
        text: string,
    ): TextToSpeechService {
        this.Options.input = text

        return this
    }
    /**
     * ApiKeyが登録されているか確認
     * @returns boolean
     */
    public checkApi(): boolean {
        return GptHelper.call().checkApiKey()
    }

    /**
     * API呼び出し結果を返す
     * @returns any
     */
    public getResult(): any {
        return GptHelper.call().getResult()
    }

    public getInfo(): {type: string, message: string} {
        return GptHelper.call().getInfo()
    }

    /**
     * API呼び出し実行
     * @returns Promise<void>
     */
    public async do(): Promise<void> {
        GptHelper.call().ini()
        await GptHelper.call().doTextToSpeech(this.Options)
        return
    }

    public reset(): void {
        this.Options = initialTextToSpeechOptions
    }
}