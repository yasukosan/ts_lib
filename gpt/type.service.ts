import {
    ChatOptionsType as ChatOptionsORG,
    initialChatOption as initialChatOptionORG,
} from './_helper/chat.helper'
import {
    CompletionOptions as CompletionOptionsORG,
} from './_helper/completion.helper'
import {
    EmbedOptionsType as EmbedOptionsORG,
    initialEmbedOptions as initialEmbedOptionsORG,
} from './_helper/embed.helper'
import {
    ImageOptionsType as ImageOptionsORG,
    ImageEditOptionsType as ImageEditOptionsORG,
    ImageChangeOptionsType as ImageChangeOptionsORG,
    initialImageOption as initialImageOptionORG,
    initialImageEditOption as initialImageEditOptionORG,
    initialImageChangeOption as initialImageChangeOptionORG,
} from './_helper/image.helper'
import {
    TextToSpeechOptions as TextToSpeechOptionsORG,
    initialTextToSpeechOptions as initialTextToSpeechOptionsORG,
} from './_helper/speech.helper'
import {
    VisionOptions as VisionOptionsORG,
    initialVisionOption as initialVisionOptionORG,
} from './_helper/vision.helper'
import {
    WhisperOptions as WhisperOptionsORG,
    initialWhisperOption as initialWhisperOptionORG,
} from './_helper/whisper.helper'

export type ChatOptions = ChatOptionsORG
export const initialChatOption = initialChatOptionORG

export type CompletionOptions = CompletionOptionsORG

export type EmbedOptions = EmbedOptionsORG
export const initialEmbedOptions = initialEmbedOptionsORG

export type ImageOptions = ImageOptionsORG
export type ImageEditOptions = ImageEditOptionsORG
export type ImageChangeOptions = ImageChangeOptionsORG
export const initialImageOption = initialImageOptionORG
export const initialImageEditOption = initialImageEditOptionORG
export const initialImageChangeOption = initialImageChangeOptionORG

export type TextToSpeechOptions = TextToSpeechOptionsORG
export const initialTextToSpeechOptions = initialTextToSpeechOptionsORG

export type VisionOptions = VisionOptionsORG
export const initialVisionOption = initialVisionOptionORG

export type WhisperOptions = WhisperOptionsORG
export const initialWhisperOption = initialWhisperOptionORG
