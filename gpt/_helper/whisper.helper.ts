
export type WhisperOptions = {
    audio: File | string | undefined,
    model: 'whisper-1',
    prompt?: string,
    output_format?: 'txt' | 'json' | 'srt' | 'vtt' | 'verbose_json',
    response_format?: 'text' | 'json' | 'srt' | 'vtt' | 'verbose_json',
    temperature?: number,
    language?: string,
}

export const initialWhisperOption: WhisperOptions = {
    audio: undefined,
    model: 'whisper-1',
    prompt: '',
    output_format: 'txt',
    response_format: 'vtt',
    temperature: 0.5,
    language: 'ja',
}

export const WhisperParser = (data: any): string => {
    if (typeof data === 'string') return data
    return ''
}

