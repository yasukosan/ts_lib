// 利用可能なEmbeddingAPIのモデル
export type ModelType = 
    'text-embedding-3-large'   // 3072トークン
    | 'text-embedding-3-small' // 1536トークン
    | 'text-embedding-ada-002' // 1536トークン

// エンコードフォーマット
export type EncodeFormatType = 'float' | 'base64'

/**
 * EmbeddingAPIの戻り値の型
 * 
 * @param data: {
 *    embedding: number[],
 *    index: number,
 *    object: 'embedding',
 * }[]
 * @param model: Model
 * @param object: 'list'
 * @param usage: {
 *   prompt_tokens: number,
 *   total_tokens: number,
 * }
 */
export type EmbedReturnType = {
    data: {
        embedding: number[],
        index: number,
        object: 'embedding',
    }[],
    model: ModelType,
    object: 'list',
    usage: {
        prompt_tokens: number,
        total_tokens: number,
    }
}

/**
 * EmbeddAPI呼び出しオプションの型
 */
export type EmbedOptionsType = {
    model   : ModelType,
    input   : string,
    encoding_format? : EncodeFormatType,
    dimensions? : number,
}

export const initialEmbedOptions: EmbedOptionsType = {
    model: 'text-embedding-3-small',
    input: 'Hello!',
    encoding_format: 'float',
    dimensions: 1536, // 1536 or 3072 only for 'text-embedding-3'
}

export const EmbedParser = (data: any): number[] => {
    const result: number[] = []
    if ('result' in data) {
        data.result.data.map((d: any) => {
            if ('embedding' in d) {
                result.push(d.embedding)
            }
        })
    }
    return result
}