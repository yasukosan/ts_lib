

export type Model = 'tts-1' | 'tts-1-hd'

export type Voice = 'alloy' | 'echo' | 'fable' | 'onyx' | 'nova' | 'shimmer'

/**
 * TextToSpeechAPI呼び出しオプションの型
 * @property model TextToSpeechAPIのモデル名
 * @property voice TextToSpeechAPIのボイス名
 * @property input TextToSpeechAPIに送るメッセージ
 * @property response_format TextToSpeechAPIのレスポンス形式
 * @property speed TextToSpeechAPIの再生速度
 */
export type TextToSpeechOptions = {
    model   : Model,
    voice   : 'alloy'
    input   : string,
    response_format? : 'mp3' | 'opus' | 'aac' | 'flac',
    speed?  : number,
}


export const initialTextToSpeechOptions: TextToSpeechOptions = {
    model: 'tts-1',
    voice: 'alloy',
    input: 'Hello!',
    response_format: 'mp3',
    speed: 1,
}

export const TextToSpeechParser = (data: any): string => {
    if (data.hasOwnProperty('result')) {
        if (typeof data.result === 'string') {
            return data.result
        }
    }
    return ''
}