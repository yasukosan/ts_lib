export type CompletionModels = 
    'text-davinci-003' | 'text-davinci-002' | 'text-curie-001' |
    'text-babbage-001' | 'text-ada-001'

/**
 * テキスト生成のオプション
 * @param model       : CompletionModels
 * @param prompt?     : string
 * @param suffix?     : string
 * 挿入テキストの完了後に追加するテキスト
 * @param max_tokens? : number　デフォルト16
 * 生成するトークンの最大値、デフォルト16、最大2048、（最新モデルでは4096）
 * @param temperature?: number デフォルト１
 * 0～２の数値、2に近いほどランダム性が増し、0に近いほど確定的な内容になる
 * top_pと同時に使用することは推奨されない
 * @param top_p?      : number
 * 0～1の数値、確率のしきい値、確率がこの値を超えるトークンのみを生成する
 * temperatureと同時に使用することは推奨されない
 * @param n?          : number デフォルト１
 * 生成するテキストの数、デフォルト１
 * @param stream?     : boolean
 * @param logprobs?   : number
 * @param echo?       : boolean
 * @param stop?       : string | string[]
 * @param presence_penalty?   : number
 * @param frequency_penalty?  : number
 * @param best_of?    : number デフォルト１
 * @param logit_bias? : { [key: string]: number }
 * @param user?       : string
 */
export type CompletionOptions = {
    model       : CompletionModels,
    prompt?     : string,
    suffix?     : string,
    max_tokens? : number,
    temperature?: number,
    top_p?      : number,
    n?          : number,
    stream?     : boolean,
    logprobs?   : number,
    echo?       : boolean,
    stop?       : string | string[],
    presence_penalty?   : number,
    frequency_penalty?  : number,
    best_of?    : number,
    logit_bias? : { [key: string]: number },
    user?       : string,
}

export const initialCompletionOptions: CompletionOptions = {
    model       : 'text-davinci-003',
    prompt      : '',
    max_tokens  : 16,
    temperature : 1,
    top_p       : 1,
    n           : 1,
    logprobs    : 1,
    presence_penalty   : 0,
    frequency_penalty  : 0,
    best_of     : 1,
    logit_bias  : {},
}