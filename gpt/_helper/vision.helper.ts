
export type Role = 'user' | 'system' | 'assistant'
export type VisionModel = 'gpt-4o'

/**
 * VisionAPI呼び出しオプションの型
 * @property model VisionAPIのモデル名
 * @property messages VisionAPIに送るメッセージ
 * @property messages.role メッセージの送信者
 * @property messages.content メッセージの内容
 * @property messages.content.type メッセージのタイプ
 * @property messages.content.text メッセージのテキスト
 * @property messages.content.image_url メッセージの画像URL
 */
export type VisionOptions = {
    model: VisionModel,
    messages: {
        role: Role,
        content: {
            type: 'text' | 'image_url',
            text?: string,
            image_url?: string,
        }[]
    }[],
}

export const initialVisionOption: VisionOptions = {
    model: 'gpt-4o',
    messages: [
        {
            role: 'user',
            content: [
                {
                    type: 'text',
                    text: 'Hello!'
                }
            ]
        }
    ]
}

export type VisionMessages = {
    role: Role,
    message: string,
}

export const VisionParser = (data: any): VisionMessages | false  => {
    if (data.hasOwnProperty('result')) {
        return {
            message: data.result.message.content,
            role: data.result.message.role,
        }
    }
    return false
}