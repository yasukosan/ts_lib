export type ModelType = 'dall-e-3' | 'dall-e-2'

/**
 *
 */
export type ImageOptionsType = {
    model           : ModelType,
    // 生成文字列 最大1000文字
    prompt          : string,
    // 生成する画像の数、1~10 デフォルト1
    n?              : number,
    // 生成する画像のサイズ、デフォルト1024x1024 より大きいサイズは dall-e-3 でのみ使用可能
    size?           : '256x256' | '512x512' | '1024x1024' | '1024x1792' | '1792x1024',
    response_format?: 'url' | 'b64_json',
    style?          : 'vivid' | 'natural', // default vivid used for dall-e-3
    quality?        : 'standard' | 'hd',
    user?           : string,
}

export const initialImageOption: ImageOptionsType = {
    model       : 'dall-e-3',
    prompt      : '',
    n           : 1,
    size        : '1024x1024',
    style       : 'vivid',
    quality     : 'standard',
    response_format : 'b64_json',
}

export type ImageEditOptionsType = {
    model           : ModelType,
    image           : File, // 4MB以下
    image_base64    : string,
    mask            : File, // 4MB以下 imageと同じサイズ
    mask_base64     : string,
    prompt          : string,
    // 生成する画像の数、1~10 デフォルト1
    n?              : number,
    size?           : '256x256' | '512x512' | '1024x1024',
    response_format?: 'url' | 'b64_json',
    user?           : string,
}

export const initialImageEditOption: ImageEditOptionsType = {
    image       : new File([], ''),
    image_base64: '',
    mask        : new File([], ''),
    mask_base64 : '',
    model       : 'dall-e-2',
    prompt      : '',
    size        : '1024x1024',
    n           : 1,
    response_format : 'b64_json',
}

export type ImageEditResultType = {
    choices: {
        finish_reason: string,
        index: number,
        logprobs: any,
        message: {
            content: string,
            role: string
        }
    }[],
    created: number,
    id: string,
    model: string
    object: string
    system_fingerprint: string
}

export type ImageChangeOptionsType = {
    model           : ModelType,
    image           : File, // 4MB以下
    image_base64    : string,
    prompt          : string,
    // 生成する画像の数、1~10 デフォルト1
    n?              : number,
    size?           : '256x256' | '512x512' | '1024x1024',
    response_format?: 'url' | 'b64_json',
    user?           : string,
}

export const initialImageChangeOption: ImageChangeOptionsType = {
    image       : new File([], ''),
    image_base64: '',
    prompt      : '',
    model       : 'dall-e-2',
    size        : '1024x1024',
    response_format : 'b64_json',
    n           : 1,
}


export type ImageResultType = {
    image: string,
    prompt: string,
}[]

export const ImageParser = (data: any): ImageResultType => {
    console.log(data)
    const images: ImageResultType = []
    if (data.result.hasOwnProperty('data')) {
        data.result.data.map((val: any) => {
            if (val.hasOwnProperty('b64_json'))
                images.push({
                    image: val.b64_json,
                    prompt: val.revised_prompt,
                })
            else
                images.push({
                    image: val.url,
                    prompt: val.revised_prompt,
                })
        })
    }
    return images
}