import WebSocket from 'ws'
import decodeAudio from 'audio-decode'

export type StreamOptionsType = {
    url: string
    callback: (data) => void
}

export const initialStreamOption: StreamOptionsType = {
    url: 'wss://api.openai.com/v1/realtime?model=gpt-4o-realtime-preview-2024-10-01',
    callback: (data) => {}
}

export type SendTextType = {
    type: 'conversation.item.create',
    item: {
        type: 'mesage',
        role: 'user' | 'bot',
        content: [
            {
                type: 'input_text',
                text: string
            }
        ]
    }
}

export type SendAudioType = {
    type: 'conversation.item.create',
    item: {
        type: 'message',
        role: 'user' | 'bot',
        content:[
            {
                type: 'audio',
                audio: string
            }
        ]
    }
}


export class StreamHelper {
    private static instance: StreamHelper;

    private Options: StreamOptionsType = initialStreamOption
    private Ws: WebSocket | undefined

    public static call(): StreamHelper {
        if (!StreamHelper.instance)
            StreamHelper.instance = new StreamHelper()
        return StreamHelper.instance
    }

    public connect(): StreamHelper {

        if (this.Ws !== undefined) return this

        this.Ws = new WebSocket(this.Options.url, {
            headers: {
                'Authorization': 'Bearer ' + process.env.OPENAI_API_KEY,
                'OpenAI-Beta': 'realtime=v1'
            }
        })

        this.Ws.on("open", () => {
            console.log("Connected to server.")
            this.Ws.send(JSON.stringify({
                type: "response.create",
                response: {
                    modalities: ["text"],
                    instructions: "Please assist the user.",
                }
            }));
        });
        return this
    }

    public sendText(text: string): StreamHelper {
        if (this.Ws === undefined) return this

        this.Ws.send(JSON.stringify({
            type: "conversation.item.create",
            item: {
                type: "message",
                role: "user",
                content: [
                    {
                        type: "input_text",
                        text: text
                    }
                ]
            }
        }))
        return this
    }

    public async sendAudio(audio: Float32Array): Promise<StreamHelper> {
        if (this.Ws === undefined) return this

        const audioBuffer = await decodeAudio(audio)
        // モノラルの音声データのみ対応
        const channelData = audioBuffer.getChannelData(0)
        const base64AudioData = base64EncodeAudio(channelData)

        this.Ws.send(JSON.stringify({
            type: "conversation.item.create",
            item: {
                type: "message",
                role: "user",
                content: [
                    {
                        type: "audio",
                        audio: base64AudioData
                    }
                ]
            }
        }))
        return this
    }
}


// Converts Float32Array of audio data to PCM16 ArrayBuffer
const floatTo16BitPCM = (float32Array) => {
    const buffer = new ArrayBuffer(float32Array.length * 2)
    const view = new DataView(buffer)
    let offset = 0
    for (let i = 0; i < float32Array.length; i++, offset += 2) {
        let s = Math.max(-1, Math.min(1, float32Array[i]))
      view.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7fff, true)
    }
    return buffer
}

// Converts a Float32Array to base64-encoded PCM16 data
const base64EncodeAudio = (float32Array) => {
    const arrayBuffer = floatTo16BitPCM(float32Array)
    let binary = ''
    let bytes = new Uint8Array(arrayBuffer)
    const chunkSize = 0x8000; // 32KB chunk size
    for (let i = 0; i < bytes.length; i += chunkSize) {
        let chunk = bytes.subarray(i, i + chunkSize)
        binary += String.fromCharCode.apply(null, chunk)
    }
    return btoa(binary);
}