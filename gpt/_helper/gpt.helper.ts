import { OpenAI } from "openai"
import { ChatOptionsType } from "./chat.helper"

import {
    ImageOptionsType, ImageEditOptionsType, ImageChangeOptionsType
} from "./image.helper"
import { WhisperOptions } from "../whisper.service"
import { VisionOptions } from "../vision.type"
import { EmbedOptionsType } from "./embed.helper"

export type APIOptions = {
    endpoints  : {
        comp        : string,
        chat        : string,
        image       : string,
        whisper     : string,
    },
    headers     : {},
}
export class GptHelper {
    private static instance: GptHelper
    private API_KEY: string = ''
    private OpenAI: OpenAI | undefined

    private Result: any;
    private info: {type: string, message: string} = {type: 'none', message: ''}

    public static call() {
        if (!GptHelper.instance) {
            GptHelper.instance = new GptHelper();
        }

        return GptHelper.instance
    }

    /**
     * APIKeyを設定
     * @param key string
     * @returns 
     */
    public setAPIKey(key: string): GptHelper {
        this.API_KEY = key
        return this
    }

    /**
     * 処理結果を返す
     * @returns any
     */
    public getResult(): any {
        //console.log(this.Result)
        return this.Result
    }

    public getInfo(): {type: string, message: string} {
        return this.info
    }

    public ini(): boolean | {type: string, message: string} {
        if (this.API_KEY === '') {
            this.info = {type: 'error', message: 'API_KEY Not Set'}
            return this.info
        }
        this.OpenAI = new OpenAI({
            apiKey: this.API_KEY,
            dangerouslyAllowBrowser: true,
        })

        return true
    }

    /**
     * WhisperAPIをコール
     * @param {audio} File | ReadStream
     * @returns Promise<void>
     */
    public async doTranscription(options: WhisperOptions): Promise<void> {
        if (!this.checkModule()) return;
        if (options.audio === undefined) return;

        try {
            this.Result = await this.OpenAI!
                    .audio.transcriptions.create({
                        file    : options.audio as File,
                        model   : options.model,
                        prompt  : options.prompt,
                        response_format : options.response_format,
                        temperature : options.temperature,
                        language    : options.language
                    })

        } catch (error) {
            console.log(error)
        }
        return
    }

    /**
     * ChatAPIをコール
     * @param message ChatOptions
     * @returns Promise<void>
     */
    public async doChatCompletion(
        message : ChatOptionsType
    ): Promise<void> {
        if (!this.checkModule()) return
        console.log(message)
        try {
            const result = 
            await this.OpenAI?.chat.completions.create({
                model   : message.model,
                messages: message.messages as any, // 参照されている全ての型に合わせるの無理
                frequency_penalty: message.frequency_penalty,
                presence_penalty: message.presence_penalty,
                max_completion_tokens: message.max_completion_tokens,
                temperature: message.temperature,
                top_p: message.top_p,
                n: message.n,
                stop: message.stop,
            })
            console.log(result)
            this.Result = (
                    result !== undefined
                    && 'choices' in result
                ) ? result : false
        } catch (error) {
            console.error(error)
        }
    }

    /**
     * ImageAPIをコール
     * @param options ImageOptions
     * @returns Promise<void>
     */
    public async doImageCompletion(
        options: ImageOptionsType
    ): Promise<void> {
        if (!this.checkModule()) return;

        const result = await this.OpenAI!
                            .images.generate(options);
        //console.log(result)
        this.Result = ('data' in result)
            ? result
            : false
    }

    public async doImageEdit(
        options: ImageEditOptionsType
    ): Promise<void> {
        if (!this.checkModule()) return;

        const result = await this.OpenAI!
                            .images.edit(options)
        //console.log(result)
        this.Result = ('data' in result)
            ? result
            : false
    }

    public async doImageChange(
        options: ImageChangeOptionsType
    ): Promise<void> {
        if (!this.checkModule()) return;

        const result = await this.OpenAI!
                            .images.createVariation(options)
        //console.log(result)
        this.Result = ('data' in result)
            ? result
            : false
    }

    /**
     * VisionAPIをコール
     * @param options VisionOptions
     * @returns Promise<void>
     * 
     * openai側の型指定↓
     * export type ChatCompletionMessageParam =
        | ChatCompletionSystemMessageParam
        | ChatCompletionUserMessageParam
        | ChatCompletionAssistantMessageParam
        | ChatCompletionToolMessageParam
        | ChatCompletionFunctionMessageParam;
        を満たす型情報を内部に仕込めないので、anyで回避
     */
    public async doVision(
        options: VisionOptions | any
    ): Promise<void> {
        if (!this.checkModule()) return

        const result = 
        await this.OpenAI!.chat.completions.create(options)
        //console.log(result)
        this.Result = ('choices' in result)
            ? result.choices[0]
            : {};
    }

    public async doTextToSpeech(
        options: any
    ): Promise<void> {
        if (!this.checkModule()) return

        const result = await this.OpenAI!.audio.speech.create(options)

        try {
            // const b = this.arrayBufferToBinaryString(await result.arrayBuffer())
            // const b = new Uint8Array(await result.arrayBuffer())
            // this.Result = Buffer.from(b).toString('base64')
            this.Result = await result.arrayBuffer()
            console.log(this.Result)
        } catch (error) {
            console.error(error)
            this.Result = {}
        }
    }

    public async doEmbed(
        options: EmbedOptionsType
    ): Promise<void> {

        if (!this.checkModule()) return

        try {
            const result = await this.OpenAI!.embeddings.create(options)
            this.Result = result
            console.log('Send Embed Options', options)
            console.log('Retrun Embed Result', result)
        } catch (error) {
            console.error(error)
            this.Result = {}
        }
    }

    public checkApiKey(): boolean {
        return (this.API_KEY === '') ? false : true;
    }

    private checkModule(): boolean {
        if (this.OpenAI === undefined) {
            this.info = {type: 'error', message: 'OpenAI Module Not Set'}
            console.info(this.info)
            return false;
        }
        return true;
    }

}