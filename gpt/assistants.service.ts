import { GptHelper } from "./_helper/gpt.helper"
import {
    ChatOptionsType, ChatMessagesType, initialChatOption,
    ChatContentType
} from "./_helper/chat.helper"

export class ChatService {
    private static instance: ChatService;

    private Options: ChatOptionsType = initialChatOption
    private History: ChatMessagesType[] | undefined
    public static call(): ChatService {
        if (!ChatService.instance)
            ChatService.instance = new ChatService()
        return ChatService.instance
    }

    /**
     * APIKeyを設定
     * @param key string
     * @returns 
     */
    public setAPIKey(key: string): ChatService {
        GptHelper.call().setAPIKey(key)
        return this
    }

    /**
     * Historyを変数に格納
     * @param history [{role: 'user' | 'system', message: string}
     * @returns ChatService
     */
    public setHistory(
        history: [{
            role: 'user' | 'system' | 'assistant',
            content: ChatContentType[] | string
        }]
    ): ChatService {
        this.History = history.map(val => {
            return {role: val.role, content: val.content}
        })

        this.Options.messages = this.History!
        return this
    }

    /**
     * ChatOptionsを設定
     * @param options ChatOptions
     * @returns ChatService
     */
    public setOptions(options: ChatOptionsType): ChatService {
        this.Options = Object.assign(this.Options, options);
        return this;
    }

    public setMessage(
        message: ChatContentType[],
        role: 'system' | 'user' | 'assistant' = 'user'
    ): ChatService {
        if (this.History !== undefined) 
            this.Options.messages.push({role: role, content: message})
        else
            this.Options.messages = [{role: role, content: message}]

        return this
    }
    /**
     * ApiKeyが登録されているか確認
     * @returns boolean
     */
    public checkApi(): boolean {
        return GptHelper.call().checkApiKey()
    }

    /**
     * API呼び出し結果を返す
     * @returns any
     */
    public getResult(): any {
        return GptHelper.call().getResult()
    }

    public getInfo(): {type: string, message: string} {
        return GptHelper.call().getInfo()
    }

    /**
     * API呼び出し実行
     * @returns Promise<void>
     */
    public async do(): Promise<void> {
        GptHelper.call().ini()
        await GptHelper.call().doChatCompletion(this.Options)
        return;
    }

    public reset(): void {
        this.History = undefined
        this.Options = initialChatOption
    }
}