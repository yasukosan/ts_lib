

export type ServerOptionsType = {
    url: string
    apiKey: string
    modalities?: ['audio' | 'text' | 'audio', 'text']
    model: string
    instructions?: string
    voice: 'alloy' | 'ballad' | 'coral' | 'echo' | 'shimmer' | 'verse'
    inpiut_audio_format?: 'pcm16' | 'g711_ulaw' | 'g711_alaw',
    output_audio_format?: 'pcm16' | 'g711_ulaw' | 'g711_alaw',
    // 現時点ではWhisper-1のみ (default: null)
    inpiut_audio_transcription?: {
        model: 'whisper-1'
    },
    // (default: null）
    turn_detection?: {
        type?: number // 0.0~1.0 (default: 0.5)
        threshold?: number // 0.0~1.0 (default: 0.5)
        // 何ミリ秒以上会話を続けるとターンが変わったとみなすか
        prefix_padding_ms?: number // (default: 300ms)
        // 会話終了とみなすための無音の長さ（ミリ秒）
        silence_duration_ms?: number // (default: 500ms)
        create_responce?: boolean // (default: true)
    },
    tools?: {
        type?: 'function',
        name?: string,
        description?: string,
        parameters?: {[key: string]: any}
    }[],
    tool_choice?: 'auto' | 'none' | 'required',
    tempertaure?: number // 0.6~1.2 (default: 0.8)
    max_response_output_tokens?: number // 1~4096 (default: 100)
}

const ServerOptions: ServerOptionsType = {
    url: 'https://api.openai.com/v1/realtime/sessions',
    apiKey: process.env.OPENAI_API_KEY as string,
    model: 'gpt-4o-realtime-preview-2024-12-17',
    voice: 'verse'
}

export const setServerOptions = (options: Partial<ServerOptionsType>): ServerOptionsType => Object.assign(ServerOptions, options)
export const getServerOptions = (): ServerOptionsType => ServerOptions

export const getEphemeralKey = async () => {
    const response = await fetch(ServerOptions.url, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${ServerOptions.apiKey}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            model: ServerOptions.model,
            voice: ServerOptions.voice
        })
    })

    return response.json()
}
