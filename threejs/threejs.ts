import * as THREE from 'three';
import { PlaneService } from './helper/plane.service';
import { SphereService } from './helper/sphere.service';
import { TextService } from './helper/text.service';

import { ImageHelper } from '../image/helper/image.helper';

export class ThreeTest {
    private static instance: ThreeTest;

    private renderer: any;
    private scene: any;
    private camera: any;

    private animation = false;
    private animationId = 0;

    private renderTarget: HTMLCanvasElement | any;

    private mesh: any;
    private texture: any;
    private texture2: any;

    public static call(reset = false): ThreeTest {
        if (!ThreeTest.instance) {
            ThreeTest.instance = new ThreeTest();
        }
        return ThreeTest.instance;
    }

    /**
     * 描画用のCanvasターゲットを設定
     * @param target string canvasタグのID
     * @returns ThreeTest
     */
    public setCanvas(target = 'threetest'): ThreeTest {
        this.renderTarget = document.querySelector(
            '#' + target
        ) as HTMLCanvasElement;
        return this;
    }

    public setMesh(mesh: number[]): ThreeTest {
        this.mesh = mesh;
        return this;
    }

    public buildRenderer(): ThreeTest {
        this.renderer = new THREE.WebGLRenderer({
            canvas: this.renderTarget,
            alpha: true,
            antialias: true
        });
        return this;
    }

    public buildScene(): ThreeTest {
        // シーンを作成
        this.scene = new THREE.Scene();
        return this;
    }

    public setRendereSize(): ThreeTest {
        this.renderer.setSize(400, 600);
        document.body.appendChild(this.renderer.domElement);
        return this;
    }

    public setCamera(): ThreeTest {
        // カメラを作成
        this.camera = new THREE.PerspectiveCamera(45, 400 / 600, 1, 10000);
        this.camera.position.set(0, 0, 1000);
        return this;
    }

    public setLight(): ThreeTest {
        // 平行光源を生成
        //const light = new THREE.DirectionalLight(0xffffff);
        //light.position.set(100, 130, 80);

        // 環境光源の作成
        const light = new THREE.AmbientLight(0xffffff, 1.0);

        this.scene.add(light);
        return this;
    }

    /**
     *
     * @returns
     */
    public setup(): ThreeTest {
        this.buildRenderer() // レンダラーの作成
            .setRendereSize() // レンダラーのサイズを設定
            .buildScene() // シーンを作成
            .setCamera() // カメラを作成
            .setLight() // 平行光源を生成
            .startAnimation(); // アニメーションを開始

        return this;
    }

    public async addSphere(im: string): Promise<ThreeTest> {
        await SphereService.call().add(
            'front',
            -5,
            150,
            0,
            100,
            32,
            16,
            0,
            Math.PI * 1.3,
            0,
            Math.PI,
            im
        );
        this.scene.add(SphereService.call().getGeoMetryByName('front'));

        await SphereService.call().add(
            'left',
            100,
            105,
            -50,
            100,
            32,
            16,
            0,
            Math.PI * 1.3,
            0,
            Math.PI,
            im
        );
        SphereService.call().changeRotation(
            'left',
            Math.PI / 8,
            Math.PI / 6,
            -Math.PI / 10
        );
        this.scene.add(SphereService.call().getGeoMetryByName('left'));

        await SphereService.call().add(
            'right',
            -100,
            105,
            -50,
            100,
            32,
            16,
            0,
            Math.PI * 1.3,
            0,
            Math.PI,
            im
        );
        SphereService.call().changeRotation(
            'right',
            Math.PI / 8,
            -Math.PI / 6,
            Math.PI / 10
        );
        this.scene.add(SphereService.call().getGeoMetryByName('right'));

        return this;
    }

    public async updateSphere(name: string, im: string): Promise<ThreeTest> {
        await SphereService.call().changeMap('front', im);
        await SphereService.call().changeMap('left', im);
        await SphereService.call().changeMap('right', im);
        return this;
    }

    public async addCube(): Promise<ThreeTest> {
        /*
        await TextService.call()
            .setTextData([{
                txt     : 'うんこ',
                size    : 80,
                font    : 'MS 明朝',
                x       : 0,
                y       : 0,
            }])
            .build();
        this.scene.add(TextService.call().getGeometry());*/

        await ImageHelper.call().setImageURL('/gid02.png');
        const im = ImageHelper.call().getImageForString('image/png');

        await PlaneService.call().add('gid', 580, 800, 0, 0, -60, im);
        const geo = PlaneService.call().getByName('gid');
        console.log(geo);

        this.scene.add(geo['geometry']);
        return this;
    }

    public async addTexture(img: string): Promise<ThreeTest> {
        const loader = new THREE.TextureLoader();
        await loader.load(img, (texture) => {
            this.texture = texture;
        });
        return this;
    }

    public addGeometry(): ThreeTest {
        const geo = new THREE.BufferGeometry();

        const posi = new Float32Array(this.mesh.length * 3);
        const color = new Float32Array(this.mesh.length * 3);

        const L = 400;
        let c = 0;
        for (let i = 0; i < this.mesh.length; i++) {
            posi[c] = this.mesh[i][0];
            posi[c + 1] = this.mesh[i][1];
            posi[c + 2] = this.mesh[i][2];

            const R = Math.abs(posi[c]) / (L / 2);
            const G = Math.abs(posi[c + 1]) / (L / 2);
            const B = Math.abs(posi[c + 2]) / (L / 2);

            const _color = new THREE.Color().setRGB(R, G, B);
            color[c] = _color.r;
            color[c + 1] = _color.g;
            color[c + 2] = _color.b;
            c += 3;
        }
        geo.setAttribute('position', new THREE.BufferAttribute(posi, 3));
        geo.setAttribute('color', new THREE.BufferAttribute(color, 3));

        // var material = new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, vertexColors: THREE.vertexColors});
        const material = new THREE.MeshBasicMaterial({
            side: THREE.DoubleSide,
            vertexColors: true
        });
        const triangles = new THREE.Mesh(geo, material);
        this.scene.add(triangles);
        return this;
    }

    public addShader(): ThreeTest {
        // 頂点シェーダーのソース
        const vertexSource = `
        void main() {
        gl_Position = vec4(position, 1.0);
        }
        `;

        // ピクセルシェーダーのソース
        const fragmentSource = `
        void main() {
        gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
        }
        `;

        // シェーダーソースを渡してマテリアルを作成
        new THREE.ShaderMaterial({
            vertexShader: vertexSource,
            fragmentShader: fragmentSource
        });
        return this;
    }

    public startAnimation(): ThreeTest {
        this.animation = true;

        const tick = (): void => {
            this.animationId = requestAnimationFrame(tick);

            // box.rotation.x += 0.01;
            // box.rotation.y += 0.01;

            // 描画
            this.renderer.render(this.scene, this.camera);
        };
        tick();
        return this;
    }

    public stopAnimation(): ThreeTest {
        cancelAnimationFrame(this.animationId);
        return this;
    }

    private async loadImage(img: string): Promise<any> {
        const im: HTMLImageElement = await new Promise((resolve, reject) => {
            const _img: HTMLImageElement = new Image();
            _img.onload = (e: any) => resolve(_img);
            _img.onerror = (e: any) => reject(e);
            _img.src = '/gid02.png';
        });
        const canvas: HTMLCanvasElement = document.createElement('canvas');
        canvas.setAttribute('width', im.width.toString());
        canvas.setAttribute('height', im.height.toString());

        const ctx: any = canvas.getContext('2d');
        ctx.drawImage(im, 0, 0);
        return canvas.toDataURL('image/jpg');
    }
}
