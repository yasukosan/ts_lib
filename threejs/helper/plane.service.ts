import * as THREE from 'three';

export interface PlaneInterface {
    name: string;
    width: number;
    height: number;
    geometry: any;
}

export class PlaneService {
    private static instance: PlaneService;

    private geometrys: PlaneInterface[] | any = [];

    public static call(reset = false): PlaneService {
        if (!PlaneService.instance) {
            PlaneService.instance = new PlaneService();
        }
        return PlaneService.instance;
    }

    /**
     * PlaneGeometryを追加
     * @param name
     * @param width
     * @param height
     * @param x
     * @param y
     * @param z
     * @param texture
     * @returns
     */
    public async add(
        name: string,
        width = 400,
        height = 600,
        x = 0,
        y = 0,
        z = 0,
        texture: any = null
    ): Promise<PlaneService> {
        let txt: any;
        let mat: any;

        if (texture !== null) {
            txt = await this.loadTexture(texture);
            mat = new THREE.MeshLambertMaterial({
                transparent: true,
                alphaTest: 1,
                map: txt
            });
        } else {
            mat = new THREE.MeshLambertMaterial({
                transparent: true,
                alphaTest: 1
            });
        }

        const cu = new THREE.Mesh(new THREE.PlaneGeometry(width, height), mat);
        cu.position.set(x, y, z);
        cu.name = name;

        this.savePlane({
            name: name,
            width: width,
            height: height,
            geometry: cu
        });
        return this;
    }

    /**
     * Geometry情報を名前から取得
     * @param name
     * @returns PlaneInterface Geometryを含む保存データ
     */
    public getByName(name: string): any {
        return this.searchByName(name);
    }

    /**
     * Geometry情報を名前から取得（Geometryデータのみ）
     * @param name
     * @returns any geometryデータ
     */
    public getGeoMetryByName(name: string): any {
        const geo = this.getByName(name);
        return geo['geometry'];
    }

    /**
     * Geometryの3次元座標を移動させる
     * @param name string
     * @param x number default = 0
     * @param y number default = 0
     * @param z number default = 0
     * @returns PlaneService
     */
    public changePosition(name: string, x = 0, y = 0, z = 0): PlaneService {
        const geo = this.searchByName(name);
        if (geo === false) return this;

        return this;
    }

    /**
     * Geometryの向きを変える
     * @param name string
     * @param x number default = 0
     * @param y number default = 0
     * @param z number default = 0
     * @returns PlaneService
     */
    public changeRotation(name: string, x = 0, y = 0, z = 0): PlaneService {
        const geo = this.searchByName(name);
        if (geo === false) return this;

        geo['geometry'].rotation.x += x;
        geo['geometry'].rotation.y += y;
        geo['geometry'].rotation.z += z;

        return this;
    }

    /**
     * 保存配列にGeometryを追加する
     * @param plane PlaneInterface
     * @returns PlaneService
     */
    public savePlane(plane: PlaneInterface): PlaneService {
        this.geometrys.push(plane);
        return this;
    }

    /**
     * 保存済みのGeometryを削除する
     * @param name string Geometry登録名
     * @returns any | boolean
     */
    public deletePlane(name: string): any | boolean {
        const geo = this.searchByName(name);

        if (geo === false) return false;

        geo['geometry'].material.dispose();
        geo['geometry'].geometry.dispose();
        return geo['geometry'];
    }

    /**
     * 登録名からGeometry登録データを探す
     * @param name string
     * @returns
     */
    private searchByName(name: string): any {
        for (const key in this.geometrys) {
            if (Object.prototype.hasOwnProperty.call(this.geometrys, key)) {
                if (this.geometrys[key]['name'] === name) {
                    return this.geometrys[key];
                }
            }
        }
        return false;
    }

    /**
     * 画像データをテクスチャに変換
     * @param texture string 画像文字列
     * @returns Promise<any>
     */
    private async loadTexture(texture: string): Promise<any> {
        const loader = new THREE.TextureLoader();
        return await loader.load(texture);
    }
}
