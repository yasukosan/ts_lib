export class KalmanFilter {
    private R: number; // 測定ノイズの共分散
    private Q: number; // プロセスノイズの共分散
    private A: number; // 状態行列
    private B: number; // 制御行列
    private C: number; // 観測行列
    private cov: number; // 共分散
    private x: number; // 値

    constructor({ R = 2, Q = 1, A = 1, B = 0, C = 1 } = {}) {
        this.R = R; // 測定ノイズの共分散
        this.Q = Q; // プロセスノイズの共分散
        this.A = A; // 状態行列
        this.B = B; // 制御行列
        this.C = C; // 観測行列
        this.cov = NaN; // 共分散
        this.x = NaN; // 値
    }

    filter(z, u = 0) {
        if (isNaN(this.x)) {
            this.x = (1 / this.C) * z;
            this.cov = (1 / this.C) * this.Q * (1 / this.C);
        } else {
            // 予測
            const predX = (this.A * this.x) + (this.B * u);
            const predCov = ((this.A * this.cov) * this.A) + this.R;

            // 更新
            const K = predCov * this.C * (1 / ((this.C * predCov * this.C) + this.Q));
            this.x = predX + K * (z - (this.C * predX));
            this.cov = predCov - (K * this.C * predCov);
        }
        return this.x;
    }
}