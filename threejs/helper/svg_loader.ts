import AjaxService from '../../http/ajax.service';

export class SVGLoader {
    private static instance: SVGLoader;

    private loadFile = '';

    public static call(): SVGLoader {
        if (!SVGLoader.instance) {
            SVGLoader.instance = new SVGLoader();
        }
        return SVGLoader.instance;
    }

    public getFile(): string {
        return this.loadFile;
    }

    public async getSVGFile(url: string): Promise<SVGLoader> {
        const as = new AjaxService();
        // getResultメソッドはサーバーレスポンスからbodyのみを返す
        const result = await as.setURL(url).setMethod('GET').getResult();

        console.log(result.message[0]);
        this.loadFile = result.message[0]['svg'];
        return this;
    }
}
