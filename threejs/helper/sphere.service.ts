import * as THREE from 'three';

export interface SphereInterface {
    name: string;
    geometry: any;
}

export class SphereService {
    private static instance: SphereService;

    private geometrys: SphereInterface[] | any = [];
    private Key = '9999';

    public static call(): SphereService {
        if (!SphereService.instance) {
            SphereService.instance = new SphereService();
        }
        return SphereService.instance;
    }

    /**
     * SphereGeometryを追加
     * @param name
     * @param x
     * @param y
     * @param z
     * @param radius
     * @param widthSegments
     * @param heightSegments
     * @param phiStart
     * @param phiLength
     * @param thetaStart
     * @param thetaLength
     * @param texture
     * @returns
     */
    public async add(
        name: string,
        x = 0,
        y = 0,
        z = 0,
        radius = 10,
        widthSegments = 32,
        heightSegments = 16,
        phiStart = 0,
        phiLength: number = Math.PI * 2,
        thetaStart = 0,
        thetaLength: number = Math.PI * 2,
        texture: any = null
    ): Promise<SphereService> {
        let txt: any;
        let mat: any;

        // すでに登録済みの場合テクスチャと座標を更新
        if (this.searchByName(name) !== false) {
            if (texture !== null) {
                await this.changeMap(name, texture);
            }

            this.changeRotation(name, x, y, z);
            return this;
        }

        if (texture !== null) {
            txt = await this.loadTexture(texture);
            mat = new THREE.MeshLambertMaterial({
                transparent: true,
                alphaTest: 1,
                map: txt
            });
        } else {
            mat = new THREE.MeshLambertMaterial({
                transparent: true,
                alphaTest: 1
            });
        }
        const cu = new THREE.Mesh(
            new THREE.SphereGeometry(
                radius,
                widthSegments,
                heightSegments,
                phiStart,
                phiLength,
                thetaStart,
                thetaLength
            ),
            mat
        );

        cu.position.set(x, y, z);
        cu.name = name;

        this.saveSphere({
            name: name,
            geometry: cu
        });
        return this;
    }

    /**
     * 保存されたGeometry情報を名前から取得
     * @param name string
     * @returns SphereInterface Geometryを含む保存データ
     */
    public getByName(name: string): any {
        return this.searchByName(name);
    }

    /**
     * Geometry情報を名前から取得（Geometryデータのみ）
     * @param name
     * @returns any geometryデータ
     */
    public getGeoMetryByName(name: string): any {
        const geo = this.getByName(name);
        return geo['geometry'];
    }

    public updateState(status: any): SphereService {
        for (const key in status) {
            if (
                Object.prototype.hasOwnProperty.call(
                    this.geometrys[this.Key],
                    key
                )
            ) {
                this.geometrys[this.Key][key] = status[key];
            }
        }
        return this;
    }

    /**
     * Geometryデータの座標を変更する
     * @param name
     * @param x
     * @param y
     * @param z
     * @returns
     */
    public changePosition(name: string, x = 0, y = 0, z = 0): SphereService {
        const geo = this.searchByName(name);
        if (geo === false) return this;

        return this;
    }

    /**
     * 画像データを変更
     * @param name
     * @returns
     */
    public async changeMap(name: string, texture: any): Promise<SphereService> {
        const geo = this.searchByName(name);

        geo['geometry']['material']['map'] = await this.loadTexture(texture);
        return this;
    }

    /**
     * Geometryの向きを変える
     * @param name string
     * @param x number default = 0
     * @param y number default = 0
     * @param z number default = 0
     * @returns PlaneService
     */
    public changeRotation(name: string, x = 0, y = 0, z = 0): SphereService {
        const geo = this.searchByName(name);

        if (geo === false) return this;

        geo['geometry'].rotation.x += x;
        geo['geometry'].rotation.y += y;
        geo['geometry'].rotation.z += z;

        return this;
    }

    /**
     * 保存配列にGeometryを追加する
     * @param plane PlaneInterface
     * @returns PlaneService
     */
    public saveSphere(sphere: SphereInterface) {
        this.geometrys.push(sphere);
    }

    /**
     * 保存済みのGeometryを削除する
     * @param name string Geometry登録名
     * @returns any | boolean
     */
    public deletePlane(name: string): any | boolean {
        const geo = this.searchByName(name);

        if (geo === false) return false;

        geo['geometry'].material.dispose();
        geo['geometry'].geometry.dispose();
        return geo['geometry'];
    }

    /**
     * 登録名からGeometry登録データを探す
     * @param name string
     * @returns
     */
    private searchByName(name: string): any {
        for (const key in this.geometrys) {
            if (Object.prototype.hasOwnProperty.call(this.geometrys, key)) {
                if (this.geometrys[key]['name'] === name) {
                    this.Key = key;
                    return this.geometrys[key];
                }
            }
        }
        return false;
    }

    /**
     * 画像データをテクスチャに変換
     * @param texture string 画像文字列
     * @returns Promise<any>
     */
    private async loadTexture(texture: string): Promise<any> {
        const loader = new THREE.TextureLoader();
        return await loader.load(texture);
    }
}
