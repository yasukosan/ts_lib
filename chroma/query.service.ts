
export type QueryType<K extends string> = {
    [key: string]: {
        [key in K]?: string | number
    }
}

export type QueryKeyType1 = '$and' | '$or'
export type QueryKeyType2 = '$eq' | '$ne'
export type QueryKeyType3 = '$gt' | '$gte'
export type QueryKeyType4 = '$lt' | '$lte'
export type QueryKeyType5 = '$in' | '$nin'

export class ChromaQueryService {

    private static instance: ChromaQueryService

    private eqQuerys: QueryType<QueryKeyType2>[] = []
    private gtQuerys: QueryType<QueryKeyType3>[] = []
    private ltQuerys: QueryType<QueryKeyType4>[] = []
    private inQuerys: QueryType<QueryKeyType5>[] = []

    private query: {QueryKeyType1?: QueryType<string>[]} = {}

    public static call(): ChromaQueryService {
        if (!ChromaQueryService.instance)
            ChromaQueryService.instance = new ChromaQueryService()
        return ChromaQueryService.instance;
    }

    public addEQ(
        key: string,
        value: string | number,
        not: boolean = false
    ): ChromaQueryService {
        if (not) this.eqQuerys.push({ [key]: { $ne: value } })
        else this.eqQuerys.push({ [key]: { $eq: value } })
        return this
    }

    public addGT(
        key: string,
        value: string | number,
        not: boolean = false
    ): ChromaQueryService {
        if (not) this.gtQuerys.push({ [key]: { $gte: value } })
        else this.gtQuerys.push({ [key]: { $gt: value } })
        return this
    }

    public addLT(
        key: string,
        value: string | number,
        not: boolean = false
    ): ChromaQueryService {
        if (not) this.ltQuerys.push({ [key]: { $lte: value } })
        else this.ltQuerys.push({ [key]: { $lt: value } })
        return this
    }

    public addIn(
        key: string,
        value: string[] | number[],
        not: boolean = false
    ): ChromaQueryService {
        if (not) this.inQuerys.push({ [key]: { $nin: value } })
        else this.inQuerys.push({ [key]: { $in: value } })
        return this
    }

    public build(
        flag: QueryKeyType1 = '$and'
    ): ChromaQueryService {
        if (
            this.eqQuerys.length === 0
            && this.gtQuerys.length === 0
            && this.ltQuerys.length === 0
            && this.inQuerys.length === 0
        ) return this

        this.query[flag] = []
        this.query[flag].push(...this.eqQuerys)
        this.query[flag].push(...this.gtQuerys)
        this.query[flag].push(...this.ltQuerys)
        this.query[flag].push(...this.inQuerys)
        
        if (this.query[flag].length === 1) 
            this.query = this.query[flag][0]

        return this
    }

    public getQuery(): {QueryKeyType1?: QueryType<string>[]} {
        return this.query
    }
}