import { uuidService } from "../indentification/uuid"

export type InputDataType = {
    ids: string[]
    metaData: [{ [key: string]: string | number }]
    document: string[]
    embeddings?: number[][]
}

export const InputDataInit: InputDataType = {
    ids: [],
    metaData: [{}],
    document: [],
    embeddings: []
}

export const Extensions: string[] = [
    'txt',
    'doc',
    'docx',
    'pdf'
]

export class DataBuildService
{
    private static instance: DataBuildService

    private ConvDatas: InputDataType = structuredClone(InputDataInit)
    private InputData: InputDataType = structuredClone(InputDataInit)

    public static call(): DataBuildService {
        if (!DataBuildService.instance)
            DataBuildService.instance = new DataBuildService()
        return DataBuildService.instance;
    }

    /**
     * IDを追加する (複数可)
     * @param id string | string[]
     * @returns DataBuildService
     */
    public addID(id: string | string[]): DataBuildService {
        if (typeof id === 'string') {
            if (this.InputData.ids.length === 0){
                this.InputData.ids = [id]
            } else {
                this.InputData.ids.push(id)
            }
        }
        if (Array.isArray(id)) {
            if (this.InputData.ids.length === 0){
                this.InputData.ids = id
            } else {
                this.InputData.ids.concat(id)
            }
        }

        return this
    }

    /**
     * メタデータを追加する
     * @param metadata { [key: string]: string | number }
     * @returns DataBuildService
     */
    public addMetadata(
        metadata: {[key: string]: string | number}
    ): DataBuildService {
        // 一軒目データ登録の場合
        if (this.eq(this.InputData.metaData[0], {})){
            this.InputData.metaData = [metadata]
        } else {
            this.InputData.metaData.push(metadata)
        }
        return this
    }

    /**
     * ドキュメントを追加する
     * @param document string
     * @returns DataBuildService
     */
    public addDocument(
        document: string | string[]
    ): DataBuildService {
        if (typeof document === 'string') {
            if (this.InputData.document.length === 0){
                this.InputData.document = [document]
            } else {
                this.InputData.document.push(document)
            }
        }
        if (Array.isArray(document)) {
            if (this.InputData.document.length === 0){
                this.InputData.document = document
            } else {
                this.InputData.document.concat(document)
            }
        }
        return this
    }

    /**
     * 組み込みベクトルを追加する
     * @param embedding number[] | number[][]
     * @returns DataBuildService
     */
    public addEmbedding(
        embedding: number[] | number[][]
    ): DataBuildService {
        if (embedding.some(Array.isArray)) {
            if (this.InputData.embeddings?.length === 0){
                this.InputData.embeddings = embedding as number[][]
            } else {
                this.InputData.embeddings?.concat(embedding as number[][])
            }
        } else {
            if (this.InputData.embeddings?.length === 0){
                this.InputData.embeddings = [embedding as number[]]
            } else {
                this.InputData.embeddings?.push(embedding as number[])
            }
        }

        return this
    }

    /**
     * データを取得する
     * @returns InputDataType
     */
    public get(): InputDataType {
        return this.InputData
    }

    public build(): InputDataType {
        if (this.ConvDatas.ids.length !== 0) {
            this.ConvDatas = InputDataInit
        }

        for (let i = 0; i < this.InputData.document.length; i++) {
            const slicedTexts = 
                    this.sliceText(
                        this.removeBlankLines(this.InputData.document[i])
                    )

            slicedTexts.forEach((text, index) => {
                this.ConvDatas.ids.push(uuidService.call().getUUIDv4())
                this.ConvDatas.document.push(text)

                if (JSON.stringify(this.ConvDatas.metaData[0]) === JSON.stringify({})) {
                    this.ConvDatas.metaData = [this.InputData.metaData[i]]
                } else {
                    this.ConvDatas.metaData.push(this.InputData.metaData[i])
                }
            })
        }

        console.log(this.ConvDatas)
        return this.ConvDatas
    }

    /**
     * 拡張子情報がExtensionsに含まれているか確認する
     * @param extention string
     * @returns boolean
     */
    public checkFileType(
        extention: string
    ): boolean {
        return Extensions.some(str => extention.includes(str))
    }

    private eq(data1: any, data2: any): boolean {
        return JSON.stringify(data1) === JSON.stringify(data2)
    }

    private removeBlankLines(
        text: string
    ): string {
        return text.split('\n').filter(line => line.trim() !== '').join('\n')
    }

    private sliceText(
        text: string,
        sliceSize: number = 1000,
        overlapSize: number = 100
    ): string[] {
        const slicedTexts: string[] = []

        for (let i = 0; i < text.length; i += sliceSize - overlapSize) {
            console.log(i)
            slicedTexts.push(text.slice(i, i + sliceSize))

            if (i > 100000) {
                break
            }
        }
        console.log(slicedTexts)
        return slicedTexts
    }
}