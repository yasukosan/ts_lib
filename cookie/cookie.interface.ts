export interface StringKeyObject {
    [key: string]: any;
}
export interface CookieInterface {
    /**
     * Cookie情報を上書きする、原則既存のCookieは消える
     *
     * @param data object
     * @return boolean
     */
    setCookie(data: object): Promise<boolean>;

    /**
     * Cookieにデータを1件追記する
     * 追記データはKeyValue形式
     * @param key string
     * @param data string
     * @return boolean
     */
    addCookie(key: string, data: string): Promise<boolean>;

    /**
     * 既存のCookieデータの内容を更新
     * ※存在しないデータは追加される
     *
     * @param data StringKeyObject
     * @return boolean
     */
    updateCookie(data: StringKeyObject): Promise<boolean>;

    /**
     * 全てのCookie取得
     * @return string 全てのcookie文字列を返す
     */
    getCookie(): any;

    /**
     * Cookieから取り出した値を連想配列に変換して返す
     *
     * @return object
     */
    getCookieToArray(): Promise<StringKeyObject>;

    /**
     * Keyに一致する値がCookieに含まれている場合
     * レコードの値部分のみ返す
     *
     * @param key
     * @return string
     */
    getCookieByKey(key: string): Promise<string>;

    /**
     * Keyを指定してCookieを削除
     * 該当するKeyが存在しない場合は処理は何もしない
     *
     * @param key
     */
    deleteCookieToKey(key: string): Promise<boolean>;

    /**
     * 全てのCookieを削除
     * 有効期限を過去に指定して削除する
     */
    deleteCookieAll(): Promise<boolean>;

    /**
     * Cookie情報を更新する
     *
     * @reuturn CookieService
     */
    refreshCookie(): Promise<boolean>;
}
