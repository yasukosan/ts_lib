import {
    urlBase64ToUint8Array,
    urlbase64ToArrayBuffer,
    arrayBufferToBase64url,
    fileToBase64,
    blobToBase64,
    blobToFile,
    base64ToUint8Array,
    base64ToBlob,
    base64ToBuffer,
    bufferToBase64,
    bufferToUint8Array,
} from '../_helper/convert.helper'

export const dataService = {
    /**
     * urlBase64ToUint8Arrayに変換
     * @param base64 string
     * @returns Uint8Array
     */
    urlBase64ToUint8Array,

    /**
     * urlbase64ToArrayBufferに変換
     * @param base64 string
     * @returns ArrayBuffer
     */
    urlbase64ToArrayBuffer,

    /**
     * arrayBufferToBase64urlに変換
     * @param arrayBuffer ArrayBuffer
     * @returns string
     */
    arrayBufferToBase64url,

    /**
     * fileToBase64に変換
     * @param file File
     * @returns Promise<string>
     */
    fileToBase64,
    blobToBase64,
    blobToFile,
    base64ToUint8Array,
    base64ToBlob,
    base64ToBuffer,
    bufferToBase64,
    bufferToUint8Array,
}