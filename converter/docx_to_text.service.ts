import mammoth from 'mammoth'
import {
    urlbase64ToArrayBuffer
} from '../_helper/convert.helper'

export class DocxToTextService {
    private static instance: DocxToTextService

    private file: string | ArrayBuffer = ''
    private result: {value: string, messages: Array<any>} | undefined

    public static call(): DocxToTextService {
        if (!DocxToTextService.instance)
            DocxToTextService.instance = new DocxToTextService()
        return DocxToTextService.instance;
    }

    public setFile(file: string | Buffer): DocxToTextService {
        this.file = file
        return this
    }

    /**
     * Base64をBufferに変換
     */
    public toBuffer(): DocxToTextService {
        if (typeof this.file === 'string') {
            this.file = urlbase64ToArrayBuffer(this.file as string)
        }

        return this
    }

    public async conv(): Promise<string> {
        if (typeof this.file === 'string') return ''

        this.result = await mammoth.extractRawText({
                arrayBuffer: this.file as ArrayBuffer
            })

        return (this.result === undefined) ? '' : this.result.value as string
    }
}