import * as pdfjsLib from 'pdfjs-dist/build/pdf'
import * as pdfjsWorker from 'pdfjs-dist/build/pdf.worker'
/*const PdfjsWorker = require(
    "worker-loader?esModule=false&filename=[name].[contenthash].js!pdfjs-dist/build/pdf.worker.js"
);*/
pdfjsLib.GlobalWorkerOptions.workerSrc = pdfjsWorker

// import Helper
import {
    base64ToBuffer
} from '../_helper/convert.helper'

export class PdfToTextService {
    private static instance: PdfToTextService

    private file: string | ArrayBuffer = ''
    private pdfJS: any = undefined

    public static call(): PdfToTextService {
        if (!PdfToTextService.instance)
            PdfToTextService.instance = new PdfToTextService()
        return PdfToTextService.instance;
    }

    public setFile(file: string | Uint8Array): PdfToTextService {
        this.file = file
        return this
    }

    /**
     * Base64をUint8Arrayに変換
     */
    public toUint8Array(): PdfToTextService {
        this.file = base64ToBuffer(this.file as string)
        return this
    }

    public async conv(): Promise<string> {

        await this.loader()
        //else this.pdfJS.cleanup()

        const doc = await this.pdfJS.getDocument(this.file).promise
        console.log(doc)
        let text = ''
        for (let i = 1; i <= doc.numPages; i++) {
            const page = await doc.getPage(i)
            const content = await page.getTextContent({ disableCombineTextItems: true, includeMarkedContent: false })
            console.log(content)
            text += content.items.map((item: any) => item.str).join('')
        }
        return text
    }

    private async loader(): Promise<void> {
        this.pdfJS = null
        this.pdfJS = await pdfjsLib
    }
}