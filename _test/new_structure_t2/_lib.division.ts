import {
    init as resultInit, get,
    resultMath
} from './_result.math';

export interface divisionState {
    a: number;
    b: number;
}

export interface divisionObject {
    calc: (di: divisionState) => {}
}

export const num: divisionState = {a: 0, b: 0};

export const init = (a: number, b: number) => {
    num.a = a;
    num.b = b;
}

export const calc = (): resultMath => {
    resultInit(num.a - num.b);
    return {get: get}
}
