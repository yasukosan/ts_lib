import {
    init as additionInit, calc as additionCalc,
    additionState, additionObject
} from './_lib.addition';
import {
    init as divisionInit, calc as divisionCalc,
    divisionState, divisionObject
} from './_lib.division';
import {
    init as multiplicationInit, calc as multiplicationCalc,
    multiplicationState, multiplicationObject
} from './_lib.multiplication';
import {
    init as subtractionInit, calc as subtractionCalc,
    subtractionState, subtractionObject
} from './_lib.subtraction';

export type MathType = {
    set: <T extends additionState, divisionState, multiplicationState, subtractionState>
        (val: T) => {}
}


export const setAddition = (
    val: additionState
): additionObject => {
    additionInit(val.a, val.b)
    return {calc: additionCalc};
}

export const setDivision = (
    val: divisionState
): divisionObject => {
    divisionInit(val.a, val.b)
    return {calc: divisionCalc};
}

export const setMultiplication = <T extends multiplicationObject>(
    val: multiplicationState
): T | void => {
    multiplicationInit(val.a, val.b)
    //return {calc: multiplicationCalc};
    return
}

export const setSubtraction = (
    val: subtractionState
): subtractionObject => {
    subtractionInit(val.a, val.b)
    return {calc: subtractionCalc};
}



const a = setAddition({a: 100, b:100})
//const b = a.additionCalc()
