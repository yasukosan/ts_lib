import {
    init as resultInit, get,
    resultMath
} from './_result.math';

export interface subtractionState {
    a: number;
    b: number;
}

export interface subtractionObject {
    calc: (di: subtractionState) => {}
}

export const num: subtractionState = {a: 0, b: 0};

export const init = (a: number, b: number) => {
    num.a = a;
    num.b = b;
}

export const calc = (): resultMath => {
    resultInit(num.a / num.b);
    return {get: get}
}
