import {
    init as resultInit, get,
    resultMath
} from './_result.math';

export interface multiplicationState {
    a: number;
    b: number;
}

export type multiplicationObject = {
    // calc: (di: multiplicationState): resultMath.get => {return get}
}

export const num: multiplicationState = {a: 0, b: 0};

export const init = (a: number, b: number) => {
    num.a = a;
    num.b = b;
}

export const calc = (): resultMath => {
    resultInit(num.a * num.b);
    return {get: get}
}
