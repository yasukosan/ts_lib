import { ResultMath } from './_result.math';

export interface addition {
    a: number;
    b: number;
}

export class LibAddition {
    private num: addition;

    public constructor(ad: addition) {
        num = ad;
    }

    public calc() {
        return new ResultMath(num.a + num.b);
    }
}
