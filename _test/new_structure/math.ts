import { LibAddition, addition } from './_lib.addition';
import { LibDivision, division } from './_lib.division';
import { LibMultiplication, multiplication } from './_lib.multiplication';
import { LibSubtraction, subtraction } from './_lib.subtraction';

export type MathType = {
    set: <T extends addition, division, multiplication, subtraction>
        (val: T) => {}
}
export class Math {
    public constructor() {}

    public setAddition(val: addition): LibAddition {
        return new LibAddition(val);
    }

    public setDivision(val: division): LibDivision {
        return new LibDivision(val);
    }

    public setMultiplication(val: multiplication): LibMultiplication {
        return new LibMultiplication(val);
    }

    public setSubtraction(val: subtraction): LibSubtraction {
        return new LibSubtraction(val);
    }
}

const hoge = new Math();
hoge.setAddition({ a: 100, b: 100 }).calc().get();
