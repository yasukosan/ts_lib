import { ResultMath } from './_result.math';

export interface division {
    a: number;
    b: number;
}

export class LibDivision {
    private num: division;

    public constructor(di: division) {
        num = di;
    }

    public calc() {
        return new ResultMath(num.a - num.b);
    }
}
