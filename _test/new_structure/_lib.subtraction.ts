import { ResultMath } from './_result.math';

export interface subtraction {
    a: number;
    b: number;
}

export class LibSubtraction {
    private num: subtraction;

    public constructor(su: subtraction) {
        num = su;
    }

    public calc() {
        return new ResultMath(num.a / num.b);
    }
}
