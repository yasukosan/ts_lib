import { ResultMath } from './_result.math';

export interface multiplication {
    a: number;
    b: number;
}

export class LibMultiplication {
    private num: multiplication;

    public constructor(mu: multiplication) {
        num = mu;
    }

    public calc() {
        return new ResultMath(num.a * num.b);
    }
}
