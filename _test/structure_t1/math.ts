
export class Math {
    private num: {a: number, b: number} = {a:0, b:0};
    private result: number = 0;

    public setNum(a:number, b: number) {
        this.num.a = a;
        this.num.b = b;
    }

    public getResult() {
        return this.result;
    }

    public doAddition() {
        this.result = this.num.a + this.num.b
    }

    public doDivision() {
        this.result = this.num.a - this.num.b
    }

    public doMultiplication() {
        this.result =this.num.a * this.num.b
    }

    public doSubtraction() {
        this.result =this.num.a / this.num.b
    }
}

const hoge = new Math();
hoge.setNum(100, 100);
hoge.doAddition();
hoge.getResult();
