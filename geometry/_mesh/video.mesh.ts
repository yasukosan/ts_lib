import * as THREE from 'three';

export class VideoMesh {
    private static instance: VideoMesh;

    private Name = 'webcam';
    private Width = 640;
    private Height = 480;
    private Positions: number[] = [0, 0, 0];
    private Scale: number[] = [0, 0];
    private Color: number[] = [0, 0, 0, 0];
    private Mesh: THREE.Mesh | undefined;
    private Material: THREE.MeshBasicMaterial | undefined;
    private Geometry: THREE.PlaneGeometry | undefined;

    public static call() {
        if (!VideoMesh.instance) {
            VideoMesh.instance = new VideoMesh();
        }
        return VideoMesh.instance;
    }

    /**
     * メッシュの名前を設定(デフォルト: webcam)
     * @param name string
     * @returns VideoMesh
     */
    public setName(name: string): VideoMesh {
        this.Name = name;
        return this;
    }

    /**
     * Meshの幅を設定(デフォルト: 600)
     * @param width number
     * @returns VideoMesh
     */
    public setWidth(width: number): VideoMesh {
        this.Width = width;
        return this;
    }

    /**
     * Meshのタカサを設定(デフォルト: 400)
     * @param height number
     * @returns VideoMesh
     */
    public setHeight(height: number): VideoMesh {
        this.Height = height;
        return this;
    }

    /**
     * 表示座標を設定(デフォルト: x: 0, y: 0, z: 0)
     * @param x number
     * @param y number
     * @param z number
     * @returns VideoMesh
     */
    public setPositions(x: number, y: number, z: number): VideoMesh {
        this.Positions = [x, y, z];
        return this;
    }

    /**
     * Meshの拡大設定(デフォルト: x: 0, y: 0)
     * @param x number
     * @param y number
     * @returns VideoMesh
     */
    public setScale(x: number, y: number): VideoMesh {
        this.Scale = [x, y];
        return this;
    }

    /**
     * 色と透過率(デフォルト: r: 0, g: 0, b: 0, opaciry: 0)
     * @param r number 0 ~ 255
     * @param g number 0 ~ 255
     * @param b number 0 ~ 255
     * @param opacity number 0.0 ~ 1.0
     * @returns
     */
    public setColor(
        r: number,
        g: number,
        b: number,
        opacity: number
    ): VideoMesh {
        const _r = r >= 0 && r <= 255 ? r : 0;
        const _g = g >= 0 && g <= 255 ? g : 0;
        const _b = b >= 0 && b <= 255 ? b : 0;
        const _o = opacity >= 0 && opacity <= 1 ? opacity : 0;
        this.Color = [_r, _g, _b, _o];
        return this;
    }

    public getMesh(): THREE.Mesh {
        return this.Mesh as THREE.Mesh;
    }

    public getMaterial(): THREE.MeshBasicMaterial {
        return this.Material as THREE.MeshBasicMaterial;
    }

    /**
     * ビデオ映像メッシュ作成
     * @param video HTMLVideoElement
     * @returns THREE.Mesh
     */
    public createVideoMesh(video: HTMLVideoElement): THREE.Mesh {
        this.reset();
        this.Geometry = new THREE.PlaneGeometry(this.Width, this.Height); // ジオメトリ
        const videoTexture = new THREE.VideoTexture(video); // 動画テクスチャの作成
        videoTexture.minFilter = THREE.LinearFilter;
        videoTexture.format = THREE.RGBAFormat;
        this.Material = new THREE.MeshBasicMaterial({
            map: videoTexture
        }); // 動画テクスチャのマテリアルの作成
        this.Mesh = new THREE.Mesh(this.Geometry, this.Material); // メッシュ
        this.Mesh.position.z = 0;
        this.Mesh.name = this.Name;
        return this.Mesh;
    }

    /**
     * メッシュをアニメーションさせる
     * 事前に、座標、スケール、色情報を設定の上呼び出す。
     * @param obj THREE.Mesh
     */
    public animation(obj: THREE.Mesh | any): void {
        obj.scale.x = this.Scale[0];
        obj.scale.y = this.Scale[1];

        obj.position.x = this.Positions[0];
        obj.position.y = this.Positions[1];
        obj.position.z = this.Positions[2];

        obj.material.color = new THREE.Color(
            this.Color[0],
            this.Color[1],
            this.Color[2]
        );
        obj.material.opacity = this.Color[3];
    }

    private reset() {
        this.Name = 'webcam';
        this.Width = 600;
        this.Height = 400;
        this.Positions = [0, 0, 0];
        this.Scale = [0, 0];
        this.Color = [0, 0, 0, 0];
        this.Mesh = undefined;
        this.Material = undefined;
        this.Geometry = undefined;
    }
}
