import * as THREE from 'three';
import { GeoHelper } from '../helper/geo.helper';
export class FireMesh {
    private static instance: FireMesh;

    private fireParticles: THREE.Mesh[] | any = []; // パーティクルを格納する配列
    private fireParticlesNum = 10; // パーティクルの数
    private firePos = -20; //パーティクルのy軸初期位置
    private fireSize = 400; //パーティクルサイズ

    private Texture = 'texture/tex.png';

    private Opacity = 0.0;
    private Color: number[] = [0.0, 0.0, 0.0];

    private Position: any = {
        x: 0,
        y: 0
    };

    public static call() {
        if (!FireMesh.instance) {
            FireMesh.instance = new FireMesh();
        }
        return FireMesh.instance;
    }

    /**
     * 表示位置設定
     * @param x number
     * @param y number
     * @returns FireMesh
     */
    public setPosition(x: number, y: number): FireMesh {
        this.Position.x = x;
        this.Position.y = y;
        return this;
    }

    /**
     * パーティクルの数設定
     * @paam num number 0 ~
     * @returns FireMesh
     */
    public setFireNumber(num: number): FireMesh {
        this.fireParticlesNum = num;
        return this;
    }

    /**
     * エフェクトの大きさ設定
     * パーティクルあたりの直径(px単位)
     * @param size number デフォルト:400
     * @returns FireMesh
     */
    public setFireSize(size: number): FireMesh {
        this.fireSize = size;
        return this;
    }

    /**
     * エフェクトの透過率を設定
     * @param opacity number 0.0 ~ 1.0
     * @returns FireMesh
     */
    public setFireOpacity(opacity: number): FireMesh {
        this.Opacity = opacity;
        return this;
    }

    /**
     * エフェクトの色を設定
     * @param color number[] [r: 0.0 ~ 1.0,r: 0.0 ~ 1.0,r: 0.0 ~ 1.0]
     * @returns FireMesh
     */
    public setFireColor(color: number[]): FireMesh {
        this.Color = color;
        return this;
    }

    /**
     * エフェクトの色を設定
     * @param color number[] [r: 0 ~ 255,r: 0 ~ 255,r: 0 ~ 255]
     * @returns FireMesh
     */
    public setFireColorForRGB(color: number[]): FireMesh {
        const conv = (c: number): number => {
            return 250 / c;
        };
        this.Color = [conv(color[0]), conv(color[1]), conv(color[2])];
        return this;
    }

    /**
     * エフェクトメッシュ作成
     * @returns THREE.Mesh
     */
    public createFireMesh(): THREE.Mesh[] {
        /* テクスチャの用意 */
        const loader = new THREE.TextureLoader();
        const texture = loader.load(this.Texture);

        /* 炎パーティクルを作成する */
        const geometry = new THREE.PlaneGeometry(
            this.fireSize,
            this.fireSize,
            1
        );
        for (let i = 0; i < this.fireParticlesNum; i++) {
            const material = new THREE.MeshLambertMaterial({
                map: texture, //読み込んだテクスチャを貼る
                transparent: true //画像の透明度有効にする
            });
            /* パーティクルをランダムな座標に初期配置 */
            const particle = new THREE.Mesh(geometry, material);
            particle.position.y = Math.random() * this.fireSize + this.firePos;
            particle.rotation.z = 10;
            this.fireParticles.push(particle); //配列に格納
        }
        return this.fireParticles;
    }

    /**
     * 所定位置周辺にパーティクルを散乱させる前提のため
     * 炎エフェクト、煙、などを再現する場合は、下段の参照元オリジナルを改変すべし
     */
    public animation() {
        /* パーティクルのアニメーション設定 */
        for (let i = 0; i < this.fireParticlesNum; i++) {
            // 座標に追従しながらランダムに拡散
            this.fireParticles[i].position.x =
                Math.random() * (this.Position.x - 120);
            this.fireParticles[i].position.y =
                Math.random() * (100 - this.Position.y);

            // 色変更
            this.fireParticles[i].material.color = new THREE.Color(
                this.Color[0],
                this.Color[1],
                this.Color[2]
            );
            this.fireParticles[i].material.opacity = this.Opacity;
        }
    }

    /*
    public animation_org()
    {
        for(let i = 0; i < this.fireParticlesNum; i++){
    
            //(1)位置と回転 
            const limit = this.fireSize*0.6; //炎が上昇する距離
            if ( this.fireParticles[i].position.y < this.firePos+limit ) {
                this.fireParticles[i].position.y += Math.random()*(this.fireSize/20);
                this.fireParticles[i].position.y += Math.random() * (this.g_position.y / 20);
                this.fireParticles[i].rotation.z += 0.01;
            } else {
                this.fireParticles[i].position.y = this.firePos; //上昇限界位置まで行ったら初期位置に戻る
            }
        
            // (2)y座標を1～0に変換 
            let y = ((this.firePos+limit) - this.fireParticles[i].position.y)/limit;
        
            // (3)大きさに変化をつける
            this.fireParticles[i].scale.x = y; //上に行くほど横幅小さく
            this.fireParticles[i].scale.y = y; //上に行くほど横幅小さく
        
            // (4)うねうね 
            let amp = (this.fireSize/15)*Math.random(); //うねうね大きさ
            let freq = 2*Math.random()+5; //うねうね量
            this.fireParticles[i].position.x = amp * Math.sin(freq*y*Math.PI);
        
            // (5)色を付ける 
            this.fireParticles[i].material.opacity = Math.pow(y, 4); //上に行くほど透明に
            let r = Math.sin(Math.PI/4*y+Math.PI/2);
            let b = Math.pow(y, 20);
            this.fireParticles[i].material.color = new THREE.Color(r, y, b);
        }
    
    } */
}
