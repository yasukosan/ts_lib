import * as THREE from 'three';

export class TextureBoardMesh {
    private static instance: TextureBoardMesh;

    private Name = 'textureboard';
    private Width = 600;
    private Height = 400;
    private Scale: number[] = [0, 0];
    private Opacity = 1;

    private Texture = '';
    private Positions: any = {
        x: 0,
        y: 0,
        width: 0,
        height: 0,
        base_width: 0,
        base_height: 0
    };

    private Mesh: THREE.Mesh | any;

    public static call() {
        if (!TextureBoardMesh.instance) {
            TextureBoardMesh.instance = new TextureBoardMesh();
        }
        return TextureBoardMesh.instance;
    }

    /**
     * メッシュの名前を設定(デフォルト: textureboard)
     * @param name string
     * @returns TextureBoardMesh
     */
    public setName(name: string): TextureBoardMesh {
        this.Name = name;
        return this;
    }

    /**
     * Meshの幅を設定(デフォルト: 600)
     * @param width number
     * @returns TextureBoardMesh
     */
    public setWidth(width: number): TextureBoardMesh {
        this.Width = width;
        return this;
    }

    /**
     * Meshのタカサを設定(デフォルト: 400)
     * @param height number
     * @returns TextureBoardMesh
     */
    public setHeight(height: number): TextureBoardMesh {
        this.Height = height;
        return this;
    }

    /**
     * テクスチャを設定
     * @param texture string base64 画像文字列
     * @returns TextureBoardMesh
     */
    public setTexture(texture: string): TextureBoardMesh {
        this.Texture = texture;
        return this;
    }

    /**
     * 表示座標を設定
     * @param positions {
     *     x:number, y:number, width: number, height: number,
     *     base_width: number, base_height
     * }
     * @returns TextureBoardMesh
     */
    public setPositions(positions: any): TextureBoardMesh {
        this.Positions = positions;
        return this;
    }

    /**
     * Meshの拡大設定(デフォルト: x: 0, y: 0)
     * @param x number
     * @param y number
     * @returns TextureBoardMesh
     */
    public setScale(x: number, y: number): TextureBoardMesh {
        this.Scale = [x, y];
        return this;
    }

    /**
     * メッシュの透過率を設定
     * @param opaciry number 0.0 ~ 1.0
     * @returns TextureBoardMesh
     */
    public setOpacity(opaciry: number): TextureBoardMesh {
        this.Opacity = opaciry;
        return this;
    }

    /**
     * ビデオ映像メッシュ作成
     * @param video MediaStream
     * @returns THREE.Mesh
     */
    public createTextureBoardMesh(): THREE.Mesh {
        const loader = new THREE.TextureLoader();
        const map = loader.load('./texture/aura3_type2.png');
        const geometry = new THREE.PlaneGeometry(this.Width, this.Height);
        const material = new THREE.MeshBasicMaterial({
            map: map,
            side: THREE.DoubleSide,
            transparent: true
        });
        this.Mesh = new THREE.Mesh(geometry, material);
        this.Mesh.name = this.Name;
        return this.Mesh;
    }

    /**
     * メッシュをアニメーションさせる
     * 事前に、座標、スケール、色情報を設定の上呼び出す。
     * @param obj THREE.Mesh
     */
    public animation(): void {
        // 出力サイズと画像データの縦横比の差を求める
        const r_x = this.Width / this.Positions.base_width;
        const r_y = this.Height / this.Positions.base_height;
        // 差に合わせてスケール変更
        this.Mesh.scale.set(
            (this.Positions.width * r_x) / this.Width,
            (this.Positions.height * r_y) / this.Height,
            25
        );

        // Canvasの座標系とWebGLの座標系の変換
        // 出力差に合わせて座標を調整
        this.Mesh.position.x =
            this.Positions.x * r_x -
            (this.Width / 2 - (this.Positions.width * r_x) / 2);
        this.Mesh.position.y =
            this.Height / 2 -
            (this.Positions.height * r_y) / 2 -
            this.Positions.y * r_y;
        this.Mesh.position.z = 25;

        if (this.Opacity > 0) {
            // 画像文字列の状態からWebGLテクスチャを生成し貼り付け
            const image = new Image();
            image.onload = () => {
                const texture = new THREE.Texture(image);
                texture.needsUpdate = true;
                this.Mesh.material.map = texture;
                this.Mesh.material.opacity = this.Opacity;
            };
            image.src = this.Texture;
        }
    }
}
