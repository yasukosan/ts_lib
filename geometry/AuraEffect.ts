import * as THREE from 'three';
import { GeoHelper } from './helper/geo.helper';

import { VideoMesh } from './_mesh/video.mesh';
import { FireMesh } from './_mesh/fire.mesh';
import { TextureBoardMesh } from './_mesh/textureboard.mesh';

export class AuraEffect {
    private static instance: AuraEffect;

    private animtionId = 0;

    public static call() {
        if (!AuraEffect.instance) {
            AuraEffect.instance = new AuraEffect();
        }
        return AuraEffect.instance;
    }

    public async setup(video: any = '') {
        await GeoHelper.call()
            .setCanvas()
            .buildRenderer() // レンダラーの作成
            .setRendereSize() // レンダラーのサイズを設定
            .buildScene() // シーンを作成
            .setCamera() // カメラを作成
            .setLight(); // 平行光源を生成
        if (video !== '') {
            GeoHelper.call().addScene(VideoMesh.call().createVideoMesh(video));
            GeoHelper.call().addScene(
                TextureBoardMesh.call().createTextureBoardMesh()
            );
            this.addScene(FireMesh.call().createFireMesh());
        }
        return this;
    }

    /**
     * 顔画像設定
     * @param ganmen string base64イメージ文字列
     * @returns AuraEffect
     */
    public setGanmen(ganmen: string): AuraEffect {
        TextureBoardMesh.call().setTexture(ganmen);
        return this;
    }

    /**
     * 表示位置を設定
     *
     * ジオメトリ内のx,y座標位置に
     * 指定の高さ幅にスケールした
     * テクスチャを貼り付ける
     * @param positions {
     *     x:number,            // x 座標
     *     y:number,            // y 座標
     *     width: number,       // テクスチャの幅
     *     height: number,      // テクスチャの高さ
     *     base_width: number,  // ジオメトリの幅
     *     base_height: number  // ジオメトリの高さ
     * }
     * @returns AuraEffect
     */
    public setGposition(positions: object | any): AuraEffect {
        FireMesh.call().setPosition(positions['x'], positions['y']);
        TextureBoardMesh.call().setPositions(positions);
        return this;
    }

    /**
     * 透過率を設定
     * @param opacity number 0.0 ~ 1.0
     * @returns AuraEffect
     */
    public setFireOpacity(opacity: number): AuraEffect {
        FireMesh.call().setFireOpacity(opacity);
        TextureBoardMesh.call().setOpacity(opacity);
        return this;
    }

    /**
     * エフェクトの色設定
     * @param color number[]
     * @returns
     */
    public setFireColor(color: number[]): AuraEffect {
        FireMesh.call().setFireColor(color);
        return this;
    }

    /**
     * エフェクトサイズ設定
     * @param size number
     * @returns AuraEffect
     */
    public setFireSize(size: number): AuraEffect {
        FireMesh.call().setFireSize(size);
        return this;
    }
    /**
     * アニメーション開始
     */
    public start(): void {
        this.animtionId = requestAnimationFrame(() =>
            AuraEffect.call().start()
        );

        FireMesh.call().animation();
        TextureBoardMesh.call().animation();

        GeoHelper.call().render();
    }

    /**
     * アニメーション停止
     * @returns
     */
    public stop(): AuraEffect {
        cancelAnimationFrame(this.animtionId);
        return this;
    }

    private addScene(mesh: any): void {
        if (Array.isArray(mesh)) {
            for (let i = 0; i < mesh.length; i++) {
                GeoHelper.call().addScene(mesh[i]);
            }
            return;
        }
        GeoHelper.call().addScene(mesh);
        return;
    }
}
