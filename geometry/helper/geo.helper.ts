import * as THREE from 'three';

export class GeoHelper {
    private static instance: GeoHelper;

    private RenderWidth = 600;
    private RenderHeight = 400;
    private renderer: THREE.WebGLRenderer | undefined;
    private scene: THREE.Scene | undefined;
    private camera: THREE.PerspectiveCamera | undefined;

    private animationId = 0;

    private renderTarget: HTMLCanvasElement | undefined;

    public static call(reset = false): GeoHelper {
        if (!GeoHelper.instance) {
            GeoHelper.instance = new GeoHelper();
        }
        return GeoHelper.instance;
    }

    /**
     * 描画用のCanvasターゲットを設定
     * @param target string canvasタグのID
     * @returns ThreeTest
     */
    public setCanvas(target = 'threetest'): GeoHelper {
        this.renderTarget = document.getElementById(
            target
        ) as HTMLCanvasElement;
        return this;
    }

    /**
     * 描画領域のサイズを設定
     * @param width: number (default: 600)
     * @param height: number (default: 400)
     * @returns
     */
    public setRendereSize(width = 600, height = 400): GeoHelper {
        if (this.renderer === undefined) return this;

        this.renderer.setSize(width, height);

        this.RenderHeight = height;
        this.RenderWidth = width;
        document.body.appendChild(this.renderer.domElement);
        return this;
    }

    /**
     * カメラを設置(3D空間用)
     * @returns
     */
    public setCamera(): GeoHelper {
        // カメラを作成
        // this.camera = new THREE.PerspectiveCamera(75, 400 / 400, 0.1, 10000);
        this.camera = new THREE.PerspectiveCamera(
            45,
            this.RenderWidth / this.RenderHeight,
            1,
            1000
        );
        // this.camera.lookAt({x: 0, y: 0, z: 0});
        // this.camera.position.set(0, 0, 1000);
        this.camera.position.set(0, 0, 500);
        return this;
    }

    public setLight(): GeoHelper {
        // 平行光源を生成
        //const light = new THREE.DirectionalLight(0xffffff);
        //light.position.set(100, 130, 80);

        // 環境光源の作成
        //const light = new THREE.AmbientLight(0xFFFFFF, 1.0);
        if (this.scene === undefined) return this;
        const light = new THREE.AmbientLight(0xffffff, 1.0);

        this.scene.add(light);
        return this;
    }

    public getMesh(mesh: string): THREE.Object3D | undefined {
        if (this.scene === undefined) return undefined;
        const _mesh = this.scene.getObjectByName(mesh) as THREE.Object3D;
        return _mesh;
    }

    public buildRenderer(): GeoHelper {
        this.renderer = new THREE.WebGLRenderer({
            canvas: this.renderTarget,
            alpha: true,
            antialias: true
        });

        this.renderer.setClearColor(0x000000, 0);

        return this;
    }

    public buildScene(): GeoHelper {
        // シーンを作成
        this.scene = new THREE.Scene();
        return this;
    }

    /**
     *
     * @returns
     */
    public setup(): GeoHelper {
        this.buildRenderer() // レンダラーの作成
            .setRendereSize() // レンダラーのサイズを設定
            .buildScene() // シーンを作成
            .setCamera() // カメラを作成
            .setLight() // 平行光源を生成
            .startAnimation(); // アニメーションを開始

        return this;
    }

    /**
     * Meshをシーンに追加
     * @param mesh THREE.Mesh
     * @returns GeoHelper
     */
    public addScene(mesh: THREE.Mesh): GeoHelper {
        if (this.scene === undefined) return this;
        this.scene.add(mesh);
        return this;
    }

    /**
     * Meshを削除する
     * ※ geometry、materialは削除されないので
     * 　 disposeプロパティを保持するMeshの場合は
     * 　 別途disposeを実行する
     * @param mesh THREE.Mesh
     * @returns GeoHelper
     */
    public removeScene(mesh: THREE.Mesh): GeoHelper {
        if (this.scene === undefined) return this;
        this.scene.remove(mesh);
        return this;
    }

    /**
     * レンダラーを返す
     * @returns THREE.WebGLRenderer
     */
    public callRenderer(): any {
        return this.renderer;
    }

    /**
     * レンダリング開始
     * @returns
     */
    public startAnimation(): GeoHelper {
        const tick = (): void => {
            this.animationId = requestAnimationFrame(tick);

            // 描画
            // console.log(this.scene);
            if (this.renderer === undefined) {
            } else {
                this.renderer.render(
                    this.scene as THREE.Scene,
                    this.camera as THREE.PerspectiveCamera
                );
            }
        };
        tick();
        return this;
    }

    /**
     * 単ページレンダリング 絶対使わん
     */
    public render() {
        if (this.renderer === undefined) return;
        this.renderer.render(
            this.scene as THREE.Scene,
            this.camera as THREE.PerspectiveCamera
        );
    }

    public stopAnimation(): GeoHelper {
        cancelAnimationFrame(this.animationId);
        return this;
    }

    public allReset(): GeoHelper {
        this.renderer = undefined;
        this.scene = undefined;
        this.camera = undefined;
        this.animationId = 0;
        this.renderTarget = undefined;

        return this;
    }

    /*
    private async loadImage(img: string): Promise<any>
    {

        const im: HTMLImageElement = await new Promise((resolve, reject) => {
            const _img: HTMLImageElement = new Image();
            _img.onload = (e: any) => resolve(_img);
            _img.onerror = (e: any) => reject(e);
            _img.src = '/gid02.png';
        });
        const canvas: HTMLCanvasElement = document.createElement('canvas');
        canvas.setAttribute('width', (im.width).toString());
        canvas.setAttribute('height', (im.height).toString());

        const ctx: any = canvas.getContext('2d');
        ctx.drawImage(im, 0, 0);
        return canvas.toDataURL('image/jpg');
    } */
}
