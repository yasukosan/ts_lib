import * as THREE from 'three';

export interface TextDataInterface {
    txt: string;
    size: number;
    font: string;
    x: number;
    y: number;
}

export class TextService {
    private static instance: TextService;

    private geo: any;
    private textData: any;

    private Resolution = 4.86;

    public static call(): TextService {
        if (!TextService.instance) {
            TextService.instance = new TextService();
        }
        return TextService.instance;
    }

    /**
     * 印刷時のDPI比率を指定
     * Web表示用DPI 72 と印刷用DPIを割った倍率をセットする
     * デフォルト4.86倍(72dpiと350dpiの比率)
     * @param resolution
     * @returns
     */
    public setPaperResolution(resolution = 4.86): TextService {
        this.Resolution = resolution;
        return this;
    }

    /**
     * テキストデータを登録
     * @param texts {
     *      txt: string テキスト文字列, size: number フォントサイズ, font: string フォント名
     *      x: number X座標, y: number Y座標
     * }
     * @returns
     */
    public setTextData(texts: TextDataInterface[]): TextService {
        this.textData = texts;
        return this;
    }

    /**
     * Geometryデータ作成
     * @returns
     */
    public async build(): Promise<TextService> {
        const txt = await new THREE.CanvasTexture(this.buildTextCanvas());
        txt.needsUpdate = false;
        txt.minFilter = THREE.LinearFilter;
        txt.magFilter = THREE.LinearFilter;
        txt.format = THREE.RGBAFormat;

        this.geo = new THREE.Mesh(
            new THREE.PlaneGeometry(400, 800),
            new THREE.MeshLambertMaterial({
                map: txt
            })
        );
        console.log(this.geo);
        return this;
    }

    /**
     * Geomertyデータを取得
     * @returns
     */
    public getGeometry(): any {
        return this.geo;
    }

    /**
     * テキスト画像を作成
     * @returns HTMLCanvasElement
     */
    private buildTextCanvas(): HTMLCanvasElement {
        const co = document.createElement('canvas');
        const ctx = co.getContext('2d');
        if (!ctx) {
            return co;
        }

        for (const key in this.textData) {
            if (Object.prototype.hasOwnProperty.call(this.textData, key)) {
                console.log(this.textData[key]);
                ctx.font =
                    'bold ' +
                    this.textData[key]['size'] +
                    'px ' +
                    this.textData[key]['font'];
                ctx.textAlign = 'left';
                ctx.textBaseline = 'hanging';
                ctx.fillStyle = 'rgba(0, 0, 0, 1.0)';
                ctx.fillText(
                    this.textData[key]['txt'],
                    this.textData[key]['x'] * (this.Resolution / 3),
                    this.textData[key]['y'] * (this.Resolution / 3)
                );
            }
        }

        return co;
    }
}
