import * as THREE from 'three';
import { MathService } from '../Math/math.service';

import { GeoHelper } from './helper/geo.helper';

import { VideoMesh } from './_mesh/video.mesh';

export class MovieEffect {
    private static instance: MovieEffect;

    private animtionId = 0;
    private VideoMeshs: { [key: string]: THREE.Mesh } | undefined;

    public static call() {
        if (!MovieEffect.instance) {
            MovieEffect.instance = new MovieEffect();
        }
        return MovieEffect.instance;
    }

    public async setup(width: number, height: number): Promise<MovieEffect> {
        await GeoHelper.call()
            .allReset()
            .setCanvas('testcanvas2')
            .buildRenderer() // レンダラーの作成
            .setRendereSize(width, height) // レンダラーのサイズを設定
            .buildScene() // シーンを作成
            .setCamera() // カメラを作成
            .setLight(); // 平行光源を生成

        return this;
    }

    public addVideoMesh(name: string, video: HTMLVideoElement): MovieEffect {
        VideoMesh.call().createVideoMesh(video);

        this.VideoMeshs = {
            ...this.VideoMeshs,
            ...{
                [name]: VideoMesh.call().getMesh()
            }
        };

        GeoHelper.call().addScene(this.VideoMeshs[name] as THREE.Mesh);
        return this;
    }

    public addEffectMesh(
        name: string,
        video: HTMLVideoElement,
        p1: number[],
        p2: number[],
        scale = 1
    ): MovieEffect {
        this.addVideoMesh(name, video);
        if (this.VideoMeshs !== undefined && name in this.VideoMeshs) {
            this.VideoMeshs[name].scale.x = scale;
            this.VideoMeshs[name].scale.y = scale;
            this.VideoMeshs[name].position.x = p1[0];
            this.VideoMeshs[name].position.y = p1[1];
            this.VideoMeshs[name].position.z = p1[2];
            this.VideoMeshs[name].rotation.z = MathService.calc2DAngle(p1, p2);
        }

        return this;
    }

    /**
     * Meshを削除
     * @param name string
     * @returns MovieEffect
     */
    public removeVideoMesh(name: string): MovieEffect {
        if (this.VideoMeshs !== undefined && name in this.VideoMeshs) {
            const mesh = this.VideoMeshs[name] as THREE.Mesh;
            GeoHelper.call().removeScene(mesh);
            // mesh.material.dispose();
            mesh.geometry.dispose();
        }
        return this;
    }

    /**
     * アニメーション開始
     */
    public start(): void {
        this.animtionId = requestAnimationFrame(() =>
            MovieEffect.call().start()
        );

        // TextureBoardMesh.call().animation();

        GeoHelper.call().render();
    }

    /**
     * アニメーション停止
     * @returns
     */
    public stop(): MovieEffect {
        cancelAnimationFrame(this.animtionId);
        return this;
    }

    public reset(): MovieEffect {
        this.animtionId = 0;
        this.VideoMeshs = undefined;
        return this;
    }
}
