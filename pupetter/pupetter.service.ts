import puppeteer, { Browser, Page } from 'puppeteer'

let browser: Browser | undefined
let page: Page | undefined
let url: string

const ScreenSizes = {
    mobile: {
        width: 320,
        height: 480
    },
    desktop: {
        width: 1440,
        height: 900
    },
    fullHD: {
        width: 1920,
        height: 1080
    },
    fourK: {
        width: 3840,
        height: 2160
    }
}

export const init = async (url: string): Promise<void> => {
    if (browser === undefined) {
        browser = await puppeteer.launch()
        page = await browser.newPage()
    }
    await page?.goto(url)
}

/**
 * 指定URLのスクリーンショットを取得
 * @param url string
 * @param path string
 * @returns Promise<string | false>
 */
export const takeScreenShot = async (
    url: string,
    path: string
): Promise<string | false> => {
    
    await init(url)

    if (!browser) {
        console.error('Browser is not initialized')
        return false
    }
    try {
        await page?.setViewport({
            ...ScreenSizes.desktop,
            deviceScaleFactor: 1
        })
        await page?.goto(url) 
        const ss = await page?.screenshot({
                    path: 'screenshot.png',
                    encoding: 'base64'
                }) as string
        close()
        return ss
    } catch (error) {
        console.error('Pupetter ERROR : ', error)
        close()
        return false
    }

}

export const close = async (): Promise<void> => {
    if (!browser) {
        throw new Error('Browser is not initialized')
    }
    await browser.close()
}
