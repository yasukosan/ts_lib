import {
    IS,
    leftBlowout,
    rightBlowout,
    loadImage,
    nullImage,
    titleImage,
    openingImage,
    mainImage,
    splitTextMark,
    setPictures
} from './_helper/flip_generate.helper'

let FPS = 2

export type BlowOutType = {
    text: string,
    flip: string[],
    speaker: string
}

export const setFPS = (fps: number) => FPS = fps

export const getNullFlips = async (
    time: number
): Promise<BlowOutType[]> => {

    await loadImage()
    await nullImage()

    const _flips: string[] = []
    for (let i = 0; i < time * FPS; i++) {
        _flips.push(IS.null_image_url)
    }
    return [{
        text: '',
        flip: _flips,
        speaker: ''
    }]
}

export const getTitleFlips = async (
    title: string,
    time: number
): Promise<BlowOutType[]> => {

    await loadImage()
    await nullImage()
    await titleImage(title)

    const _flips: string[] = []
    for (let i = 0; i < time * FPS; i++) {
        _flips.push(IS.title_image_url)
    }
    return [{
        text: '',
        flip: _flips,
        speaker: ''
    }]
}

export const getOpeningFlips = async (
    talks: any,
    titles: string[],
    timeSchedule: string[] = ['0秒', '0秒', '0秒', '0秒', '0秒','0秒', '0秒', '0秒', '0秒', '0秒']
//): Promise<BlowOutType> => {
): Promise<BlowOutType[]> => {
    const _flips: BlowOutType[] = []

    await loadImage()
    await nullImage()
    await openingImage(titles, timeSchedule)

    for (const talk of talks) {
        const frames = Math.ceil(talk.time) * FPS
        const _frames: string[] = []
        for (let i = 0; i < frames; i++) {
            _frames.push(IS.op_image_url)
        }

        _flips.push({
            text: '',
            flip: _frames,
            speaker: ''
        })
    }

    return _flips

}

/**
 * テキストを20文字分割して、吹き出しを生成する
 * @param param0 
 */
export const generateBlowout = async (
    speechs: {
        message: string,
        talker: string
    }[],
    screenShot: string,
    url: string,
    title: string,
    talks: any
): Promise<BlowOutType[]> => {
    const flips: BlowOutType[] = []

    if (screenShot !== 'error') setPictures({ not_found_image: screenShot })

    await loadImage()
    await mainImage(title, url)

    for (const i in speechs) {

        console.log('Speech Set :: ', speechs[i])

        const frames = Math.ceil(talks[i].time) * FPS
        const _flips: string[] = await generateFlips(speechs[i])
        const _frames: string[] = await generateFrame(_flips, frames)

        flips.push({
            // text: speechs[i].message.join(''),
            text: speechs[i].message,
            flip: _frames,
            speaker: speechs[i].talker
        })
    }
    return flips
}

export const textToImageTag = (text: string): Promise<HTMLImageElement> => {
    return new Promise((resolve, reject) => {
        const img = new Image()
        img.onload = () => resolve(img)
        img.onerror = (e) => reject(e)
        img.src = text
    })
}


const generateFlips = async (
    speech: {
        message: string,
        talker: string
    }
): Promise<string[]> => {
    const flips: string[] = []
    for (const _text of splitTextMark(speech.message)) {
        const r =  (speech.talker === 'ずんだもん')
                ? await leftBlowout(_text)
                : await rightBlowout(_text)
        if (r !== false) flips.push(r)
    }
    return flips
}

const generateFrame = async (
    flips: string[],
    frame: number
): Promise<string[]> => {

    const frames: string[] = []
    const flip_rate = (frame - 1) / flips.length

    console.log('Flip Rate :: ', flip_rate, frame, flips.length)

    let flip_index = 1

    for (let i = 0; i < frame; i++) {
        if (i === 0) frames.push(IS.base_image_url)

        if (i > flip_rate * flip_index) flip_index++

        frames.push(flips[flip_index - 1])

        if (i === frame - 1) frames.push(IS.base_image_url)
    }
    return frames
}
