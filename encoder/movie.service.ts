import { FFmpegHelper } from './_helper/ffmpeg_movie_generate.helper'
import { FFmpegHelper as AudioHelper } from './_helper/ffmpeg.helper'

export class MovieService {
    private static instance: MovieService

    public static call() {
        if (!MovieService.instance) {
            MovieService.instance = new MovieService()
        }
        return MovieService.instance;
    }

    /**
     * 結果を取得
     * @returns {data: File, name: string}
     */

    public getBase64Result(): {data: string, name: string} {
        return FFmpegHelper.call().getBase64Result();
    }


    /**
     * 結果をダウンロード
     */
    public downloadMovie(
        extension   : string = 'mp3',
        filename    : string = 'example'
    ): void {
        FFmpegHelper.call().fileDownload(
            FFmpegHelper.call().getBase64Result().data,
            extension,
            filename
        )
    }

    public async generateMovie(
        images: string[],
        audio: Uint8Array | string | null = null,
        bgm: Uint8Array | string | null = null,
        time: number = 1,
        name: string = 'example',
        log: boolean = false
    ): Promise<{
        movie:Uint8Array,
        name: string
    }> {
        return await FFmpegHelper.call()
            .factory(
                images,
                audio,
                bgm,
                time,
                name,
                log
            )
    }

    /**
     * 動画ファイルを結合
     * @param files 
     * @returns 
     */
    public async concatMovie(
        files: any,
        bgm: Uint8Array | string | null = null,
        time: number = 1,
        log: boolean = false
    ): Promise<Uint8Array> {
        return await FFmpegHelper.call()
                .factoryConcat(files, bgm, time, log)
    }

    public async addFadeFilter(
        file: Uint8Array, 
        options: {
            fadeIn?: number,
            fadeOut?: number
        },
        log: boolean = false
    ): Promise<Uint8Array> {
        return await FFmpegHelper.call()
                .filterFadeInFadeOut(file, options, log)
    }

    public async trimAudio(
        file: Uint8Array | string,
        time: number,
        start: number = 0,
        end: number = 0
    ): Promise<Uint8Array> {
        return await AudioHelper.call()
                .factoryCutAudio(file, time, start, end)
    }

    /**
     * オーディオファイルをダウンロード保存
     * @param data Uint8Array | string
     * @param extension string
     * @param name string
     * @returns void
     */
    public saveAudio(
        data: Uint8Array | string,
        extension: string = '',
        name    : string = ''
    ): void {
        new Promise(resolve => {
            FFmpegHelper.call()
            .fileDownload(
                data,
                extension,
                name);
        })
    }
}