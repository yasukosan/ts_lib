import {
    fileToBase64, base64ToBlob
} from '../_helper/convert.helper'
import { FFmpegHelper } from './_helper/ffmpeg.helper';

import {
    blobToUint8Array
} from '../_helper/convert.helper'

export class EncoderService {
    private static instance: EncoderService

    public static call() {
        if (!EncoderService.instance) {
            EncoderService.instance = new EncoderService()
        }
        return EncoderService.instance;
    }

    /**
     * 初期化処理
     * @param file string | Uint8Array | ArrayBuffer
     * @param name string
     * @returns EncoderService
     * @example
     * EncoderService.call().setup(file, name)
     * .wavToMp3()
     * .getResult()
     */
    public setup(
        file: string | Uint8Array | ArrayBuffer,
        name: string
    ): EncoderService {
        FFmpegHelper.call().setFile(file, name);
        return this;
    }

    /**
     * 結果を取得
     * @returns {data: File, name: string}
     */
    public getResult(): {data: File, name: string} {
        return FFmpegHelper.call().getResult();
    }

    /**
     * 結果をBase64で取得
     */
    public getBase64Result(): {data: string, name: string} {
        return FFmpegHelper.call().getBase64Result();
    }

    /**
     * 無音のオーディオを取得
     */
    public async getNullAudio(
        time: number,
        log: boolean = false
    ): Promise<Uint8Array> {
        return await FFmpegHelper.call().factoryNullAudio(time, [], log)
    }

    /**
     * 結果をダウンロードイベントにセット
     */
    public downloadResult(
        extension   : string = 'mp3',
        filename    : string = 'example'
    ): void {
        FFmpegHelper.call().fileDownload(
            FFmpegHelper.call().getBase64Result().data,
            extension,
            filename
        )
    }

    /**
     * ビデオ・オーディオファイルをmp3に変換
     * @param extension string
     * @returns Promise<void>
     */
    public async toMp3(
        extension: string = 'wav',
        log: boolean = false
    ): Promise<void> {
        const options = [
            '-vn', '-ac', '2', '-ar','44100', '-ab', '256k',
            '-acodec', 'libmp3lame', '-f', 'mp3'
        ]
        await FFmpegHelper.call().factory(extension, 'mp3', options, log)
        return
    }

    /**
     * WavファイルをMp3に変換し結果をResultに保存
     */
    public async wavToMp3(
        log: boolean = false
    ): Promise<void> {
        const options = [
            '-vn', '-ac', '2', '-ar','44100', '-ab', '256k',
            '-acodec', 'libmp3lame', '-f', 'mp3'
        ]
        await FFmpegHelper.call().factory('wav', 'mp3', options, log)
        return
    }

    /**
     * Mp3ファイルをffmpegで分割し結果をResultに保存
     * @param {start} number
     * @param {end} number
     * @returns {Promise<void>}
     */
    public async splitMp3(
        start: number,
        end: number,
        log: boolean = false
    ): Promise<void> {
        const options = [
            '-ss', start.toString(), '-t', end.toString(),
        ]
        await FFmpegHelper.call().factory('mp3', 'mp3', options, log)
        return
    }

    /**
     * Waveファイルをffmpegで分割し結果をResultに保存
     * @param {start} number
     * @param {end} number
     * @returns {Promise<void>}
     */
    public async splitWav(
        start: number,
        end: number,
        log: boolean = false
    ): Promise<void> {
        const options = [
            '-ss', start.toString(), '-t', end.toString(),
        ]
        await FFmpegHelper.call().factory('wav', 'wav', options, log)
        return
    }

    /**
     * Mp3ファイルをffmpegで分割し結果をResultに保存
     * @param {extension} string
     * @returns {Promise<void>}
     */
    public async separateMp3(
        extension: string,
        log: boolean = false
    ): Promise<void> {
        const options = [
            '-vn', '-qscale:a', '0'
        ]
        await FFmpegHelper.call().factory(extension, 'mp3', options, log)
        return
    }

    /**
     * Wavファイルを結合
     * @param files 
     * @returns 
     */
    public async concatWav(
        files: Blob[] | Uint8Array[],
        log: boolean = false
    ): Promise<Uint8Array> {
        const options = [
            '-filter_complex', 'join=inputs=' + (files.length)
        ]

        // Uint8Arrayに揃える
        const _files: Uint8Array[] = []
        for (let i = 0; i < files.length; i++) {
            if (files[i] instanceof Uint8Array) {
                _files.push(files[i] as Uint8Array)
            } else {
                _files.push(await blobToUint8Array(files[i] as Blob))
            }
        }
        return await FFmpegHelper.call()
                    .factoryConcat(_files, 'output.wav', log)
    }

    /**
     * FileをBase64に変換
     * @param {file} File
     * @returns Promise<string>
     */
    public async fileToBase64(file: File): Promise<string> {
        return await fileToBase64(file)
    }

    public Base64ToBlob(base64: string, type: string = 'video.webm'): Blob {
        return base64ToBlob(base64, type)
    }

    /**
     * オーディオファイルをダウンロード保存
     * @param data Uint8Array | string
     * @param extension string
     * @param name string
     * @returns void
     */
    public saveAudio(
        data: Uint8Array | string,
        extension: string = '',
        name    : string = ''
    ): void {
        new Promise(resolve => {
            FFmpegHelper.call()
            .fileDownload(
                data,
                extension,
                name);
        })
    }
}