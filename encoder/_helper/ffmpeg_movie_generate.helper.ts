import { FFmpeg } from "@ffmpeg/ffmpeg"
import { toBlobURL, fetchFile } from '@ffmpeg/util'
import {
    base64ToUint8Array
} from '../../_helper/convert.helper'

export class FFmpegHelper {
    private static instance: FFmpegHelper

    private FFmpeg: FFmpeg | undefined;

    private WriteFiles: string[] = []

    private FileName: string = '';
    private Extension: string = '';
    private Result  : any;
    private Base64String  : string = '';
    private logTarget: string = 'ffmpeg-log'
    
    public static call() {
        if (!FFmpegHelper.instance) {
            FFmpegHelper.instance = new FFmpegHelper()
        }
        return FFmpegHelper.instance;
    }

    public constructor() {
        // this.FFmpeg = createFFmpeg({log: true})
        this.FFmpeg = new FFmpeg()
    }

    /**
     * Base64Stringに変換した結果を取得
     * @returns {data: string, name: string}
     */
    public getBase64Result(): {data: string, name: string} {
        return {
            data    : this.Base64String,
            name    : this.FileName + '.' + this.Extension,
        }
    }

    public async fileDownload(
        data: Uint8Array | string,
        extension: string = '',
        name    : string = '',
    ): Promise<void> {
        const buff = (typeof data === 'string')
                    ? base64ToUint8Array(data)
                    : data
        const ext = (extension === '') ? this.Extension : extension;
        const na = (name === '') ? this.FileName : name;
        const myURL = window.URL || window.webkitURL
        // extensionに'wav' || 'wave' || 'mp3' || 'ogg' || 'acc' || 'flac' || 'm4a'が含まれているか
        const type = (ext.match(/wav|wave|mp3|ogg|acc|flac|m4a/)) ? 'audio' : 'video'

        const blobURL = myURL.createObjectURL(
                            new Blob([buff], { type: type + ext }))
        
        const downloadLink = document.createElement('a')
        downloadLink.href = blobURL
        downloadLink.download = na + '.' + ext
        downloadLink.click();
    }

    /**
     * 画像から、動画を生成
     * @param images string[]
     * @param audios Uint8Array
     * @param time number
     * @param name string
     * @param log boolean default false
     * @returns Promise<{
     *     movie:Uint8Array,
     *     name: string
     * }>
     */
    public async factory(
        images: string[],
        audio: Uint8Array | string | null,
        bgm: Uint8Array | string | null,
        time: number,
        name: string,
        log: boolean = false
    ): Promise<{
        movie:Uint8Array,
        name: string
    }> {

        // await this.clearFiles()

        const audio_filter: string[] = []
        
        return new Promise(async resolve => {

            if (this.FFmpeg?.loaded === false) {
                await this.load()
                if (log) this.logs()
            }

            images.forEach(async (image, i) => {
                await this.FFmpeg?.writeFile(
                    'input' + i + '.png', base64ToUint8Array(image)
                )
                this.WriteFiles.push('input' + i + '.png')
            })

            if (audio !== null) {
                const _audio = (typeof audio === 'string')
                                ? await fetchFile(audio)
                                : audio
                console.log('audio', _audio)
                await this.FFmpeg?.writeFile(
                    'input_audio.wav', _audio
                )
                this.WriteFiles.push('input_audio.wav')
                audio_filter.push('-i')
                audio_filter.push('input_audio.wav')
            }

            if (bgm !== null) {
                const _bgm = (typeof bgm === 'string')
                                ? await fetchFile(bgm)
                                : bgm
                console.log('bgm', _bgm)
                await this.FFmpeg?.writeFile(
                    'input_bgm.wav', _bgm
                )
                this.WriteFiles.push('input_bgm.wav')
                audio_filter.push('-i')
                audio_filter.push('input_bgm.wav')
            }

            console.log('Time   ', time, images.length)

            await this.FFmpeg?.exec([
                '-r', '2',
                '-i', 'input%d.png',
                ...audio_filter,
                '-vcodec', 'libx264',
                '-pix_fmt', 'yuv420p',
                '-t', String(time),
                name + '.mp4'
            ])
            this.WriteFiles.push(name + '.mp4')

            this.Result = await this.FFmpeg?.readFile(name + '.mp4')
            // this.fileDownload(this.Result, 'mp4', name)
            resolve({
                movie: this.Result,
                name: name + '.mp4'
            })
        })
    }

    /**
     * 複数の動画ファイルを結合
     * 
     * 謎の原因で突然上手く行かなくなるので、使用には注意
     * @param files any
     * @param log boolean default false
     * 
     * @returns Promise<Uint8Array>
     * 
     */
    public async factoryConcat(
        files: {
            movie: Uint8Array,
            name: string
        }[],
        bgm: Uint8Array | string | null = null,
        time: number = 1,
        log: boolean = false
    ): Promise<Uint8Array> {

        // await this.clearFiles()

        return new Promise(async (resolve, reject) => {
            if (this.FFmpeg?.loaded === false) {
                await this.load()

                if (log) this.logs()
            }
            
            //let inputs: string = 'concat '
            let inputs: string = ''
            let filter: string = ''
            const inputText: string[] = []
            const _name = 'unkoman'
            const audio_filter: string[] = []
            const time_filter: string[] = []

            for (let i = 0; i < files.length; i++) {
                // コピー作成
                const movie = new Uint8Array(files[i].movie)
                const file_name = _name + i + '.mp4'
                //console.log('write file', movie)

                await this.FFmpeg?.writeFile(
                    file_name, movie
                )
                this.WriteFiles.push(file_name)
                inputText.push('-i')
                inputText.push(file_name)
                inputs += "file " + file_name + "\n"
                
                if (i === 0){
                    //inputs += file_name
                } else {
                    //filter += '[' + i + ':v:0][' + i + ':a:0]'
                    //inputs += '|' + file_name
                }
                filter += '[' + i + ':v:0][' + i + ':a:0]'
            }

            filter += 'concat=n=' + files.length + ':v=1:a=1[v][a]'

            if (bgm !== null) {
                const _bgm = (typeof bgm === 'string')
                                ? await fetchFile(bgm)
                                : bgm
                console.log('bgm', _bgm)
                await this.FFmpeg?.writeFile(
                    'input_bgm.wav', _bgm
                )
                this.WriteFiles.push('input_bgm.wav')
                audio_filter.push('-i')
                audio_filter.push('input_bgm.wav')
            }

            if (time > 1) {
                time_filter.push('-t')
                time_filter.push(String(time))
            }

            // await this.FFmpeg?.writeFile('concat_list.txt', new TextEncoder().encode(inputs))
            await this.FFmpeg?.writeFile('concat_list.txt', inputs)

            this.WriteFiles.push('concat_list.txt')
            const list = await this.FFmpeg?.listDir('./')
            console.log('Directory List :: ', list)
            console.log('Filter :: ', filter)
            console.log('Input Text :: ', inputs)

            try {
                await this.FFmpeg?.exec([
                    '-nostdin',
                    '-f', 'concat',
                    '-safe', '0',
                    '-i', 'concat_list.txt',
                    ...audio_filter,
                    ...time_filter,
                    // ...inputText,
                    //'-c', 'copy',
                    //'-filter_complex', filter,
                    // '-map', '[v]',
                    'concat.mp4'
                ])
                this.WriteFiles.push('concat.mp4')
                this.Result = await this.FFmpeg?.readFile('concat.mp4')
                this.fileDownload(this.Result, 'mp4', 'concat')
                resolve(this.Result)
            } catch (error) {
                console.error('factoryConcat error', error)
                reject(error)
            }

        })
    }


    /**
     * Fadein, Fadeoutを追加
     * @param file Uint8Array
     * @param time number
     * @returns Promise<Uint8Array>
     * 
     * フィルターで映像自体にフェードイン・フェードアウトを追加
     * するのかと思ったが、実際にはフレーム単位で塗りつぶしされるため
     * 映像変化の少ない動画の場合、一発目のPフレームが潰されると
     * その後のフレームが全て潰されるため、映像が破綻する
     */
    public async filterFadeInFadeOut(
        file: Uint8Array,
        optioins: Partial<{
            fadeIn: number,
            fadeOut: number
        }>,
        log: boolean = false
    ): Promise<Uint8Array> {
        if (optioins.fadeIn === undefined && optioins.fadeOut === undefined) {
            console.error('Not Found Option FadeIn or FadeOut')
            return file
        }

        // await this.clearFiles()

        return new Promise(async resolve => {
            if (this.FFmpeg?.loaded === false) {
                await this.load()
                if (log) this.logs()
            }
            
            let option = ''
            // コピー作成
            const movie = new Uint8Array(file)
            await this.FFmpeg?.writeFile(
                'filter_input.mp4', movie
            )
            // fadeInオプションがある場合
            if (optioins.fadeIn !== undefined) {
                option += `fade=in:st=0:d=${optioins.fadeIn}`
            }
            // fadeOutオプションがある場合
            if (optioins.fadeOut !== undefined) {
                if (option !== '') option += ','
                option += `fade=out:st=${optioins.fadeOut}:d=${optioins.fadeOut}`
            }

            await this.FFmpeg?.exec([
                '-i', 'filter_input.mp4',
                '-vf', option,
                'filter.mp4',
            ])
            this.Result = await this.FFmpeg?.readFile('filter.mp4')
            this.fileDownload(this.Result, 'mp4', 'filter')
            resolve(this.Result)
        })
    }
    

    private async load(): Promise<void> {
        console.log('load ffmpeg...')
        
        const baseURL = "https://unpkg.com/@ffmpeg/core-mt@0.12.6/dist/esm"
        try {
            await this.FFmpeg?.load({
                coreURL: await toBlobURL(`${baseURL}/ffmpeg-core.js`, "text/javascript"),
                wasmURL: await toBlobURL(
                    `${baseURL}/ffmpeg-core.wasm`,
                    "application/wasm"
                ),
                workerURL: await toBlobURL(
                    `${baseURL}/ffmpeg-core.worker.js`,
                    "text/javascript"
                )
            })
        } catch (error) {
            console.error('load ffmpeg error', error)
        }

    }
    
    private logs(showDom: boolean = false): void {
        this.FFmpeg?.on("log", ({ message }) => {
            console.log(message)
        })
        if (!showDom) return
        const m = document.getElementById(this.logTarget) as HTMLElement
        this.FFmpeg?.on("progress", ({ progress, time }) => {
            m.innerHTML = `${progress * 100} %, time: ${time / 1000000} s`
        });
    }

    private clearFiles(): void {
        
        if (this.WriteFiles.length === 0) return

        this.WriteFiles.forEach(async (file) => {
            await this.FFmpeg?.deleteFile(file)
        })
    }
}

const gcd = (a: number, b: number): number => {
    return b === 0 ? a : gcd(b, a % b);
}

const decimalToFraction = (decimal: number): string => {
    const tolerance = 1.0E-6;
    let h1 = 1, h2 = 0, k1 = 0, k2 = 1, b = decimal;
    do {
        const a = Math.floor(b);
        let aux = h1; h1 = a * h1 + h2; h2 = aux;
        aux = k1; k1 = a * k1 + k2; k2 = aux;
        b = 1 / (b - a);
    } while (Math.abs(decimal - h1 / k1) > decimal * tolerance);

    const numerator = h1;
    const denominator = k1;
    const divisor = gcd(numerator, denominator);
    console.log(`${numerator / divisor}/${denominator / divisor}`)
    return `${numerator / divisor}/${denominator / divisor}`
}

/**
 * sleep処理(秒指定)
 */
export const sleep = (sec: number): Promise<void> => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve()
        }, sec * 1000)
    })
}