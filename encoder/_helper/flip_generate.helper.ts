export type LoadedImageType = {
    im_speaker_a: HTMLImageElement,
    im_speaker_b: HTMLImageElement,
    im_blowout_l: HTMLImageElement,
    im_blowout_r: HTMLImageElement,
    im_title: HTMLImageElement,
    im_bg: HTMLImageElement,
    screenShot: HTMLImageElement,
    base_image: HTMLImageElement,
    base_image_url: string,
    null_image: HTMLImageElement,
    null_image_url: string,
    title_image: HTMLImageElement,
    title_image_url: string,
    op_image: HTMLImageElement,
    op_image_url: string,
    loaded: true
}

export type UnloadedImageType = {
    im_speaker_a: null,
    im_speaker_b: null,
    im_blowout_l: null,
    im_blowout_r: null,
    im_title: null,
    im_bg: null,
    screenShot: null,
    base_image: null,
    base_image_url: string,
    null_image: null,
    null_image_url: string,
    title_image: null,
    title_image_url: string,
    op_image: null,
    op_image_url: string,
    loaded: false
}

export type PicturesType = {
    speaker_a: string,
    speaker_b: string,
    title: string,
    blowout_left: string,
    blowout_right: string,
    bg: string,
    op: string,
    not_found_image: string
}

let caution = '※この動画は、Aiを使った自動生成です。'

export const Pictures: PicturesType = {
    speaker_a: '/speaker_a.png',
    speaker_b: '/speaker_b.png',
    title: '/title.png',
    blowout_left: '/blowout_left.png',
    blowout_right: '/blowout_right.png',
    bg: '/bg_4.png',
    op: '/op.png',
    not_found_image: '/not_found.png'
}

export let IS: LoadedImageType | UnloadedImageType = {
    im_speaker_a: null,
    im_speaker_b: null,
    im_blowout_l: null,
    im_blowout_r: null,
    im_title: null,
    im_bg: null,
    screenShot: null,
    base_image: null,
    base_image_url: '',
    null_image: null,
    null_image_url: '',
    title_image: null,
    title_image_url: '',
    op_image: null,
    op_image_url: '',
    loaded: false
}

export const setCaution = (text: string) => caution = text
export const setPictures = (pictures: Partial<PicturesType>) => Object.assign(Pictures, pictures)


export const mainImage = async (
    title: string,
    url: string
): Promise<HTMLImageElement | false> => {
    const [canvas, ctx] = newCanvas()
    if (canvas === false) return false

    const _IS = isLoadedImageType()
    if (_IS === false) return false

    if (IS.loaded === false) {
        await loadImage()
        await nullImage()
        await titleImage(title)
        await openingImage([], [])
    }

    ctx.drawImage(_IS.im_bg, 0, 0, 1920, 1080, 0, 0, 1920, 1080)
    ctx.drawImage(_IS.im_title, 0, 0, 1200, 400, 350, 50, 1200, 100)
    ctx.drawImage(_IS.screenShot, 0, 0, 1440, 900, 702, 180, 576, 270)
    ctx.drawImage(_IS.im_speaker_a, 0, 0, 800, 800, 50, 600, 400, 400)
    ctx.drawImage(_IS.im_speaker_b, 0, 0, 900, 900, 1500, 600, 400, 400)
    ctx.font = '40px "Noto Sans JP"'

    // titleが30文字以上の場合、20文字までにする
    if (title.length > 30) {
        title = title.substring(0, 28) + '...'
    }
    ctx.fillText(title, 420, 120)
    ctx.fillText(url, 420, 550)

    ctx.font = '40px "Noto Sans JP"'
    ctx.fillStyle = '#ffffff'
    ctx.fillText(caution, 582, 1022)
    ctx.font = '40px "Noto Sans JP"'
    ctx.fillStyle = '#000000'
    ctx.fillText(caution, 580, 1020)

    _IS.base_image_url = canvas.toDataURL().split(',')[1]

    // HTMLImageElementを返す
    return new Promise((resolve, reject) => {
        const img = new Image()
        img.src = canvas.toDataURL()
        img.onload = () => {
            _IS.base_image = img
            resolve(img)
        }
    })
}

export const nullImage = async (

): Promise<HTMLImageElement | false> => {

    const [canvas, ctx] = newCanvas()
    if (canvas === false) return false

    const _IS = isLoadedImageType()
    if (_IS === false) return false

    ctx.drawImage(_IS.im_bg, 0, 0, 1920, 1080, 0, 0, 1920, 1080)
    ctx.drawImage(_IS.im_speaker_a, 0, 0, 800, 800, 50, 600, 400, 400)
    ctx.drawImage(_IS.im_speaker_b, 0, 0, 900, 900, 1500, 600, 400, 400)
    ctx.font = '40px "Noto Sans JP"'
    ctx.fillStyle = '#ffffff'
    ctx.fillText(caution, 582, 1022)
    ctx.font = '40px "Noto Sans JP"'
    ctx.fillStyle = '#000000'
    ctx.fillText(caution, 580, 1020)

    _IS.null_image_url = canvas.toDataURL().split(',')[1]

    // HTMLImageElementを返す
    return new Promise((resolve, reject) => {
        const img = new Image()
        img.src = canvas.toDataURL()
        img.onload = () => {
            _IS.null_image = img
            resolve(img)
        }
    })
}

export const titleImage = async (
    title: string
): Promise<HTMLImageElement | false> => {
    const [canvas, ctx] = newCanvas()
    if (canvas === false) return false

    const _IS = isLoadedImageType()
    if (_IS === false) return false

    ctx.drawImage(_IS.null_image, 0, 0, 1920, 1080, 0, 0, 1920, 1080)
    ctx.drawImage(_IS.im_title, 0, 0, 1200, 400, 350, 150, 1200, 250)
    ctx.font = '60px "Noto Sans JP"'

    if (title.length > 16) {
        title = title.substring(0, 15) + '...'
    }
    ctx.fillText(title, 420, 280)

    _IS.title_image_url = canvas.toDataURL().split(',')[1]

    return new Promise((resolve, reject) => {
        const img = new Image()
        img.src = canvas.toDataURL()
        img.onload = () => {
            _IS.title_image = img
            resolve(img)
        }
    })
}

export const openingImage = async (
    titles: string[],
    timeSchedule: string[],
    limit: number = 5
): Promise<HTMLImageElement | false> => {

    const loop = (titles.length > limit) ? limit : titles.length

    const [canvas, ctx] = newCanvas()
    if (canvas === false) return false

    const _IS = isLoadedImageType()
    if (_IS === false) return false

    ctx.drawImage(_IS.null_image, 0, 0, 1920, 1080, 0, 0, 1920, 1080)
    ctx.font = '35px "Noto Sans JP"'
    for (let i = 0; i < loop; i++) {
        // タイトルが20文字以上の場合、20文字までにする
        if (titles[i].length > 30) {
            titles[i] = titles[i].substring(0, 28) + '...　' + timeSchedule[i] + '～'
        }
        ctx.drawImage(_IS.op_image, 0, 0, 1200, 200, 470, 100 + (i * 130), 1000, 120)
        ctx.fillText(titles[i], 550, 160 + (i * 130))
    }

    _IS.op_image_url = canvas.toDataURL().split(',')[1]

    return new Promise((resolve, reject) => {
        const img = new Image()
        img.src = canvas.toDataURL()
        img.onload = () => {
            _IS.op_image = img
            resolve(img)
        }
    })
}


export const leftBlowout = async (
    text: string
): Promise<string | false> => {
    const [canvas, ctx] = newCanvas()
    if (canvas === false) return false

    const _IS = isLoadedImageType()
    if (_IS === false) return false

    ctx.drawImage(_IS.base_image, 0, 0, 1920, 1080, 0, 0, 1920, 1080)
    ctx.drawImage(_IS.im_blowout_l, 0, 0, 1200, 200, 400, 600, 1000, 120)
    ctx.font = '40px "Noto Sans JP"'
    ctx.fillText(text, 480, 660)
    drowScreen(canvas.toDataURL())
    return canvas.toDataURL().split(',')[1]
}

export const rightBlowout = async (
    text: string
): Promise<string | false> => {
    const [canvas, ctx] = newCanvas()
    if (canvas === false) return false

    const _IS = isLoadedImageType()
    if (_IS === false) return false

    ctx.drawImage(_IS.base_image, 0, 0, 1920, 1080, 0, 0, 1920, 1080)
    ctx.drawImage(_IS.im_blowout_r, 0, 0, 1200, 200, 500, 700, 1000, 120)
    ctx.font = '40px "Noto Sans JP"'
    ctx.fillText(text, 620, 760)
    drowScreen(canvas.toDataURL())
    return canvas.toDataURL().split(',')[1]
}


export const loadImage = async (
    reload: boolean = false
): Promise<void> => {

    if (IS.loaded && !reload) return

    [
        IS.im_speaker_a,
        IS.im_speaker_b,
        IS.im_title,
        IS.im_blowout_l,
        IS.im_blowout_r,
        IS.im_bg,
        IS.op_image,
        IS.screenShot,
        IS.loaded
    ] = await Promise.all([
        ImageLoader(Pictures.speaker_a),
        ImageLoader(Pictures.speaker_b),
        ImageLoader(Pictures.title),
        ImageLoader(Pictures.blowout_left),
        ImageLoader(Pictures.blowout_right),
        ImageLoader(Pictures.bg),
        ImageLoader(Pictures.op),
        ImageLoader(Pictures.not_found_image),
        true
    ])

}

export const ImageLoader = async (img: string): Promise<HTMLImageElement> => {
    return new Promise((resolve, reject) => {
        const _img = new Image();
        _img.onload = () => resolve(_img);
        _img.onerror = () => reject('Image Load Error');
        _img.src = img
    })
}

const newCanvas = (): [HTMLCanvasElement, CanvasRenderingContext2D ] | [false, false] => {
    const canvas = document.createElement('canvas') as HTMLCanvasElement
    canvas.width = 1920
    canvas.height = 1080
    if (canvas === null) return [false, false]
    
    const ctx = canvas.getContext('2d')
    if (ctx === null) return [false, false]

    return [canvas, ctx]
}
/**
 * テキストが指定文字数を超えた場合
 * 指定文字数以下になるように分割する
 * 
 * @param text string
 * @param split number
 * @returns string[]
 */
export const splitTextMark = (
    text: string,
    split: number = 20
): string[] => {
    const texts: string[] = []
    const split_count = (text.length / split) + 1
    let _text = ''
    

    if (text.length < split) {
        return [text]
    }

    // 20文字以上の場合、文字数を半分にして分割
    for (let i = 0; i < text.length; i++) {
        _text += text[i]
        if (_text.length > text.length / split_count) {
            texts.push(_text)
            _text = ''
        }
    }
    // 分割後のあまりを追加
    if (_text.length > 0) texts.push(_text)

    return texts
}

const isLoadedImageType = (): LoadedImageType | false => {
    return (IS.loaded) ? IS as LoadedImageType : false
}
const onUnloaded = (): UnloadedImageType | false => {
    return (IS.loaded) ? false : IS as UnloadedImageType
}


/**
 * img要素を作成し、画像を表示する
 * @param image 
 * 
 */
const drowScreen = (
    image: string
) => {
    /*
    const img = new Image()
    img.src = image
    img.onload = () => {
        const canvas = document.createElement('canvas') as HTMLCanvasElement
        const ctx = canvas.getContext('2d')
        ctx.drawImage(img, 0, 0, 1920, 1080, 0, 0, 1920, 1080)

        const dom = document.getElementById('movie-test') as HTMLDivElement
        dom.appendChild(img)
    }*/
}
