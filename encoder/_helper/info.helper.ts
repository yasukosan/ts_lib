
import { FFmpegHelper } from "./ffmpeg.helper";

/**
 * 202309時点でffprobeのwasm版が存在せず
 * 現実的な方法でメディア情報を取得する手段が無い
 * 公式によるとffprobeのwasm版は2021年中にリリースされる予定
 */
export class InfoHelper
{
    private static instance: InfoHelper

    public static call() {
        if (!InfoHelper.instance) {
            InfoHelper.instance = new InfoHelper()
        }
        return InfoHelper.instance;
    }

    public async getVideoInfo(file: string, name: string): Promise<any> {
        const ffmpeg = FFmpegHelper.call().setFile(file, name);
        return '';
    }

}
