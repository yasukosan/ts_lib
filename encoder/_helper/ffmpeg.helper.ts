import { FFmpeg } from "@ffmpeg/ffmpeg"
import { fetchFile, toBlobURL } from '@ffmpeg/util'
import {
    base64ToUint8Array, fileToBase64
} from '../../_helper/convert.helper'

export class FFmpegHelper {
    private static instance: FFmpegHelper

    private FFmpeg: FFmpeg | undefined;
    private WriteFiles: string[] = []

    private Target  : string | Uint8Array = '';
    private FileName: string = '';
    private Extension: string = '';
    private Result  : any;
    private Base64String  : string = '';

    private logTarget: string = 'ffmpeg-log'
    
    public static call() {
        if (!FFmpegHelper.instance) {
            FFmpegHelper.instance = new FFmpegHelper()
        }
        return FFmpegHelper.instance;
    }

    public constructor() {
        // this.FFmpeg = createFFmpeg({log: true})
        this.FFmpeg = new FFmpeg()
    }

    /**
     * 初期化処理
     * @param file string | Uint8Array | ArrayBuffer
     * @returns FFmpegHelper
     */
    public setFile(
        file: string | Uint8Array | ArrayBuffer,
        name: string
    ): FFmpegHelper {
        this.Target = (file instanceof ArrayBuffer) ? new Uint8Array(file) : file
        this.FileName = name.split('.').slice(0, -1).join('.')
        // console.log(this.Target)
        return this
    }

    /**
     * ログを表示するターゲットを設定
     * @param target string
     * @returns FFmpegHelper
     */
    public setLogTarget(target: string): FFmpegHelper {
        this.logTarget = target
        return this
    }

    /**
     * 結果を取得
     * @returns {data: File, name: string}
     */
    public getResult(): {data: File, name: string} {
        const f = new File(
            [this.Result.buffer],
            this.FileName + '.' + this.Extension,
            {
                type: 'audio/' + this.Extension,
            }
        );
        return {
            data    : f,
            name    : this.FileName + '.' + this.Extension,
        }
    }

    /**
     * Base64Stringに変換した結果を取得
     * @returns {data: string, name: string}
     */
    public getBase64Result(): {data: string, name: string} {
        return {
            data    : this.Base64String,
            name    : this.FileName + '.' + this.Extension,
        }
    }

    public async getInformation(): Promise<any>{
        // await this.factory_info('wav')
    }

    public async fileDownload(
        data: Uint8Array | string,
        extension: string = '',
        name    : string = '',
    ): Promise<void> {
        const buff = (typeof data === 'string')
                    ? base64ToUint8Array(data)
                    : data
        const ext = (extension === '') ? this.Extension : extension;
        const na = (name === '') ? this.FileName : name;
        const myURL = window.URL || window.webkitURL
        // extensionに'wav' || 'wave' || 'mp3' || 'ogg' || 'acc' || 'flac' || 'm4a'が含まれているか
        const type = (ext.match(/wav|wave|mp3|ogg|acc|flac|m4a/)) ? 'audio' : 'video'

        const blobURL = myURL.createObjectURL(
                            new Blob([buff], { type: type + ext }))
        
        const downloadLink = document.createElement('a')
        downloadLink.href = blobURL
        downloadLink.download = na + '.' + ext
        downloadLink.click();
    }

    /**
     * 変換処理の実行
     * @param extension1 string // 変換前の拡張子
     * @param extension2 string // 変換後の拡張子 
     * @param options string[]  // ffmpegのオプション
     * @returns Promise<{
     *      extension: string,
     *      result: Uint8Array,
     * }>
     */
    public async factory(
        extension1: string,
        extension2: string,
        options: string[],
        log: boolean = false
    ): Promise<{
        extension: string,
        result: Uint8Array,
    }> {

        //await this.clearFiles()

        return new Promise(resolve => {
            const transcode = async (file: string | Uint8Array) => {
                if (this.FFmpeg?.loaded === false) {
                    // 初回読み込み(FFmpegの実行ファイル読み込み)
                    // 動的にほぼ毎回読み込むので、今後脆弱性があるかもしれない
                    await this.load()
                    // ログ設定・表示
                    if (log) this.logs()
                }
                await this.FFmpeg?.writeFile(
                    'input.' + extension1,
                    (typeof file === 'string')
                        ? base64ToUint8Array(file)
                        : file
                )
                this.WriteFiles.push('input.' + extension1)
                await this.FFmpeg?.exec([
                    '-i', 'input.' + extension1,
                    ...options,
                    'output.' + extension2
                ])
                this.WriteFiles.push('output.' + extension2)
                this.Extension = extension2
                this.Result = await this.FFmpeg?.readFile('output.' + extension2)
                this.Base64String = await this.fileEncoder()

                resolve(this.Result)
            }
            transcode(this.Target)
        })
    }

    /**
     * 複数のwavファイルを結合
     */
    public async factoryConcat(
        files: Uint8Array[],
        filename: string,
        log: boolean = false
    ): Promise<Uint8Array> {

        // await this.clearFiles()

        return new Promise(resolve => {
            const transcode = async (file: Uint8Array[]) => {
                if (this.FFmpeg?.loaded === false) {
                    // 初回読み込み(FFmpegの実行ファイル読み込み)
                    // 動的にほぼ毎回読み込むので、今後脆弱性があるかもしれない
                    await this.load()

                    // ログ設定・表示
                    if (log) this.logs()
                }
                let inputText: string[] = []
                for (let i = 0; i < file.length; i++) {
                    const fileCopy = new Uint8Array(file[i])
                    await this.FFmpeg?.writeFile(
                        'input' + i + '.wav',
                        fileCopy
                    )
                    this.WriteFiles.push('input' + i + '.wav')
                    inputText.push('-i')
                    inputText.push('input' + i + '.wav')
                }
                //console.log('INPUT OPTIONS', inputText)
                //console.log('OPTIONS', options)
                await this.FFmpeg?.exec([
                    ...inputText,
                    '-filter_complex', 'concat=n=' + file.length + ':v=0:a=1',
                    filename + '.wav'
                ])
                this.WriteFiles.push(filename + '.wav')
                this.Extension = 'wav'
                this.Result = await this.FFmpeg?.readFile(filename + '.wav')

                resolve(this.Result)
            }
            transcode(files)
        })
    }

    /**
     * 指定時間でオーディオファイルをカット
     * 
     * ■ timeのみ指定されている場合
     * - time秒以降をカット
     * ■ startが指定されている場合（timeで全体の再生時間の指定必須）
     * - start秒から手前をカット
     * ■ endが指定されている場合（timeで全体の再生時間の指定必須）
     * - end秒から後ろをカット
     * ■ start, endが指定されている場合（timeで全体の再生時間の指定必須）
     * - start秒から手前と、end秒から後ろをカット
     */
    public async factoryCutAudio(
        audio: Uint8Array | string,
        time: number,
        stime: number = 0,
        etime: number = 0,
        log: boolean = false
    ): Promise<Uint8Array> {
            
        // await this.clearFiles()

        return new Promise(resolve => {
            const transcode = async () => {
                if (this.FFmpeg?.loaded === false) {
                    await this.load()

                    if (log) this.logs()
                }

                const time_filter: string[] = []
                const stime_filter: string[] = []
                const etime_filter: string[] = []
                const _audio = (typeof audio === 'string')
                                ? await fetchFile(audio)
                                : audio

                if (stime === 0 && etime === 0) {
                    stime_filter.push('-t')
                    stime_filter.push(String(time))
                }

                if (stime > 0) {
                    stime_filter.push('-ss')
                    stime_filter.push(String(stime))
                }
                if (etime > 0) {
                    etime_filter.push('-t')
                    etime_filter.push(String(time - stime - etime))
                }
                await this.FFmpeg?.writeFile('input.wav', _audio)
                
                await this.FFmpeg?.exec([
                    ...stime_filter,
                    '-i', 'input.wav',
                    ...time_filter,
                    ...etime_filter,
                    '-c', 'copy',
                    'output.wav'
                ])
                this.WriteFiles.push('output.wav')
                this.Extension = 'wav'
                this.Result = await this.FFmpeg?.readFile('output.wav')
                this.Base64String = await this.fileEncoder()

                resolve(this.Result)
            }
        })
    }

    /**
     * 無音のオーディオファイルを作成
     * @param time number
     * @param options string[]
     * @param log boolean
     * @returns Uint8Array
     */
    public async factoryNullAudio(
        time: number,
        options: string[],
        log: boolean = false
    ): Promise<Uint8Array> {

        // await this.clearFiles()

        return new Promise(resolve => {
            const transcode = async () => {
                if (this.FFmpeg?.loaded === false) {
                    await this.load()

                    if (log) this.logs()
                }
                await this.FFmpeg?.exec([
                    '-f', 'lavfi',
                    '-i', 'anullsrc=channel_layout=stereo:sample_rate=44100',
                    '-t', String(time),
                    ...options,
                    'null_audio_' + time + '.wav'
                ])
                this.WriteFiles.push('null_audio_' + time + '.wav')
                this.Extension = 'wav'
                this.Result = await this.FFmpeg?.readFile('null_audio_' + time + '.wav')
                this.Base64String = await this.fileEncoder()

                resolve(this.Result)
            }
            transcode()
        })
    }

    /**
     * FileをBase64文字列に変換
     * @param file File
     * @returns string
     */
    public async fileEncoder(): Promise<string> {
        return new Promise(resolve => {
            const reader = new FileReader()
            reader.onload = () => {
                resolve(reader.result as string)
            }
            reader.readAsDataURL(
                new File(
                    [this.Result.buffer],
                    this.FileName + '.' + this.Extension,
                    {
                        type: 'audio/' + this.Extension
                    }
                )
            )
        })
    }

    private async load(): Promise<void> {
        console.log('load ffmpeg...')

        const baseURL = "https://unpkg.com/@ffmpeg/core-mt@0.12.6/dist/esm"
        try {
            await this.FFmpeg?.load({
                coreURL: await toBlobURL(`${baseURL}/ffmpeg-core.js`, "text/javascript"),
                wasmURL: await toBlobURL(
                    `${baseURL}/ffmpeg-core.wasm`,
                    "application/wasm"
                ),
                workerURL: await toBlobURL(
                    `${baseURL}/ffmpeg-core.worker.js`,
                    "text/javascript"
                )
            })
        } catch (error) {
            console.error('load ffmpeg error', error)
        }
    }

    private logs(showDom: boolean = false): void {
        this.FFmpeg?.on("log", ({ message }) => {
            console.log(message)
        })
        if (!showDom) return
        const m = document.getElementById(this.logTarget) as HTMLElement
        this.FFmpeg?.on("progress", ({ progress, time }) => {
            m.innerHTML = `${progress * 100} %, time: ${time / 1000000} s`
        });
    }

    private clearFiles(): void {

        if (this.WriteFiles.length === 0) return

        this.WriteFiles.forEach(async (file) => {
            await this.FFmpeg?.deleteFile(file)
        })
    }
}