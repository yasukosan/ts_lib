


# example

## wavをmp3に変換
```javascript
import { EncoderService } from './encoder.service'

const convToMP3 = async (file, name) => {
    await EncoderService.call()
        .setup(file, name)
        .wavTompt()
    
    return EncoderService.call().getResult()
}
```

## mp3を任意の再生時間で分割
```javascript
import { EncoderService } from './encoder.service'

const convToMP3 = async (file, name) => {
    await EncoderService.call()
        .setup(file, name)
        splitMp3(0, 350)
    
    return EncoderService.call().getResult()
}
```
