import Tesseract, { createWorker } from 'tesseract.js'

let worker: Tesseract.Worker

export const setup = async (): Promise<void> => {
    worker = await createWorker()
    await worker.loadLanguage('jpn+eng')
    await worker.initialize('jpn+eng')
    return
}

export const doOcr = async (file: File | string): Promise<any> => {
    if (typeof file === 'string') {
        if (isUrl(file)) {
            const { data } = await worker.recognize(file)

            return data.text
        }
    }
    const { data } = await worker.recognize(file)
    return data
}

const isUrl = (url: string): boolean => {
    const regexp = new RegExp(
        '^https?://([\\w-]+\\.)+[\\w-]+(/[\\w-./?%&=]*)?$',
        'i'
    )
    return regexp.test(url)
}