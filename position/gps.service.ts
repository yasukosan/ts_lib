import {
    ReturnSuccess, ReturnError
} from '../definisions'

const ViewTarget = {
    latitude: 'latitude_value',
    longitude: 'longitude_value',
}

const geolocation = {
    latitude: 0,
    longitude: 0,
    timestamp: 0
}

const Options = {
    enableHighAccuracy: false,  // true: 高精度な位置情報を取得する
    timeout: 5000,              // タイムアウト時間（ミリ秒）
    maximumAge: 1000            // キャッシュ有効時間（ミリ秒）
}

let showFlag = false
let successCallBack: (position: GeolocationPosition) => void = () => {}

let watchId: number | null = null

// 座標情報を表示する要素のIDを設定する
export const setViewTarget = (
    latitude: string,
    longitude: string
) => {
    ViewTarget.latitude = latitude
    ViewTarget.longitude = longitude
}
// 座標情報を表示するかどうかを設定する
export const setShowFlag = (flag: boolean) => showFlag = flag

// 成功時のコールバック関数を設定する
export const setSuccessCallBack = (callback: (position: GeolocationPosition) => void) => successCallBack = callback

// 座標情報を取得する
export const getGeolocation = () => geolocation

/**
 * GPS座標を取得する（１度だけ）
 * @returns ReturnSuccess<string> | ReturnError
 */
export const execGeolocation = (): ReturnSuccess<string> | ReturnError => {
    if (isGeoLocation()) {
        try {
            navigator.geolocation
                .getCurrentPosition(
                    onSuccessGetPosition,
                    onErrorGetPosition,
                    Options
                )
            return {
                status: true,
                message: JSON.stringify(geolocation)
            }
        } catch (error: any) {
            console.error(error)
            return {
                status: false,
                message: error
            }
        }
    } else {
        console.error('navigator.geolocation is not found')
        return {
            status: false,
            message: 'navigator.geolocation is not found'
        }
    }
}

/**
 * GPS座標を監視する
 * @param callback (position: GeolocationPosition) => void
 */
export const execWatchGeolocation = (
    callback: (position: GeolocationPosition) => void
): void => {
    successCallBack = callback

    if (watchId) clearWatchGeolocation()

    watchId = navigator.geolocation
                .watchPosition(
                    onSuccessGetPosition,
                    onErrorGetPosition,
                    Options
                )
}

/**
 * GPS座標の監視を解除する
 */
export const clearWatchGeolocation = () => {
    if (watchId) {
        navigator.geolocation.clearWatch(watchId)
    }
}

/**
 * 座標取得成功時のコールバック関数
 * @param position GeolocationPosition
 */
const onSuccessGetPosition = (
    position: GeolocationPosition
) => {
    geolocation.latitude = position.coords.latitude
    geolocation.longitude = position.coords.longitude
    geolocation.timestamp = position.timestamp

    // 座標を表示する
    if (showFlag) {
        statusView(geolocation.latitude, geolocation.longitude)
    }

    // コールバック関数を実行
    successCallBack(position)
}

/**
 * 座標取得失敗時のコールバック関数
 * @param error GeolocationPositionError
 */
const onErrorGetPosition = (
    error: GeolocationPositionError
) => {
    console.error(`ERROR(${error.code}): ${error.message}`)
    // alert(`ERROR(${error.code}): ${error.message}`)
}

/**
 * 座標を表示する
 * @param latitude number
 * @param longitude number
 */
const statusView = (
    latitude: number,
    longitude: number
) => {
    const latitudeView = document.getElementById(ViewTarget.latitude)
    const longitudeView = document.getElementById(ViewTarget.longitude)

    if (latitudeView) latitudeView.innerHTML = String(latitude)
    if (longitudeView) longitudeView.innerHTML = String(longitude)
}

/**
 * GPS座標取得が可能かどうかを返す
 * @returns boolean
 */
const isGeoLocation = () => {
    return navigator.geolocation ? true : false
}