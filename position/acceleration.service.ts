import {
    ReturnSuccess, ReturnError
} from '../definisions'

const ViewTarget = {
    x: 'acceleration_x_value',
    y: 'acceleration_y_value',
    z: 'acceleration_z_value'
}

let logTarget = 'position_log'
let LT: HTMLDivElement | null = null

export type PositionType = {
    acceleration: {x: string, y: string, z: string},
    gravity: {x: string, y: string, z: string},
    rotate: {alpha: string, beta: string, gamma: string}
}

let showFlag = false
let successCallBack = (position: PositionType): void => {}

/**
 * 
 */
export const execDeviceMotion = (
    callback: (position: PositionType) => void
) => {
    //LT = document.getElementById(logTarget) as HTMLDivElement

    successCallBack = callback

    if (DeviceMotionEvent !== undefined) {
        // iOS13以降の場合、DeviceMotionEvent.requestPermissionが存在する
        if('requestPermission' in DeviceMotionEvent) {
            (DeviceMotionEvent as any)
                .requestPermission()
                .then(permissionState => {
                    //LT!.innerHTML = permissionState
                    if (permissionState === 'granted') {
                        deviceMotionEventWatch()
                    }
                })
                .catch(console.error)
        // iOS13以前の場合、DeviceMotionEvent.requestPermissionが存在しない
        // そのため、そのままdeviceMotionEventWatchを実行する
        // Apple製品以外の場合、そのままdeviceMotionEventWatchを実行する
        } else {
            deviceMotionEventWatch()
            //LT!.innerHTML = 'DeviceMotionEvent.requestPermission is not found'
        }
    } else {
        alert('DeviceMotionEvent.requestPermission is not found')
    }
}

const deviceMotionEventWatch = () => {
    window.addEventListener("devicemotion", function (event) {
        if (!event.accelerationIncludingGravity) {
            alert('event.accelerationIncludingGravity is null');
            return;
        }
        const { x, y, z } = getAccelerationValue(event.accelerationIncludingGravity)
        const { x: x2, y: y2, z: z2 } = getAccelerationValue(event.acceleration)
        const { alpha, beta, gamma } = getRotateValue(event.rotationRate)

        if (showFlag) {
            statusView(String(x), String(y), String(z))
        }

        successCallBack({
            acceleration: {x: x, y: y, z: z},
            gravity: {x: x2, y: y2, z: z2},
            rotate: {alpha: alpha, beta: beta, gamma: gamma}
        })
    })
}

const statusView = (
    x: string,
    y: string,
    z: string
) => {
    const xView = document.getElementById(ViewTarget.x)
    const yView = document.getElementById(ViewTarget.y)
    const zView = document.getElementById(ViewTarget.z)

    if (xView) xView.innerHTML = x
    if (yView) yView.innerHTML = y
    if (zView) zView.innerHTML = z
}

const getAccelerationValue = (
    value: DeviceMotionEventAcceleration | null
): {
    x: string,
    y: string,
    z: string
} => {
    if (value === null) return { x: 'null', y: 'null', z: 'null' }

    if (isIPhone() || isIPad()) {
        return {
            x: (value.x === null ) ? 'null' :  String(value.x * -1),
            y: (value.y === null ) ? 'null' :  String(value.y * -1),
            z: (value.z === null ) ? 'null' :  String(value.z * -1)
        }
    }

    return {
        x: (value.x === null ) ? 'null' :  String(value.x),
        y: (value.y === null ) ? 'null' :  String(value.y),
        z: (value.z === null ) ? 'null' :  String(value.z)
    }
}

const getRotateValue = (
    value: DeviceMotionEventRotationRate  | null
): {
    alpha: string,
    beta: string,
    gamma: string
} => {
    if (value === null) return { alpha: 'null', beta: 'null', gamma: 'null' }

    return {
        alpha: (value.alpha === null ) ? 'null' :  String(value.alpha),
        beta: (value.beta === null ) ? 'null' :  String(value.beta),
        gamma: (value.gamma === null ) ? 'null' :  String(value.gamma)
    }
}

const isIPhone = () => {
    return /iPhone/.test(navigator.userAgent)
}

const isIPad = () => {
    return /Macintosh/.test(navigator.userAgent)
}
