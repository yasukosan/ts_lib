import {
    base64ToBlob,
    blobToBase64
} from '../_helper/convert.helper'
import { TimerHelper } from '../_helper/timer.helper';

export type RecorderType = {
    mimeType: string;
    audioBitsPerSecond?: number;
    videoBitsPerSecond?: number;
    bitsPerSecond?: number;
}

export type Recorder = {
    extension: string,
    time: number,
    name: string,
    move: Blob,
}

export const RecorderInitialOption: RecorderType = {
    mimeType: 'video/webm; codecs=vp9',
    audioBitsPerSecond: 128000,
    videoBitsPerSecond: 2500000,
}
const VideoTypes = [
    'video/webm; codecs=vp9',
    "video/webm; codecs=vp8",
    "video/webm; codecs=h264",
    "video/mp4; codecs=avc1",
];

const AudioTypes = [
    "audio/webm; codecs=opus",
    "audio/ogg; codecs=opus",
    "audio/mp4; codecs=mp4a",
];

export class RecorderService
{
    private static instance: RecorderService

    private Recorder: MediaRecorder | undefined;
    private Options: MediaRecorderOptions = RecorderInitialOption;
    private SupportVideoMimeType: string[] = [];
    private SupportAudioMimeType: string[] = [];
    private RecorderData: Recorder[] = [];

    private showTimerTarget: string = '';

    private callback: Function | undefined;

    public static call() {
        if (!RecorderService.instance) {
            RecorderService.instance = new RecorderService()
        }
        return RecorderService.instance;
    }

    /**
     * 設定可能なMimeTypeを取得
     * @returns RecorderService
     */
    public checkMimeType(): RecorderService {
        for (const type of VideoTypes) {
            if (MediaRecorder.isTypeSupported(type)) {
                console.log(type)
                this.SupportVideoMimeType.push(type)
            }
        }
        for (const type of AudioTypes) {
            if (MediaRecorder.isTypeSupported(type)) {
                this.SupportAudioMimeType.push(type)
            }
        }
        return this;
    }

    public setCallback(callback: Function): RecorderService {
        this.callback = callback;
        return this;
    }

    /**
     * オプションを設定
     * @param {options} RecorderType
     * @returns RecorderService
     */
    public setOptions(options: RecorderType): RecorderService {
        this.Options = {
            mimeType: options.mimeType ?? RecorderInitialOption.mimeType,
            audioBitsPerSecond: options.audioBitsPerSecond ?? RecorderInitialOption.audioBitsPerSecond,
            videoBitsPerSecond: options.videoBitsPerSecond ?? RecorderInitialOption.videoBitsPerSecond,
        }
        return this
    }

    /**
     * MediaRecorderを設定
     * @param {stream} MediaStream
     * @returns RecorderService
     */
    public setStream(stream: MediaStream): RecorderService {
        this.Recorder = new MediaRecorder(stream, this.Options)
        return this
    }

    /**
     * タイマーを表示するターゲットを指定
     * @param {target} string
     * @returns RecorderService
     */
    public setTimerTarget(target: string): RecorderService {
        this.showTimerTarget = target;
        return this;
    }

    /**
     * サポートされているMimeTypeを取得
     * @returns string[]
     */
    public getSupportMimeType(): { video: string[], audio: string[] } {
        if (this.SupportVideoMimeType.length === 0) {
            this.checkMimeType()
        }
        return {
            video: this.SupportVideoMimeType,
            audio: this.SupportAudioMimeType
        };
    }

    /**
     * RecorderのMimeTypeを取得
     * @returns string
     */
    public getMimeType(): string {
        return this.Recorder?.mimeType ?? '';
    }

    /**
     * Recorderの状態を取得
     * @returns 'inactive' | 'recording' | 'paused' | 'none'
     */
    public getState(): 'inactive' | 'recording' | 'paused' | 'none' {
        return this.Recorder?.state ?? 'none';
    }

    /**
     * RecorderのVideoビットレートを取得
     * @returns number
     */
    public getVideoBitsPerSecond(): number {
        return this.Recorder?.videoBitsPerSecond ?? 0;
    }

    /**
     * RecorderのAudioビットレートを取得
     * @returns number
     */
    public getAudioBitsPerSecond(): number {
        return this.Recorder?.audioBitsPerSecond ?? 0;
    }

    /**
     * 録画データを取得
     * @returns Blob[]
     */
    public getRecorderData(): Recorder[] {
        return this.RecorderData;
    }

    /**
     * ArrayBufferをBase64に変換して返す
     * @returns {Promise<string[]>}
     */
    public async getRecorderDataToBase64(): Promise<string[]> {
        const base64: string[] = []
        for (const rec of this.RecorderData) {
            base64.push(await this.BlobToBase64(rec.move))
        }
        return base64;
    }

    /**
     * 録画開始
     * @param {slice} number = 0
     * @returns RecorderService
     */
    public start(slice: number = 0): RecorderService {
        this.reset()

        if (slice > 0)
            this.Recorder?.start(slice)
        else
            this.Recorder?.start()
        
        // 録画データのBlobが吐き出されるイベント
        this.Recorder?.addEventListener('dataavailable', (e) => {
            if (e.data.size > 0) {
                TimerHelper.call().stop()

                const result: Recorder = {
                    extension: this.Options.mimeType?.split(';')[0].split('/')[1] as string,
                    name: this.createFileName(),
                    time: TimerHelper.call().get(),
                    move: e.data
                }

                this.RecorderData.push(result)
                // console.log(this.RecorderData)

                if (this.callback) {
                    this.callback(result)
                }
            }
        })

        this.Recorder?.addEventListener('start', () => {
            TimerHelper.call().start()
        })

        // 何故かdataavailableイベントより後に呼ばれるので、タイマーはここで止めない
        this.Recorder?.addEventListener('stop', (e) => {})

        this.Recorder?.addEventListener('pause', () => {
            TimerHelper.call().pause()
        })

        this.Recorder?.addEventListener('resume', () => {
            TimerHelper.call().restart()
        })

        // エラーイベント
        this.Recorder?.addEventListener('error', (e) => {
            console.error(e)
        })

        return this;
    }

    /**
     * 録画停止
     * @returns RecorderService
     */
    public stop(): RecorderService {
        this.Recorder?.stop()
        return this;
    }

    /**
     * 録画一時停止
     * @returns RecorderService
     */
    public pause(): RecorderService {
        this.Recorder?.pause()
        return this;
    }

    /**
     * 録画再開
     * @returns RecorderService
     */
    public resume(): RecorderService {
        this.Recorder?.resume()
        return this;
    }

    /**
     * タイマーを表示
     * @returns RecorderService
     */
    public showTimer(): RecorderService {
        if (this.showTimerTarget === '') return this;

        const t = document.getElementById(this.showTimerTarget)
        if (t !== undefined && t?.hasOwnProperty('innerText'))
            t.innerText = String(TimerHelper.call().getLap())
        return this;
    }

    public async BlobToBase64(blob: Blob): Promise<string> {
        return await blobToBase64(blob)
    }

    /**
     * 日付と時間を使用したファイル名を生成
     * @returns string
     * @example 2021-01-01_00-00-00.mp4
     */
    public createFileName(): string {
        const date = new Date()
        const year = date.getFullYear()
        const month = ('0' + (date.getMonth() + 1)).slice(-2)
        const day = ('0' + date.getDate()).slice(-2)
        const hour = ('0' + date.getHours()).slice(-2)
        const minute = ('0' + date.getMinutes()).slice(-2)
        const second = ('0' + date.getSeconds()).slice(-2)
        return `${year}-${month}-${day}_${hour}-${minute}-${second}`
    }



    private reset(): RecorderService {
        this.RecorderData = []
        return this
    }
}