import { ImageHelper } from './helper/image.helper'

export class ImageStreamService {
    private static instance: ImageStreamService;

    private VideoTarget: HTMLVideoElement | undefined
    private ih: ImageHelper;

    public constructor() {
        this.ih = new ImageHelper()
    }

    public static call(): ImageStreamService {
        if (!ImageStreamService.instance) {
            ImageStreamService.instance = new ImageStreamService()
        }
        return ImageStreamService.instance
    }

    /**
     * videoタグのID名か、videoタグ自体を格納
     *
     * @param target string | HTMLVideoELement
     * @returns ImageStreamService
     */
    public setVideoTarget(
        target: string | HTMLVideoElement
    ): ImageStreamService {
        if (typeof target === 'string') {
            this.VideoTarget = document.querySelector(
                '#' + target
            ) as HTMLVideoElement
        } else {
            this.VideoTarget = target
        }
        return this
    }

    public async getImageToElement(): Promise<HTMLImageElement> {
        await ImageHelper.call().setImage(await this.getImageToStream())

        return ImageHelper.call().getImageForElement()
    }

    /**
     * Videoタグから画像を生成
     * @return Promise<string> dataURL
     */
    public async getImageToStream(): Promise<string> {

        if (this.VideoTarget === undefined) {
            console.error('Video Target Error')
            return 'Video Target Error'
        }

        const canvas = this.ih.getNewCanvas(
            this.VideoTarget.videoWidth,
            this.VideoTarget.videoHeight
        ) as HTMLCanvasElement
        const ctx: CanvasRenderingContext2D | null = canvas.getContext('2d')

        if (!ctx) {
            console.error('Canvas Context Error')
            return 'Canvas Context Error'
        }
        
        ctx.drawImage(this.VideoTarget, 0, 0, canvas.width, canvas.height)
        return canvas.toDataURL('image.jpg')
    }

    /**
     * videoタグから複数枚画像を生成
     * @param count number 枚数 デフォルト1
     * @param interval number 撮影間隔（ミリ秒）デフォルト1000mm秒
     * @return Promise<string[]> dataURL配列
     */
    public async getImageToStreamMulti(
        count = 1,
        interval = 1000
    ): Promise<string[]> {
        const imgs: string[] = []
        return new Promise((resolve) => {
            const getImage = async () => {
                imgs.push(await this.getImageToStream())
                if (imgs.length === count) {
                    resolve(imgs)
                } else {
                    setTimeout(() => getImage(), interval)
                }
            };
            getImage()
        })
    }
}
