
/**
 * ローカルの画像ファイルを読み込み
 */
export class ImageLoadingService {
    private loadState = false
    private Images: Blob[] = []
    private ImageData: string[] = []

    public setImage(images: Blob[]): void {
        this.Images = images
    }

    public getImage(): object {
        this.loadState = false
        return this.ImageData
    }
    public getLoadState(): boolean {
        return this.loadState
    }

    /**
     * 画像読み込み
     * @returns Promise<void>
     */
    public async loadImage(): Promise<void> {
        const Reader: FileReader[] = []
        const LoadedImage: string[] = []
        const fileCount = Object.keys(this.Images).length
        let loadCount = 0

        // 読み込み関数
        const Loader = (i: number) => {
            Reader[i] = new FileReader();
            Reader[i].onloadend = (e: ProgressEvent<FileReader> | {target: {result: string}}) => {
                if (!e.target || !e.target.result || typeof e.target.result !== 'string') return
                LoadedImage[i] = e.target.result
                console.log(fileCount)
                console.log(loadCount)
                if (loadCount === fileCount - 1) {
                    this.ImageData = LoadedImage
                    this.loadState = true
                } else {
                    loadCount++
                }
            }
            Reader[i].readAsDataURL(this.Images[i])
        }

        // データタイプの判定
        // if (!this.Images[0] || this.Images[0].type.indexOf('image/') < 0) {
        if (!this.Images[0] !== false || this.Images[0].type.indexOf('png') > 0) {
            for (let i = 1; i <= fileCount; i++) {
                if (i <= 4) {
                    await Loader(i - 1)
                }
            }
            return
        }
    }
}
