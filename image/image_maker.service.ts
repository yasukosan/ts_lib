export type PaperType = {
    width: number
    height: number
}

export type SheetPropertiesType = {
    marginTop: number
    marginLeft: number
}

export type TextDesineType = {
    fontDesine: string
    fontWeight: string
}

export type TextPropertiesType = {
    text: string
    size: number
    x: number
    y: number
}

export type ImagePropertiesType = {
    img: string
    x: number
    y: number
    width: number
    height: number
}

export class ImageMakerService {
    alreadyEnlargement = false

    private sheetSet: {
        [key: string]: PaperType
    } = {
        A7: { width: 74, height: 105 },
        A6: { width: 105, height: 148 },
        A5: { width: 148, height: 210 },
        A4: { width: 210, height: 297 },
        A3: { width: 297, height: 420 },
        A2: { width: 420, height: 594 },
        B7: { width: 91, height: 128 },
        B6: { width: 128, height: 182 },
        B5: { width: 182, height: 257 },
        B4: { width: 257, height: 364 },
        B3: { width: 364, height: 515 },
        B2: { width: 515, height: 728 }
    };
    // 印刷用紙のサイズ
    private sheetSize: PaperType = {
        width: 210,
        height: 297
    }
    // 余白他の印刷情報
    private sheetSpec: SheetPropertiesType = {
        marginTop: 0, // ページ余白上
        marginLeft: 0 // ページ余白左
    }

    // フォント
    private textDesine: TextDesineType = {
        fontDesine: 'MS PMincho',
        fontWeight: 'normal'
    }

    // 背景になるSVGイメージ
    private sheetBackground: string = ''
    // 拡大率 （通常印刷350dpiの場合は13.78095）
    private resulution = 13.78095;
    // 印刷テキスト
    private printTexts: TextPropertiesType[] = []
    // 印刷画像
    private printImages: ImagePropertiesType[] = []
    // 出力画像
    private sheetImage: string = ''

    private static instance: ImageMakerService
    
    public static call(): ImageMakerService {
        if (!ImageMakerService.instance) {
            ImageMakerService.instance = new ImageMakerService()
        }
        return ImageMakerService.instance
    }

    /**
     * 用紙サイズ設定
     * @param width
     * @param height
     * @returns ImageMakerService
     */
    public setSheetSize(width: number, height: number): ImageMakerService {
        this.sheetSize.width = width
        this.sheetSize.height = height
        this.alreadyEnlargement = false
        return this
    }

    /**
     * 用紙規格からサイズ設定
     *
     * 「 A2 ~ A7  B2 ~ B7 」
     * @param paper string 用紙規格
     * @returns ImageMakerService
     */
    public setSheetStandard(paper = 'A4'): ImageMakerService {
        const size = this.getStandardSize(paper)
        this.sheetSize.width = size.width
        this.sheetSize.height = size.height

        return this
    }

    /**
     * 拡大率設定
     * 印刷用に350dpiに合わせる場合13.78095を設定
     * @param number magnification
     */
    public setResulution(magnification: number): ImageMakerService {
        this.resulution = magnification
        this.alreadyEnlargement = false
        return this
    }
    /**
     * 余白、他の設定
     * @param spec
     */
    public setSheetPropatie(
        spec: Partial<SheetPropertiesType>
    ): ImageMakerService {
        this.sheetSpec = Object.assign(this.sheetSpec, spec)
        return this
    }
    /**
     * 背景画像設定
     * @param bg string
     */
    public setSheetBackground(bg: string): ImageMakerService {
        this.sheetBackground = bg
        return this
    }

    /**
     * 埋め込み文字列の設定
     *
     * @param texts [[text,size,x,y],....]
     * @returns ImageMakerService
     * text: 埋め込み文字列
     * size: フォントサイズ（px）
     * x   : X座標
     * y   : y座標
     */
    public setPrintTexts(
        texts: TextPropertiesType[]
    ): ImageMakerService {
        this.printTexts = texts
        return this
    }
    /**
     * 印刷画像コンテンツを設定
     * @param images {
     *  img: string,
     *  x: number,
     *  y: number,
     *  width: number,
     *  height: number }
     * @returns
     */
    public setPrintImages(
        images: ImagePropertiesType | ImagePropertiesType[]
    ): ImageMakerService {
        if (Array.isArray(images)) this.printImages = images
        else this.printImages.push(images)
        return this
    }
    public setTextDesine(
        desine: Partial<TextDesineType>
    ): ImageMakerService {
        this.textDesine = Object.assign(this.textDesine, desine)
        return this
    }

    public async sheetMaker(): Promise<string> {
        this.doEnlargement()
        const oc: HTMLCanvasElement = this.getNewCanvas(
            this.sheetSize.width,
            this.sheetSize.height
        )
        const bg: HTMLCanvasElement = this.getNewCanvas(
            this.sheetSize.width,
            this.sheetSize.height
        )
        const fg: HTMLCanvasElement = this.getNewCanvas(
            this.sheetSize.width,
            this.sheetSize.height
        )
        const ctx: CanvasRenderingContext2D | null = oc.getContext('2d')
        const bgctx: CanvasRenderingContext2D | null = bg.getContext('2d')
        const fgctx: CanvasRenderingContext2D | null = fg.getContext('2d')

        if (!ctx || !bgctx || !fgctx) {
            return 'Canvas Context Error'
        }

        for (const key in this.printTexts) {
            fgctx.font =
                this.printTexts[key]['size'] * (this.resulution / 3) +
                'px "' +
                this.textDesine.fontDesine +
                '"'
            fgctx.fillText(
                this.printTexts[key]['text'],
                this.printTexts[key]['x'] * (this.resulution / 3),
                this.printTexts[key]['y'] * (this.resulution / 3)
            )
        }

        const img = await this.loadImage(this.sheetBackground)
        bgctx.drawImage(img, 0, 0, this.sheetSize.width, this.sheetSize.height)

        ctx.drawImage(bg, 0, 0, this.sheetSize.width, this.sheetSize.height)
        ctx.drawImage(fg, 0, 0, this.sheetSize.width, this.sheetSize.height)

        this.sheetImage = oc.toDataURL('image/jpg')
        return this.sheetImage
    }

    public async svgToImg(): Promise<string> {
        this.doEnlargement()
        const oc = this.getNewCanvas(
            this.sheetSize.width,
            this.sheetSize.height
        )
        const bg = this.getNewCanvas(
            this.sheetSize.width,
            this.sheetSize.height
        )
        const ctx: CanvasRenderingContext2D | null = oc.getContext('2d')
        const bgctx: CanvasRenderingContext2D | null = bg.getContext('2d')

        if (!ctx || !bgctx) {
            return 'Canvas Context Error'
        }

        const img = await this.loadImage(this.sheetBackground)
        bgctx.drawImage(img, 0, 0, this.sheetSize.width, this.sheetSize.height)
        ctx.drawImage(bg, 0, 0, this.sheetSize.width, this.sheetSize.height)

        this.sheetImage = oc.toDataURL('image/jpg')
        return this.sheetImage
    }

    /**
     * 顔写真付きの履歴書作成
     * @param face
     * @returns
     */
    public async overFaceShot(face: string): Promise<string> {
        this.doEnlargement()
        const oc = this.getNewCanvas(
            this.sheetSize.width,
            this.sheetSize.height
        )
        const bg = this.getNewCanvas(
            this.sheetSize.width,
            this.sheetSize.height
        )
        const fg = this.getNewCanvas(
            this.sheetSize.width,
            this.sheetSize.height
        )
        const ctx: CanvasRenderingContext2D | null = oc.getContext('2d')
        const bgctx: CanvasRenderingContext2D | null = bg.getContext('2d')
        const fgctx: CanvasRenderingContext2D | null = fg.getContext('2d')

        if (!ctx || !bgctx || !fgctx) {
            return 'Canvas Context Error'
        }

        const imgFace = new Image()
        imgFace.src = face
        fgctx.drawImage(imgFace, 2205, 234, 400, 524)

        const img = await this.loadImage(this.sheetBackground)
        bgctx.drawImage(img, 0, 0, this.sheetSize.width, this.sheetSize.height)

        ctx.drawImage(bg, 0, 0, this.sheetSize.width, this.sheetSize.height)
        ctx.drawImage(fg, 0, 0, this.sheetSize.width, this.sheetSize.height)

        this.sheetImage = oc.toDataURL('image/jpg')
        return this.sheetImage
    }

    public async buildCardImage(): Promise<string> {
        this.doEnlargement()
        const oc = this.getNewCanvas(
            this.sheetSize.width,
            this.sheetSize.height
        )

        const ctx: CanvasRenderingContext2D | null = oc.getContext('2d')

        if (!ctx) {
            return 'Canvas Context Error'
        }

        for (const key in this.printImages) {
            if (Object.prototype.hasOwnProperty.call(this.printImages, key)) {
                const im: ImagePropertiesType = this.imageEnlargement(this.printImages[key]);
                const img = await this.loadImage(im.img)
                ctx.drawImage(
                    img,
                    0,
                    0,
                    this.sheetSize.width,
                    this.sheetSize.height,
                    im.x,
                    im.y,
                    im.width,
                    im.height
                )
            }
        }

        this.sheetImage = oc.toDataURL('image/jpg')
        return this.sheetImage
    }

    public async buildSphererImage(): Promise<string> {
        const oc = this.getNewCanvas(
            this.sheetSize.width,
            this.sheetSize.height
        )
        const ctx: CanvasRenderingContext2D | null = oc.getContext('2d')
        if (!ctx) return 'Canvas Context Error'

        ctx.beginPath()
        ctx.fillStyle = 'rgba(' + [0, 0, 0, 0] + ')'
        ctx.fillRect(0, 0, this.sheetSize.width, this.sheetSize.height)
        const im: ImagePropertiesType = this.printImages[0]
        const img = await this.loadImage(im.img)
        ctx.drawImage(
            img,
            0,
            0,
            this.sheetSize.width,
            this.sheetSize.height,
            im.x,
            im.y,
            im.width,
            im.height
        )

        this.sheetImage = oc.toDataURL('image/png')
        return this.sheetImage
    }

    /**
     * 作成された画像データを取得
     * (base64 string)
     * @returns string
     */
    public getSheetImage(): string {
        return this.sheetImage
    }

    /**
     * 用紙規格から用紙サイズを取得
     * @param paper string
     * @returns string[]
     */
    public getStandardSize(paper = 'A4'): PaperType {
        return paper in this.sheetSet
            ? this.sheetSet[paper]
            : this.sheetSet['A4']
    }

    public initialization(): void {
        this.sheetImage = ''
        this.sheetSize = {
            width: 100,
            height: 148
        }
        this.sheetSpec = {
            marginTop: 0,
            marginLeft: 0
        }

        this.textDesine = {
            fontDesine: 'MS PMincho',
            fontWeight: 'normal'
        }

        this.resulution = 13.78095
    }
    /**
     * 作業用HTMLCanvasElementを作成
     * @param width
     * @param height
     * @returns
     */
    private getNewCanvas(width: number, height: number): HTMLCanvasElement {
        const oc: HTMLCanvasElement = document.createElement('canvas')
        oc.setAttribute('width', width.toString())
        oc.setAttribute('height', height.toString())

        return oc
    }

    /**
     * 画像データをHtmlImageElementに変換
     * @param img string
     * @returns Promise<HTMLImageElement>
     */
    private async loadImage(img: string): Promise<HTMLImageElement> {
        return new Promise((resolve, reject) => {
            const _img = new Image()
            _img.onload = () => resolve(_img)
            _img.onerror = () => reject('Image Load Error')
            _img.src = img
        })
    }

    /**
     * 指定倍率の応じたシートサイズの拡大
     */
    private doEnlargement(): void {
        if (!this.alreadyEnlargement) {
            this.sheetSize.width = this.sheetSize.width * this.resulution
            this.sheetSize.height = this.sheetSize.height * this.resulution
            this.alreadyEnlargement = true
        }
    }

    /**
     * 画像の座標情報を倍率に合わせて拡大
     * @param im { img, x, y, width. height }
     * @returns PrintImageType
     * { img: string, x: number, y: number, width: number. height: number }
     */
    private imageEnlargement(
        im: ImagePropertiesType
    ): ImagePropertiesType {
        return {
            img: im.img,
            x: im.x * this.resulution,
            y: im.y * this.resulution,
            width: Math.floor(im.width * this.resulution),
            height: Math.floor(im.height * this.resulution)
        }
    }
}
