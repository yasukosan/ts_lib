export class ImageOrientationService {
    private ImageData: string[] = new Array<string>()

    public getImage(): string[] {
        return this.ImageData
    }

    public doOrientation(image: string[]): void {
        const count = Object.keys(image).length - 1
        let counter = 0
        const checkCount = () => {
            if (count === counter) {
                // this.hub('load_orientation_image', '');
            } else {
                counter++
            }
        }
        for (const it of image) {
            const rotate = this.getOrientation(it);
            if (rotate === 0 || rotate === 1) {
                this.ImageData[count] = it
                checkCount()
            } else {
                this.ImgRotation(it, rotate).then((_image) => {
                    this.ImageData[counter] = _image
                    checkCount()
                })
            }
        }
        return
    }

    getOrientation(imgDataURL: string): number {
        const byteString = atob(imgDataURL.split(',')[1])
        const orientaion = this.byteStringToOrientation(byteString)
        return orientaion
    }

    /**
     * バイナリ文字列から撮影時の傾き情報を切り出す
     * @param img string 画像文字列
     * @returns number 画像向きの定義番号(0~8)
     */
    private byteStringToOrientation(img: string): number {
        let head = 0
        let orientation = 0
        let loop = true

        while (loop) {
            if (
                img.charCodeAt(head) === 255 &&
                img.charCodeAt(head + 1) === 218
            ) {
                loop = false
                break
            }
            if (
                img.charCodeAt(head) === 255 &&
                img.charCodeAt(head + 1) === 216
            ) {
                head += 2
            } else {
                const length =
                    img.charCodeAt(head + 2) * 256 + img.charCodeAt(head + 3)
                const endPoint = head + length + 2
                if (
                    img.charCodeAt(head) === 255 &&
                    img.charCodeAt(head + 1) === 225
                ) {
                    const segment = img.slice(head, endPoint)
                    const bigEndian = segment.charCodeAt(10) === 77
                    let count
                    if (bigEndian) {
                        count =
                            segment.charCodeAt(18) * 256 +
                            segment.charCodeAt(19)
                    } else {
                        count =
                            segment.charCodeAt(18) +
                            segment.charCodeAt(19) * 256
                    }
                    for (let i = 0; i < count; i++) {
                        const field = segment.slice(20 + 12 * i, 32 + 12 * i)
                        if (
                            (bigEndian && field.charCodeAt(1) === 18) ||
                            (!bigEndian && field.charCodeAt(0) === 18)
                        ) {
                            orientation = bigEndian
                                ? field.charCodeAt(9)
                                : field.charCodeAt(8)
                        }
                    }
                    loop = false
                    break
                }
                head = endPoint
            }
            if (head > img.length) {
                loop = false
                break
            }
        }
        return orientation
    }

    /**
     * 画像回転
     * ローテーションの意味
     * ０：未定義 １：通常 ２：左右反転 ３：180度回転 ４：上下反転
     * ５：反時計回りに９０度回転 上下反転
     * ６：時計回りに９０度回転
     * ７：時計回りに９０度回転 上下反転
     * ８：反時計回りに９０度回転
     * @param imgB64_src string 画像データ
     * @param width number 横幅
     * @param height number 高さ
     * @param rotate number 取得したオリエンテーション情報
     * @param callback Promise<string> 回転後の画像文字列
     */
    public async ImgRotation(
        imgB64_src: string,
        rotate: number
    ): Promise<string> {
        // Image Type
        const img_type = imgB64_src.substring(5, imgB64_src.indexOf(';'))

        // Source Image
        const img = new Image()
        return new Promise((resolve) => {
            img.onload = () => {
                // New Canvas
                const canvas = document.createElement(
                    'canvas'
                ) as HTMLCanvasElement
                const width = img.naturalWidth
                const height = img.naturalHeight
                if (
                    rotate === 5 ||
                    rotate === 6 ||
                    rotate === 7 ||
                    rotate === 8
                ) {
                    // swap w <==> h
                    canvas.setAttribute('width', height.toString())
                    canvas.setAttribute('height', width.toString())
                } else {
                    canvas.setAttribute('width', width.toString())
                    canvas.setAttribute('height', height.toString())
                }

                // Draw (Resize)
                const ctx: CanvasRenderingContext2D | null = canvas.getContext('2d')
                if (ctx === null) return 'Get Context Error'
                switch (rotate) {
                    case 0: break
                    case 1: break
                    case 2:
                        ctx.transform(1, -1, 0, 0, 0, 0)
                        break
                    case 3:
                        ctx.rotate((180 * Math.PI) / 180)
                        ctx.translate(-width, -height)
                        break
                    case 4:
                        ctx.transform(1, 0, 0, -1, 0, 0)
                        break
                    case 5:
                        ctx.rotate((270 * Math.PI) / 180)
                        ctx.translate(-width, 0)
                        ctx.transform(1, 0, 0, -1, 0, 0)
                        break
                    case 6:
                        ctx.rotate((90 * Math.PI) / 180)
                        ctx.translate(0, -height)
                        break
                    case 7:
                        ctx.rotate((rotate * Math.PI) / 180)
                        ctx.translate(0, -height)
                        ctx.transform(1, 0, 0, -1, 0, 0)
                        break
                    case 8:
                        ctx.rotate((270 * Math.PI) / 180)
                        ctx.translate(-width, 0)
                        break
                    default:
                        break
                }

                ctx.drawImage(img, 0, 0, width, height)

                resolve(canvas.toDataURL(img_type))
            }
            img.src = imgB64_src
        })
    }
}
