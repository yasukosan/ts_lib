export type ColorPropertyType = {
    r: number
    g: number
    b: number
}

export class ImageResizeService {
    private static instance: ImageResizeService

    private Image = '';
    private ImageData: Uint8ClampedArray | undefined = undefined
    private ResizeImage = ''

    private originalWidth = 0
    private originalHeight = 0
    private resizeWidth = 0
    private resizeHeight = 0
    private rate = 0

    public static call(): ImageResizeService {
        if (!ImageResizeService.instance) {
            ImageResizeService.instance = new ImageResizeService()
        }
        return ImageResizeService.instance
    }

    /**
     * 画像を指定サイズか、倍率に変換する
     * @param img string 画像文字列
     * @param w number 幅
     * @param h number 高さ
     * @param r number 倍率(0 の場合は指定の幅と高さに変換)
     * @returns Promise<string>
     */
    public async publishResize(
        img: string,
        w = 0,
        h = 0,
        r = 0
    ): Promise<string> {
        this.setImage(img)
        await this.getImageSize(img)
        if (r === 0) {
            this.resizeWidth = w
            this.resizeHeight = h
            this.setRate()
        } else {
            this.resizeWidth = this.originalWidth * r
            this.resizeHeight = this.originalHeight * r
            this.rate = r
        }
        return await this.resizeImage()
    }

    /**
     * アルゴリズムべースのリサイズ処理
     * @param img string 変換元画像データ
     * @param r number 変換倍率
     * @param mode 'bicubic' | 'lanczos3' | 'bilinear' リサイズアルゴリズム
     * バイキュービック法： bicubic
     * ランチョス法  ： lanczos3
     * バイリニア法  ： bilinear
     * @return Promise<string>
     */
    public async publishCalcResize(
        img: string,
        r = 1,
        mode: 'bicubic' | 'lanczos3' | 'bilinear' = 'bicubic'
    ): Promise<string> {
        this.setImage(img)
        await this.getImageSize(img)
        this.resizeWidth = this.originalWidth * r
        this.resizeHeight = this.originalHeight * r
        this.rate = r
        return await this.calcResize(mode)
    }

    public setRate(): ImageResizeService {
        const rate = (this.resizeWidth / this.originalWidth).toFixed(2)
        this.rate = Number(rate)
        return this
    }

    /**
     * 用紙サイズ設定
     * @param width number
     * @param height number
     */
    public setimageSize(width: number, height: number): ImageResizeService {
        this.originalWidth = width
        this.originalHeight = height
        return this
    }

    /**
     * 画像データを変数に保持
     * @param img string 画像文字列
     */
    public setImage(img: string): ImageResizeService {
        this.Image = img
        return this
    }

    /**
     * リサイズ実行
     * @returns Promise<string> リサイズ後の画像文字列
     */
    public async resizeImage(): Promise<string> {
        const oc: HTMLCanvasElement = document.createElement('canvas')
        const ctx: CanvasRenderingContext2D | null = oc.getContext('2d')
        if (ctx === null) return 'Get Context Error'

        oc.setAttribute('width', this.resizeWidth.toString())
        oc.setAttribute('height', this.resizeHeight.toString())

        return new Promise((resolve) => {
            const loadImage = () => {
                const img: HTMLImageElement = new Image()
                img.onload = () => {
                    ctx.drawImage(
                        img,
                        0,
                        0,
                        this.resizeWidth,
                        this.resizeHeight
                    )
                    this.ResizeImage = oc.toDataURL('image/jpg')
                    resolve(this.ResizeImage)
                }
                img.src = this.Image
            }
            loadImage()
        })
    }

    /**
     * 画像のリサイズ実行
     * @param mode string
     * @returns Promise<string>
     */
    private async calcResize(mode: string): Promise<string> {
        const oc: HTMLCanvasElement = document.createElement('canvas');
        const ctx: CanvasRenderingContext2D = oc.getContext(
            '2d'
        ) as CanvasRenderingContext2D;
        oc.setAttribute('width', this.originalWidth.toString());
        oc.setAttribute('height', this.originalHeight.toString());

        const img = await this.loadImage();
        // const img = this.Image;

        ctx.drawImage(img, 0, 0, this.originalWidth, this.originalHeight)
        this.ImageData = ctx.getImageData(
            0,
            0,
            this.originalWidth,
            this.originalHeight
        ).data

        switch (mode) {
            case 'bicubic':
                this.ResizeImage = await this.resizeByBicubic()
                break
            case 'lanczos3':
                this.ResizeImage = await this.resizeByLanczos3()
                break
            case 'bilinear':
                break
        }

        return this.ResizeImage
    }

    /**
     * bycubic法によるリサイズ処理
     * @returns Promise<string>
     */
    private async resizeByBicubic(): Promise<string> {
        const dc: HTMLCanvasElement = document.createElement('canvas')
        const dstCtx: CanvasRenderingContext2D | null = dc.getContext('2d')
        if (dstCtx === null) return 'Get Context Error'

        const dstImgData: ImageData = dstCtx.getImageData(
            0,
            0,
            this.resizeWidth,
            this.resizeHeight
        )
        const dData = dstImgData.data

        const range = [-1, 0, 1, 2]
        let pos = 0

        for (let dy = 0; dy < this.resizeHeight; dy++) {
            const sy = dy / this.rate
            const rangeY = range.map((i) => i + Math.floor(sy))

            for (let dx = 0; dx < this.resizeWidth; dx++) {
                const sx = dx / this.rate
                const rangeX = range.map((i) => i + Math.floor(sx))

                let r = 0,
                    g = 0,
                    b = 0
                for (const y of rangeY) {
                    const weightY = this.getWeightByBicubic(y, sy)
                    for (const x of rangeX) {
                        const weight = weightY * this.getWeightByBicubic(x, sx)
                        if (weight === 0) {
                            continue
                        }

                        const color: ColorPropertyType = this.rgb(x, y)
                        r += color.r * weight
                        g += color.g * weight
                        b += color.b * weight
                    }
                }
                dData[pos++] = Math.floor(r)
                dData[pos++] = Math.floor(g)
                dData[pos++] = Math.floor(b)
                dData[pos++] = 255
            }
        }

        return dc.toDataURL('image/jpg')
    }

    /**
     * Lanczos法によるリサイズ処理
     * @returns Promise<string>
     */
    private async resizeByLanczos3(): Promise<string> {
        const dc: HTMLCanvasElement = document.createElement('canvas')
        const dstCtx: CanvasRenderingContext2D | null = dc.getContext('2d')
        if (dstCtx === null) return 'Get Context Error'

        const dstImgData: ImageData = dstCtx.getImageData(
            0,
            0,
            this.resizeWidth,
            this.resizeHeight
        )
        const dData = dstImgData.data

        const range = [-2, -1, 0, 1, 2, 3]

        let pos = 0
        for (let dy = 0; dy < this.resizeHeight; dy++) {
            const sy = dy / this.rate
            const rangeY = range.map((i) => i + Math.floor(sy))

            for (let dx = 0; dx < this.resizeWidth; dx++) {
                const sx = dx / this.rate
                const rangeX = range.map((i) => i + Math.floor(sx))

                let r = 0,
                    g = 0,
                    b = 0
                for (const y of rangeY) {
                    const weightY = this.getWeightByLanczos(y, sy)
                    for (const x of rangeX) {
                        const weight = weightY * this.getWeightByLanczos(x, sx)
                        if (weight === 0) {
                            continue
                        }

                        const color: ColorPropertyType = this.rgb(x, y)
                        r += color.r * weight
                        g += color.g * weight
                        b += color.b * weight
                    }
                }

                dData[pos++] = Math.floor(r)
                dData[pos++] = Math.floor(g)
                dData[pos++] = Math.floor(b)
                dData[pos++] = 255
            }
        }

        return dc.toDataURL('image/jpg')
    }

    /**
     * 画像読み込み
     * @returns Promise<HTMLImageElement>
     */
    private async loadImage(): Promise<HTMLImageElement> {
        return new Promise((resolve, reject) => {
            const _img = new Image()
            _img.onload = () => resolve(_img)
            _img.onerror = () => reject('Image Load Error')
            _img.src = this.Image
        })
    }

    /**
     * Bycubic法による重み計算
     * @param t1 number
     * @param t2 number
     * @returns number
     */
    private getWeightByBicubic(t1: number, t2: number): number {
        const a = -1
        const d = Math.abs(t1 - t2)
        if (d < 1) {
            return (a + 2) * Math.pow(d, 3) - (a + 3) * Math.pow(d, 2) + 1
        } else if (d < 2) {
            return (
                a * Math.pow(d, 3) - 5 * a * Math.pow(d, 2) + 8 * a * d - 4 * a
            )
        } else {
            return 0
        }
    }

    /**
     * Lanczos法による重み計算
     * @param t1 number
     * @param t2 number
     * @returns number
     */
    private getWeightByLanczos(t1: number, t2: number): number {
        const sinc = (t: number) => {
            return Math.sin(t * Math.PI) / (t * Math.PI)
        }
        const d = Math.abs(t1 - t2)

        if (d === 0) return 1
        else if (d < 3) return sinc(d) * sinc(d / 3)
        else return 0
    }

    /**
     * 
     * @param img string
     * @returns Promise<void>
     */
    private async getImageSize(img: string): Promise<void> {
        const _img = new Image();
        _img.onload = () => {
            this.setimageSize(_img.naturalWidth, _img.naturalHeight);
        };
        _img.src = img
    }

    private rgb(x: number, y: number): ColorPropertyType {
        if (this.ImageData === undefined) {
            return { r: 0, g: 0, b: 0 }
        }
        x = x < 0 ? 0 : x < this.originalWidth ? x : this.originalWidth - 1
        y = y < 0 ? 0 : y < this.originalHeight ? y : this.originalHeight - 1
        const p = (this.originalWidth * y + x) * 4
        return {
            r: this.ImageData[p],
            g: this.ImageData[p + 1],
            b: this.ImageData[p + 2]
        };
    }
}
