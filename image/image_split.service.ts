import { ImageHelper } from './helper/image.helper'

export class ImageSplitService {
    private static instance: ImageSplitService

    private originalWidth = 0
    private originalHeight = 0
    private splitWidth = 0
    private splitHeight = 0

    private ih: ImageHelper

    public constructor() {
        this.ih = new ImageHelper()
    }

    public static call(): ImageSplitService {
        if (!ImageSplitService.instance) {
            ImageSplitService.instance = new ImageSplitService()
        }
        return ImageSplitService.instance
    }

    /**
     * 分割後の画像のサイズ
     * [横幅、縦幅]
     * @returns [width, height]
     */
    public getSplitSize(): number[] {
        return [this.splitWidth, this.splitHeight]
    }

    /**
     * ImageタグのID、HTMLImageElement、dataURIいずれかを設定
     * @param img HTMLImageElement | string
     * @returns Promise<ImageSplitService>
     */
    public async setImage(
        img: HTMLImageElement | string
    ): Promise<ImageSplitService> {
        await this.ih.setImage(img)
        this.getImageSize()
        return this
    }

    /**
     * 画像を4分割する
     * 縦横をピクセルを2で割った数値で分割するため
     * ピクセル数が奇数の場合サイズが完全一致しない。
     * @returns Promise<string[]> 分割画像を文字列化した配列
     */
    public async splitQuatro(): Promise<Uint8ClampedArray[]> {
        const w = Math.floor(this.originalWidth / 2)
        const h = Math.floor(this.originalHeight / 2)
        const i: Uint8ClampedArray[] = []

        const oc = this.ih.getNewCanvas(w, h)
        const ctx: CanvasRenderingContext2D | null = oc.getContext('2d')
        if (ctx === null) return []

        const im: HTMLImageElement = this.ih.getImageForElement()

        ctx.drawImage(im, 0, 0, w, h, 0, 0, w, h)
        const imageData1 = ctx.getImageData(0, 0, w, h)
        i.push(imageData1.data)

        ctx.drawImage(im, w, 0, w, h, 0, 0, w, h)
        const imageData2 = ctx.getImageData(0, 0, w, h)
        i.push(imageData2.data)

        ctx.drawImage(im, 0, h, w, h, 0, 0, w, h)
        const imageData3 = ctx.getImageData(0, 0, w, h)
        i.push(imageData3.data)

        ctx.drawImage(im, w, h, w, h, 0, 0, w, h)
        const imageData4 = ctx.getImageData(0, 0, w, h)
        i.push(imageData4.data)

        this.splitWidth = w
        this.splitHeight = h
        return i
    }

    /**
     * パスで画像を分割
     * @param path number[][]
     * @returns
     */
    public async splitPath(path: number[][]): Promise<number[]> {
        const oc = this.ih.getNewCanvas(
            this.originalWidth,
            this.originalHeight
        );
        const ctx = oc.getContext('2d') as CanvasRenderingContext2D

        const im = this.ih.getImageForElement()

        ctx.beginPath()
        ctx.moveTo(path[0][0], path[0][1])
        for (let i = 1; i <= path.length; i++) {
            ctx.lineTo(path[i][0], path[i][1])
        }
        ctx.closePath()
        ctx.clip()
        ctx.drawImage(
            im, 0, 0, this.originalWidth, this.originalHeight,
            0, 0, this.originalWidth, this.originalHeight
        )
        const imdata: ImageData = ctx.getImageData(
            0,
            0,
            this.originalWidth,
            this.originalHeight
        )
        return imdata.data as unknown as number[]
    }

    /**
     * 画像データの縦横サイズを取得
     * @param img string 画像データ
     * @returns Promise<void>
     */
    private async getImageSize(): Promise<void> {
        const size: {
            width: number
            height: number
        } = this.ih.getImageSize()
        this.originalWidth = size.width
        this.originalHeight = size.height
        return
    }
}
