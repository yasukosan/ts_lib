import { ImageHelper } from './helper/image.helper'

interface rectMap {
    name: string
    start_x: number
    start_y: number
    end_x: number
    end_y: number
}

export type TextType = {
    font: string
    size: number
    color: string
    x: number
    y: number
    text: string
}

export class ImagePaintService {
    private static instance: ImagePaintService

    private Image = ''
    private ImageSize: { width: number; height: number } = {
        width: 0,
        height: 0
    };
    private PaintCanvas: HTMLCanvasElement | undefined = undefined
    private RectMap: rectMap[] = []

    private PaintImage = ''

    public static call(): ImagePaintService {
        if (!ImagePaintService.instance) {
            ImagePaintService.instance = new ImagePaintService()
        }
        return ImagePaintService.instance
    }

    public setImage(image: string): ImagePaintService {
        this.Image = image
        return this
    }

    /**
     * 枠を塗る座標を設定
     * @param map PaintMap[]
     * @returns
     */
    public setRectMap(map: rectMap[]): ImagePaintService {
        this.RectMap = map
        return this
    }

    /**
     * 画像の所定位置に枠を描く
     */
    public async makeRectImage(): Promise<void> {
        await this.buildCanvas()
        this.PaintImage = await this.paintRect()
    }

    /**
     * 画像の所定位置にパス画像を描く
     * @returns
     */
    public async makePathImage(): Promise<void> {
        await this.buildCanvas()
        this.PaintImage = await this.paintPath()
    }

    public getPaintImage(): string {
        return this.PaintImage
    }

    /**
     * 新規Cavnasの作成
     * 画像データをCanvasで編集可能な形式に変換
     * Canvas上に先に画像を貼り付け
     *
     * @return Promise<void>
     */
    private async buildCanvas(): Promise<void> {
        await ImageHelper.call().setImageURL(this.Image)
        this.ImageSize = ImageHelper.call().getImageSize()
        this.PaintCanvas = ImageHelper.call().getNewCanvas(
            this.ImageSize.width,
            this.ImageSize.height
        ) as HTMLCanvasElement
        return
    }

    private paintRect(): string {
        if (this.PaintCanvas === undefined) {
            return ''
        }
        const ctx = this.PaintCanvas.getContext(
            '2d'
        ) as CanvasRenderingContext2D;

        ctx.drawImage(
            ImageHelper.call().getImageForElement(),
            0,
            0,
            this.ImageSize.width,
            this.ImageSize.height
        )

        for (const key in this.RectMap) {
            if (Object.prototype.hasOwnProperty.call(this.RectMap, key)) {
                ctx.beginPath()
                ctx.font = 'bold 12px Arial, meiryo, sans-serif'
                ctx.fillStyle = 'rgba( 255, 255, 0, 0.8 )'
                ctx.fillText(
                    this.RectMap[key].name,
                    this.RectMap[key].start_x,
                    this.RectMap[key].start_y - 20,
                    200
                )
                ctx.rect(
                    this.RectMap[key].start_x,
                    this.RectMap[key].start_y,
                    this.RectMap[key].end_x,
                    this.RectMap[key].end_y
                );
                ctx.strokeStyle = 'white'
                ctx.lineWidth = 2
                ctx.stroke()
            }
        }
        return this.PaintCanvas.toDataURL('Image/jpeg')
    }

    private paintPath(): string {
        console.log(this.RectMap)
        if (this.PaintCanvas === undefined) return ''

        const ctx = this.PaintCanvas.getContext(
            '2d'
        ) as CanvasRenderingContext2D

        ctx.drawImage(
            ImageHelper.call().getImageForElement(),
            0,
            0,
            this.ImageSize.width,
            this.ImageSize.height
        )

        for (const key in this.RectMap) {
            if (Object.prototype.hasOwnProperty.call(this.RectMap, key)) {
                ctx.beginPath()
                ctx.moveTo(
                    this.RectMap[key].start_x,
                    this.RectMap[key].start_y
                )
                ctx.lineTo(this.RectMap[key].end_x, this.RectMap[key].end_y)
                ctx.strokeStyle = 'red'
                ctx.lineWidth = 10
                ctx.stroke()
            }
        }
        return this.PaintCanvas.toDataURL('Image/jpeg')
    }
}
