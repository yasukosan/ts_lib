export interface HTMLEvent<T extends EventTarget> extends Event {
    target: T
}


export class ImageHelper {
    private static instance: ImageHelper

    private Image: HTMLImageElement | undefined = undefined

    private originalWidth = 0
    private originalHeight = 0

    public static call(): ImageHelper {
        if (!ImageHelper.instance) {
            ImageHelper.instance = new ImageHelper();
        }
        return ImageHelper.instance
    }

    /**
     * 画像をセット
     * ImageタグのID、HtmlImageElement、dataURLどれでも可
     *
     * @param img any (ImageタグのID(#除外)、HTMLImageElement、dataURL)
     * @returns Promise<ImageHelper>
     */
    public async setImage(
        img: string | HTMLImageElement
    ): Promise<ImageHelper> {
        // imageタグのID、画像文字列の場合
        if (typeof img === 'string') {
            this.Image = img.match(/^data:image/)
                ? await this.loadImage(img)
                : (document.getElementById('#' + img) as HTMLImageElement)
        } else {
            this.Image = img
        }
        this.saveImageSize()
        return this
    }

    /**
     * 画像文字列をセット
     * @param img string 画像文字列
     * @returns Promise<ImageHelper>
     */
    public async setImageURL(img: string): Promise<ImageHelper> {
        this.Image = await this.loadImage(img)
        this.saveImageSize()
        return this
    }

    /**
     * 読み込んだ画像情報取得
     * @returns HTMLImageElement
     */
    public getImageForElement(): HTMLImageElement {
        return (this.Image === undefined)
                ? new Image()
                : this.Image
    }

    /**
     * 画像のdataURLを取得
     * 画像フォーマットは、デフォルト「'image/jpeg'」
     *
     * @param type string(image/jpeg, image.png,...etc)
     * @returns string
     */
    public getImageForString(type = 'image/jpeg'): string {

        if (this.Image === undefined) return 'Image Error'

        const canvas: HTMLCanvasElement = this.getNewCanvas(
            this.originalWidth,
            this.originalHeight
        )
        const ctx: CanvasRenderingContext2D | null = canvas.getContext('2d')
        if (ctx === null) return 'Get Context Error'
        
        ctx.drawImage(this.Image, 0, 0)
        return canvas.toDataURL(type)
    }

    /**
     * 画像のサイズ情報を取得
     * @returns object {width: number, height: number}
     */
    public getImageSize(): { width: number; height: number } {
        return {
            width: this.originalWidth,
            height: this.originalHeight
        }
    }

    /**
     * 作業用HTMLCanvasElementを作成
     * @param width number
     * @param height number
     * @returns HTMLCanvasElement
     */
    public getNewCanvas(width: number, height: number): HTMLCanvasElement {
        const oc: HTMLCanvasElement = document.createElement('canvas')
        oc.setAttribute('width', width.toString())
        oc.setAttribute('height', height.toString())

        return oc
    }

    /**
     * 画像データをHtmlImageElementに変換
     * @param img string
     * @returns Promise<HTMLImageElement>
     */
    public async loadImage(img: string): Promise<HTMLImageElement> {
        return new Promise((resolve, reject) => {
            const _img = new Image()
            _img.onload = () => resolve(_img)
            _img.onerror = () => reject('Image Load Error')
            _img.src = img
        });
    }

    /**
     * 画像データの縦横サイズを取得
     * @param img string 画像データ
     * @returns Promise<void>
     */
    private saveImageSize(): ImageHelper {
        if (this.Image === undefined) {
            return this
        }
        this.originalWidth = this.Image.naturalWidth
        this.originalHeight = this.Image.naturalHeight
        return this
    }
}
