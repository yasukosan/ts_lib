
export type TextType = {
    x: number
    y: number
    size: number
    font: string
    text: string
}

export type ImageType = {
    x: number
    y: number
    width: number
    height: number
    image: string
}

export class SvgBuilderService {
    /**
     * 印刷サイズ
     * A4を350dpiで出力する場合
     * 横2894px、縦4093px必要
     * 倍率は13.78095倍
     */
    private sheetSize = {
        width: 210,
        height: 297
    }

    private printImage: SVGSVGElement = new SVGSVGElement()
    private svgTemplate = ''
    private images: ImageType[] = []
    private texts: TextType[] = []

    private resulution = 13.78095
    private sheetImage: undefined
    private prevewImage: undefined

    public setSheetSize(width = 0, height = 0): SvgBuilderService {
        this.sheetSize.width = width > 0 ? width : 210
        this.sheetSize.height = height > 0 ? height : 297
        return this
    }

    public setResulution(magnification: number): SvgBuilderService {
        this.resulution = magnification
        return this
    }

    public setSvgTemplate(template: string): SvgBuilderService {
        this.svgTemplate = template
        return this
    }

    public setImageData(images: ImageType[]): SvgBuilderService {
        this.images = images
        return this
    }

    public setTextData(texts: TextType[]): SvgBuilderService {
        this.texts = texts
        return this
    }

    public getSvg(): SVGSVGElement {
        return (this.printImage === undefined)
            ? document.createElementNS('http://www.w3.org/2000/svg', 'svg')
            : this.printImage
    }

    private async svgBuilder(): Promise<void> {
        const sv: SVGSVGElement  = document.createElementNS<'svg'>(
            'http://www.w3.org/2000/svg',
            'svg'
        )
        sv.setAttribute('width', String(this.sheetSize.width))
        sv.setAttribute('height', String(this.sheetSize.height))
        sv.setAttribute(
            'viewBox',
            '0 0 ' + this.sheetSize.width + ' ' + this.sheetSize.height
        )

        if (this.svgTemplate !== '') {
            sv.appendChild(this.buildTemplateContent())
        }

        if (Array.isArray(this.images) === false) {
            this.buildImageContent().forEach((i: SVGImageElement) => {
                sv.appendChild(i)
            })
        }

        if (Array.isArray(this.texts) !== false) {
            this.buildTextContent().forEach((t: SVGTextElement) => {
                sv.appendChild(t)
            })
        }
        this.printImage = sv
    }

    private buildTemplateContent(): SVGImageElement {
        const tm: SVGImageElement  = document.createElementNS<'image'>(
            'http://www.w3.org/2000/svg',
            'image'
        );
        tm.setAttribute('x', '0')
        tm.setAttribute('y', '0')
        tm.setAttribute('width', String(this.sheetSize.width))
        tm.setAttribute('height', String(this.sheetSize.height))
        tm.setAttribute('href', this.svgTemplate)
        return tm
    }

    private buildTextContent(): SVGTextElement[] {
        if (this.texts.length <= 1) {
            return new Array<SVGTextElement>()
        }
        const _lists: SVGTextElement[] = this.texts.map((
                t: TextType
        ): SVGTextElement => {
            const txt: SVGTextElement = document.createElementNS<'text'>(
                'http://www.w3.org/2000/svg',
                'text'
            )
            txt.setAttribute('x', this.doEnlargement(t.x))
            txt.setAttribute('y', this.doEnlargement(t.y))
            txt.setAttribute('fontSize', t.size.toString())
            txt.setAttribute('fontFamily', t.font)
            txt.setAttribute('stroke', 'black')
            txt.setAttribute('textAnchor', 'Super Sans')
            txt.setAttribute('strokeWidth', '0.3')
            txt.textContent = t.text
            return txt
        });

        return _lists
    }

    private buildImageContent(): SVGImageElement[] {
        if (this.images[0].image === '') {
            return new Array<SVGImageElement>()
        }
        const _lists: SVGImageElement[] = this.images.map((i: ImageType) => {
            const im = document.createElementNS<'image'>(
                'http://www.w3.org/2000/svg',
                'image'
            );
            im.setAttribute('x', this.doEnlargement(i.x))
            im.setAttribute('y', this.doEnlargement(i.y))
            im.setAttribute('width', this.doEnlargement(i.width))
            im.setAttribute('height', this.doEnlargement(i.height))
            im.setAttribute('href', i.image)
            return im
        })
        //return _lists.join(' ')
        return _lists
    }

    /**
     * 拡大処理
     */
    private doEnlargement(val: number): string {
        return val * this.resulution + '';
    }
}
