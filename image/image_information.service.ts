import { ImageHelper } from "./helper/image.helper"

export class ImageInformationService {
    private static instance: ImageInformationService

    public static call(): ImageInformationService {
        if (!ImageInformationService.instance) {
            ImageInformationService.instance = new ImageInformationService()
        }

        return ImageInformationService.instance
    }

    public async set(
        image: string | HTMLImageElement
    ): Promise<ImageInformationService> {
        await ImageHelper.call().setImage(image)
        return this
    }

    public getSize(): { width: number; height: number } {
        return ImageHelper.call().getImageSize()
    }

    public getImageForElement(): HTMLImageElement {
        return ImageHelper.call().getImageForElement()
    }

    public getImageDataURL(): string {
        return ImageHelper.call().getImageForString()
    }
}

