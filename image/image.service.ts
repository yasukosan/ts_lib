import { ImageInformationService as IIS } from "./image_information.service"
import { ImageLoadingService as ILS } from "./image_loading.service"
import { ImageMakerService as IMS } from "./image_maker.service"
import { ImageOrientationService  as IOS } from "./image_orientation.service"
import { ImagePaintService as IPS } from "./image_paint.service"
import { ImageResizeService as IRS } from "./image_resize.service"
import { ImageSplitService as ISS } from "./image_split.service"
import { ImageStreamService as IStS } from "./image_stream.service"

export class ImageInformationService extends IIS {}
export class ImageLoadingService extends ILS {}
export class ImageMakerService extends IMS {}
export class ImageOrientationService extends IOS {}
export class ImagePaintService extends IPS {}
export class ImageResizeService extends IRS {}
export class ImageSplitService extends ISS {}
export class ImageStreamService extends IStS {}


