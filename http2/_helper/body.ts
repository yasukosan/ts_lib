export type BodyType = {
    type: string
    message: any
}

export const BodyInitial: BodyType = {
    type: '',
    message: {}
};

/**
 * レスポンスのBodyデータを管理
 *
 * Bodyデータ自体がストリームデータになる
 * 別処理を間に挟むとデータ自体が取得できなくなるため
 * オーバーライドする場合はデータの取得順に注意
 */

let Body: any = undefined
let _Body: BodyType = BodyInitial

/**
 * Bodyデータを設定
 *
 * @param body any
 * @return Promise<void>
 */
export const setBody = async (
    body: any
): Promise<void> => {
    // 特定のメソッド通信の場合Bodyが空になるので
    // Jsonエンコードエラーを拾って処理する
    try {
        await body
            .clone()
            .json()
            .then((res: any) => {
                Body = res
            })
    } catch (error: any) {
        const reader = await body.body.getReader()
        const res = await reader.read()
        Body = new TextDecoder().decode(Uint8Array.from(res.value))
    }
    return
}

/**
 * パース処理済みのBodyデータを返す
 */
export const getParseBody = (): BodyType => {
    return _Body
}

export const parse = (job: string): void => {
    if (Body === undefined) return
    if (job === 'normal') buildNormalBody()
    if (job === 'token') buildTokenBody()
    if (job === 'success') buildSuccessBody()
    if (job === 'error') buildErrorBody()
    return
}

export const reset = (): void => {
    Body = undefined
    _Body = BodyInitial
    return
}

const buildNormalBody = (): void => {
    if (Body.statusText === 'OK' || Body.status === 200 || Body.success === true) {
        _Body.type = 'success'
    } else {
        _Body.type = 'error'
    }
    _Body.message = Body
    return
}

const buildTokenBody = (): void => {
    _Body.type = 'success'
    _Body.message = Body
    return
}

const buildSuccessBody = (): void => {
    _Body.type = 'success'
    _Body.message = Body
    return
}

const buildErrorBody = (): void => {
    _Body.type = 'error'
    _Body.message = Body
    return this;
}

