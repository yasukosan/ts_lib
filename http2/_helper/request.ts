export type RequestType = {
    url: string
    method: 'GET' | 'POST' | 'DELETE' | 'UPDATE' | 'PUT' | 'PATCH'
    header: { [key: string]: string }
    body?: string | {[key: string]: string | number | boolean} | null
    csrf?: string
    bearer?: string
}

export const RequestHeaderInitial = {
    'content-type': 'application/json',
}

export const RequestInitial: RequestType = {
    url: '',
    method: 'POST',
    header: RequestHeaderInitial,
    body: null,
    csrf: '',
    bearer: ''
}



/**
 * 送信リクエスト管理
 */

// サーバーリクエストに必要なすべての情報を格納
let request: RequestType = RequestInitial
// JSON文字列に変換したBodyデータを格納

/**
 * サーバーリクエスト情報を設定する
 *
 * @param request RequestInterface
 * @return RequestService
 */
export const setRequest = (_request: Partial<RequestType>): void => {
    request = Object.assign(request, _request)
    return
}


export const setOptions = (options:Partial<RequestType>): void => {
    request.url = options.url ?? request.url
    request.method = options.method ?? request.method
    request.header = options.header ?? request.header
    request.body = options.body ?? request.body
    request.csrf = options.csrf ?? request.csrf
    request.bearer = options.bearer ?? request.bearer
    return
}

/**
 * 設定された全てのリクエストデータを返す
 *
 * @return RequestInterface
 */
export const getAllRequest = (): RequestType => {
    return request
}

/**
 * リクエストデータをイニシャルデータで上書き
 *
 * @return RequestService
 */
export const reset = (): void => {
    request = RequestInitial
    return
}

/**
 * ヘッダー、送信データをHttpオプション形式に変換
 *
 * @return RequestService
 */
export const buildRequestParam = (): void => {
    // トークンをヘッダーに追加
    attachToken()
    // ボディを送信用に整形
    buildSendDataToJson()

    if (request.method === 'GET') {
        request.body = jsonToURLParam(request.body as string)
    }

    return
}

/**
 * ヘッダー、送信データをHttpオプション形式に変換
 * HTMLフォーム形式で送信する場合
 * @return RequestService
 */
export const buildFormParam = (): void => {

    // トークンをヘッダーに追加
    attachToken()
    // ボディを送信用に整形
    //request.body = request.body

    return
}

/**
 * 送信オプションの作成
 *
 * @return object
 */
export const buildOption = (): object => {
    const option: Option = {
        method: request.method,
        headers: request.header
    }
    if (request.method !== 'GET')
        option.body = request.body

    return option
}

/**
 * URLをメソッドに合わせて変換
 *
 * @return string
 */
export const buildURL = (): string => {
    if (request.method === 'GET') {
        const body = (typeof request.body === 'object')
                        ? jsonToURLParam(JSON.stringify(request.body))
                        : request.body
        return request.url + body
    }
    return request.url;
}

export const run = async (): Promise<Response | false> => {
    return fetch(buildURL(), buildOption())
        .then((data) => {
            return data
        })
        .catch((error) => {
            console.error('Error:', error)
            return false
        })
}

/**
 * JSONデータをURLリクエストに変換
 *
 * @param json Json文字列
 */
const jsonToURLParam = (json: string): string => {
    if (json.length < 5 || json === undefined) {
        return ''
    }
    const _json = JSON.parse(json);
    let param = '?'
    for (const key in _json) {
        if (key in _json) {
            param = param + key + '=' + _json[key] + '&'
        }
    }
    return param
}

/**
 * フォームのデータをJSONに変換
 */
const buildSendDataToJson = (): void => {
    const ch = Object.prototype.toString
    const w = ch.call(request.body).slice(8, -1).toLowerCase()
    let pdata = request.body
    if (w !== 'array' && w !== 'object') {
        pdata = parseFactory('body')
    }
    request.body = JSON.stringify(pdata)
}

/**
 * Body、Header情報を所定のオブジェクト形式に変換
 *
 * @param target string
 * @return any
 */
const parseFactory = (
    target: keyof RequestType
): {[key: string]: string | number} => {
    if (target in request === false) return {}

    const delimita = target === 'header' ? ':' : '='
    const factoryObj: {[key: string]: string | number} = {}

    // 配列・オブジェクト以外の場合は改行で分割
    const _obj = request[target] as {[key: string]: string | number}
    
    for (const key in _obj) {
        if (key in _obj) {
            const val = (_obj[key] as string).split(delimita)
            factoryObj[val[0]] = val[1]
        }
    }
    return factoryObj
}

/**
 * トークンが設定されている場合に、トークン情報を追加
 * @returns boolean
 */
const attachToken = (): boolean => {
    if (request.csrf === '' && request.bearer === '') 
        return false

    if (request.csrf !== undefined && request.csrf !== '')
        request.header['X-XSRF-TOKEN'] = request.csrf
    
    if (request.bearer !== undefined && request.bearer !== '')
        request.header['Authorization'] = 'Bearer ' + request.bearer
    
    return true
}


export type Option = {
    method: string
    headers: { [key: string]: string }
    body?: string | {[key: string]: string | number | boolean} | null
}
