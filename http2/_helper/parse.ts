import * as HEADER from './header'
import * as RES from './response'
import * as BODY from './body'

/**
 * 非同期通信、APIの戻り値を固定形式に変換する
 */

/**
 * APIからの戻りデータを受け取る
 * HTTPステータスコードに依存しない
 *
 * @param result json object
 * @return AjaxParseHelper
 */
export const setResult = async (
    result: Response
): Promise<void> => {
    
    HEADER.setHeaders(result.headers)
    RES.setResponse(result)
    await BODY.setBody(result)
    BODY.parse('normal')
    return
}

/**
 * サーバーレスポンスのBody要素を返す
 *
 * @return object
 */
export const getBody = (): object => {
    return BODY.getParseBody()
}

/**
 * サーバーレスポンスのヘッダー要素を返す
 *
 * @return ResponseHeadersType
 */
export const getHeader = (): Headers => {
    return HEADER.getAllHeaders()
}

/**
 * サーバーレスポンスのプロパティを返す
 *
 * @return ResponseInterface
 */
export const getResponse = (): Response => {
    return RES.getAllResponse()
}

/**
 * サーバーレスポンスのステータスコードを返す
 *
 * @return number
 */
export const getStatusCode = (): number => {
    return RES.getStatusCode()
}

/**
 * Responseデータを全てイニシャルデータで埋める
 *
 * @return this
 */
export const allReset = (): void => {
    RES.reset()
    BODY.reset()
    HEADER.reset()
    return
}

