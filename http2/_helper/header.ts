

/**
 * レスポンスヘッダー情報を管理
 */

let headers: Headers = new Headers()

/**
 * ヘッダー情報を設定
 * ResponceHeaderInterfaceに含まれないデータは無視
 *
 * @param header any
 * @return this
 */
export const setHeaders = (header: Headers): void => {
    console.log(header.get('Uza-Token'))
    headers = header
    return this;
}

/**
 * ヘッダーAPIを返す
 *
 * @return ResponceHeadersInterface
 */
export const getAllHeaders = (): Headers => {
    return headers
}

/**
 * ヘッダーの一致するKey情報を返す
 * ResponceHeadersInterfaceに含まれるデータのみ
 *
 * @param key string
 * @return object
 */
export const getHeader = (key: string): {[key: string]: string} => {
    const r = headers.get(key)

    return r ? { key: r } : {}
}

/**
 * ヘッダーにKeyが含まれるか判定
 *
 * @param key string
 * @return boolean
 */
export const checkHeader = (key: string): boolean => {
    return headers.has(key)
}

/**
 * hedersを初期化
 *
 * @return this
 */
export const reset = (): void => {
    headers = new Headers()
    return
}

