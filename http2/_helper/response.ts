export const ResponseInitial: Response = new Response('', {
    status: 200,
    statusText: '',
})

/**
 * サーバーレスポンスの管理サービス
 *
 * body header部分は含まない
 *
 */
let response: Response = ResponseInitial;

/**
 * サーバーレスポンスを設定
 * Body、headerは受け取らない
 *
 * @param response any
 * @return this
 */
export const setResponse = (_response: Response): void => {
    response = _response
    return this
}

/**
 * レスポンスデータを全て返す
 *
 * @return Response
 */
export const getAllResponse = (): Response => {
    return response
}

export const getStatusCode = (): number => {
    return response.status
}

/**
 * レスポンスデータをイニシャルデータで埋める
 *
 * @return this
 */
export const reset = (): void => {
    Object.assign(response, ResponseInitial)
    return
}

