import { describe, it, expect, beforeEach } from 'vitest'
import { setBody, getParseBody, parse, reset, BodyInitial, BodyType } from '../_helper/body' // 適切なパスに置き換えてください

describe('Bodyデータ管理ライブラリのテスト', () => {
    beforeEach(() => {
        reset()
    })

    it('Bodyデータを設定できることを確認する', async () => {
        const mockBody = {
        clone: () => ({
            json: () => Promise.resolve({ status: 200, statusText: 'OK' })
        })
        }
        await setBody(mockBody)
        expect(getParseBody()).toEqual(BodyInitial)
    })

    it('BodyデータがJSONエンコードエラーの場合に処理できることを確認する', async () => {
        const mockBody = {
        body: {
            getReader: () => ({
            read: () => Promise.resolve({ value: new TextEncoder().encode('エラーメッセージ') })
            })
        }
        }
        await setBody(mockBody)
        expect(getParseBody()).toEqual(BodyInitial)
    })

    it('normalジョブで正常なBodyデータをパースできることを確認する', async () => {
        const mockBody = {
        clone: () => ({
            json: () => Promise.resolve({ status: 200, statusText: 'OK' })
        })
        }
        await setBody(mockBody)
        parse('normal')
        expect(getParseBody()).toEqual({ type: 'success', message: { status: 200, statusText: 'OK' } })
    })

    it('tokenジョブで正常なBodyデータをパースできることを確認する', async () => {
        const mockBody = {
        clone: () => ({
            json: () => Promise.resolve({ token: 'abc123' })
        })
        }
        await setBody(mockBody)
        parse('token')
        expect(getParseBody()).toEqual({ type: 'success', message: { token: 'abc123' } })
    })

    it('successジョブで正常なBodyデータをパースできることを確認する', async () => {
        const mockBody = {
        clone: () => ({
            json: () => Promise.resolve({ data: 'success' })
        })
        }
        await setBody(mockBody)
        parse('success')
        expect(getParseBody()).toEqual({ type: 'success', message: { data: 'success' } })
    })

    it('errorジョブでエラーのBodyデータをパースできることを確認する', async () => {
        const mockBody = {
        clone: () => ({
            json: () => Promise.resolve({ error: 'error message' })
        })
        }
        await setBody(mockBody)
        parse('error')
        expect(getParseBody()).toEqual({ type: 'error', message: { error: 'error message' } })
    })

    it('resetメソッドがBodyデータを初期化することを確認する', () => {
        const mockBody: BodyType = { type: 'test', message: 'test message' }

        reset()
        expect(getParseBody()).toEqual(BodyInitial)
    })
})