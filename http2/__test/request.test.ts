import { describe, it, expect, beforeEach } from 'vitest'
import {
    setRequest,
    setOptions,
    getAllRequest,
    reset,
    buildRequestParam,
    buildFormParam,
    buildOption,
    buildURL,
    RequestInitial,
    RequestType
} from '../_helper/request' // 適切なパスに置き換えてください

describe('リクエスト管理サービスのテスト', () => {
    beforeEach(() => {
        reset()
    })

    it('リクエスト情報を設定できることを確認する', () => {
        const mockRequest: Partial<RequestType> = {
            url: 'https://example.com',
            method: 'POST',
            header: { 'Content-type': 'application/json' },
            body: { key: 'value' },
            csrf: 'csrf-token',
            bearer: 'bearer-token'
        }
        setRequest(mockRequest)
        expect(getAllRequest()).toEqual({
            ...RequestInitial,
            ...mockRequest
        })
    })

    it('オプションを設定できることを確認する', () => {
        const options: Partial<RequestType> = {
            url: 'https://example.com',
            method: 'POST',
            header: { 'Content-type': 'application/json' },
            body: { key: 'value' },
            csrf: 'csrf-token',
            bearer: 'bearer-token'
        }
        setOptions(options)
        expect(getAllRequest()).toEqual({
            ...RequestInitial,
            ...options
        })
    })

    it('全てのリクエストデータを取得できることを確認する', () => {
        const requestData = getAllRequest()
        expect(requestData).toEqual(RequestInitial)
    })

    it('リクエストデータを初期化できることを確認する', () => {
        const mockRequest: Partial<RequestType> = {
            url: 'https://example.com',
            method: 'POST',
            header: { 'Content-type': 'application/json' },
            body: { key: 'value' },
            csrf: 'csrf-token',
            bearer: 'bearer-token'
        }
        setRequest(mockRequest)
        reset()
        expect(getAllRequest()).toEqual(RequestInitial)
    })

    it('リクエストパラメータを構築できることを確認する', () => {
        const mockRequest: Partial<RequestType> = {
            method: 'POST',
            body: { key: 'value' }
        }
        setRequest(mockRequest)
        buildRequestParam()
        expect(getAllRequest().body).toEqual(JSON.stringify({ key: 'value' }))
    })

    it('フォームパラメータを構築できることを確認する', () => {
        const mockRequest: Partial<RequestType> = {
            body: { key: 'value' },
            header: { 'content-type': 'multipart/form-data' }
        }
        setRequest(mockRequest)
        buildFormParam()
        expect(getAllRequest().method).toBe('POST')
        expect(getAllRequest().header['content-type']).toBe('multipart/form-data')
    })

    it('送信オプションを作成できることを確認する', () => {
        const mockRequest: Partial<RequestType> = {
            method: 'POST',
            body: { key: 'value' },
            header: { 'content-type': 'application/json' }
        }
        setRequest(mockRequest)
        const options = buildOption()
        expect(options).toEqual({
            method: 'POST',
            headers: {
                'content-type': 'application/json',
            },
            body: { key: 'value' }
        })
    })

    it('URLを構築できることを確認する', () => {
        const mockRequest: Partial<RequestType> = {
            url: 'https://example.com',
            method: 'GET',
            body: { key: 'value' }
        }
        setRequest(mockRequest)
        const url = buildURL()
        expect(url).toBe('https://example.com?key=value&')
    })
})