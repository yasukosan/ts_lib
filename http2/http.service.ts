import * as PARSE from './_helper/parse'
// import * as RES from './_helper/response'
import * as REQ from './_helper/request'

export class HttpService {

    private static instance: HttpService

    public static call(): HttpService {
        if (!HttpService.instance)
            HttpService.instance = new HttpService()
        return HttpService.instance
    }

    private result: any = {}

    public _result(): any {
        return this.result
    }

    /**
     * URLを設定
     *
     * @param url string
     */
    public setURL(url: string): HttpService {
        REQ.setOptions({url: url})
        return this
    }

    /**
     * メソッドを設定
     *
     * @param method 'GET' | 'POST' | 'DELETE' | 'UPDATE'
     */
    public setMethod(
        method: 'GET' | 'POST' | 'DELETE' | 'UPDATE' | 'PUT' | 'PATCH'
    ): HttpService {
        REQ.setOptions({method: method})
        return this
    }

    /**
     * トークンを設定
     * @param token string
     * @param type 'csrf' | 'bearer'
     * @returns HttpService
     */
    public setToken(token: string, type: 'csrf' | 'bearer'): HttpService {
        if (type === 'csrf') {
            REQ.setOptions({csrf: token})
        }
        if (type === 'bearer') {
            REQ.setOptions({bearer: token})
        }
        return this
    }

    /**
     * ヘッダーを設定
     *
     * @param header string
     */
    public setHeader(header: {[key:string]: string}): HttpService {
        REQ.setOptions({header: header})
        return this
    }

    /**
     * 送信データを設定
     *
     * @param body any
     */
    public setBody(body: any): HttpService {
        REQ.setOptions({body: body})
        return this
    }

    /**
     * ヘッダー、送信データをHttpオプション形式に変換
     */
    public buildRequestParam(type: 'api' | 'form' = 'api'): HttpService {
        if (type === 'api') {
            REQ.buildRequestParam();
        }
        if (type === 'form') {
            REQ.buildFormParam();
        }
        return this;
    }

    
    /**
     * HTTPのステータスコードを取得します。
     * 
     * @returns ステータスコードの数値
     */
    public getStatusCode(): number {
        return PARSE.getStatusCode()
    }

    public getHeader(key: string = ''): Headers | string {
        if (key !== '')
            return PARSE.getHeader().get(key) as string
        else
            return PARSE.getHeader()
    }

    /**
     * 通信結果の受け取り
     */
    public async getResult<T>(): Promise<T> {
        await this.run()
        return this.result
    }

    /**
     * サーバーレスポンスを初期化
     *
     * @return this
     */
    public resetResult(): HttpService {
        PARSE.allReset()
        return this
    }

    /**
     * サーバーリクエストを初期化
     *
     * @return this
     */
    public resetRequest(): HttpService {
        REQ.reset()
        return this
    }

    /**
     * サーバーにリクエストを投げる
     *
     * @return Promise<any>
     */
    private async run(): Promise<void | false> {
        const r = await REQ.run()
        if (r === false) {
            return false
        }
        await PARSE.setResult(r)
        this.result = PARSE.getBody()
    }
}
