

export class MouseHelper {
    private static instance: MouseHelper

    private startX: number = 0
    private startY: number = 0
    private moveX: number = 0
    private moveY: number = 0
    private offsetX: number = 0
    private offsetY: number = 0

    private diff: number = 50

    public static call(): MouseHelper {
        if (!MouseHelper.instance) {
            MouseHelper.instance = new MouseHelper()
        }

        return MouseHelper.instance
    }

    /**
     * マウスイベントの開始点を設定
     * @param e MouseEvent
     * @returns MouseHelper
     */
    public setStart(e: MouseEvent): MouseHelper {
        this.startX = e.offsetX - this.diff
        this.startY = e.offsetY - this.diff
        this.offsetX = e.offsetX - this.diff
        this.offsetY = e.offsetY - this.diff
        return this
    }

    /**
     * マウスイベントの移動点を設定
     * @param e MouseEvent
     * @returns MouseHelper
     */
    public setMove(e: MouseEvent): MouseHelper {
        //console.log(e.offsetX, e.offsetY)
        this.moveX = e.offsetX - this.diff
        this.moveY = e.offsetY - this.diff

        return this
    }

    public updateOffset(): MouseHelper {
        this.offsetX = this.moveX
        this.offsetY = this.moveY
        return this
    }

    /**
     * 開始点の座標を取得
     * @returns { x: number, y: number }
     */
    public getStart(): { x: number; y: number } {
        return {
            x: this.startX,
            y: this.startY
        }
    }

    /**
     * 移動点の座標を取得
     * @returns { x: number, y: number }
     */
    public getMove(): { x: number; y: number } {
        return {
            x: this.moveX,
            y: this.moveY
        }
    }

    /**
     * オフセット座標を取得
     * @returns { x: number, y: number }
     */
    public getOffset(): { x: number; y: number } {
        return {
            x: this.offsetX,
            y: this.offsetY
        }
    }
}