
export type DrawOptions = {
    lineColor: string,
    fillColor: string,
    width: number,
    height: number,
    lineWidth: number,
    scale: { x: number, y: number },
    ratio: number,
    eraser: boolean,
}
export class DrawHelper {
    private static instance: DrawHelper

    private paintTarget: HTMLCanvasElement | null = null
    private paintOption: DrawOptions = {
        lineColor: 'black',
        fillColor: 'black',
        width: 0,
        height: 0,
        lineWidth: 10,
        scale: { x: 1, y: 1 },
        ratio: 1,
        eraser: false
    }

    public static call(): DrawHelper {
        if (!DrawHelper.instance) {
            DrawHelper.instance = new DrawHelper()
        }

        return DrawHelper.instance
    }

    /**
     * Canvasをセット
     * @param paintTarget HTMLCanvasElement
     * @returns 
     */
    public setPaintTarget(paintTarget: HTMLCanvasElement): DrawHelper {
        this.paintTarget = paintTarget
        return this
    }

    public setPaintOption(
        option: Partial<DrawOptions>
    ): DrawHelper {
        this.paintOption = Object.assign(this.paintOption, option)
        return this
    }

    public getPaintOption(): DrawOptions {
        return this.paintOption
    }

    public getPaintTarget(): HTMLCanvasElement {
        return (this.paintTarget === null)
                ? document.createElement('canvas')
                : this.paintTarget
    }

    /**
     * 線を描画
     * @param move {x: number, y: number}
     * @param start {x: number, y: number}
     * @returns 
     */
    public paintPath(
        move: { x: number, y: number },
        start: { x: number, y: number },
    ): void {
        if (!this.paintTarget) return

        const ctx = this.paintTarget.getContext('2d') as CanvasRenderingContext2D
        if (!ctx) return

        if (this.paintOption.eraser) {
            ctx.globalCompositeOperation = 'destination-out'
        }

        ctx.beginPath()
        ctx.strokeStyle = this.paintOption.lineColor
        ctx.lineWidth = this.paintOption.lineWidth
        ctx.lineCap = 'round'
        ctx.lineJoin = 'round'
        ctx.moveTo(start.x * this.paintOption.ratio, start.y * this.paintOption.ratio)
        ctx.lineTo(move.x * this.paintOption.ratio, move.y * this.paintOption.ratio)
        ctx.stroke()
        return
    }

    /**
     * 円を描画
     * @param start {x: number, y: number}
     * @returns 
     */
    public paintArc(
        start: { x: number, y: number },
    ): void {
        if (!this.paintTarget) return

        const ctx = this.paintTarget.getContext('2d') as CanvasRenderingContext2D
        if (!ctx) return

        if (this.paintOption.eraser) {
            ctx.globalCompositeOperation = 'destination-out'
        }

        ctx.beginPath()
        ctx.strokeStyle = this.paintOption.lineColor
        ctx.fillStyle = this.paintOption.fillColor
        ctx.arc(
            start.x * this.paintOption.ratio, start.y * this.paintOption.ratio,
            this.paintOption.lineWidth / 2, 0, Math.PI * 2, false)
        ctx.fill()
        // ctx.stroke()
        return
    }

    /**
     * 四角形を描画
     * @param move {x: number, y: number}
     * @param start {x: number, y: number}
     * @param color string Default: 'black'
     * @param style fill or stroke or clear Default: 'fill'
     * @param lineWith number Default: 2
     */
    public paintRect(
        start: { x: number, y: number },
        move: { x: number, y: number },
        style: 'fill' | 'stroke' | 'clear' = 'fill',
    ): void {
        if (!this.paintTarget) return

        const ctx = this.paintTarget.getContext('2d') as CanvasRenderingContext2D
        if (!ctx) return

        ctx.beginPath()
        ctx.rect(
            start.x * this.paintOption.ratio, start.y * this.paintOption.ratio,
            move.x * this.paintOption.ratio, move.y * this.paintOption.ratio)

        if (style === 'fill') {
            ctx.fillStyle = this.paintOption.fillColor
            ctx.fill()
        }
        if (style === 'stroke') {
            ctx.lineWidth = this.paintOption.lineWidth
            ctx.strokeStyle = this.paintOption.lineColor
            ctx.stroke()
        }
        if (style === 'clear') 
            ctx.clearRect(
                start.x * this.paintOption.ratio, start.y * this.paintOption.ratio,
                move.x * this.paintOption.ratio, move.y * this.paintOption.ratio)
        return
    }

    /**
     * Canvasに画像を描画
     * @param image base64文字列
     * @param x number x座標
     * @param y number y座標
     * @param width number 幅
     * @param height number 高さ
     * @returns
     */
    public async paintImage(
        image: string,
        sp: { x: number, y: number },
        ss: { width: number, height: number },
        dp: { x: number, y: number },
        ds: { width: number, height: number }
    ): Promise<void> {
        if (!this.paintTarget) return

        const ctx = this.paintTarget.getContext('2d') as CanvasRenderingContext2D
        if (!ctx) return

        const img = new Image()
        img.src = image
        img.onload = () => {
            ctx.drawImage(
                img, sp.x, sp.y, ss.width, ss.height,
                dp.x, dp.y, ds.width, ds.height
            )
        }
        return
    }

    /**
     * 渡されたベース画像とCanvas要素を結合して新しい画像を生成
     * @param image base64文字列
     * @returns string base64文字列
     */
    public async mergeImage(image: string): Promise<string> {
        if (!this.paintTarget) return ''

        const canvas = document.createElement('canvas')
        const ctx = canvas.getContext('2d') as CanvasRenderingContext2D
        if (!ctx) return ''

        return new Promise((resolve) => {
            const img = new Image()
            img.src = image
            img.onload = () => {
                if (!this.paintTarget) return

                canvas.width = img.width
                canvas.height = img.height

                ctx.drawImage(img, 0, 0)
                ctx.drawImage(this.paintTarget, 0, 0, img.width, img.height)
                resolve(canvas.toDataURL())
            }
        })
    }

    /**
     * 画像の透明部分と着色部分を反転する
     */
    public async reverseImage(image: string): Promise<string> {
        if (!this.paintTarget) return ''

        const canvas = document.createElement('canvas')
        const ctx = canvas.getContext('2d') as CanvasRenderingContext2D
        if (!ctx) return ''

        return new Promise((resolve) => {
            const img = new Image()
            img.src = image
            img.onload = () => {
                if (!this.paintTarget) return

                canvas.width = img.width
                canvas.height = img.height

                ctx.drawImage(img, 0, 0)
                //ctx.globalCompositeOperation = 'destination-out'
                //ctx.drawImage(this.paintTarget, 0, 0, img.width, img.height)
                // 画像データを取得
                const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                const data = imageData.data;

                // 画像データを反転
                for (let i = 0; i < data.length; i += 4) {
                    data[i] = 255 - data[i];         // 赤
                    data[i + 1] = 255 - data[i + 1]; // 緑
                    data[i + 2] = 255 - data[i + 2]; // 青
                    data[i + 3] = 255 - data[i + 3]; // アルファ
                }

                // 反転した画像データをキャンバスに戻す
                ctx.putImageData(imageData, 0, 0);


                resolve(canvas.toDataURL())
            }
        })
    }
    /**
     * Canvasをクリア
     * @returns 
     */
    public clear(): void {
        if (!this.paintTarget) return

        const ctx = this.paintTarget.getContext('2d') as CanvasRenderingContext2D
        if (!ctx) return

        ctx.clearRect(0, 0, this.paintTarget.width, this.paintTarget.height)
        return
    }
}