import { ExportHelper } from "../_helper/export.helper"

import { MouseHelper } from "./_helper/mouse.helper"
import { DrawHelper } from "./_helper/draw.helper"

import { ImageInformationService } from "../image/image.service"

export class PaintService {
    private static instance: PaintService

    private adjustSize: number = 800
    private moveFlag: boolean = false

    public static call(): PaintService {
        if (!PaintService.instance) {
            PaintService.instance = new PaintService()
        }

        return PaintService.instance
    }

    public callDrawHelper(): DrawHelper {
        return DrawHelper.call()
    }

    public async downloadImage(
        name: string,
        extention: 'jpeg' | 'jpg' | 'png' | 'svg' | 'pdf' = 'jpeg',
        image: string = '',
    ): Promise<void> {
        ExportHelper.call().setExtension(extention)
        if (image !== '')
            ExportHelper.call().setData(await DrawHelper.call().mergeImage(image))
        else {
            ExportHelper.call().setData(DrawHelper.call().getPaintTarget().toDataURL('image/' + extention))
        }
        await ExportHelper.call().download(name)
        return
    }

    /**
     * 描画線の太さを設定
     * @param width number
     * @returns 
     */
    public setLineWidth(width: number): PaintService {
        DrawHelper.call().setPaintOption({
            lineWidth: width,
        })
        return this
    }

    /**
     * 描画線の色を設定
     * @param color string
     * @returns 
     */
    public setLineColor(color: string): PaintService {
        DrawHelper.call().setPaintOption({
            lineColor: color,
        })
        return this
    }

    /**
     * 塗りつぶしの色を設定
     * @param color string
     * @returns 
     */
    public setFillColor(color: string): PaintService {
        DrawHelper.call().setPaintOption({
            fillColor: color,
        })
        return this
    }

    public setScale(scale: { x: number, y: number }): PaintService {
        DrawHelper.call().setPaintOption({
            scale: scale,
        })
        return this
    }

    public setAdjustSize(width: number): PaintService {
        this.adjustSize = width
        return this
    }

    public async setBaseImage(
        image: string,
    ): Promise<PaintService> {
        if (!DrawHelper.call().getPaintTarget()) return this

        const scale = await this.getImageScale(image)

        DrawHelper.call().getPaintTarget().width = scale.x
        DrawHelper.call().getPaintTarget().height = scale.y

        DrawHelper.call().setPaintOption({
            width: scale.x,
            height: scale.y,
        })
        return this
    }

    /**
     * 下地画像を設定
     * @param image string
     * @param point { x: number, y: number }
     * @param size { width: number, height: number}
     * @returns 
     */
    public async setBackGroundImage(
        image: string,
        spoint: { x: number; y: number },
        size: { width: number; height: number },
        dpoint: { x: number; y: number } = { x: 0, y: 0 },
        dsize: { width: number; height: number } = { width: 500, height: 500 }
    ): Promise<PaintService> {
        await DrawHelper.call().paintImage(
            image, spoint, size, dpoint, dsize
        )

        this.setBaseImage(image)
        return this
    }

    /**
     * 消しゴムモードを設定
     * @param mode boolean
     * @returns 
     */
    public setEraseMode(mode: boolean): PaintService {
        DrawHelper.call().setPaintOption({
            eraser: mode,
        })
        return this
    }

    /**
     * Canvas要素をセット
     * @param paintTarget HTMLCanvasElement | string
     * @returns 
     */
    public setPaintTarget(
        paintTarget: HTMLCanvasElement | string,
        size: { width: number; height: number } = { width: 500, height: 500 }
    ): PaintService {
        if (typeof paintTarget === 'string')
            DrawHelper.call().setPaintTarget(document.getElementById(paintTarget) as HTMLCanvasElement)
        else
            DrawHelper.call().setPaintTarget(paintTarget)

        DrawHelper.call().getPaintTarget().width = size.width
        DrawHelper.call().getPaintTarget().height = size.height

        
        return this
    }

    public setRatio(ratio: number): PaintService {
        //MouseHelper.call().setRatio(ratio)
        DrawHelper.call().setPaintOption({
            ratio: ratio,
        })
        return this
    }

    /**
     * paintTargetにマウスイベントを設定
     */
    public setMouseEvent(): boolean {
        if (!DrawHelper.call().getPaintTarget()) {
            console.error('paintTarget is not set')
            return false
        }

        try {
            DrawHelper.call().getPaintTarget().addEventListener(
                'mousedown',
                (e) => this.mouseDownEvent(e)
            )
    
            DrawHelper.call().getPaintTarget().addEventListener(
                'mousemove',
                (e) => this.mouseMoveEvent(e)
            )
    
            DrawHelper.call().getPaintTarget().addEventListener(
                'mouseup',
                () => this.mouseUpEvent()
            )
            return true            
        } catch (error) {
            console.error(error)
            return false
        }
    }

    /**
     * マウスイベントを削除
     * @returns 
     */
    public removeMouseEvent(): boolean {
        if (!DrawHelper.call().getPaintTarget()) {
            console.error('paintTarget is not set')
            return false
        }

        try {
            DrawHelper.call().getPaintTarget().removeEventListener(
                'mousedown',
                (e) => this.mouseDownEvent(e)
            )
    
            DrawHelper.call().getPaintTarget().removeEventListener(
                'mousemove',
                (e) => this.mouseMoveEvent(e)
            )
    
            DrawHelper.call().getPaintTarget().removeEventListener(
                'mouseup',
                () => this.mouseUpEvent()
            )
            return true
        } catch (error) {
            console.error(error)
            return false
        }

    }

    /**
     * 四角形を描画
     * @param start { x: number, y: number }
     * @param end { x: number, y: number}
     * @param style fill | stroke Default: fill
     * @returns PaintService
     */
    public addRect(
        start: { x: number; y: number },
        end: { x: number; y: number },
        style: 'fill' | 'stroke' = 'fill'
    ): PaintService {
        DrawHelper.call().paintRect(start, end, style)
        return this
    }

    /**
     * Canvasをクリア
     * @returns PaintService
     */
    public clear(): PaintService {
        DrawHelper.call().clear()
        return this
    }

    private mouseDownEvent(e: MouseEvent): void {
        this.moveFlag = true
        MouseHelper.call().setStart(e)
        //DrawHelper.call().paintArc(MouseHelper.call().getStart())
    }

    private mouseMoveEvent(e: MouseEvent): void {
        
        if (!this.moveFlag) return

        MouseHelper.call().setMove(e)
        DrawHelper.call().paintPath(
            MouseHelper.call().getMove(),
            MouseHelper.call().getOffset(),
        )

        //DrawHelper.call().paintArc(MouseHelper.call().getMove())
        MouseHelper.call().updateOffset()
    }

    private mouseUpEvent(): void {
        this.moveFlag = false
    }


    private async getImageScale(
        org_image: string,
    ): Promise<{
        x: number,
        y: number,
        base_width: number,
        base_height: number
    }> {
        let size = {width: 0, height: 0}
    
        await ImageInformationService.call().set(org_image)
        size = ImageInformationService.call().getSize()
    
        const scale = this.adjustSize / size.width

        DrawHelper.call().setPaintOption({
            scale: { x: scale, y: scale }
        })
   
        return {
            x: size.width * scale,
            y: size.height * scale,
            base_width: size.width,
            base_height: size.height
        }
    }
}