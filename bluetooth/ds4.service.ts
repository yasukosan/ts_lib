'use client'
//import { DualShock4 } from 'webhid-ds4'
//import { Dualsense } from "dualsense-ts";
import { init as DualSense, DualSenseStateType } from './_helper/webhid_ds'

export const ButtonStatus: { [key: string]: HTMLElement | null } = {
    triangle: null,
    circle: null,
    cross: null,
    square: null,
    dPadUp: null,
    dPadDown: null,
    dPadLeft: null,
    dPadRight: null,
    l1: null,
    l2: null,
    l3: null,
    r1: null,
    r2: null,
    r3: null,
    leftStickX: null,
    leftStickY: null,
    rightStickX: null,
    rightStickY: null,
    leftAxes: null,
    rightAxes: null,
    gyroX: null,
    gyroY: null,
    gyroZ: null,
    accelX: null,
    accelY: null,
    accelZ: null,
    touchX: null,
    touchY: null,
    logs: null,
}

export const init = async (
    next: (
        state: DualSenseStateType,
        data: DataView
    ) => void,
) => {
    DualSense(next)
}

