type DualShock4Interface = {
    Disconnected: string,
    USB: 'usb' | 'bt',
    Bluetooth: 'bt' | 'usb'
}
const DualShock4Interface = {
    Disconnected: 'none',
    USB: 'usb',
    Bluetooth: 'bt'
}

const VenderFilter = [
    // Official Sony Controllers
    { vendorId: 0x054C, productId: 0x0BA0 },
    { vendorId: 0x054C, productId: 0x05C4 },
    { vendorId: 0x054C, productId: 0x09CC },
    { vendorId: 0x054C, productId: 0x05C5 },
    // Razer Raiju
    { vendorId: 0x1532, productId: 0x1000 },
    { vendorId: 0x1532, productId: 0x1007 },
    { vendorId: 0x1532, productId: 0x1004 },
    { vendorId: 0x1532, productId: 0x1009 },
    // Nacon Revol
    { vendorId: 0x146B, productId: 0x0D01 },
    { vendorId: 0x146B, productId: 0x0D02 },
    { vendorId: 0x146B, productId: 0x0D08 },
    // Other third party controllers
    { vendorId: 0x0F0D, productId: 0x00EE },
    { vendorId: 0x7545, productId: 0x0104 },
    { vendorId: 0x2E95, productId: 0x7725 },
    { vendorId: 0x11C0, productId: 0x4001 },
    { vendorId: 0x0C12, productId: 0x57AB },
    { vendorId: 0x0C12, productId: 0x0E16 },
    { vendorId: 0x0F0D, productId: 0x0084 },
    //{ vendorId: 0x1356, productId: 0x2508 },
    { vendorId: 0x054C, productId: 0x09CC },
]

/**
 * Default / Initial State
 * @ignore
 */
const defaultState = {
    interface: DualShock4Interface.Disconnected,
    battery: 0,
    charging: false,
    axes: {
        leftStickX: 0,
        leftStickY: 0,
        rightStickX: 0,
        rightStickY: 0,
        l2: 0,
        r2: 0,
        accelX: 0,
        accelY: 0,
        accelZ: 0,
        gyroX: 0,
        gyroY: 0,
        gyroZ: 0
    },
    buttons: {
        triangle: false,
        circle: false,
        cross: false,
        square: false,
        dPadUp: false,
        dPadRight: false,
        dPadDown: false,
        dPadLeft: false,
        l1: false,
        l2: false,
        l3: false,
        r1: false,
        r2: false,
        r3: false,
        options: false,
        share: false,
        playStation: false,
        touchPadClick: false
    },
    touchpad: {
        touches: []
    },
    timestamp: -1
};

export type DualSenseStateType = typeof defaultState

let state: DualSenseStateType = defaultState;
let device: any = null;
let callback: (
    state: DualSenseStateType,
    data: DataView
) => void = () => {};  

export const init = async (
    _callback: (state: DualSenseStateType, data: DataView) => void
) => {

    state = defaultState
    callback = null

    if (!('hid' in navigator) || !navigator.hid) {
        throw new Error('WebHID not supported by browser or not available.');
    }

    if (device && device.opened)
        return

    const devices = await navigator.hid.requestDevice({
        filters: VenderFilter
    })
    device = devices[0]
    await device.open()

    callback = _callback;
    device.oninputreport = (e) => processControllerReport(e);
}

const processControllerReport = (report) => {
    const { data } = report;

    if (state.interface === DualShock4Interface.Disconnected) {
        // USB接続
        /*if (data.byteLength === 63) {
            this.state.interface = DualShock4Interface.USB;
        // Blutooth接続
        } else {*/
            state.interface = DualShock4Interface.Bluetooth;
            device.receiveFeatureReport(0x02);
            return;
        //}
        // Player 1 Color
        //this.lightbar.setColorRGB(0, 0, 64).catch(e => console.error(e));
    }
    state.timestamp = report.timeStamp;
    // USB Reports
    /*if (this.state.interface === DualShock4Interface.USB && report.reportId === 0x01) {
        console.log('USB')
        this.updateState(data);
    }*/
    // Bluetooth Reports
    // if (this.state.interface === DualShock4Interface.Bluetooth && report.reportId === 0x11) {
    if (state.interface === DualShock4Interface.Bluetooth && report.reportId === 1) {
        updateState(new DataView(data.buffer, 0));
        // this.device.receiveFeatureReport(0x02);
    }
}

const updateState = (data: DataView) => {

    // Update thumbsticks
    state.axes.leftStickX = normalizeThumbstick(data.getUint8(0));
    state.axes.leftStickY = normalizeThumbstick(data.getUint8(1));
    state.axes.rightStickX = normalizeThumbstick(data.getUint8(2));
    state.axes.rightStickY = normalizeThumbstick(data.getUint8(3));
    // Update main buttons
    const buttons1 = data.getUint8(4);
    state.buttons.triangle = !!(buttons1 & 0x80);
    state.buttons.circle = !!(buttons1 & 0x40);
    state.buttons.cross = !!(buttons1 & 0x20);
    state.buttons.square = !!(buttons1 & 0x10);
    // Update D-Pad
    const dPad = buttons1 & 0x0F;
    state.buttons.dPadUp = dPad === 7 || dPad === 0 || dPad === 1;
    state.buttons.dPadRight = dPad === 1 || dPad === 2 || dPad === 3;
    state.buttons.dPadDown = dPad === 3 || dPad === 4 || dPad === 5;
    state.buttons.dPadLeft = dPad === 5 || dPad === 6 || dPad === 7;
    // Update additional buttons
    const buttons2 = data.getUint8(5);
    state.buttons.l1 = !!(buttons2 & 0x01);
    state.buttons.r1 = !!(buttons2 & 0x02);
    state.buttons.l2 = !!(buttons2 & 0x04);
    state.buttons.r2 = !!(buttons2 & 0x08);
    state.buttons.share = !!(buttons2 & 0x10);
    state.buttons.options = !!(buttons2 & 0x20);
    state.buttons.l3 = !!(buttons2 & 0x40);
    state.buttons.r3 = !!(buttons2 & 0x80);
    const buttons3 = data.getUint8(6);
    state.buttons.playStation = !!(buttons3 & 0x01);
    state.buttons.touchPadClick = !!(buttons3 & 0x02);
    // Update Triggers
    state.axes.l2 = normalizeTrigger(data.getUint8(7));
    state.axes.r2 = normalizeTrigger(data.getUint8(8));
    // Update battery level
    state.charging = !!(data.getUint8(29) & 0x10);
    if (state.charging) {
        state.battery = Math.min(Math.floor((data.getUint8(29) & 0x0F) * 100 / 11));
    }
    else {
        state.battery = Math.min(100, Math.floor((data.getUint8(29) & 0x0F) * 100 / 8));
    }
    // Update motion input
    state.axes.gyroX = data.getUint16(13);
    state.axes.gyroY = data.getUint16(15);
    state.axes.gyroZ = data.getUint16(17);
    state.axes.accelX = data.getInt16(19);
    state.axes.accelY = data.getInt16(21);
    state.axes.accelZ = data.getInt16(23);

    if (callback) callback(state, data);
}

const checkVenderCode = async () => {
    // ベンダーID検出用の試験コード
    let _devices = await navigator.hid.getDevices()
    _devices.forEach((dev) => {
        console.log(`HID: ${dev.vendorId}`)
    })
}

const normalizeThumbstick = (
    input: number,
    deadZone: number = 0
): number => {
    const rel = (input - 128) / 128;
    if (Math.abs(rel) <= deadZone)
        return 0;
    return Math.min(1, Math.max(-1, rel));
}

const normalizeTrigger = (
    input: number,
    deadZone: number = 0
): number => {
    return Math.min(1, Math.max(deadZone, input / 255));
}
