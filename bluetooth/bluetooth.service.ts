/*
interface BluetoothRemoteGATTCharacteristic {
    readonly attribute BluetoothRemoteGATTService service;
    readonly attribute UUID uuid;
    readonly attribute BluetoothCharacteristicProperties properties;
    readonly attribute DataView? value;
    Promise<BluetoothRemoteGATTDescriptor> getDescriptor(BluetoothDescriptorUUID descriptor);
    Promise<sequence<BluetoothRemoteGATTDescriptor>>
        getDescriptors(optional BluetoothDescriptorUUID descriptor);
    Promise<DataView> readValue();
    Promise<void> writeValue(BufferSource value);
    Promise<void> startNotifications();
    Promise<void> stopNotifications();
}*/

export class BluetoothService {
    private static instance: BluetoothService

    //private SERVICE_UUID = '77920fa2-1586-4de5-adc0-4eebe0861350'
    //private CHARACTERISTIC_UUID = '8f47052e-907e-4824-9300-368fa08fadaa'
    private SERVICE_UUID = '7254a5ea-51d7-4c6e-b649-cc421603d7b5'
    private CHARACTERISTIC_UUID = 'eb9593fa-67c7-4752-be02-aba276ddaefb'

    private Device: any
    private Characteristic: any
    private ReadBuffer: string[] = []

    private readCallBack: (res: any) => any = (res) => {return false}
    private disconnectCallBack: (res: any) => any = (res) => {return false}
    private namePrefix = 'Super_HELL_Met'
    //private namePrefix: string = 'ESP32_IO'

    public static call(): BluetoothService {
        if (!this.instance) {
            this.instance = new BluetoothService();
        }
        return this.instance;
    }

    public setReadCallBack(callBack: (res: any) => any): BluetoothService {
        this.readCallBack = callBack
        return this
    }

    public setDisconnectCallBack(callBack: (res: any) => any): BluetoothService {
        this.disconnectCallBack = callBack
        return this
    }

    public setNamePrefix(namePrefix: string): BluetoothService {
        this.namePrefix = namePrefix
        return this
    }

    public getCharacteristic(): any {
        return this.Characteristic
    }

    public async connect(): Promise<boolean> {
        try {
            let result = true
            if (navigator.bluetooth! === undefined) {
                //throw new Error('Web Bluetooth API is not available.\n' +
                //    'Please make sure the Web Bluetooth flag is enabled.')
                console.error('Web Bluetooth API is not available')
                return false
            }
            await this.setupDevice()
            const server = await this.setupServer()
            const service = await this.setupService(server)
            result = await this.setupCharacteristic(service)

            return true
        } catch (error) {
            console.log(error);
            return false
        }
    }

    private async setupDevice(): Promise<any> {
        this.Device = await navigator.bluetooth!.requestDevice({
            //acceptAllDevices: true,
            filters: [
                //{ services: [this.SERVICE_UUID] },
                { namePrefix: this.namePrefix }
            ],
            optionalServices: [0x1801, this.SERVICE_UUID]
        }).catch((error) => {
            console.log(error)
            return false
        })
        if (!this.Device) return false
        return true
    }

    private async setupServer(): Promise<any> {
        const server =
        await this.Device.gatt
                .connect()
                .catch((error) => {
                    console.error(error)
                    return false
                })
        if (!server) return false
        return server
    }

    private async setupService(server: any): Promise<any> {
        const service =
        await server
                .getPrimaryService(this.SERVICE_UUID)
                .catch((error) => {
                    console.error(error)
                    return false
                })
        if (!service) return false
        return service
    }

    private async setupCharacteristic(service: any): Promise<boolean> {
        this.Characteristic =
        await service
                //.getCharacteristic(this.CHARACTERISTIC_UUID)
                .getCharacteristic(this.CHARACTERISTIC_UUID)
                .catch((error) => {
                    console.error(error)
                    return false
                })
        console.log(this.Characteristic)
        if (!this.Characteristic) return false
        return true
    }

    private onDisconnected(event: any) {
        if (this.disconnectCallBack) {
            this.disconnectCallBack(event)
        }
        console.log('Device is disconnected.')
    }
        
    public async disconnect() {
        if (!this.Device) {
            return false
        }
        if (this.Device.gatt.connected) {
            await this.Device.gatt.disconnect()
        }
        console.log('Device is disconnected.')
        return true
    }

    public async write(data: string) {
        try {
            const encoder = new TextEncoder();
            await this.Characteristic.writeValue(encoder.encode(data));
        } catch (error) {
            console.log(error);
        }
    }

    public async read() {
        try {
            const decoder = new TextDecoder();
            const value = await this.Characteristic.readValue();
            if (this.readCallBack) {
                this.readCallBack(decoder.decode(value))
            }
            return decoder.decode(value);
        } catch (error) {
            console.log(error);
        }
    }
}