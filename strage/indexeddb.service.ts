
export type ReturnType = {
    id: string,
    [key: string]: string | string[] | number[] | object[] | boolean[], 
}

export class IndexedDBService
{
    private static instance: IndexedDBService

    #Database: IDBDatabase | undefined

    public static call(): IndexedDBService {
        if (!IndexedDBService.instance) {
            IndexedDBService.instance = new IndexedDBService()
        }
        return IndexedDBService.instance
    }

    public checkConnection(): boolean {
        return this.#Database !== undefined
    }

    public async openDB(
        dbName: string,
        version: number,
        storeNames: string[]
    ): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const request = window.indexedDB.open(dbName, version)
            request.onupgradeneeded = () => {
                this.#Database = request.result
                for (const storeName of storeNames) {
                    if (!this.#Database.objectStoreNames.contains(storeName)) {
                        this.#Database.createObjectStore(storeName, { keyPath: 'id' })
                    }
                }
            }
            request.onsuccess = () => {
                this.#Database = request.result
                resolve(true)
            }
            request.onerror = (event: Event) => {
                console.error(event)
                console.error('Database open error', request.error)
                reject(false)
            }
        })
    }

    public async putData(
        storeName: string,
        data: string | number | object | boolean
    ): Promise<void> {

        if (this.#Database === undefined) {
            console.error('Database is not opened')
            return
        }

        return new Promise((resolve, reject) => {
            const transaction = this.#Database?.transaction(storeName, 'readwrite') as IDBTransaction 
            const store = transaction.objectStore(storeName)
            const request = store.put(data)
            request.onsuccess = () => {
                //console.log(event)
                resolve()
            }
            request.onerror = (event: Event) => {
                console.error(event)
                reject(request.error)
            }
        })
    }

    public async getData(
        storeName: string,
        id: string
    ): Promise<ReturnType | false> {

        if (this.#Database === undefined) {
            console.error('Database is not opened')
            return false
        }

        return new Promise((resolve, reject) => {
            const transaction = this.#Database?.transaction(storeName, 'readonly') as IDBTransaction
            const store = transaction.objectStore(storeName)
            const request = store.get(id)
            request.onsuccess = () => {
                resolve(request.result)
            }
            request.onerror = (event) => {
                console.error(event)
                reject(request.error)
            }
        })
    }

    public async getAllData(
        storeName: string
    ): Promise<ReturnType[] | false> {

        if (this.#Database === undefined) {
            console.error('Database is not opened')
            return false
        }

        return new Promise((resolve, reject) => {
            const transaction = this.#Database?.transaction(storeName, 'readonly') as IDBTransaction
            const store = transaction.objectStore(storeName)
            const request = store.getAll()
            request.onsuccess = () => {
                resolve(request.result as ReturnType[] | false)
            }
            request.onerror = (event) => {
                console.error(event)
                reject(request.error)
            }
        })
    }

    public async deleteData(
        db: IDBDatabase,
        storeName: string,
        id: string
    ): Promise<void> {
        return new Promise((resolve) => {
            const transaction = db.transaction(storeName, 'readwrite')
            const store = transaction.objectStore(storeName)
            const request = store.delete(id)
            request.onsuccess = () => {
                resolve()
            }
            request
        })
    }
}
