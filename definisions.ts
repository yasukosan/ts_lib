
export type ReturnSuccess<T> = {
    status: true,
    message: T
}

export type ReturnError = {
    status: false,
    message: string
}