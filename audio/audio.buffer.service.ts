import { Buffer } from 'buffer'
import { AnalyserHelper } from './_helper/Analyser.Helper'
import { FilterHelper } from './_helper/Filter.Helper'
import { MeydaHelper } from './_helper/Meyda.Helper'


export class AudioBufferService {
    private static instance: AudioBufferService

    private source: MediaStreamAudioSourceNode[] | any = []
    private audioContext: AudioContext[] | any = []
    private filter: BiquadFilterNode[] | any = []
    private analyser: AnalyserNode[] | any = []

    private doPlay = false

    private ffsize: 32 | 64 | 128 | 256 | 512 | 1024 | 2048 = 128

    public static call(): AudioBufferService {
        if (!AudioBufferService.instance) {
            AudioBufferService.instance = new AudioBufferService()
        }
        return AudioBufferService.instance
    }

    /**
     * AnalyserHelperを返す
     * @returns AnalyserHelper
     */
    public callAnalyser(): AnalyserHelper {
        return AnalyserHelper.call()
    }

    /**
     * FilterHelperを返す
     * @returns FilterHelper
     */
    public callFilter(): FilterHelper {
        return FilterHelper.call()
    }

    public getAudioSource(id: string): MediaStreamAudioSourceNode {
        return this.source[id]
    }

    /**
     * 初期設定
     * @param id string ストリーム管理用の識別子
     */
    public setup(id: string): void {
        console.log(`
        [Audio Service Info] : Setup Audio Buffer API
        `);
        if ('AudioContext' in window) {
            const AudioContext = window.AudioContext
            this.audioContext[id] = new AudioContext()
        } else {
            throw new Error('Audio Context Not Support')
            return
        }

        this.audioContext[id] = new AudioContext();

        // バッファソースの入れ物作成
        this.source[id] = this.audioContext[id].createBufferSource()
    }

    public loadAudioElement(
        id: string,
        element: HTMLAudioElement
    ): void {
        this.source[id] = this.audioContext[id].createMediaElementSource(
            element
        )
    }

    /**
     * オーディオファイルを読み込み
     *
     * @param id string Audiocontextの管理用ID
     * @param url string 読み込み先URL
     * @returns
     */
    public async loadAudioFile(
        id: string,
        url: string
    ): Promise<boolean> {
        return new Promise((result, reject) => {
            const request: XMLHttpRequest = new XMLHttpRequest()

            request.onload = async () => {
                if (request.status === 200) {
                    const res = request.response

                    if (res instanceof ArrayBuffer) {
                        //console.log(res);
                    }
                    // Web Audio APIで使える形式に変換
                    await this.audioContext[id].decodeAudioData(
                        res,
                        (buf: AudioBuffer) => {
                            this.source[id].buffer = buf
                            this.source[id].loop = false
                        },
                        () => {
                            console.error('Audio Buffer Decode ERROR')
                        }
                    )
                    result(true)
                }
                reject(false)
            }

            request.open('GET', url, true)
            request.responseType = 'arraybuffer'
            request.send()
        })
    }

    public async loadAudioString(
        id: string,
        audio: string
    ): Promise<void> {
        // Blobに変換
        const blob = await new Blob(
            [await Buffer.from(audio, 'base64')],
            { type: 'audio/mpeg' }
        )
        console.log(blob) // コレがないとBlobが空になる
        // ArrayBufferに変換
        const arrayBuffer = await blob.arrayBuffer()

        // Web Audio APIのコンテキストを作成
        // window.AudioContext = window.AudioContext
        const context = new AudioContext()
        // ArrayBufferからAudioBufferに変換
        this.source[id].buffer = await (new Promise((res, rej) => {
                                context.decodeAudioData(arrayBuffer, res, rej)
                            })) as AudioBuffer
        this.source[id].loop = false
    }

    /**
     * オーディオストリームを削除
     * @param id string ストリーム識別子
     */
    public delStream(id: string): void {
        this.audioContext[id] = null
        this.source[id] = null
        this.filter[id] = null
    }

    /**
     *
     * @param id
     */
    public play(id: string) {
        if (!this.doPlay) {
            this.doPlay = true
            this.source[id].connect(this.audioContext[id].destination)
            this.source[id].start(0)
            this.source[id].onended = () => {
                this.doPlay = false
            }
        }
    }

    public pause(id: string) {
        if (this.doPlay) {
            this.audioContext[id].suspend()
            this.doPlay = false
        } else {
            this.audioContext[id].resume()
            this.doPlay = true
        }
    }

    public stop(id: string) {
        if (this.doPlay) {
            console.log(this.source[id])
            this.doPlay = false;
            this.source[id].stop()
        }
    }

    public startDecibelChecker(id: string, callback: Function) {
        this.analyser[id].initDecibelChecker(callback)
        this.analyser[id].startDecibelChecker()
    }

    public audioConnect(id: string, visual = false): AudioBufferService {
        this.analyser[id] = new AnalyserHelper()
        this.analyser[id].setAnalyser(this.audioContext[id], 128)
        if (visual) this.analyser[id].setVisualiser('analyser_' + id)

        return this
    }

    /**
     * 波形を表示
     * @param type 'bar' | 'sin' bar: 棒グラフ sin: 波形グラフ
     * @param id string 表示するアナライザーID
     */
    public showAnalyser(type: 'bar' | 'sin', id: string) {
        this.analyser[id].showVisual(type)
    }

}
