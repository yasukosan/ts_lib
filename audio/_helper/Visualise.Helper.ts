export class VisualiseHelper {
    private static instance: VisualiseHelper
    private canvas: HTMLCanvasElement | any
    private canvasContext: CanvasRenderingContext2D | any

    public static call(): VisualiseHelper {
        if (!VisualiseHelper.instance) {
            VisualiseHelper.instance = new VisualiseHelper()
        }
        return VisualiseHelper.instance
    }

    public setCanvas(canvas: string): VisualiseHelper {
        this.canvas = document.getElementById(canvas) as HTMLCanvasElement
        this.canvasContext = this.canvas.getContext(
            '2d'
        ) as CanvasRenderingContext2D
        return this
    }

    public clearCanvas(): VisualiseHelper {
        this.canvasContext.clearRect(
            0,
            0,
            this.canvas.width,
            this.canvas.height
        )
        return this
    }

    /**
     * Sine波を使用したビジュアライズ
     */
    public visualSineWave(
        dataArray: Uint8Array | Float32Array,
        length: number
    ): void {
        this.canvasContext.fillStyle = 'rgb(200, 200, 200)'
        this.canvasContext.fillRect(
            0,
            0,
            this.canvas.width,
            this.canvas.height
        )
        this.canvasContext.fill()

        this.canvasContext.lineWidth = 2
        this.canvasContext.strokeStyle = 'rgb(0, 0, 0)'

        this.canvasContext.beginPath()

        const sliceWidth = (this.canvas.width * 1.0) / length
        let x = 0

        for (let i = 0; i < length; i++) {
            const v = dataArray[i] / 128.0
            const y = (v * this.canvas.height) / 2

            if (i === 0)
                this.canvasContext.moveTo(x, y)
            else
                this.canvasContext.lineTo(x, y)

            x += sliceWidth
        }
        this.canvasContext.lineTo(this.canvas.width, this.canvas.height / 2)
        this.canvasContext.stroke()
        return
    }

    /**
     * 棒グラフを使用したビジュアライズ
     */
    public visualFrequencybars(
        dataArray: Uint8Array | Float32Array,
        length: number
    ): void {
        this.canvasContext.fillStyle = 'rgb(0, 0, 0)'
        this.canvasContext.fillRect(
            0,
            0,
            this.canvas.width,
            this.canvas.height
        )

        const barWidth = (this.canvas.width / length) * 2.5
        let barHeight
        let x = 0

        for (let i = 0; i < length; i++) {
            barHeight = dataArray[i]

            this.canvasContext.fillStyle =
                'rgb(' + (barHeight + 100) + ',50,50)'
            this.canvasContext.fillRect(
                x,
                this.canvas.height - barHeight / 2,
                barWidth,
                barHeight / 2
            )

            x += barWidth + 1
        }
    }
}
