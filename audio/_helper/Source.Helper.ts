/*
■ 作成可能なメディアソース

：：createMediaElementSource()
既存のHTMLまたは要素を指定し、新しいAudioContextオブジェクトを作成する
※オーディオタグを引数に渡して使う
    const audioCtx = new AudioContext();
    const audio = document.querySelector('audio');
    const source = audioCtx.createMediaElementSource(audio);

：：createMediaStreamDestination()
WebRTCのオーディオストリームに関連付ける用のAudioContextオブジェクトを新規に作成
このオブジェ自体を保存、別PCに送信することで利用する。

：：createMediaStreamSource()
メディアストリームを指定して新しいAudioContextオブジェクトを作成する

：：createMediaStreamTrackSource()
指定されたデータからAudioContextを作成する

*/

/**
 * ※
 * ※MediaDeviceサービスと被るので現在未使用
 * ※
 */
export class SourceHelper {
    private static instance: SourceHelper

    private Source: MediaStreamAudioSourceNode | any

    public static call() {
        if (!SourceHelper.instance) {
            SourceHelper.instance = new SourceHelper()
        }
        return SourceHelper.instance
    }

    public getStreamSource(): MediaStream {
        return this.Source.stream
    }

    public setSource(context: AudioContext): SourceHelper {
        this.Source = context.createMediaStreamDestination()
        return this;
    }

    private gteStreamSorce() {}

    private getStreamDestination() {}

    private getStreamTrackSource() {}
}
