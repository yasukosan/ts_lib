export class RecorderHelper {
    private static instance: RecorderHelper

    // wave file
    private waveFile: DataView | any
    private waveBlob: Blob | any
    private waveArrayBuffer: Float32Array | any
    private blobURL: Blob | MediaSource | any

    // for audio
    private scriptProcessor: ScriptProcessorNodeEventMap | any
    private audio_sample_rate = 44100
    private audioContext: AudioContext | any
    private audioStream: MediaStreamAudioSourceNode | any
    private callback: Function | any
    private recordTime = 1000

    private onRec = false

    // audio data
    private audioData: Float32Array[] = []
    private bufferSize = 1024
    private timeStamp = 0
    private recTime = 0

    private animationId = 0

    private waveFormat: any = {
        RIFF_HEADER: 0,
        WAVE_HEADER: 8,
        FMT_CHANK: 12,
        FMT_BYTE: 16,
        FORMAT_ID: 1,
        CHANNEL: 1,
        SAMPLING_RATE: 44100,
        DATA_RATE: 44100,
        BLOCK_SIZE: 2,
        BIT_COUNT: 16,
        DATA_CHUNK: 36,
        WAVE_RATE: 44
    }

    private fileName = 'output.wav'

    public static call(): RecorderHelper {
        if (!RecorderHelper.instance) {
            RecorderHelper.instance = new RecorderHelper()
        }
        return RecorderHelper.instance
    }

    /**
     * 録音データを返す
     *
     * @param type 'Blob' | 'ArrayBuffer' | 'DataView' | 'File'
     * @returns Blob | Float32Array | DataView | File
     */
    public getData(
        type: 'Blob' | 'ArrayBuffer' | 'DataView' | 'File'
    ): Blob | Float32Array | DataView | File {
        if (type === 'DataView') return this.waveFile
        if (type === 'Blob') return this.waveBlob
        if (type === 'ArrayBuffer') return this.waveArrayBuffer
        if (type === 'File')
            return new File([this.waveFile], 'output.wav', {
                type: 'audio/wav'
            });
        return this.waveFile
    }

    public getRecordTime() {
        return this.recTime
    }

    /**
     * ダウンロードイベント用のBlobファイルの作成
     * @returns string ダウンロード用のBlobURL
     */
    public buildDownloadFile(): void {
        this.waveBlob = new Blob([this.waveFile], { type: 'audio/wav' })
        const myURL = window.URL || window.webkitURL
        const blobURL = myURL.createObjectURL(this.waveBlob)
    }

    /**
     * ダウンロードイベントを発生させる
     * ファイル名「output.wav」でダウンロードが実行される
     */
    public saveAudio(): void {
        if (!this.blobURL) this.buildDownloadFile()
        const downloadLink = document.createElement('a')
        downloadLink.href = this.blobURL
        downloadLink.download = this.fileName
        downloadLink.click()
    }

    /**
     * 初期化
     * これを未実行の場合何も出来ない
     * @param context AudioContext
     * @param stream MediaStreamAudioSourceNode
     * @param callback Function
     * @param rectime number
     * @returns RecorderHelper
     */
    public init(
        context: AudioContext,
        stream: MediaStreamAudioSourceNode,
        callback: Function | null = null,
        rectime = 10000
    ): RecorderHelper {
        this.audioContext = context
        this.audioStream = stream
        this.callback = callback
        this.recordTime = rectime
        this.scriptProcessor = this.audioContext.createScriptProcessor(
            this.bufferSize,
            1,
            1
        )
        return this
    }

    /**
     * レコーダーのループ
     * AnimationFrameでループさせているが
     * レコード中はループ開始処理が無視されるので
     * 「常に開始しているわけではない。」ことに注意
     */
    public startRecorder(
        type: 'single' | 'timeout' | 'loop' = 'loop'
    ): void {
        if (!this.onRec) {
            this.onRec = true;
            this.timeStamp = this.getNow()
            this.doRecorder(type)
        }
        if (type === 'loop')
        this.animationId = requestAnimationFrame(() => this.startRecorder())
    }

    public stopRecorder(): RecorderHelper
    {
        // Waveに変換
        this.convWave(this.audioData)
        // Download用のBlobデータ作成
        this.buildDownloadFile()
        if (this.callback && typeof this.callback === 'function') {
            this.callback(this.getData('File'))
        }
        // 録音時間記録
        this.recTime = (this.getNow() - this.timeStamp) / 1000
        // scriptProcessor切断
        this.reset()
        return this
    }

    /**
     * レコード
     */
    private doRecorder(type: string): void {
        const convAudioBuffer = (audio: any): void => {
            const input = audio.inputBuffer.getChannelData(0)
            const bufferData = new Float32Array(this.bufferSize)
            for (let i = 0; i < this.bufferSize; i++) {
                bufferData[i] = input[i]
            }

            this.audioData.push(bufferData)
        }

        // scriptProcessor接続
        this.audioStream.connect(this.scriptProcessor)
        this.scriptProcessor.onaudioprocess = convAudioBuffer
        this.scriptProcessor.connect(this.audioContext.destination)

        // when time passed without pushing the stop button
        if (type === 'single') return

        setTimeout(() => {
            // console.log("Recorder Complete " + this.recordTime + " sec");
            this.stopRecorder()
        }, this.recordTime)
    }

    /**
     * Waveファイルに変換
     * ファイルヘッダー、書式をWave形式で記録した
     * DataViewのバイナリデータに変換
     * @param audioData Float32Array[]
     * @returns
     */
    public convWave(audioData: Float32Array[]): void {
        // サンプリングレート計算
        let sampleLength = 0
        for (let i = 0; i < audioData.length; i++) {
            sampleLength += audioData[i].length
        }

        // Float32Arrayに変換
        this.waveArrayBuffer = new Float32Array(sampleLength)
        let sampleIdx = 0
        for (let i = 0; i < audioData.length; i++) {
            for (let j = 0; j < audioData[i].length; j++) {
                this.waveArrayBuffer[sampleIdx] = audioData[i][j]
                sampleIdx++
            }
        }

        this.waveFile = this.encodeWAV(
            this.waveArrayBuffer,
            this.audio_sample_rate
        )
        return
    }

    /**
     * 波形データをWave形式に変換
     * @param samples Float32Array
     * @param sampleRate number
     * @returns DataView
     */
    private encodeWAV(samples: Float32Array, sampleRate: number): DataView {
        const buffer = new ArrayBuffer(44 + samples.length * 2)
        const view: DataView = new DataView(buffer)

        this.writeString(view, this.waveFormat.RIFF_HEADER, 'RIFF') // RIFFヘッダ
        view.setUint32(4, 32 + samples.length * 2, true) // これ以降のファイルサイズ
        this.writeString(view, this.waveFormat.WAVE_HEADER, 'WAVE') // WAVEヘッダ
        this.writeString(view, this.waveFormat.FMT_CHANK, 'fmt ') // fmtチャンク
        view.setUint32(16, this.waveFormat.FMT_BYTE, true) // fmtチャンクのバイト数
        view.setUint16(20, this.waveFormat.FORMAT_ID, true) // フォーマットID
        view.setUint16(22, this.waveFormat.CHANNEL, true) // チャンネル数
        view.setUint32(24, sampleRate, true) // サンプリングレート
        view.setUint32(28, sampleRate * 2, true) // データ速度
        view.setUint16(32, this.waveFormat.BLOCK_SIZE, true) // ブロックサイズ
        view.setUint16(34, this.waveFormat.BIT_COUNT, true) // サンプルあたりのビット数
        this.writeString(view, this.waveFormat.DATA_CHUNK, 'data') // dataチャンク
        view.setUint32(40, samples.length * 2, true) // 波形データのバイト数
        this.floatTo16BitPCM(view, this.waveFormat.WAVE_RATE, samples) // 波形データ

        return view
    }

    /**
     * 文字列をDataViewに追記
     * @param view DataView
     * @param offset number
     * @param string string
     */
    private writeString(view: DataView, offset: number, string: string): void {
        for (let i = 0; i < string.length; i++) {
            view.setUint8(offset + i, string.charCodeAt(i))
        }
    }

    /**
     * Floatを16bitPCMに変換しDataViewに追記
     * @param output DataView
     * @param offset number
     * @param input Float32Array
     */
    private floatTo16BitPCM(
        output: DataView,
        offset: number,
        input: Float32Array
    ): void {
        for (let i = 0; i < input.length; i++, offset += 2) {
            const s = Math.max(-1, Math.min(1, input[i]))
            output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7fff, true)
        }
    }

    /**
     * 録音の初期化
     */
    private reset(): void {
        this.scriptProcessor.disconnect()
        this.scriptProcessor.onaudioprocess = null
        this.onRec = false
        this.audioData = []
    }

    private getNow(): number
    {
        const n = new Date()
        return n.getTime()
    }
}
