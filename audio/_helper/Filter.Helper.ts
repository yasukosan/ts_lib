/**
■ フィルターの種類
lowpass ローパスフィルター      : 特定の周波数より下だけを通す
highpass ハイパスフィルター     : 特定の周波数より上だけを通す
bandpass バンドパスフィルター   : 特定の周波数範囲だけを通す
lowshelf ローシェルフフィルター : 特定の周波数より下を増幅または減衰させる
highshelf ハイシェルフフィルター: 特定の周波数より上を増幅または減衰させる
peaking ピーキングフィルター    : 特定の周波数を増幅する(減衰も可)
notch ノッチフィルター          : 特定の周波数を減衰させる
allpass オールパスフィルター    : すべての周波数を通し位相特性だけを回転させる

■ フィルター毎のプロパティ
frequency AudioParam    Hz	    350	    0～     
            ヘルツ（Hz）単位で測定される、現在のフィルタアルゴリズムの周波数、dableデータを返す
            データは「a-rate」(サンプルフレームごとのオーディオパラメータのサンプリングデータ)

detune    AudioParam    セント	0	    -∞ ～ +∞	
            デチューン。カットオフ周波数をずらします。
            単位の「セント」は半音の 1/100 で 1200 で 1 オクターブになります。
            データは「a-rate」(サンプルフレームごとのオーディオパラメータのサンプリングデータ)

Q         AudioParam	なし	1	    -∞ ～ +∞	
            サンプリング品質 デフォルト１ 最小0.0001～無限の範囲
            データは「a-rate」(サンプルフレームごとのオーディオパラメータのサンプリングデータ)

gain      AudioParam	dB	    0	    -∞ ～ +∞	
            デシベルで表されるフィルターのゲインです。
            ローシェルフ/ハイシェルフ/ピーキングの場合のみ使用されます。
            データは「a-rate」(サンプルフレームごとのオーディオパラメータのサンプリングデータ)

getFrequencyResponse(freq,mag,phase)	関数	-	-	-	
            フィルター特性カーブを取得します。
 */

type FilterType =
    | 'lowpass'
    | 'highpass'
    | 'bandpass'
    | 'lowshelf'
    | 'highsheld'
    | 'peaking'
    | 'notch'
    | 'allpass';

interface Options {
    frequency: number
    detune: number
    Q: number
    gain: number
}

export class FilterHelper {
    private static instance: FilterHelper

    private Filter: BiquadFilterNode | any
    private FilterType: FilterType = 'lowpass'
    private FilterOption: Options = {
        frequency: 350,
        detune: 0,
        Q: 1,
        gain: 0
    };

    public static call(): FilterHelper {
        if (!FilterHelper.instance) {
            FilterHelper.instance = new FilterHelper();
        }
        return FilterHelper.instance
    }

    /**
     * フィルターをアナライザーか、オーディオコンテキストに繋ぐ
     * @param target AnalyserNode | AudioContext
     * @returns FilterHelper
     */
    public connect(target: AnalyserNode | AudioContext): FilterHelper {
        this.Filter.connect(target)
        return this
    }

    /**
     * BiquadFilterNodeを返す
     * @returns BiquadFilterNode
     */
    public getFilter(): BiquadFilterNode {
        return this.Filter as BiquadFilterNode;
    }

    /**
     * FilterNodeを繋ぐ
     * @param context AudioContext
     * @param analyser AanlyserNode
     * @returns FilterHelper
     */
    public setFilter(
        context: AudioContext,
        analyser?: AnalyserNode
    ): FilterHelper {
        this.Filter = new BiquadFilterNode(context, this.FilterOption)
        this.Filter.connect(analyser)
        return this
    }

    /**
     * プロパティの設定（初期化時用、後から変えても適応されない）
     * @param options 'frequency' | 'detune' | 'Q' | 'gain'
     * @param val number 設定値
     * @returns FilterHelper
     */
    public setOption(
        options: 'frequency' | 'detune' | 'Q' | 'gain',
        val: number
    ): FilterHelper {
        this.FilterOption[options] = val
        return this
    }

    /**
     * フィルターの変更
     * @param filter FilterType 適応するフィルター名
     * @returns FilterHelper
     */
    public changeFilter(filter: FilterType = 'lowpass'): FilterHelper {
        this.FilterType = filter
        return this
    }

    /**
     * プロパティの変更（後から変更する用、先に変えたらエラー）
     * @param options 'frequency' | 'detune' | 'Q' | 'gain'
     * @param val number 設定値
     * @returns FilterHelper
     */
    public changeOptions(
        options: 'frequency' | 'detune' | 'Q' | 'gain',
        val: number
    ): FilterHelper {
        this.Filter[options].value = val
        return this
    }
}
