import { VisualiseHelper } from './Visualise.Helper';

/*
■ Analyser Propatie
fftSize
符号なしのlong型の値でFFT（高速フーリエ変換）において
周波数領域を決定するために使われているサイズを表している。

frequencyBinCount ※読取専用
符号なしのlong型でFFT（高速フーリエ変換）のサイズの半分の値。
一般的に音声再生時の可視化に用いられる。

minDecibels
unsigned byte型値へ変換するFFT分析データのスケーリング時の
最小のパワー値を表すdouble型の値である。
一般的に、この値は、getByteFrequencyData()の使用時の結果の範囲の最小値として明記される。

maxDecibels
unsigned byte型値へ変換するFFT分析データのスケーリング時の
最大のパワー値を表すdouble型の値である。
一般的に、この値は、getByteFrequencyData()の使用時の結果の範囲の最大値として明記される。

smoothingTimeConstant
分析フレームの平均間隔を表すdouble型の値で、
使用例として時間的にスペクトルを平滑化させるのに用いられる。

■ Analyserメソッド 
getFloatFrequencyData() 
  周波数データを引数として渡されたFloat32Array配列へコピーする。
  周波数領域の波形データ(振幅スペクトル)をデシベル単位で取得するメソッド
  ※ デシベル単位で取れるらしい
getByteFrequencyData()
  周波数データを引数として渡されたUint8Array配列(unsigned byte配列)へコピーする。
  周波数領域の波形データ(振幅スペクトル)を取得するメソッド
getFloatTimeDomainData()
  音声波形データを引数として渡されたFloat32Array配列へコピーする。
  時間領域の波形データを取得するメソッド
getByteTimeDomainData()
  音声波形データを引数として渡されたUint8Array配列(unsigned byte配列)へコピーする。
  時間領域の波形データをデシベル単位で取得するメソッド？
*/
export class AnalyserHelper {
    private static instance: AnalyserHelper

    private analyser: AnalyserNode | any

    private dataArray: Uint8Array | Float32Array | any
    private bufferLength = 0

    private interval = 1000
    private laptime = 0

    private animationId = 0
    private animationCallback: Function = () => {}

    public static call(): AnalyserHelper {
        if (!AnalyserHelper.instance) {
            AnalyserHelper.instance = new AnalyserHelper()
        }
        return AnalyserHelper.instance
    }

    /**
     * AnalyserNodeを返す
     * @returns AnalyserNode
     */
    public getAnalyser(): AnalyserNode {
        return this.analyser
    }

    /**
     * Analyserを作成
     * @param context AudioContext
     * @param fft 32 | 64 | 128 | 256 | 512 | 1024 | 2048
     * @returns AnalyserHelper
     */
    public setAnalyser(
        context: AudioContext,
        fft: 32 | 64 | 128 | 256 | 512 | 1024 | 2048
    ): AnalyserHelper {
        this.analyser = new AnalyserNode(context, {
            fftSize: fft,
            minDecibels: -100,
            maxDecibels: -30,
            smoothingTimeConstant: 0.8
        }) as AnalyserNode
        return this
    }

    public initDecibelChecker(callback: Function) {
        this.animationCallback = callback
        this.setBuffeLength('bincount').initDataArray('float32')
        // this.setBuffeLength('bincount').initDataArray('uint8')
    }

    public startDecibelChecker() {
        this.setDataArray('FloatFrequencyData')
        this.animationCallback(Math.max(...this.dataArray))
        this.animationId = requestAnimationFrame(() =>
            this.startDecibelChecker()
        )
    }

    /**
     * 波形表示用のcanvasターゲットの指定
     * @param canvas string Canvasタグのid
     * @returns AnalyserHelper
     */
    public setVisualiser(canvas: string): AnalyserHelper {
        VisualiseHelper.call().setCanvas(canvas)
        return this
    }

    /**
     * 波形グラフの表示
     * @param type 'sin' | 'bar'
     *
     * 周波数情報のバッファデータは引数で受け取るのではなく
     * バッファデータを格納する変数を渡す際にポインターアドレスを紐づけされ
     * メソッド間でコヒーレントが取れたポインターを参照る格好になる
     * パッと見波形データの処理フローがわかりにくいので注意
     */
    public showVisual(type: 'sin' | 'bar'): AnalyserHelper {
        if (type === 'sin') {
            this.setBuffeLength('fft').initDataArray('uint8').drawSineWave()
        }
        if (type === 'bar') {
            this.setBuffeLength('bincount')
                .initDataArray('uint8')
                .drawFrequencybars()
        }
        return this
    }

    /**
     * アナライザーを止める
     */
    public stopAnalyser() {
        cancelAnimationFrame(this.animationId)
    }

    /**
     * データアレイにストリームデータを入れる
     * @param datatype 'ByteTimeDomainData' | 'ByteFrequencyData' |
                    'FloatTimeDomainData' | 'FloatFrequencyData' 
     *
     * 引数で配列を渡しているが実際にはポインタベースで
     * メモリ参照しているので戻りの記述は不要
     */
    private setDataArray(
        datatype:
            | 'ByteTimeDomainData'
            | 'ByteFrequencyData'
            | 'FloatTimeDomainData'
            | 'FloatFrequencyData'
    ) {
        switch (datatype) {
            case 'ByteTimeDomainData':
                this.analyser.getByteTimeDomainData(this.dataArray as Uint8Array)
                break
            case 'ByteFrequencyData':
                this.analyser.getByteFrequencyData(this.dataArray)
                break
            case 'FloatTimeDomainData':
                this.analyser.getFloatTimeDomainData(this.dataArray)
                break
            case 'FloatFrequencyData':
                this.analyser.getFloatFrequencyData(this.dataArray)
                break
        }
    }

    /**
     * バッファを配列に格納する数
     * 基本的に配列の数になる
     * fft 　　　 ： 配列の数
     * bitcount　：　配列の数の半分
     * @param size  'fft' | 'bincount'
     * @returns AnalyserHelper
     */
    private setBuffeLength(size: 'fft' | 'bincount'): AnalyserHelper {
        if (size === 'fft') this.bufferLength = this.analyser.fftSize
        if (size === 'bincount')
            this.bufferLength = this.analyser.frequencyBinCount
        return this
    }

    /**
     * データアレイを初期化
     * @param type 'uint8' | 'float32'
     */
    private initDataArray(type: 'uint8' | 'float32'): AnalyserHelper {
        switch (type) {
            case 'uint8':
                this.dataArray = new Uint8Array(this.bufferLength) as Uint8Array
                break
            case 'float32':
                this.dataArray = new Float32Array(this.bufferLength) as Float32Array
                break
        }
        return this
    }

    /**
     * サイン波の描画
     */
    private drawSineWave() {
        this.setDataArray('ByteTimeDomainData')

        VisualiseHelper.call()
            .clearCanvas()
            .visualSineWave(this.dataArray, this.bufferLength)
        this.animationId = requestAnimationFrame(() => this.drawSineWave())
    }

    /**
     * サウンドバーの描画
     */
    private drawFrequencybars() {
        this.setDataArray('ByteFrequencyData')

        VisualiseHelper.call()
            .clearCanvas()
            .visualFrequencybars(this.dataArray, this.bufferLength)

        this.animationId = requestAnimationFrame(() =>
            this.drawFrequencybars()
        )
    }

    private checkInterval(): boolean {
        const n = new Date().getTime()
        let over = false
        if (this.laptime === 0) {
            this.laptime = n
        } else {
            over = n - this.laptime > this.interval ? true : false
            this.laptime = over ? 0 : this.laptime
        }
        return over
    }
}
