import Meyda, { MeydaFeaturesObject } from 'meyda';
import MeydaAnalyzer from 'meyda';

/*
audioContext              : AudioContectの参照情報
bufferSize?               : 抽出するバッファの長さ
chromaBands?              : 抽出機スペクトルを分割する数
featureExtractors         : 抽出に使用するツール（返ってくるデータの形式）
melBands?                 : Melバンドの数
numberOfMFCCCoefficients? : Mfcc抽出機が返すMFCC係数の数 12 ~ 22あたり
sampleRate?               : 1秒あたりのサンプリングレート、AudioContext側の設定と正確に一致していない場合無視される
windowingFunction
*/

type FeatureExtractors =
    | 'amplitudeSpectrum'
    | 'chroma'
    | 'complexSpectrum'
    | 'energy'
    | 'loudness'
    | 'mfcc'
    | 'perceptualSharpness'
    | 'perceptualSpread'
    | 'powerSpectrum'
    | 'rms'
    | 'spectralCentroid'
    | 'spectralFlatness'
    | 'spectralFlux'
    | 'spectralKurtosis'
    | 'spectralRolloff'
    | 'spectralSkewness'
    | 'spectralSlope'
    | 'spectralSpread'
    | 'zcr'
    | 'buffer'

type MParam =
    | 'bufferSize'
    | 'inputs'
    | 'chromaBands'
    | 'melBands'
    | 'sampleRate'
    | 'numberOfMFCCCoefficients'
    | 'featureExtractors'

interface MeydaParams {
    bufferSize?: number
    inputs?: number
    chromaBands?: number
    melBands?: number
    sampleRate?: number
    numberOfMFCCCoefficients?: number
    featureExtractors?: FeatureExtractors
}

export class MeydaHelper {
    private static instance: MeydaHelper

    private meyda: MeydaAnalyzer | any
    private mfcc: any = []

    private Params: MeydaParams = {
        bufferSize: 256,
        inputs: 1,
        chromaBands: 0,
        melBands: 0,
        sampleRate: 11025,
        numberOfMFCCCoefficients: 50,
        featureExtractors: 'mfcc'
    }

    private maxLength = 49

    public static call(): MeydaHelper {
        if (!MeydaHelper.instance) {
            MeydaHelper.instance = new MeydaHelper()
        }
        return MeydaHelper.instance
    }

    /**
     * 解析開始
     * @returns MeydaHelper
     */
    public start(): MeydaHelper {
        this.meyda.start()
        return this
    }

    /**
     * 解析停止
     * @returns MeydaHelper
     */
    public stop(): MeydaHelper {
        this.meyda.stop()
        return this
    }

    /**
     * オプション設定
     * @param param string
     * @param val string | number
     * @returns MeydaHelper
     */
    public setParams(param: MParam, val: string | number | any): MeydaHelper {
        if (param in this.Params) {
            this.Params[param] = val
        }
        return this
    }

    /**
     * 解析結果をスタックする長さ
     * @param length number
     * @returns MeydaHelper
     */
    public setLength(length: number): MeydaHelper {
        this.maxLength = length
        return this
    }

    public getFeature(
        feature: FeatureExtractors
    ): Partial<MeydaFeaturesObject> {
        return this.meyda.get(feature)
    }

    /**
     * MeydaAnalyserをセットアップ
     * @param context AudioContext
     * @param source MediaStream
     * @param callback Funtion
     * @returns MeydaHelper
     */
    public setMeydaAnalyse(
        context: AudioContext,
        source: MediaStream,
        callback: Function
    ): MeydaHelper {
        this.meyda = Meyda.createMeydaAnalyzer({
            audioContext: context,
            source: source,
            bufferSize: this.Params.bufferSize,
            //"featureExtractors" : [this.Params.featureExtractors],
            featureExtractors: ['amplitudeSpectrum', 'mfcc', 'rms'],
            inputs: this.Params.inputs,
            numberOfMFCCCoefficients: this.Params.numberOfMFCCCoefficients,
            callback: (features: any) => {
                if (this.mfcc.length > this.maxLength) this.mfcc.shift()
                this.mfcc.push(features.mfcc)
                // console.log(this.mfcc)
                callback(this.mfcc)
            }
        })
        return this
    }

    /**
     * チャンネル切り替え（複数チャンネルの音源の場合）
     * @param channel number
     * @returns MeydaHelper
     */
    public changeChannel(channel: number): MeydaHelper {
        this.meyda.setChannel = channel
        return this
    }

    /**
     * オーディオソースを切り替え
     * @param source MediaStream
     * @returns MeydaHelper
     */
    public changeSource(source: MediaStream): MeydaHelper {
        this.meyda.setSource = source
        return this
    }
}
