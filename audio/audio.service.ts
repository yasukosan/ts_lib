import { AnalyserHelper } from './_helper/Analyser.Helper'
import { FilterHelper } from './_helper/Filter.Helper'
import { MeydaHelper } from './_helper/Meyda.Helper'
import { RecorderHelper } from './_helper/Recorder.Helper'
export class AudioService {
    private static instance: AudioService

    private source: MediaStreamAudioSourceNode[] | any = []
    private audioContext: AudioContext[] | any = []
    private audioStream: MediaStream[] | any = []
    private filter: BiquadFilterNode[] | any = []
    private analyser: AnalyserNode[] | any = []

    private ffsize: 32 | 64 | 128 | 256 | 512 | 1024 | 2048 = 128;

    public static call(): AudioService {
        if (!AudioService.instance) {
            AudioService.instance = new AudioService()
        }
        return AudioService.instance
    }

    /**
     * AnalyserHelperを返す
     * @returns AnalyserHelper
     */
    public callAnalyser(): AnalyserHelper {
        return AnalyserHelper.call()
    }

    /**
     * FilterHelperを返す
     * @returns FilterHelper
     */
    public callFilter(): FilterHelper {
        return FilterHelper.call()
    }

    /**
     * 初期設定
     * @param target MediaStream ストリームデータ
     * @param id string ストリーム管理用の識別子
     */
    public setup(target: MediaStream, id: string): void {
        console.log(`
        [Audio Service Info] : Setup Audio API
        `)
        if ('AudioContext' in window) {
            const AudioContext = window.AudioContext
            this.audioContext[id] = new AudioContext()
        } else {
            throw new Error('Audio Context Not Support')
            return;
        }

        this.addStream(target, id)
    }

    /**
     * オーディオストリームを追加
     * @param stream MediaStream オーディオストリーム
     * @param id string 管理用Index文字列
     */
    public addStream(stream: MediaStream, id: string): void {
        this.audioStream[id] = stream
    }

    /**
     * オーディオストリームを削除
     * @param id string ストリーム識別子
     */
    public delStream(id: string): void {
        this.audioStream[id] = null
        this.audioContext[id] = null
        this.source[id] = null
        this.filter[id] = null
    }

    /**
     * オーディオソースとフィルターを繋ぐ
     * @param id string ストリーム識別子
     * @param visual boolean true: 波形を表示する
     * @returns AudioService
     */
    public audioConnect(id: string, visual = false): AudioService {
        this.analyser[id] = new AnalyserHelper()
        this.filter[id] = new FilterHelper()

        this.analyser[id].setAnalyser(this.audioContext[id], this.ffsize)
        if (visual) this.analyser[id].setVisualiser('analyser_' + id)

        this.filter[id]
            .setOption('frequency', 44100)
            .setOption('Q', 0.35)
            .setFilter(this.audioContext[id], this.analyser[id].getAnalyser())

        // media stream からストリームソースを作成
        // 生成されたソースは、再生・編集が行える
        this.source[id] = this.audioContext[id].createMediaStreamSource(
            this.audioStream[id]
        )

        // ストリームソースにフィルターを繋ぐ
        this.source[id].connect(this.filter[id].getFilter())

        return this
    }

    /**
     * 波形を表示
     * @param type 'bar' | 'sin' bar: 棒グラフ sin: 波形グラフ
     * @param id string 表示するアナライザーID
     */
    public showAnalyser(type: 'bar' | 'sin', id: string) {
        this.analyser[id].showVisual(type)
    }

    /**
     * MeydaAnalyzerで解析開始
     * @param id
     * @param callback
     */
    public startMeydaAnalyse(id: string, callback: Function) {
        MeydaHelper.call()
            .setMeydaAnalyse(this.audioContext[id], this.source[id], callback)
            .start()
    }

    /**
     * 
     * @param id string
     * @param callback () => void
     * @returns 
     */
    public startDecibelChecker(id: string, callback: () => void) {
        this.analyser[id].initDecibelChecker(callback)
        this.analyser[id].startDecibelChecker()
        return this
    }

    /**
     * 録音開始
     * @param id
     */
    public startRecorder(
        id: string,
        callback: Function,
        rectime: number,
        type: 'single' | 'timeout' | 'loop' = 'loop'
    ) {
        RecorderHelper.call()
            .init(this.audioContext[id], this.source[id], callback, rectime)
            .startRecorder(type)
    }

    public stopRecord()
    {
        RecorderHelper.call().stopRecorder()
        return this
    }

    public getRecord(
        dataType: 'Blob' | 'ArrayBuffer' | 'DataView' | 'File' = 'File'
    ): { rec: File, time: number, name: string}
    {
        return {
            rec     : RecorderHelper.call().getData(dataType) as File,
            time    : RecorderHelper.call().getRecordTime(),
            name    : 'output.wav'
        }
    }

    public getStream(id: string): MediaStream {
        return this.source[id]
    }
}
