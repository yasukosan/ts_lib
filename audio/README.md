AudioContextは凄まじく奥が深いので
必要な機能の実装実装のみに注力スべし

ただ、安易な実装は後の変更拡張に被害が大きいので
各API、Node機能を実装する場合は必ずリファレンスを参照の上
実装の検討を先に行うべし


# ファイル構成

## _helper
* Analyser.Helper
aa
* Filter.Helper
* Meyda.Helper
* Source.Helper
* Visualise.Helper

## /
* audio.service


# 依存関係
* MediaSercie
* * 音声ストリームを取得する、デバイスリストの取得
* * デバイスからオーディオストリームの取得

# 使い方　例

### 初期化
```javascript
    // 使用可能なデバイスリスト作成
    await MediaService.call()
        .callDeviceListService()
        .SearchDeviceList();

    // オーディオデバイス一覧取得
    const devices = MediaService.call()
                        .callDeviceListService()
                        .getAudioInputDevices();

    // 生成するストリームの設定（ビデオ：無効、オーディオ：有効）
    MediaService.call()
        .callStreamModeService()
        .offVideo()
        .onAudio();

    // ストリーム設定を使用し、デバイスからストリームデータを取得
    await MediaService.call().getLocalStream(
        MediaService.call().callStreamModeService().getStreamMode()
    );

    /**
     * ビジュアライズ用のNodeを作成
     */
    // オーディオサービスにローカルストリームを渡す
    AudioService.call().setup(
        MediaService.call().getStream(),    // ローカルストリーム
        'testaudio'                         // ストリーム管理用の識別子
    );

    AudioService.call()
            .audioConnect('testaudio', true);

    /**
     * 音圧チェック用のNodeを作成
     */
    AudioService.call().setup(
        MediaService.call().getStream(),    // ローカルストリーム
        'desiberu'                         // ストリーム管理用の識別子
    );
    AudioService.call().audioConnect('desiberu');
```
### 実行
* MeydaAnalyserで解析
  * MeydaAnaliserは単独で機能するため、解析用のNodeの追加は不要
```javascript
    AudioService.call().startMeydaAnalyse('testaudio',callback2);
```
* ビジュアライズの開始、前段で該当するNodeが繋がれている事
```javascript
    AudioService.call().showAnalyser('sin', 'testaudio');
```
* 音圧チェック、前段で該当するNodeが繋がれている事
```javascript
    AudioService.call().startDecibelChecker('desiberu', callback1);
```


# 例 オーディオファイル再生

* オーディオファイル読み込み
```javascript
// HTMLAudioElementのid
const tag = 'forth'
// IDをセットアップに渡す
AudioBufferService.call().setup(tag);
// 読み込むオーディオファイルのURLを渡す
await AudioBufferService.call()
            .loadAudioFile(tag, 'http://localhost:3000/enter.wav');
```
* 再生
* 
```javascript
// HTMKAudioElementの再生を呼ぶ
AudioBufferService.call().play(tag);
```