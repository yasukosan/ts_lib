import { PrismaClient } from "@prisma/client";

export class SqliteService{
	private static instance: SqliteService;

	private prisma: PrismaClient | undefined;

	public static call() {
		if (!SqliteService.instance) {
			SqliteService.instance = new SqliteService();
		}
		return SqliteService.instance;
	}

	public constructor() {
		this.conn();
	}

	/**
	 * データ取得
	 * @param table string テーブル名
	 * @param key string カラム名
	 * @param search string | number 検索文字列・数値
	 * @returns Promise<T | T[] | false>
	 */
	public async get<T>(
		table: string, key: string, search: string | number
	): Promise<T | T[] | false> {
		return await this.doQuery(
			'findUnique',
			table,
			{
				where: {
					[key]: search
				}
			}
		);
	}

	/**
	 * テーブルの全件データ
	 * @param table string テーブル名
	 * @returns Promise<T>
	 */
	public async getAll <T> (
		table: string
	): Promise<T[] | false> {
		return await this.doQuery(
			'findMany',
			table,
			{}
		);
	}

	/**
	 * データ追加
	 * @param table string テーブル名
	 * @param val T 追加データ(1件分の全データ)
	 * @returns Promise<T | false>
	 */
	public async add <T> (
		table: string, val: T
	): Promise<T | false> {
		return await this.doQuery(
			'create',
			table,
			{
				data: val
			}
		);
	}

	/**
	 * データ更新
	 * @param table string テーブル名
	 * @param key string 更新対象のカラム
	 * @param val T 更新データ(1件分の全データが多分要る)
	 * @returns Promise<T | false>
	 */
	public async update <T extends {[key: string]: string}> (
		table: string, key: string, val: T
	): Promise<T | false> {
		return await this.doQuery(
			'update',
			table,
			{
				where: {
					[key]: val[key]
				},
				data: val
			}
		);
	}

	/**
	 * データ削除
	 * @param table string テーブル名
	 * @param key string　更新対象のカラム
	 * @param search string | number 検索文字列か数値
	 * @returns Promise<T | false>
	 */
	public async delete <T> (
		table: string, key: string, search: string | number
	): Promise<T | false> {
		return await this.doQuery(
			'delete',
			table,
			{
				where: {
					[key]: search
				}
			}
		);
	}

	/**
	 * クエリを実行
	 * @param job 'findUnique' | 'findMany' | 'create' | 'update' | 'delete'
	 * @param table string テーブル名
	 * @param query object クエリ本文
	 * @returns Promise<T | false> テーブルオブジェクト、テーブルオブジェクトの配列、エラー例外時はfalse
	 */
	private async doQuery <T> (
		job: 'findUnique' | 'findMany' | 'create' | 'update' | 'delete',
		table: string,
		query: object
	): Promise<T | false> {
		this.conn();
		try {
			if (this.prisma !== undefined) {
				return await this.prisma[table][job](query);
			}
		} catch (error) {
			this.disconn();
			console.error(error);
		}
		return false;
	}

	/**
	 * PrismaClientを新規作成（DB接続）
	 */
	private conn(): void {
		this.prisma = new PrismaClient();
	}

	/**
	 * DB切断
	 */
	private async disconn(): Promise<void> {
		if (this.prisma !== undefined) {
			await this.prisma.$disconnect();
		}
		this.prisma = undefined;
	}
}

