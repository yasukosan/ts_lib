
import { RegistrationHelper} from "./_helper/registration.helper"
import { CredentialHelper } from "./_helper/credential.helper";

import {
    ChallengeOptions as typeChallengeOptions,
    Credential,
    AuthenticationOptions,
} from "./__type.authentication"
import { authenticationHelper } from './_helper/authentication.helper'

export type ChallengeOptions = typeChallengeOptions

export class WebAuthService {
    private static instance: WebAuthService;

    public static call(): WebAuthService {
        if (!WebAuthService.instance) {
            WebAuthService.instance = new WebAuthService()
        }
        return WebAuthService.instance;
    }

    /**
     * PassKeyのサポートを確認する
     * @returns boolean
     */
    public async checkSupport(): Promise<boolean> {
        if (window.hasOwnProperty('PublicKeyCredential') === true
            || window.PublicKeyCredential) {
            
            window.PublicKeyCredential
                .isUserVerifyingPlatformAuthenticatorAvailable()
                .then((uvpaa) => {
                    if (!uvpaa) return false
                    else return true
                })
        }
        return false
    }

    /**
     * 登録
     */

    /**
     * チャレンジオプションを生成する
     * @returns ChallengeOptions
     */
    public makeChallengeOption(
        attention?: string | undefined,
        authenticatorAttachment?: 'platform' | 'cross-platform' | undefined,
        requireResidentKey?: boolean | undefined,
        userVerification?: 'discouraged' | 'preferred' | 'required' | undefined
    ): ChallengeOptions {
        if (attention !== undefined)
            RegistrationHelper.call().setAttention(attention)
        if (authenticatorAttachment !== undefined)
            RegistrationHelper.call().setAuthenticatorAttachment(authenticatorAttachment)
        if (requireResidentKey !== undefined)
            RegistrationHelper.call().setRequireResidentKey(requireResidentKey)
        if (userVerification !== undefined)
            RegistrationHelper.call().setUserVerification(userVerification)

        return RegistrationHelper.call().getOptions()
    }

    /**
     * クレデンシャルオプションを生成する
     * @param options 
     * @returns 
     */
    public async makeCredentialOption(options: any): Promise<Credential | false> {
        await CredentialHelper.call().makeCredentialOption(options)
        return CredentialHelper.call().Credential
    }


    /**
     * 認証
     */

    /**
     * 認証用オプションを生成
     * @param options 
     * @returns 
     */
    public async makeAuthenticationOption(options: any): Promise<false | AuthenticationOptions> {
        return await authenticationHelper.call().makeAuthenticationOption(options)
    }

}