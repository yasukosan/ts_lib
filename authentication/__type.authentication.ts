export type AuthenticatorAttachment = "cross-platform" | "platform"
export type AuthenticatorTransportFuture =
            "ble" | "internal" | "nfc" | "usb" | "cable" | "hybrid"
export type AuthenticationExtensionsClientInputs = {
    appid?: string,
    credProps?: boolean,
    hmacCreateSecret?: boolean,
}
export type AuthenticationExtensionsClientOutputs = {
    appid?: boolean,
    credProps?: {
        rk?: boolean,// CredentialPropertiesOutput
    },
    hmacCreateSecret?: boolean,
}



/**
 * チャレンジオプション
 * @param attention - ユーザーに注意を促すかどうか
 * @param authenticatorSelection - 認証器の選択
 * @param authenticatorAttachment - 認証器の種類
 * @param requireResidentKey - ユーザーの生体情報を認証器に保存するかどうか
 * @param userVerification - ユーザー認証の方法
 */
export type ChallengeOptions = {
    attention:  string,
    authenticatorSelection: {
        authenticatorAttachment?: 'platform' | 'cross-platform',
        requireResidentKey?: boolean,
        residentKey?: 'required' | 'preferred' | 'discouraged',
        userVerification?: 'discouraged' | 'preferred' | 'required'
    }
}

/**
 * チャレンジオプションの初期値
 * @param attention - ユーザーに注意を促すかどうか
 * @param authenticatorSelection - 認証器の選択
 * @param authenticatorAttachment - 認証器の種類
 * @param requireResidentKey - ユーザーの生体情報を認証器に保存するかどうか
 * @param userVerification - ユーザー認証の方法
 */
export const initialChallengeOptions: ChallengeOptions = {
    attention: "none",
    authenticatorSelection: {}
}

export type Credential = {
    id: string,
    rawId: string,
    type: string,
    response: {
        clientDataJSON: string,
        attestationObject: string
    }
}

export const initialCredential = {
    id: "",
    rawId: "",
    type: "",
    response: {
        clientDataJSON: "",
        attestationObject: ""
    }
}


export type CredentialOptions = {
    user    : {
        id          : Uint8Array,
        name        : string,
        displayName : string
    },
    authenticatorSelection: {
        authenticatorAttachment: 'platform' | 'cross-platform',
        requireResidentKey: boolean,
        userVerification: 'discouraged' | 'preferred' | 'required'
    },
    challenge: Uint8Array,
    pubKeyCredParams: [{
        type: 'public-key',
        alg: number
    }]
    rp: {
        id: string,
        name: string
    },
    excludeCredentials: {
        type: 'public-key',
        id: Uint8Array,
        transports?: AuthenticatorTransportFuture[]
    }[]
}

export const initialCredentialOptions: CredentialOptions = {
    user    : {
        id          : new Uint8Array(16),
        name        : "",
        displayName : ""
    },
    authenticatorSelection: {
        authenticatorAttachment: "cross-platform",
        requireResidentKey: false,
        userVerification: "preferred"
    },
    challenge: new Uint8Array(),
    pubKeyCredParams: [{
        type: "public-key",
        alg: -7
    }],
    rp: {
        id: "",
        name: ""
    },
    excludeCredentials: [
        /*{
            type: "public-key",
            id: new Uint8Array(),
            transports: ["usb"]
        }*/
    ]
}

export type RegistrationCredentialOptions = {
    user    : {
        id          : string,
        name        : string,
        displayName : string
    },
    authenticatorSelection: {
        authenticatorAttachment: 'platform' | 'cross-platform',
        requireResidentKey: boolean,
        userVerification: 'discouraged' | 'preferred' | 'required'
    },
    challenge: string,
    pubKeyCredParams: [{
        type: 'public-key',
        alg: number
    }]
    rp: {
        id: string,
        name: string
    },
    excludeCredentials: any[]
}


export type MakeAuthOptions = {
    challenge: string,
    allowCredentials: {
        id: string,
        transports?: AuthenticatorTransportFuture[],
        type: 'public-key',
    }[],
    userVerification: 'discouraged' | 'preferred' | 'required',
    timeout: number,
    rpId: string,
    extensions: AuthenticationExtensionsClientInputs
}

export type AuthenticationOptions = {
    id: string,
    type: 'public-key',
    rawId: string,
    response: {
        authenticatorData: string,
        clientDataJSON: string,
        signature: string,
        userHandle?: string
    },
    authenticatorAttachment?: AuthenticatorAttachment,
    clientExtensionResults: AuthenticationExtensionsClientOutputs,
}

export type AuthOptions = {
    challenge: Uint8Array,
    allowCredentials: any[],
    userVerification: 'discouraged' | 'preferred' | 'required',
    timeout: number,
    rpId: string
}

export const initialAuthOptions: AuthOptions = {
    challenge: new Uint8Array(),
    allowCredentials: [],
    userVerification: "preferred",
    timeout: 60000,
    rpId: ""
}