import {
    startAuthentication
} from '@simplewebauthn/browser'
import { AuthOptions, AuthenticationOptions, MakeAuthOptions } from '../__type.authentication';

export class authenticationHelper
{
    private static instance: authenticationHelper;

    public static call(): authenticationHelper {
        if (!authenticationHelper.instance) {
            authenticationHelper.instance = new authenticationHelper();
        }
        return authenticationHelper.instance;
    }

    /**
     * 認証オプションを生成する
     * @param options 
     * @returns 
     */
    public async makeAuthenticationOption(
        options: MakeAuthOptions
    ): Promise<AuthenticationOptions | false> {
        try {
            const result = await startAuthentication(options)
            if (result)
                return result
            else
                return false
        } catch (error: any) {
            console.error(`
            [MakeAuthenticationOption ERROR ${error.type}] : ${error.message}
            `)
            return false
        }
    }

}