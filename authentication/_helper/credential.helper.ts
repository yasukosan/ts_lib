import { Buffer } from 'buffer'

import {
    startRegistration
} from '@simplewebauthn/browser';

import {
    CredentialOptions,
    initialCredentialOptions,
    Credential,
    initialCredential,
    RegistrationCredentialOptions
} from "../__type.authentication";


export class CredentialHelper {
    private static instance: CredentialHelper;

    private Options: CredentialOptions = initialCredentialOptions
    private _Credential: Credential | false = initialCredential

    public static call(): CredentialHelper {
        if (!CredentialHelper.instance) {
            CredentialHelper.instance = new CredentialHelper();
        }

        return CredentialHelper.instance;
    }

    public setUserId(
        id: string
    ): CredentialHelper {

        const _id: any = Buffer.from(id, 'base64')
        //const _id: any = id
        if (_id instanceof Uint8Array) {
            this.Options.user.id = _id
        } else {
            this.Options.user.id = new Uint8Array(16)
        }
        return this
    }

    public setUserName(
        name: string
    ): CredentialHelper {
        this.Options.user.name = name
        return this
    }

    public setChallenge(
        challenge: string
    ): CredentialHelper {
        const _challenge: any = Buffer.from(challenge, "base64")
        if (_challenge instanceof Uint8Array) {
            this.Options.challenge = _challenge
        }
        return this
    }

    get Credential(): Credential | false {
        return this._Credential
    }

    /**
     * 認証器の種類を設定する
     * @param excludeCredentials { type: 'public-key', id: Uint8Array, transports?: AuthenticatorTransportFuture[]}
     * @returns CredentialHelper
     */
    public setExcludeCredentials(
        excludeCredentials: []
    ): CredentialHelper {
        this.Options.excludeCredentials = excludeCredentials.map((credential: any) => ({
            ...credential,
            id: Buffer.from(credential.id, "base64")
        }))
        return this
    }

    /**
     * 認証器の情報をSimpleWebAuthを使わずに生成する
     * 生成されるBase64コードが、SimpleWebAuthと合わないため
     * 生成は出来るが、次の検証で必ずエラーになる
     */
    /*
    public async makeCredentialOptionByLocal(): Promise<void> {
        const cred: any = await navigator.credentials.create({
            publicKey: this.Options
        })

        if (!cred) {
            throw new Error("Credential is not defined")
        }
        this.Credential.id      = cred.id
        this.Credential.rawId   = Buffer.from(cred.rawId).toString('base64url')
        this.Credential.type    = cred.type
        
        if (cred.response) {
            this.Credential.response = {
                clientDataJSON: Buffer.from(cred.response.clientDataJSON).toString('base64url'),
                attestationObject: Buffer.from(cred.response.attestationObject).toString('base64url')
            }
        }
        console.log(this.Credential)
    }*/

    /**
     * 認証器の情報をSimpleWebAuthを使って生成する
     * @param options
     * @returns
     */
    public async makeCredentialOption(
        options: RegistrationCredentialOptions
    ): Promise<void> {
        try {
            this._Credential = await startRegistration(options)            
        } catch (error) {
            console.error(error)
            this._Credential = false
        }
    }
}