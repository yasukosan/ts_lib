
import {
    ChallengeOptions,
    initialChallengeOptions
} from "../__type.authentication";

export class RegistrationHelper {
    private static instance: RegistrationHelper;

    private Options: ChallengeOptions = initialChallengeOptions;

    public static call(): RegistrationHelper {
        if (!RegistrationHelper.instance) {
            RegistrationHelper.instance = new RegistrationHelper();
        }

        return RegistrationHelper.instance;
    }

    /**
     * Attentionを設定する
     * @param attention string
     * @returns RegistrationHelper
     */
    public setAttention(
        attention: string
    ): RegistrationHelper {
        this.Options.attention = attention
        return this
    }

    /**
     * 認証器の種類を設定する
     * @param authenticatorAttachment 'platform' | 'cross-platform'
     * @returns RegistrationHelper
     */
    public setAuthenticatorAttachment(
        authenticatorAttachment: 'platform' | 'cross-platform'
    ): RegistrationHelper {
        this.Options
            .authenticatorSelection
            .authenticatorAttachment = authenticatorAttachment
        return this
    }

    /**
     * ユーザーの生体情報を認証器に保存するかどうかを設定する
     * @param requireResidentKey boolean
     * @returns RegistrationHelper
     */
    public setRequireResidentKey(
        requireResidentKey: boolean
    ): RegistrationHelper {
        this.Options.authenticatorSelection.requireResidentKey = requireResidentKey
        return this
    }

    /**
     * ユーザー認証の方法を設定する
     * @param userVerification 'discouraged' | 'preferred' | 'required'
     * @returns RegistrationHelper
     */
    public setUserVerification(
        userVerification: 'discouraged' | 'preferred' | 'required'
    ): RegistrationHelper {
        this.Options.authenticatorSelection.userVerification = userVerification
        return this
    }

    /**
     * チャレンジオプションを取得する
     * @returns ChallengeOptions
     */
    public getOptions(): ChallengeOptions {
        return this.Options
    }
    
}